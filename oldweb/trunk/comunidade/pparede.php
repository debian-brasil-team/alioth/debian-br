<? include ('../page.tpt') ?>
<? include ('secao.tpt') ?>

<!-- para os webmasters do Debian-BR: -->
<!-- respeitem o esquema de nomea��o 'thumb-' antes do nome da -->
<!-- imagem para thumbnails... os thumbs tem de ter tamanho -->
<!-- 170x136... procurem sempre incluir o .xcf.gz tamb�m! -->

<h3>Pap�is de Parede</h3>

<p>
<center>
<table border=0 cellpadding=5>

<tr> <!-- primeira linha -->

<td> <!-- imagem, xcf -->
<center>
<a href="desktop-br/wallpapers/swirl-bandeira.png">
<img src="desktop-br/wallpapers/thumb-swirl-bandeira.png">
</a>
<br>
<b>Espiral e Bandeira
<br>
1280x1024</b>
<br>
<a href="desktop-br/wallpapers/swirl-bandeira.xcf.gz">
.xcf.gz
<hr>
<a href="desktop-br/wallpapers/debian-br-mundo.jpg">
<img src="desktop-br/wallpapers/thumb-debian-br-mundo.jpg">
</a>
<br>
<b>Mundo Espiral e Bandeira
<br>
1024x768</b>
<br>
<a href="desktop-br/wallpapers/debian-br-mundo.xcf.gz">
.xcf.gz
</a>
</center>
</td>

<td> <!-- autor, coment�rios -->
<b>Autor: </b>
Gustavo Noronha Silva &lt;kov@debian.org&gt;
<br>
<b>Coment�rios:</b>
<p>
A bandeira tinha fundo branco e uma sombra. Como n�o sou nenhum
artista e n�o sei usar o Gimp direito fiz o m�ximo para que
ela ficasse bem no fundo preto. A espiral � roubada do
wallpaper padr�o do Window Maker do Debian. A bandeira est�
numa camada acima da espiral com o efeito 'Tela' ligado.
O mundo � gerado a partir do xplanet.
<p>
A bandeira foi pega do <a href="http://www.jorgeduardo.com">site 
do Jorge Eduardo</a>.
</td>

</tr>

<tr> <!-- segunda linha -->

<td> <!-- imagem, xcf -->
<center>
<a href="desktop-br/wallpapers/debianbr.jpg">
<img src="desktop-br/wallpapers/thumb-debianbr.jpg">
</a>
<br>
<b>DebianBR
<br>
1280x1024</b>
<br>
<a href="desktop-br/wallpapers/debianbr.xcf.gz">
.xcf.gz
</a>
</center>
</td>

<td> <!-- autor, coment�rios -->
<b>Autor: </b>
Lauro C. Oliveira &lt;olarva@lauro.net3.com.br&gt;
<br>
<b>Coment�rios:</b>
<p>
Brasil � um tema que todos n�s deveriamos ter na cabe�a diariamente, 
e o desktop � um alugar ideal pra lembrar disso.
</td>

</tr>

<tr> <!-- terceira linha -->

<td> <!-- imagem, xcf -->
<center>
<a href="desktop-br/wallpapers/DebianEmSoloBrazuca.jpg">
<img src="desktop-br/wallpapers/thumb-DebianEmSoloBrazuca.jpg">
</a>
<br>
<b>Debian Em Solo Brazuca
<br>
1280x1024</b>
</center>
</td>

<td> <!-- autor, coment�rios -->
<b>Autor: </b>
Itamar Grochowski Rocha &lt;itamargr@ig.com.br&gt;
<br>
<b>Coment�rios:</b>
<p>
Com a proposta do tema Debian e Brasil me remeteu
imediatamente � presen�a da Debian em solo brasileiro. Lembrei ent�o
de um efeito que eu j� havia conseguido que parecia um gramado que pra
felicidade nacional tem tons verdes e amarelos. Este efeito � obtido
facilmente com o Gimp utilizando a op��o metal escovado (brushed
metal) do efeito de renderiza��o em conjunto com um gradiente do verde
para o amarelo.
</td>

</tr>

<tr> <!-- quarta linha -->

<td> <!-- imagem, xcf -->
<center>
<a href="desktop-br/wallpapers/bg-debianwoody_br.jpg">
<img src="desktop-br/wallpapers/thumb-bg-debianwoody_br.jpg">
</a>
<br>
<b>Debian Woody Brasil
<br>
1280x1024</b>
<hr>
<a href="desktop-br/wallpapers/bg-debianwoody_br-2.jpg">
<img src="desktop-br/wallpapers/thumb-bg-debianwoody_br-2.jpg">
</a>
<br>
<b>Debian Woody Brasil 2
<br>
1280x1024</b>
</center>
</td>

<td> <!-- autor, coment�rios -->
<b>Autor: </b>
Victor Maida &lt;vmaida@terra.com.br&gt;
<br>
<b>Coment�rios:</b>
<p>
A bandeira e o texto foram feitos no gimp, a espiral eu peguei
do debian.org e o woody com o ping�im eu retirei de um papel 
de parede que est� dispon�vel em www.wapers.com.
</td>

</tr>

</table>

<? include ('../end.tpt') ?>
