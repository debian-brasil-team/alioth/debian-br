<? include ('../page.tpt') ?>
<? include ('secao.tpt') ?>

<h3>Como Aplicar os Patches das Imagens de Boot</h3>

<hr noshade>

<p>

Para aplicar as imagens de boot frame buffer no seu kernel
fa�a o seguinte:

<p>

Baixe o patch, usando o link da p�gina de imagens de boot.
Entre no diret�rio principal do fonte do seu kernel
(normalmente /usr/src/linux) e digite o seguinte:

<p>

<pre>
# zcat /caminho/para/o/patch.gz | patch -p1
</pre>

<p>
Depois basta compilar o kernel e configurar o kernel para
dar boot em framebuffer.

<? include ('../end.tpt') ?>
