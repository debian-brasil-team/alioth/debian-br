<? include ('../page.tpt') ?>
<? include ('secao.tpt') ?>

<p>	
<h1>Fotos do Debian-BR:</h1>
<hr noshade>
<font face="lucida" size="2">
<div align="left">
Fotos sempre fazem com que um projeto seja mais humano.
N�o h� raz�es para ser diferente com o Debian-BR.
<p>
Aqui se encontram algumas fotos de eventos dos quais tomaram
parte os nossos amigos.
<p>
<a href="fotos/cultura-hacker-uergs/">Debate sobre Cultura Hacker</a>
Debate acontecido na UERGS, promovido pelo Marcelo Branco.
Pablo Lorenzonni e Gustavo Noronha foram palestrantes. Pablo
falou dos hackers do mundo, da hist�ria e Gustavo falou sobre
o c�digo de conduta dos hackers do Debian e do Software Livre
em geral.
<p>
<a href="forum2002.php">F�rum Internacional de Software Livre 2002</a>
Fotos tiradas por participantes do f�rum que aconteceu em
Porto Alegre, RS. Foi o maior f�rum de inform�tica do Brasil,
reunindo quase 3.000 participantes.
<p>
<a href="fotos/campinas/">Install Fest Debian-SP na Unicamp</a>
Fotos tiradas no primeiro Install Fest do Debian-SP, em 
conjunto com o CIPSGA, com a Unicamp e com a IMA. Houve
palestras de pessoas importantes do movimento do software
livre, apresenta��es musicais e, claro, instala��o de
Debian em diversas m�quinas.
<p>
<a href="fotos/rio/">I Debian-BR/CIPSGA Install Fest</a><br>
Fotos tiradas no Primeiro Debian-BR Install Fest, realizado na
cidade do Rio de Janeiro para apoiar e com o apoio do CIPSGA.
Foram instaladas as m�quinas da rede da escola de software livre
do CIPSGA e m�quinas de algumas pessoas que estiveram presentes.
<p>
<a href="fotos/rio/8anos.mpg">V�deo do I Debian-BR/CIPSGA Install Fest</a><br>
Tamb�m comemoramos dois eventos importantes para o mundo do
software livre, nesse encontro: o unixtime 1000000000 e os 8 anos
de Debian. Este � um pequeno v�deo gravado com todos os presentes
no final do evento.
<p>
<a href="fotos/poa/">II F�rum Internacional de Software Livre
de Porto Alegre (2001)</a><br>
Fotos tiradas no campus da UFRGS e em "happy hours".
<p>
<a href="fotos/brasilia/">F�rum Internacional de Software 
Livre de Bras�lia (2001)</a><br>
Fotos tiradas no anexo 2 da C�mara dos Deputados, onde almo�amos.
<p>
</div>
<hr noshade>

<? include ('../end.tpt') ?>
