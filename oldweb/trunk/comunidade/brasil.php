<? include ('../page.tpt') ?>
<? include ('secao.tpt') ?>

<h1>Debian Brasil</h1>

<hr noshade>

<font face="lucida" size="2">
Aqui se encontram informa��es sobre o Debian e sua comunidade
no Brasil. Vai ser nosso "IBGE" =).
<hr noshade>
<div align="left">
O Eduardo Marcel Ma�an, tamb�m conhecido como 'error', criou uma
<a href="../arquivos/Brasil.xplanet.gz">lista de "marcas"</a> com latitude e 
longitude de uma por��o (todas?) de cidades brasileiras. Clique com
o bot�o direito e selecione salvar como (ele est� compactado) ou digite
"d" no lynx ou no links ;). Veja o cabe�alho do arquivo:
</font>
<p>

<pre>
# This file contains latitude longitude and name of all
# Brazilian Cities and villages.
#
# Brasil.xplanet made by Eduardo Marcel Ma�an &lt;macan@debian.org&gt;
# a partir de dados obtidos de ftp.ibge.gov.br em 17/09/2001
# este arquivo n�o � muito �til para ser plotado todo de uma vez,
# mas � muito interessante quando voc� precisa pegar esta ou
# aquela cidade para ser plotada pelo xplanet, ou xearth, voc�
# s� precisa fazer um "grep" nele.
#
# Este arquivo contem latitude, longitude e nome de todas
# as cidades e vilas brasileiras.
# Brasil.xplanet made by Eduardo Marcel Ma�an &lt;macan@debian.org&gt;
# From data obtained from ftp.ibge.gov.br in 09/17/2001
# this file is not much useful to be ploted at once, but it is very
# interesting for when you need to pick this or that town to be
# ploted by xplanet, or xearth, you just need to "grep" it.
</pre>

<font face="lucida" size="2">Esse arquivo pode ser muito �til para muita gente, n�s, por exemplo,
fizemos esse mapa:</font>

<p>
<center>
<img src="imagens/devels.png">
</center>
<p>
<font face="lucida" size="2">Usamos para isso o arquivo "Devels.xplanet", que continha o seguinte
conte�do (extra�do do arquivo do Ma�an):</font>
<p>
<pre>
-22.010 -47.533 "pzn" align=above color=red # S�o Carlos
-22.541 -43.123 "claviola" align=right color=yellow # Rio de Janeiro
-20.195 -40.173 "Gleydson" align=right color=blue # Vilha Velha
-19.552 -43.562 "kov" align=above color=green # Belo Horizonte
-23.325 -46.381 "error" align=below color=purple # S�o Paulo
-22.542 -47.034 "hmh" align=left color=white # Campinas
-30.016 -51.135 "spectra" color=brown # Porto Alegre
</pre>
<p>
<font face="lucida" size="2">E o seguinte comando foi usado para gerar o mapa:</font>
<pre>
xplanet -sh 100 -proj mercator -observer -54,-35 \
-image /usr/share/xplanet/images/earth_1600.jpg \
-markerf Devels.xplanet -output devels.png -geometry 1024x768
</pre>
</div>

<? include ('../end.tpt') ?>
