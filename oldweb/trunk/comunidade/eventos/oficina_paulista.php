<? include ('../../page.tpt') ?>
<? include ('../secao.tpt') ?>

<h1>Oficina Debian Paulista</h1>

Mais informa��es sobre o evento, podem ser encontrados
<a href="/oficina_debian/">aqui</a>. Fotos tiradas pelo
amigo rootsh podem ser vistas <a
href="http://www.rootshell.be/~rootsh/oficina_debian_part1/">aqui</a> e <a 
href="http://www.rootshell.be/~rootsh/oficina_debian_part2/">aqui</a>.

<h4>Slides</h4>

<ul>

<li><a href="http://people.debian.org/~kov/apresentacoes.html#debconf">
Debconf: a estrutura de configura��o do Debian</a></li>
<b>Gustavo Noronha Silva</b>

<br>

<li><a href="http://loureiro.dyndns.org:8080/presentation_python/">
Administra��o de sistemas usando Python</a></li>
<b>H�lio Loureiro</b>

<br>

<li><a href="http://people.debian.org/~hmh/debconf2/initscripts/">
Scripts de Inicializa��o</a></li>
<b>Henrique de Moraes Holschuh</b>

<br>

<li><a href="http://macan.debian.net/lvm">
Logical Volume Management</a></li>
<b>Eduardo Ma�an</b>

</ul>

<? include ('../../end.tpt') ?>
