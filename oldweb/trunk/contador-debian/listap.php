<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="pt">

<head>
<title>Projeto Debian-BR : Debian em Portugu�s</title>
<META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="/dbr.css">
<meta name="Author" content="Projeto Debian-BR, Ricardo Sandrin">
<meta name="KeyWords" content="Debian, Brasil, Debian-BR, GNU/Linux">
</head>

<center><br>
<table border="0" width="600" cellspacing="0" cellpadding="0">
<tr><!-- borda superior -->

<td></td>
<td colspan="2" bgcolor="#000000">
<img src="/imagens/darkness.gif" width="636" height="2" alt=""></td>
<td></td>
</tr>
<tr valign=top><!-- parte principal -->

<td width="2" bgcolor="#000000">
<img src="/imagens/darkness.gif" width="2" height="2" alt="">
</td><!-- barra esquerda -->

<td bgcolor="#ededed">
<center>
<br><br>
<a href="../index.html"><img src="/imagens/beh.gif" alt="DDP-BR" border="0">
</a>
<br><br>
<img src="/imagens/teste.gif" border="0" alt="Debian-BR">
<br>
<hr noshade width="129">

<!-- menu -->

<font face="lucida" size="3">

<br>

<a href="../sobre.html">
<img src="/imagens/help.jpg" border="0" alt="Sobre o Projeto">
<br>Sobre</a>

<br><br>

<a href=http://www.debian.org/News/weekly/>
<img src="/imagens/news.gif" border="0" alt="Novidades Semanais">
<br>Novidades</a>

<br><br>

<a href="../projetos/">
<img src="/imagens/projetos.png" border="0" alt="Projetos">
<br>Projetos</a>

<br><br>

<a href="../software.html">
<img src="/imagens/soft.gif" border="0" alt="Software">
<br>Software</a>

<br><br>

<a href="../suporte/">
<img src="/imagens/suporte.png" border="0" alt="Suporte">
<br>Suporte</a>

<br><br>

<a href="../suporte/documentacao.html">
<img src="/imagens/tutoriais.gif" border="0" alt="Documenta��o">
<br>Documenta��o</a>

<br><br>

<a href="../comunidade/">
<img src="/imagens/comunidade.png" border="0" alt="Comunidade">
<br>Comunidade</a>

<br><br>

<a href="../amigos.html">
<img src="/imagens/relacionados.png" border="0" alt="Sites Relacionados">
<br>Sites Relacionados</a>

</font>
</center>

<br><br>

</td>

<!-- menu -->

<td bgcolor="#ffffff" width="510">
<table border="0" cellspacing="0" cellpadding="30">
<tr><td>
<center>	<h1>Lista P�blica de Usu�rios</h1>
<hr noshade>
</center>
Essa � a listagem p�blica dos usu�rios cadastrados no contador. A lista est� organizada pelo n�mero
de registro de cada pessoa. Espero criar outros m�todos de "sort" para essa listagem. � importante
ressaltar que apenas os usu�rios que quiserem ser�o listados aqui. Para n�o ser visto nessa lista,
basta selecionar "N�o" na �ltima pergunta do cadastro.
<center>
<hr>
<table border=1 cellpadding=1 cellspacing=1>
<tr>
<td align=center><b>N�mero</b></td>
<td align=center><b>Nome</b></td>
<td align=center><b>Nick</b></td>
<td align=center><b>Idade</b></td>
<td align=center><b>Escolaridade</b></td>
<td align=center><b>E-Mail</b></td>
<td align=center><b>Estado</b></td>
<td align=center><b>Cidade</b></td>
<td align=center><b>In�cio do uso</b></td>
<td align=center><b>Uso</b></td></tr>

<? include('../../config.inc'); 
	$erros = 0;
	if (!$conn = mysql_pconnect ($localhost, $huser, $hpass))
	{
		echo "<font color=\"#ee4000\">";
		echo "<p>erro acessando banco de dados</p>";
		echo "</font>";
		$erros++;
	}
	mysql_select_db ("debianbr");
	
	$q = mysql_query ("select * from usuarios");
	
	$encontrados = mysql_num_rows ($q);
	
	for ($i = 0 ; $i < mysql_num_rows ($q) ; $i++)
	{
		$linha = mysql_fetch_array ($q);
		if (!strcmp($linha["publicar"], "s")) 
		{
		      $count=0;
			
			$uso = NULL;
			$usofinal = NULL;
			
			if (similar_text ($linha["uso"], "c"))  // processando o uso
			{ $uso[$count] = "em casa"; 
			$count++;}
			if (similar_text ($linha["uso"], "e"))
			{ $uso[$count] = "na escola"; 
			$count++;}
			if (similar_text ($linha["uso"], "t"))
			{ $uso[$count] = "no trabalho"; 
			$count++;}
			if (similar_text ($linha["uso"], "o"))
			{ $uso[$count] = "outros";
			$count++;}
		if (sizeof($uso) > 1)
			$usofinal = join (", ", $uso);
		else
			$usofinal = join ("", $uso);
			
			switch ($linha['escolaridade'])
			{
				case "1":
					$escolaridade = "Primeiro grau";
					break;
				case "2":
					$escolaridade = "Segundo Grau";
					break;
				case "3":
					$escolaridade = "Superior";
					break;
				case "4":
					$escolaridade = "Mestrado";
					break;
				case "5":
					$escolaridade = "Doutorado";
					break;
			}

			$dataatual = date ("m d Y");
			$datanasc = $linha ["nascimento"];
			$dataatual = explode (" ", $dataatual);
			$datanasc= explode ("-", $datanasc);
			// transfere cada numero da string pra int
			$mesa = $dataatual[0];
			$diaa = $dataatual[1];
			$anoa = $dataatual[2];
			// agora da de nascimento
			$anon = $datanasc[0];
			$mesn = $datanasc[1];
			$dian = $datanasc[2];
			for ($a = 0 ; $a < 3 ; $a++)
			{
				settype ($datanasc[$a], integer);
				settype ($dataatual[$a], integer);
			}
			
			// Agora �s compara��es
			$idade = $anoa - $anon;
			if ($mesa < $mesn)
			{
				$idade--;
			}
			else if ($mesa == $mesn && $diaa < $dian)
			{
				$idade--;
			}
									
			$numero = $linha["chave"];
			$nome = $linha["nome"];
			$nick = $linha["nick"];
			$email = $linha["email"];
			$estado = $linha["estado"];
			$cidade = $linha["cidade"];
			$datainicio = $linha["datainicio"];
						
			echo "<tr>";
			echo "<td align=center>$numero</td>";
			echo "<td align=center>$nome</td>";
			echo "<td align=center>$nick</td>";
			echo "<td align=center>$idade</td>";
			echo "<td align=center>$escolaridade</td>";
			echo "<td align=center><a href=\"mailto:$email\">$email</a></td>";
			echo "<td align=center>$estado</td>";
			echo "<td align=center>$cidade</td>";
			echo "<td align=center>$datainicio</td>";
			echo "<td align=center>$usofinal</td>";
			echo "</tr>";
		} else { $encontrados--; }
	}
	echo "</table><br><hr><h4>";
	if ($encontrados == 0)
	{ echo "N�o foi encontrado nenhum registro"; }
	elseif ($encontrados == 1)
	{ 
		echo "Foi encontrado 1 usu�rio"; 
	}
	else
	{ echo"Foram encontrados $encontrados usu�rios"; 
	}
	echo "</h4>";
?>	
	
<a href="statp.php">Voltar</a>
<!-- eof -->

</font>
</tr></table>
<td></td>
<td bgcolor="#000000"><img src="../imagens/darkness.gif" width="2" height="2" alt=""></td>
</tr>
<td>
</td>
<td colspan="2" bgcolor="#000000"><img src="../imagens/darkness.gif" width="636" height="2" alt=""></td>
<!-- TD aqui -->
</tr>
</table>

<br><br>
<a href="http://www.cipsga.org.br">
<img src="../imagens/cipsga-banner.jpg" align="middle" border="1" alt="CIPSGA"></a></center>

<br>

<!--
<div align="center">
<a href="http://www.comnet.com.br/or">
<img src="http://www.comnet.com.br/or/or.png" alt="[Orange Ribbon Campaign Against Terrorism icon]" height="76" width="112" border="1" align="middle">
<br>
Join the Orange Ribbon Against Terrorism Campaign!</a>
</div>
-->

<div align="right">
<font color="#ffffff"><small><i>Copyleft &copy 2000-2001 Comunidade Debian Mundial</i></small></font>
</div>

</body>
</html>
