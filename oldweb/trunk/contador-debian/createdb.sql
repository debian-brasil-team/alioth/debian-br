#drop table usuarios;
#drop table num_usuarios;
#drop table maquinas;
#drop table num_maquinas;

CREATE TABLE usuarios (
    	chave int,
		nome varchar(50),
		nick varchar(50),
		senha varchar(20),
		nascimento date,
		escolaridade int,
		email varchar(50),
		estado varchar(2),
		cidade varchar(50),
		datainicio varchar(20),
		uso varchar(10),
		publicar varchar(1)
);

CREATE TABLE num_usuarios (
		soma int
);

INSERT INTO num_usuarios VALUES (0);

CREATE TABLE maquinas (
		chave int,
		chaveu int,
		nome varchar(50),
		estado varchar(2),
		proposito varchar(255),
		classe varchar(100),
		cpu varchar(100),
		memoria varchar(40),
		disco varchar(40),
		rede varchar(100),
		versao varchar(30),
		usuarios int,
		contas int
);

CREATE TABLE num_maquinas (
		soma int
);

INSERT INTO num_maquinas VALUES (0);
