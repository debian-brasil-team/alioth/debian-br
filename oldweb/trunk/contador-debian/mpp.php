<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="pt">

<head>
<title>Projeto Debian-BR : Debian em Portugu�s</title>
<META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="/dbr.css">
<meta name="Author" content="Projeto Debian-BR, Ricardo Sandrin">
<meta name="KeyWords" content="Debian, Brasil, Debian-BR, GNU/Linux">
</head>

<center><br>
<table border="0" width="600" cellspacing="0" cellpadding="0">
<tr><!-- borda superior -->

<td></td>
<td colspan="2" bgcolor="#000000">
<img src="/imagens/darkness.gif" width="636" height="2" alt=""></td>
<td></td>
</tr>
<tr valign=top><!-- parte principal -->

<td width="2" bgcolor="#000000">
<img src="/imagens/darkness.gif" width="2" height="2" alt="">
</td><!-- barra esquerda -->

<td bgcolor="#ededed">
<center>
<br><br>
<a href="../index.html"><img src="/imagens/beh.gif" alt="DDP-BR" border="0">
</a>
<br><br>
<img src="/imagens/teste.gif" border="0" alt="Debian-BR">
<br>
<hr noshade width="129">

<!-- menu -->

<font face="lucida" size="3">

<br>

<a href="../sobre.html">
<img src="/imagens/help.jpg" border="0" alt="Sobre o Projeto">
<br>Sobre</a>

<br><br>

<a href=http://www.debian.org/News/weekly/>
<img src="/imagens/news.gif" border="0" alt="Novidades Semanais">
<br>Novidades</a>

<br><br>

<a href="../projetos/">
<img src="/imagens/projetos.png" border="0" alt="Projetos">
<br>Projetos</a>

<br><br>

<a href="../software.html">
<img src="/imagens/soft.gif" border="0" alt="Software">
<br>Software</a>

<br><br>

<a href="../suporte/">
<img src="/imagens/suporte.png" border="0" alt="Suporte">
<br>Suporte</a>

<br><br>

<a href="../suporte/documentacao.html">
<img src="/imagens/tutoriais.gif" border="0" alt="Documenta��o">
<br>Documenta��o</a>

<br><br>

<a href="../comunidade/">
<img src="/imagens/comunidade.png" border="0" alt="Comunidade">
<br>Comunidade</a>

<br><br>

<a href="../amigos.html">
<img src="/imagens/relacionados.png" border="0" alt="Sites Relacionados">
<br>Sites Relacionados</a>

</font>
</center>

<br><br>

</td>

<!-- menu -->

<td bgcolor="#ffffff" width="510">
<table border="0" cellspacing="0" cellpadding="30">
<tr><td>
<center>	<h1>M�quinas que usam o Debian como: </h1><hr noshade>
<table cellpadding=10>
<? 
	include ('../../config.inc');
	$conn = mysql_connect ($localhost, $huser, $hpass);
	mysql_select_db ("debianbr");
	$c = mysql_query ("select proposito from maquinas");
	
	$estacao = 0;
	$ftp = 0;
	$www = 0;
	$mail = 0;
	$dns = 0;
	$arquivos = 0;
	$outroserver = 0;
	$roteador = 0;
	$firewall = 0;
	$brincar = 0;
	$aprender = 0;
	$programar = 0;
	
	if ($linhas = mysql_num_rows($c)) {       
		for ($i = 0 ; $i < $linhas ; $i++)
		{
			$r = mysql_result ($c, $i, "proposito");
			
			$proptest = strstr ($r, "Esta��o de Trabalho");
			if ( !empty ($proptest) )
			{
				$estacao++;
			}
			$proptest = strstr ($r, "Servidor FTP");
			if ( !empty ($proptest) )
			{
				$ftp++;
			}
			$proptest = strstr ($r, "Servidor WWW");
			if ( !empty ($proptest) )
			{
				$www++;
			}
			$proptest = strstr ($r, "Servidor de Mail");
			if ( !empty ($proptest) )
			{
				$mail++;
			}
			$proptest = strstr ($r, "Servidor de DNS");
			if ( !empty ($proptest) )
			{
				$dns++;
			}
			$proptest = strstr ($r, "Servidor de Arquivos");
			if ( !empty ($proptest) )
			{
				$arquivos++;
			}
			$proptest = strstr ($r, "Outro Servidor");
			if ( !empty ($proptest) )
			{
				$outroserver++;
			}
			$proptest = strstr ($r, "Roteador");
			if ( !empty ($proptest) )
			{
				$roteador++;
			}
			$proptest = strstr ($r, "Firewall");
			if ( !empty ($proptest) )
			{
				$firewall++;
			}
			$proptest = strstr ($r, "Brincar");
			if ( !empty ($proptest) )
			{
				$brincar++;
			}
			$proptest = strstr ($r, "Aprender");
			if ( !empty ($proptest) )
			{
				$aprender++;
			}
			$proptest = strstr ($r, "Programar");
			if ( !empty ($proptest) )
			{
				$programar++;
			}
		}
	
		echo "<tr><td><b>Esta��o de Trabalho:</b></td><td>$estacao</td></tr>";
		echo "<tr><td><b>Servidor FTP:</b></td><td>$ftp</td></tr>";
		echo "<tr><td><b>Servidor WWW:</b></td><td>$www</td></tr>";
		echo "<tr><td><b>Servidor de Mail:</b></td><td>$mail</td></tr>";
		echo "<tr><td><b>Servidor de DNS:</b></td><td>$dns</td></tr>";
		echo "<tr><td><b>Servidor de Arquivos:</b></td><td>$arquivos</td></tr>";		
		echo "<tr><td><b>Outro Servidor:</b></td><td>$outroserver</td></tr>";
		echo "<tr><td><b>Roteador:</b></td><td>$roteador</td></tr>";
		echo "<tr><td><b>Firewall:</b></td><td>$firewall</td></tr>";
		echo "<tr><td><b>Brincar:</b></td><td>$brincar</td></tr>";
		echo "<tr><td><b>Aprender:</b></td><td>$aprender</td></tr>";
		echo "<tr><td><b>Programar:</b></td><td>$programar</td></tr>";
	}

?>
</table>
<br><hr noshade>
<a href="statm.php">Voltar</a><hr noshade>

</center>
<font color=red>Aten��o:</font> A soma desses valores n�o mostra o valor total correto do 
n�mero de m�quinas cadastradas, j� que um usu�rios pode ter feito mais de uma escolha para
esse item.<br>
Veja <a href="statm.php">aqui</a> o n�mero total de m�quinas cadastrados.<br>
<!-- eof -->

</font>
</tr></table>
<td></td>
<td bgcolor="#000000"><img src="../imagens/darkness.gif" width="2" height="2" alt=""></td>
</tr>
<td>
</td>
<td colspan="2" bgcolor="#000000"><img src="../imagens/darkness.gif" width="636" height="2" alt=""></td>
<!-- TD aqui -->
</tr>
</table>

<br><br>
<a href="http://www.cipsga.org.br">
<img src="../imagens/cipsga-banner.jpg" align="middle" border="1" alt="CIPSGA"></a></center>

<br>

<!--
<div align="center">
<a href="http://www.comnet.com.br/or">
<img src="http://www.comnet.com.br/or/or.png" alt="[Orange Ribbon Campaign Against Terrorism icon]" height="76" width="112" border="1" align="middle">
<br>
Join the Orange Ribbon Against Terrorism Campaign!</a>
</div>
-->

<div align="right">
<font color="#ffffff"><small><i>Copyleft &copy 2000-2001 Comunidade Debian Mundial</i></small></font>
</div>

</body>
</html>
