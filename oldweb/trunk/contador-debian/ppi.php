<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="pt">

<head>
<title>Projeto Debian-BR : Debian em Português</title>
<META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="/dbr.css">
<meta name="Author" content="Projeto Debian-BR, Ricardo Sandrin">
<meta name="KeyWords" content="Debian, Brasil, Debian-BR, GNU/Linux">
</head>

<center><br>
<table border="0" width="600" cellspacing="0" cellpadding="0">
<tr><!-- borda superior -->

<td></td>
<td colspan="2" bgcolor="#000000">
<img src="/imagens/darkness.gif" width="636" height="2" alt=""></td>
<td></td>
</tr>
<tr valign=top><!-- parte principal -->

<td width="2" bgcolor="#000000">
<img src="/imagens/darkness.gif" width="2" height="2" alt="">
</td><!-- barra esquerda -->

<td bgcolor="#ededed">
<center>
<br><br>
<a href="../index.html"><img src="/imagens/beh.gif" alt="DDP-BR" border="0">
</a>
<br><br>
<img src="/imagens/teste.gif" border="0" alt="Debian-BR">
<br>
<hr noshade width="129">

<!-- menu -->

<font face="lucida" size="3">

<br>

<a href="../sobre.html">
<img src="/imagens/help.jpg" border="0" alt="Sobre o Projeto">
<br>Sobre</a>

<br><br>

<a href=http://www.debian.org/News/weekly/>
<img src="/imagens/news.gif" border="0" alt="Novidades Semanais">
<br>Novidades</a>

<br><br>

<a href="../projetos/">
<img src="/imagens/projetos.png" border="0" alt="Projetos">
<br>Projetos</a>

<br><br>

<a href="../software.html">
<img src="/imagens/soft.gif" border="0" alt="Software">
<br>Software</a>

<br><br>

<a href="../suporte/">
<img src="/imagens/suporte.png" border="0" alt="Suporte">
<br>Suporte</a>

<br><br>

<a href="../suporte/documentacao.html">
<img src="/imagens/tutoriais.gif" border="0" alt="Documentação">
<br>Documentação</a>

<br><br>

<a href="../comunidade/">
<img src="/imagens/comunidade.png" border="0" alt="Comunidade">
<br>Comunidade</a>

<br><br>

<a href="../amigos.html">
<img src="/imagens/relacionados.png" border="0" alt="Sites Relacionados">
<br>Sites Relacionados</a>

</font>
</center>

<br><br>

</td>

<!-- menu -->

<td bgcolor="#ffffff" width="510">
<table border="0" cellspacing="0" cellpadding="30">
<tr><td>
<center>	<h1>Pesquisa por Idade</h1>
<hr noshade>
<table border=1 cellpadding=1 cellspacing=1>
<tr>
<td align=center><b>0-20</b></td>
<td align=center><b>20-30</b></td>
<td align=center><b>30-40</b></td>
<td align=center><b>40-...</b></td>
</tr>

<? include('../../config.inc'); 
	$erros = 0;
	
	if (!$conn = mysql_pconnect ($localhost, $huser, $hpass))
	{
		echo "<font color=\"#ee4000\">";
		echo "<p>erro acessando banco de dados</p>";
		echo "</font>";
		$erros++;
	}
	
	mysql_select_db ("debianbr");
	
	$anoatual = date ("Y");
	$anoatual = $anoatual - 20;
	$q = mysql_query ("select * from usuarios where nascimento >= '$anoatual-01-01'");
	$vinte = mysql_num_rows ($q); // 0-20
	$anoatual2 = $anoatual - 10;
	$q = mysql_query("select * from usuarios where nascimento >= '$anoatual2-01-01' and nascimento <= '$anoatual-01-01'");
	$trinta = mysql_num_rows ($q); // 20-30
	$anoatual3 = $anoatual2 - 10;
	$q = mysql_query("select * from usuarios where nascimento >= '$anoatual3-01-01' and nascimento <= '$anoatual2-01-01'");
	$quarenta = mysql_num_rows ($q); //30-40
	$anoatual4 = $anoatual - 100;	
	$q = mysql_query("select * from usuarios where nascimento >= '$anoatual4-01-01' and nascimento <= '$anoatual3-01-01'");
	$acima = mysql_num_rows($q); // 40-...
	
	echo "<tr>";
	echo "<td align=center>$vinte</td><td align=center>$trinta</td>";
	echo "<td align=center>$quarenta</td><td align=center>$acima</td>";
	echo "</tr>";
	
	echo "</table><br><hr noshade>";
?>	
	
<a href="statp.php">Voltar</a>
<!-- eof -->

</font>
</tr></table>
<td></td>
<td bgcolor="#000000"><img src="../imagens/darkness.gif" width="2" height="2" alt=""></td>
</tr>
<td>
</td>
<td colspan="2" bgcolor="#000000"><img src="../imagens/darkness.gif" width="636" height="2" alt=""></td>
<!-- TD aqui -->
</tr>
</table>

<br><br>
<a href="http://www.cipsga.org.br">
<img src="../imagens/cipsga-banner.jpg" align="middle" border="1" alt="CIPSGA"></a></center>

<br>

<!--
<div align="center">
<a href="http://www.comnet.com.br/or">
<img src="http://www.comnet.com.br/or/or.png" alt="[Orange Ribbon Campaign Against Terrorism icon]" height="76" width="112" border="1" align="middle">
<br>
Join the Orange Ribbon Against Terrorism Campaign!</a>
</div>
-->

<div align="right">
<font color="#ffffff"><small><i>Copyleft &copy 2000-2001 Comunidade Debian Mundial</i></small></font>
</div>

</body>
</html>
