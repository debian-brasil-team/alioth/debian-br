<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="pt">

<head>
<title>Projeto Debian-BR : Debian em Portugu�s</title>
<META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="/dbr.css">
<meta name="Author" content="Projeto Debian-BR, Ricardo Sandrin">
<meta name="KeyWords" content="Debian, Brasil, Debian-BR, GNU/Linux">
</head>

<center><br>
<table border="0" width="600" cellspacing="0" cellpadding="0">
<tr><!-- borda superior -->

<td></td>
<td colspan="2" bgcolor="#000000">
<img src="/imagens/darkness.gif" width="636" height="2" alt=""></td>
<td></td>
</tr>
<tr valign=top><!-- parte principal -->

<td width="2" bgcolor="#000000">
<img src="/imagens/darkness.gif" width="2" height="2" alt="">
</td><!-- barra esquerda -->

<td bgcolor="#ededed">
<center>
<br><br>
<a href="../index.html"><img src="/imagens/beh.gif" alt="DDP-BR" border="0">
</a>
<br><br>
<img src="/imagens/teste.gif" border="0" alt="Debian-BR">
<br>
<hr noshade width="129">

<!-- menu -->

<font face="lucida" size="3">

<br>

<a href="../sobre.html">
<img src="/imagens/help.jpg" border="0" alt="Sobre o Projeto">
<br>Sobre</a>

<br><br>

<a href=http://www.debian.org/News/weekly/>
<img src="/imagens/news.gif" border="0" alt="Novidades Semanais">
<br>Novidades</a>

<br><br>

<a href="../projetos/">
<img src="/imagens/projetos.png" border="0" alt="Projetos">
<br>Projetos</a>

<br><br>

<a href="../software.html">
<img src="/imagens/soft.gif" border="0" alt="Software">
<br>Software</a>

<br><br>

<a href="../suporte/">
<img src="/imagens/suporte.png" border="0" alt="Suporte">
<br>Suporte</a>

<br><br>

<a href="../suporte/documentacao.html">
<img src="/imagens/tutoriais.gif" border="0" alt="Documenta��o">
<br>Documenta��o</a>

<br><br>

<a href="../comunidade/">
<img src="/imagens/comunidade.png" border="0" alt="Comunidade">
<br>Comunidade</a>

<br><br>

<a href="../amigos.html">
<img src="/imagens/relacionados.png" border="0" alt="Sites Relacionados">
<br>Sites Relacionados</a>

</font>
</center>

<br><br>

</td>

<!-- menu -->

<td bgcolor="#ffffff" width="510">
<table border="0" cellspacing="0" cellpadding="30">
<tr><td>
<? include ('../../config.inc');
	$erros = 0;
	$avisos = 0;

		
	if (empty($chave))
	{
		echo "<font face=red><br><h3>O campo chave n�o pode ficar em branco!</h3><br></font>";
		$avisos++;
	}
	if (empty($senha))
	{
		echo "<font face=red><br><h3>O campo senha n�o pode ficar em branco!</h3></font>";
		$avisos++;
	}
	if (empty($nome))
	{
		echo "<font face=red><br><h3>O campo nome n�o pode ficar em branco!</h3></font>";
		$avisos++;
	}

	
	/* checagens de erro */
	if (empty ($prop))
	{
		$prop[0] = "Outros";
	}
	
	$prop = join (", ", $prop);
	
	$cpu = join ("_", $cpu);
	$rede = join ("-", $rede);
	
	if (!$conn = mysql_connect ($localhost, $huser, $hpass))
	{
		echo "<font color=\"#ee4000\">";
		echo "<p>erro acessando banco de dados</p>";
		echo "</font>";
		$erros++;
	}
	
	mysql_select_db ("debianbr");
	
	$exec = mysql_query ("select senha from usuarios where senha = password('$senha') and chave = '$chaveu'");
	if ($rows = mysql_num_rows ($exec))
	{
		$senhaCadastro = mysql_result ($exec, 0, "senha");
		$exec = mysql_query ("select * from maquinas where chave='$chave'");
		if (!mysql_num_rows ($exec))
		{
			echo "<font color=\"#ee4000\">";
			echo "<p>M�quina inexistente! Tente novamente!</p>";
			echo "</font>";
			$avisos++;
		}
	}
	else
	{
		echo "<font color=\"#ee4000\">";
		echo "<p>Senha incorreta ou chave de usu�rio inexistente! Tente novamente!</p>";
		echo "</font>";
		$avisos++;
	}

	if ($avisos == 0 && $erros == 0)
	{
		$exec = mysql_query("update maquinas set chaveu=$chaveu, nome='$nome', estado='$estado', proposito='$prop', classe='$classe', cpu='$cpu', memoria='$mem', disco='$disk', rede='$rede', versao='$versao', usuarios='$numusuarios', contas='$numcontas' where chave='$chave'");
	}
	
	if ($erros == 0 && $avisos == 0)
	{
		echo "<h3>Registro alterado!</h3></center>";
		echo "<hr>";
		echo "Sua altera��o foi efetivada e as informa��es importantes ser�o enviadas ";
		echo "por e-mail para o endere�o especificado. Obrigado =)";
		echo "<hr><center><a href=\"contador.html\">Voltar</a>";
	
		$exec = mysql_query ("select * from usuarios where chave='$chave'");
		if (mysql_num_rows($exec))
		{
			$email = mysql_result ($exec, 0, "email");
			$unome = mysql_result ($exec, 0, "nome");
			
			$para = $email;
			$assunto = "Cadastro no Contador Debian";
			$fp = fopen ("mail-msg4.txt", "r");
			$mensagem = fread ($fp, filesize("mail-msg.txt"));
			fclose ($fp);
			$mensagem = str_replace ("[nome]", $unome, $mensagem);
			$mensagem = str_replace ("[hostname]", $nome, $mensagem);
			$mensagem = str_replace ("[soma]", $r, $mensagem);
			$mensagem = str_replace ("[email]", $email, $mensagem);
			$mensagem = str_replace ("[senha]", $senha, $mensagem);
	
			$headers = "From: Contador Debian <dockov@zaz.com.br>";
	
			mail ($para, $assunto, $mensagem, $headers);
		}			
	}
	if ($avisos != 0)
	{
		echo "<font color=\"#ee4000\">";
		echo "<p>Algum dado foi fornecido errado!</p>";
		echo "</font>";
	}
	if ($erros != 0)
	{
	 	echo "<font color=\"#ee4000\">";
	 	echo "<p>Houve erros no processamento do script</p>";
	 	echo "</font>";
	}
?>

</center></font>
<!-- eof -->

</font>
</tr></table>
<td></td>
<td bgcolor="#000000"><img src="../imagens/darkness.gif" width="2" height="2" alt=""></td>
</tr>
<td>
</td>
<td colspan="2" bgcolor="#000000"><img src="../imagens/darkness.gif" width="636" height="2" alt=""></td>
<!-- TD aqui -->
</tr>
</table>

<br><br>
<a href="http://www.cipsga.org.br">
<img src="../imagens/cipsga-banner.jpg" align="middle" border="1" alt="CIPSGA"></a></center>

<br>

<!--
<div align="center">
<a href="http://www.comnet.com.br/or">
<img src="http://www.comnet.com.br/or/or.png" alt="[Orange Ribbon Campaign Against Terrorism icon]" height="76" width="112" border="1" align="middle">
<br>
Join the Orange Ribbon Against Terrorism Campaign!</a>
</div>
-->

<div align="right">
<font color="#ffffff"><small><i>Copyleft &copy 2000-2001 Comunidade Debian Mundial</i></small></font>
</div>

</body>
</html>
