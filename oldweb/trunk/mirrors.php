<? include ('page.tpt') ?>

<h1>Mirrors do Debian</h1>

<hr noshade>

<font face="lucida" size="2">
Muitas pessoas est�o sempre perguntando onde achar mirrors
brasileiros do Debian. Aqui est�o alguns dos mirrors conhecidos,
informa��es sobre eles e linhas para serem colocadas no sources.list.
<p>
Em caso de erros, pedidos, reclama��es ou sugest�es, envie um
email para &lt;<a href="mailto:kov@debian.org">kov@debian.org</a>&gt;.
Se voc� tem um mirror, por favor envie os dados para que ele
seja inclu�do nessa lista.
<p>
<b><i>Nota 1</i></b>: Est�o listadas linhas para o sources.list que
levam � vers�o est�vel do Debian. Voc� pode tentar substituir o
'stable' das linhas para 'testing' ou 'unstable', mas pode ser
que nem todos os mirrors mantenham essas vers�es. Tamb�m n�o �
recomendado que voc� instale essas vers�es caso n�o tenha conhecimento
profundo no Debian.
<p>
<b><i>Nota 2</i></b>: Alguns mirrors cont�m descri��es de pacotes
traduzidas para o portugu�s. As linhas do sources.list a serem usadas
est�o nos items "Main pt_BR". Repare que essa linha somente tem a
parte 'main' do Debian ent�o voc� precisa usar uma linha separada
para as se��es contrib e non-free. Preste aten��o, n�o inclua a
se��o main nessa linha (do contrib e non-free) pois ela pode atrapalhar
o uso das descri��es em portugu�s. Um exemplo:
<p>
</font>
<pre>
deb ftp://ftp.debian.org.br/debian stable contrib non-free
deb ftp://ftp.debian.org.br/debian pt_BR/stable main
</pre>
<hr noshade>
<br>

<!-- debian.lcmi.ufsc.br -->
<table border="0">
<tr>
<td><font face=lucida size=2><b>Mirror</b></font></td>
<td><font face=lucida size=2>debian.das.ufsc.br</td></font>
</tr><tr>
<td><font face=lucida size=2><b>Localiza��o</b></font></td>
<td><font face=lucida size=2>Florian�polis - SC (DAS UFSC)</font></td>
</tr><tr>
<td><font face=lucida size=2><b>Link</b></font></td>
<td><font face=lucida size=2>32 Mbits</font></td>
</tr><tr>
<td><font face=lucida size=2><b>Atualiza��o</b></font></td>
<td><font face=lucida size=2>Di�ria</font></td>
</tr><tr>
<td><font face=lucida size=2><b>Mantenedores</b></font></td>
<br><font face=lucida size=2><a href="mailto:rottava@lcmi.ufsc.br">Luciano Rottava &lt;rottava@lcmi.ufsc.br&gt;</font></a></td>
</tr><tr>
<td><font face=lucida size=2><b>Arquiteturas</b></font></td>
<td><font face=lucida size=2>i386</font></td>
</tr><tr>
<td><font face=lucida size=2><b>Main</b></font></td>
<td><font face=lucida size=2>deb ftp://debian.das.ufsc.br/debian stable main contrib non-free</font></td>
</tr><tr>
<td><font face=lucida size=2><b>Non-US</b></font></td>
<td><font face=lucida size=2>deb ftp://debian.das.ufsc.br/debian-non-US unstable/non-US main contrib non-free</font></td>
</tr><tr>
<td><font face=lucida size=2><b>Source</b></font></td>
<td><font face=lucida size=2>deb-src ftp://debian.das.ufsc.br/debian stable main contrib non-free</font></td>
/tr><tr>
<td><font face=lucida size=2><b>Notas</b></font></td>
<td><font face=lucida size=2>Adicione 'Passive::debian.das.ufsc.br "false";' no /etc/apt/apt.conf</font></td>
</tr>

</table>
<hr noshade>

<!-- www.debian.org.br -->
<table border=0>
<tr>
<td><font face=lucida size=2><b>Mirror</b></font></td>
<td><font face=lucida size=2>www.debian.org.br
<br>ftp.debian.org.br
<br>moscow.procergs.org.br</td>
</tr><tr>
<td><font face=lucida size=2><b>Localiza��o</b></font></td>
<td><font face=lucida size=2>Porto Alegre - RS (Procergs)</font></td>
</tr><tr>
<td><font face=lucida size=2><b>Link</b></font></td>
<td><font face=lucida size=2>32 Mbps</font></td>
</tr><tr>
<td><font face=lucida size=2><b>Atualiza��o</b></font></td>
<td><font face=lucida size=2>Di�ria</font></td>
</tr><tr>
<td><font face=lucida size=2><b>Mantenedores</b></td>
<td><font face=lucida size=2><a href="mailto:macan@debian.org">Eduardo Marcel Ma�an &lt;macan@debian.org&gt;</a>
<br><a href="mailto:gleydson@debian.org">Gleydson Mazioli da Silva &lt;gleydson@debian.org&gt;</a></td></font>
</tr><tr>
<td><font face=lucida size=2><b>Arquiteturas</b></font></td>
<td><font face=lucida size=2>i386</font></td>
</tr><tr>
<td><font face=lucida size=2><b>Main</b></font></td>
<td><font face=lucida size=2>deb ftp://ftp.debian.org.br/debian stable main contrib non-free</font></td>
</tr><tr>
<td><font face=lucida size=2><b>Main pt_BR</b></font></td>
<td><font face=lucida size=2>deb ftp://ftp.debian.org.br/debian pt_BR/stable main</font></td>
</tr><tr>
<td><font face=lucida size=2><b>Non-US</b></font></td>
<td><font face=lucida size=2>deb http://ftp.debian.org.br/debian-non-US unstable/non-US main contrib non-free</font></td>
</tr><tr>
<td><font face=lucida size=2><b>Source</b></font></td>
<td><font face=lucida size=2>deb-src ftp://ftp.debian.org.br/debian stable main contrib non-free</font></td>
</tr>
</table>
<hr noshade>

<!-- ftp.quimica.ufpr.br -->
<table border=0>
<tr>
<td><font face=lucida size=2><b>Mirror</b></font></td>
<td><font face=lucida size=2>ftp://ftp.quimica.ufpr.br/</font></td>
</tr><tr>
<td><font face=lucida size=2><b>Localiza��o</b></font></td>
<td><font face=lucida size=2>Curitiba - PR</font></td>
</tr><tr>
<td><font face=lucida size=2><b>Link</b></font></td>
<td><font face=lucida size=2>N/C</font></td>
</tr><tr>
<td><font face=lucida size=2><b>Atualiza��o</b></font></td>
<td><font face=lucida size=2>N/C</font></td>
</tr><tr>
<td><font face=lucida size=2><b>Mantenedor</b></font></td>
<td><font face=lucida size=2><a href="mailto:fmatsumo@quimica.ufpr.br">F. Matsumo &lt;fmatsumo@quimica.ufpr.br&gt;</a></font></td>
</tr><tr>
<td><font face=lucida size=2><b>Arquiteturas</b></font></td>
<td><font face=lucida size=2>N/C</font></td>
</tr><tr>
<td><font face=lucida size=2><b>Main</b></font></td>
<td><font face=lucida size=2>deb ftp://ftp.quimica.ufpr.br/debian stable main contrib non-free</font></td>
</tr><tr>
<td><font face=lucida size=2><b>Non-US</b></font></td>
<td><font face=lucida size=2>deb ftp://ftp.quimica.ufpr.br/debian-non-US stable non-US/main non-US/contrib non-US/non-free</font></td>
</tr><tr>
<td><font face=lucida size=2><b>Source</b></font></td>
<td><font face=lucida size=2>N/C</font></td>
</tr>
</table>
<hr noshade>

<? include ('end.tpt') ?>
