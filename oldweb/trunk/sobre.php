<? include ('page.tpt') ?>

<h1>Debian-BR: Ajudando na Universaliza��o do Debian</h1>
<hr noshade>
<font face="lucida" size="2">
<p>
O Software Livre traz em sua filosofia uma id�ia de sociedade melhor
onde todos t�m acesso �s tecnologias e facilidades criadas pela 
inform�tica. O Debian traz em sua ess�ncia a id�ia de universalidade,
que � justamente viabilizar essa sociedade. O Primeiro passo para que
se tenha uma verdadeira universaliza��o do Sistema Operacional 
Debian GNU/Linux � que o usu�rio entenda o que pode fazer e entenda
como fazer.
<p>
Para que isso seja verdade, ele precisa de documenta��o e interfaces
de programas em sua l�ngua nativa. A� entra o projeto Debian-BR.
Nosso principal objetivo � localizar o Debian e tudo que esteja 
relacionado a ele para o Brasil. 
<p>
Para saber mais sobre Software Livre e Debian, visite as p�ginas
da <a href="http://www.fsf.org">Free Software Foundation</a>, do 
<a href="http://www.gnu.org">Projeto GNU</a> e do 
<a href="http://www.debian.org">Debian</a>.
<hr noshade>

<h1>OK, mas por que eu usaria o Debian?</h1>
<font face="lucida" size="2">

Abaixo temos algumas "raz�es" que poderiam levar voc� a usar o 
<strong>Debian</strong> GNU/Linux:<br>

<ul>
<li>Possui um boletim semanal: 
<a href="http://www.debian.org/News/weekly">Debian Weekly News</a>.</li>

<li>Possui uma homepage completa: 
<a href="http://www.debian.org">Debian.org</a>.</li>

<li>� desenvolvida por mais de 500 pessoas ao redor do mundo. 
O maior grupo de uma distribui��o de GNU/Linux: 
<a href="http://www.br.debian.org/devel/people">desenvolvedores do
Debian</a>.</li>

<li>Com o <strong>APT</strong>, o gerenciador de pacotes � muito simples 
atualizar os pacotes do <strong>Debian</strong>. Para atualizar toda a 
sua distribui��o, use <strong>dois</strong> simples comandos:<br>
<strong><i>apt-get update; apt-get dist-upgrade</i></strong></li>

<li>Tem um banco de dados de bugs aberto ao p�blico: 
<a href="http://www.br.debian.org/Bugs">Bug Tracking System</a>.</li>
<li>Possui dezenas de listas de discuss�o de ajuda em geral e 
desenvolvimento: 
<a href="http://www.debian.org/MailingLists/subscribe">Listas do
Debian</a>, inclusive <a href="suporte/irc.php">Nacionais</a>.</li>

<li>Alto compromisso com a seguran�a. A maioria dos problemas descobertos 
s�o corrigidos em 48 horas: 
<a href="http://www.debian.org/security/">Seguran�a no Debian</a>.</li>

<li><strong>3500</strong> pacotes na vers�o est�vel e 
<strong>9000</strong> pacotes na vers�o de teste.</li>

<li>Suporte a <strong>seis</strong> arquiteturas de computador: 
Alpha, ARM, Intel x86, Motorola 680x0, PowerPC e Sparc.</li>

<li>Dezenas de Mirrors (c�pia completa) ao redor do mundo inteiro: 
<a href="http://www.debian.org/misc/README.mirrors">Mirrors do Debian</a>.</li>

<li>Menu centralizado (com todos os programas) em todos os 
Window Managers.</li>

<p>
O Debian � um sistema que procura fazer as coisas do jeito
certo. � improv�vel ver algum pacote trocar flexibilidade
e qualidade por facilidade. A facilidade � um objetivo mas
tem de ser uma adi��o, n�o um substituto.

<p>
Outras raz�es podem ser encontradas no <a 
href="http://www.debian.org/social_contract">Contrato Social Debian</a>
e na p�gina que lista os <a
href="http://www.debian.org/intro/why_debian.pt.html">pr�s e contras
do Debian</a>.

<? include ('end.tpt') ?>
