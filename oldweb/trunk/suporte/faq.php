<? include ("../page.tpt") ?>

<? include ("secao.tpt") ?>

<h1>Perguntas Feitas Frequentemente</h1>
<hr noshade>
<p>
Aqui estar� dispon�vel o FAQ do Debian-BR. Ele est� dispon�vel
tamb�m na se��o documenta��o em v�rios formatos. Note que ele
est� em est�gio muito preliminar de constru��o.
<p>
Voc� pode acess�-lo online clicando 
<a href="../docs/sgml/debian-br-faq/online">aqui</a>. Ou pode ver
mais detalhes sobre ele e conseguir outros formatos clicando
<a href="../view.php?doc=debian-br-faq">aqui</a>.
<p>
Se voc� acha que algo est� faltando, procure escrever uma
pergunta pequena com uma resposta resumida e clique
<a href="faq-help.php">aqui</a> para colaborar com o FAQ.
Por favor, n�o mande apenas perguntas, elas n�o ser�o respondidas.
O lugar certo para se conseguir suporte s�o as listas e o
canal de IRC.
<hr noshade>

 <? include ("../end.tpt") ?>

