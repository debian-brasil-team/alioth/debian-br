<? include ("../page.tpt") ?>

<? include ("secao.tpt") ?>

<!-- irc/lista -->

<table border="0" cellspacing="0" cellpadding="0">
<tr><td>
<h1>IRC/Listas de E-Mail</h1>
<font face="lucida" size="2">
A grande maioria dos desenvolvedores do Debian-BR n�o se conhece pessoalmente. 
Como a maioria dos desenvolvedores do Debian. Por isso, a Internet � 
fundamental para o desenvolvimento do projeto. Para isso, existem listas 
de discuss�o por e-mail e um canal de irc.
</font>

<hr noshade>

<br>
<h1>Listas de Discuss�o</h1>
<font face="lucida" size="2">
Existem algumas listas de discuss�o para troca de informa��es, documenta��o 
traduzida, id�ias, enfim intera��o. Mande um email para os endere�os das 
listas acrescido de "-request" com "subscribe" no assunto.
<p>
Por exemplo: para assinar a <i>debian-user-portuguese@lists.debian.org</i>
mande um email para <i>debian-user-portuguese-request@lists.debian.org</i>
com "subscribe" no assunto. Pouco tempo depois voc� receber� uma mensagem
de confirma��o. Basta responder essa mensagem sem alterar o assunto
(o <i>Re:</i> � aceito sem problemas) para ter sua inscri��o confirmada.
<p>
Voc� pode tamb�m visitar o site <a 
href="http://www.debian.org/MailingLists/subscribe">de inscri��es</a>
nas listas do Debian. A �nica exce��o a essas regras � a primeira
lista de emails da listagem abaixo. Ela � hospedada no 
<a href="http://listas.cipsga.org.br">servidor de listas do CIPSGA</a>
e pode ser assinada <a 
href="http://listas.cipsga.org.br/cgi-bin/mailman/listinfo/debian-br">
aqui</a>.
<p>
Antes de postar uma d�vida, procure saber se ela j� n�o foi respondida
no nosso <a href="../view.php?doc=debian-br-faq">FAQ</a> e fa�a uma
<a href="http://lists.debian.org/search.html">procura nos arquivos das 
listas</a>. Veja tamb�m <a 
href="http://listas.cipsga.org.br/pipermail/debian-br/">o arquivo da 
Debian-BR</a>.
<p>
Lembramos que as listas que usamos fazem parte do Projeto Debian-BR e
os princ�pios do Projeto devem ser respeitados nelas. Al�m do
<a href="http://www.debian.org/MailingLists/">C�digo de Conduta</a>
das listas do Debian, criamos nossas pr�prias <a 
href="/view.php?doc=lista">regras de conduta</a> que devem ser levadas
em considera��o quando participando de uma das listas.
<p>
Veja as listas que usamos:

<br><br>
<table border="0">
<tr>
<td bgcolor="ededed"><font face="lucida" size="2">Nome:</font></td>
<td><font face="lucida" size="2">Debian-BR</font></td>
</tr>
<tr>
<td bgcolor="ededed"><font face="lucida" size="2">Fun��o:</font></td>
<td><font face="lucida" size="2">Discuss�o e organiza��o do Projeto 
Debian-BR</font></td>
</tr>
<tr>
<td bgcolor="ededed"><font face="lucida" size="2">E-mail:</font></td>
<td><font face="lucida" size="2">
<a href="mailto:debian-br@listas.cipsga.org.br">
&lt;debian-br@listas.cipsga.org.br&gt;</font></a></td>
</table>

<br>

<table border="0">
<tr>
<td bgcolor="ededed"><font face="lucida" size="2">Nome:</font></td>
<td><font face="lucida" size="2">Debian Localization Portuguese</font></td>
</tr>
<tr>
<td bgcolor="ededed"><font face="lucida" size="2">Fun��o:</font></td>
<td><font face="lucida" size="2">Organiza��o dos projetos de tradu��o e localiza��o</font></td>
</tr>
<tr>
<td bgcolor="ededed"><font face="lucida" size="2">E-mail:</font></td>
<td><font face="lucida" size="2"><a href="mailto:debian-l10n-portuguese@lists.debian.org">&lt;debian-l10n-portuguese@lists.debian.org&gt;</font></a></td>
</table>

<br>

<table border="0">
<tr>
<td bgcolor="ededed"><font face="lucida" size="2">Nome:</font></td>
<td><font face="lucida" size="2">Debian User Portuguese</font></td>
</tr>
<tr>
<td bgcolor="ededed"><font face="lucida" size="2">Fun��o:</font></td>
<td><font face="lucida" size="2">D�vidas de Usu�rios</font></td>
</tr>
<tr>
<td bgcolor="ededed"><font face="lucida" size="2">E-mail:</font></td>
<td><font face="lucida" size="2"><a href="mailto:debian-user-portuguese-request@lists.debian.org">&lt;debian-user-portuguese-request@lists.debian.org&gt;</font></a></td>
</table>

<br>

<table border="0">
</tr>
<tr>
<td bgcolor="ededed"><font face="lucida" size="2">Nome:</font></td>
<td><font face="lucida" size="2">Debian News Portuguese</font></td>
</tr>
<tr>
<td bgcolor="ededed"><font face="lucida" size="2">Fun��o:</font></td>
<td><font face="lucida" size="2">Lista para novidades sobre o Debian em Portugu�s</font></td>
</tr>
<tr>
<td bgcolor="ededed"><font face="lucida" size="2">E-mail:</font></td>
<td><font face="lucida" size="2"><a href="mailto:debian-news-portuguese-request@lists.debian.org">&lt;debian-news-portuguese-request@lists.debian.org&gt;</font></a></td>
</tr>
</table>

<hr noshade>

<p>
<h1>IRC - O Canal #Debian-BR</h1>
<fonf face="lucida" size="2">
O nosso Projeto mantem um canal de IRC (Internet Relay Chat) no servidor
<a href="http://www.openprojects.net">OpenProjects</a> <strong>(irc.openprojects.net)</strong> ou <strong>(irc.debian.org)</strong>, l�
voc� poder� bater um papo com desenvolvedores brasileiros usu�rios do Debian,
com o pessoal do projeto, sobre Debian, GNU/Linux, outros Sistemas Operacionais,
Problemas, Nada, M�sica, Asneiras e etc, temos no canal um bot (infobot) o
apt-br, que al�m de divertir o pessoal ajuda a manter o canal em ordem e a
informar nossos amigos que est�o no canal tirando d�vidas. N�o se esque�a de
compare�er no canal, voc� pode solucionar seus problemas conosco ou ent�o nos
ajudar a fazer o que se faz mais no IRC: nada. :-)
<p>
Lembramos que o canal � parte do Projeto Debian-BR e que, por maior
liberdade que se d� nos t�picos que podem ser discutidos, os princ�pios
do Projeto precisam ser mantidos. Al�m das regras da pr�pria OPN (veja
o <i>motd</i> assim que entrar no servidor) criamos um documento com
<a href="/view.php?doc=canal">regras de conduta</a> para o canal. N�o
deixe de ler.
</font>

<hr noshade>

<p>
<h1>apt-br - O Bot Universal</h1>
<fonf face="lucida" size="2">
Como j� foi citado acima temos um bot feito em Perl por <a
href="mailto:gleydson@debian.org">Gleydson Maziolli Silva</a> e suportado por ele e
pelo <a href="mailto:kov@debian.org">Gustavo Noronha Silva</a>. De uma olhada nos registros deste ano:
<br><br>
<a href="../docs/apt-br-logs/apt-br-log-fev-2001.bz2">apt-br: Fevereiro 2001</a><br>
<a href="../docs/apt-br-logs/apt-br-log-mar-2001.bz2">apt-br: Mar�o 2001</a><br>
<a href="../docs/apt-br-logs/apt-br-log-abr-2001.bz2">apt-br: Abril 2001</a><br>
<a href="../docs/apt-br-logs/apt-br-jun-2001.bz2">apt-br: Junho 2001</a><br>
<a href="../docs/apt-br-logs/apt-br-jul-2001.bz2">apt-br: Julho 2001</a><br>
<a href="../docs/apt-br-logs/apt-br-ago-2001.bz2">apt-br: Agosto 2001</a><br>
<a href="../docs/apt-br-logs/apt-br-set-2001.bz2">apt-br: Setembro 2001</a><br>
<a href="../docs/apt-br-logs/apt-br-out-2001.bz2">apt-br: Outubro 2001</a><br>
<a href="../docs/apt-br-logs/apt-br-nov-2001.bz2">apt-br: Novembro 2001</a><br>
<a href="../docs/apt-br-logs/apt-br-dez-2001.bz2">apt-br: Dezembro 2001</a>
<br><br>
</td></tr></table>

<!-- irc/lista -->

 <? include ("../end.tpt") ?>

