<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="pt">

<head>
<title>Projeto Debian-BR : Debian em Portugu�s</title>
<META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="/dbr.css">
<meta name="Author" content="Projeto Debian-BR, Ricardo Sandrin">
<meta name="KeyWords" content="Debian, Brasil, Debian-BR, GNU/Linux">
</head>

<center><br>
<table border="0" width="600" cellspacing="0" cellpadding="0">
<tr><!-- borda superior -->

<td></td>
<td colspan="2" bgcolor="#000000">
<img src="/imagens/darkness.gif" width="636" height="2" alt=""></td>
<td></td>
</tr>
<tr valign=top><!-- parte principal -->

<td width="2" bgcolor="#000000">
<img src="/imagens/darkness.gif" width="2" height="2" alt="">
</td><!-- barra esquerda -->

<td bgcolor="#ededed">
<center>
<br><br>
<a href="../index.html"><img src="/imagens/beh.gif" alt="DDP-BR" border="0">
</a>
<br><br>
<img src="/imagens/teste.gif" border="0" alt="Debian-BR">
<br>
<hr noshade width="129">

<!-- menu -->

<font face="lucida" size="3">

<br>

<a href="../sobre.html">
<img src="/imagens/help.jpg" border="0" alt="Sobre o Projeto">
<br>Sobre</a>

<br><br>

<a href=http://www.debian.org/News/weekly/>
<img src="/imagens/news.gif" border="0" alt="Novidades Semanais">
<br>Novidades</a>

<br><br>

<a href="../projetos/">
<img src="/imagens/projetos.png" border="0" alt="Projetos">
<br>Projetos</a>

<br><br>

<a href="../software.html">
<img src="/imagens/soft.gif" border="0" alt="Software">
<br>Software</a>

<br><br>

<a href="../suporte/">
<img src="/imagens/suporte.png" border="0" alt="Suporte">
<br>Suporte</a>

<br><br>

<a href="../suporte/documentacao.html">
<img src="/imagens/tutoriais.gif" border="0" alt="Documenta��o">
<br>Documenta��o</a>

<br><br>

<a href="../comunidade/">
<img src="/imagens/comunidade.png" border="0" alt="Comunidade">
<br>Comunidade</a>

<br><br>

<a href="../amigos.html">
<img src="/imagens/relacionados.png" border="0" alt="Sites Relacionados">
<br>Sites Relacionados</a>

</font>
</center>

<br><br>

</td>

<!-- menu -->

<td bgcolor="#ffffff" width="510">
<table border="0" cellspacing="0" cellpadding="30">
<tr><td>
<center>
<table border="0" cellpadding="6">
<tr><td>
<center>
<a href="documentacao.html">
<img src="imagens/tutoriais.gif" border="0" alt="Documentac�o"></a>
<br>
<font face="lucida" size="2"><a href="documentacao.html">Documenta��o</a></font>
</td>

<td>
<center>
<a href="http://rautu.cipsga.org.br">
<img src="imagens/rau-tu.png" border="0" alt="Debian Rau-Tu"></a>
<br>
<font face="lucida" size="2"><a href="http://rautu.cipsga.org.br">Debian Rau-Tu</a></font>
</td>

<td>
<center>
<a href="irc.html">
<img src="imagens/irc.gif" border="0" alt="IRC/Lista"></a>
<br>
<font face="lucida" size="2"><a href="irc.html">IRC/Listas</a></font>
</td>

<td>
<center>
<a href="faq.html">
<img src="imagens/faq.png" border="0" alt="FAQ"></a>
<br>
<font face="lucida" size="2"><a href="faq.html">FAQ</a></font>
</center>
</td>

</tr>
</table>

<hr noshade>

</center><h1>Ajude com o FAQ</h1>
<hr noshade>
<p>
Se voc� quer ajudar no FAQ, complete as entradas abaixo.
Nome e email s�o opcionais, mas � interessante que se coloque
pois eles ser�o adicionados os cr�ditos, assim estaremos
agradecendo a sua contribui��o.
<p>
N�o deixe de enviar a resposta. Se voc� s� tem a pergunta
(ou seja, n�o sabe como fazer), discuta antes nas listas
de discuss�o ou no canal de IRC o assunto (Veja a se��o
IRC/Listas)
<hr>

<?

if ($pergunta && $resposta)
{
  $assunto = "[debian-br-faq] contribui��o de $nome";
  $para = "debian-l10n-portuguese@lists.debian.org";
  $headers = "From: kov@debian.org";
  //  $headers = $headers . "\nCC: debian-l10n-portuguese@lists.debian.org";
  $headers = $headers . "\nReply-To: debian-l10n-portuguese@lists.debian.org";

  if ($email)
    {
      $headers = $headers . ", $email";
    }

  $mensagem = "De: $nome <$email>\n\n" .
    "Pergunta:\n$pergunta\n\n" .
    "Resposta:\n$resposta\n\n" .
    "Coment�rio:\n$comentario\n";

  mail ($para, $assunto, $mensagem, $headers);

  echo "<font color=\"red\">";
  echo "Mensagem enviada. Obrigado por sua colabora��o!";
  echo "</font><hr>";
}
else
{
  if ($pergunta)
    $faltou = "resposta";
  else if ($resposta)
    $faltou = "pergunta";
  else
    $faltou = "nada";
  
  if ($faltou != "nada")
    {
      echo "<font color=\"red\">";
      echo "Faltou preencher o campo $faltou abaixo.<br>";
      echo "Por favor n�o envie apenas perguntas, elas n�o ser�o respondidas.";
      echo "</font><hr>";
    }
}  

?>

<form action=faq-help.php method="post">

Nome: <input type="text" name="nome" size="40">
<br>
Email: <input type="text" name="email" size="40">
<p>
<h3>Pergunta:</h3>
<textarea name="pergunta" cols="80" rows="2" wrap="hard"></textarea>
<p>
<h3>Resposta:</h3>
<textarea name="resposta" cols="80" rows="24" wrap="hard"></textarea>
<p>
<h3>Coment�rio (para o mantenedor do FAQ):</h3>
<textarea name="comentario" cols="80" rows="24" wrap="hard"></textarea>
<hr>
<input type="submit" value="Enviar">
</form>
<hr noshade>
<!-- eof -->

</font>
</tr></table>
<td></td>
<td bgcolor="#000000"><img src="imagens/darkness.gif" width="2" height="2" alt=""></td>
</tr>
<td>
</td>
<td colspan="2" bgcolor="#000000"><img src="imagens/darkness.gif" width="636" height="2" alt=""></td>
<!-- TD aqui -->
</tr>
</table>

<br><br>
<a href="http://www.cipsga.org.br">
<img src="imagens/cipsga-banner.jpg" align="middle" border="1" alt="CIPSGA"></a></center>

<br>

<!--
<div align="center">
<a href="http://www.comnet.com.br/or">
<img src="http://www.comnet.com.br/or/or.png" alt="[Orange Ribbon Campaign Against Terrorism icon]" height="76" width="112" border="1" align="middle">
<br>
Join the Orange Ribbon Against Terrorism Campaign!</a>
</div>
-->

<div align="right">
<font color="#ffffff"><small><i>Copyleft &copy 2000-2001 Comunidade Debian Mundial</i></small></font>
</div>

</body>
</html>
