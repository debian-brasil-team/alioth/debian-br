<? include ("../page.tpt") ?>

<? include ("secao.tpt") ?>

<!-- documentacao -->

<table border="0" cellspacing="0" cellpadding="0">
<tr><td>
<table width="100%" cellspacing="0" border="0" celpadding="0">
<tr>
<td align="left" valign="bottom" width="184">

<h1>Documenta��o</h1>
<font face="lucida" size="2">
Esta � com certeza a se��o de mais import�ncia em nossa p�gina e em
nosso Projeto aqui est�o todos os documentos traduzidos e em fase de
tradu��o pela Comunidade Debian brasileira.
<p>
</td>
</tr>
</table>

<hr noshade>

<h1>Documentos feitos em Casa</h1>
<font face="lucida" size="2">
<p>
Aqui temos uma se��o com manuais e tutoriais feitos por membros do Projeto 
Debian-BR, se voc� fez algum tutorial ou manual pode nos enviar pelo 
e-mail: <a href="mailto:kov@debian.org">&lt;kov@debian.org&gt;</a>.
<p>

<table cellpadding=6 border=2>

<tr><td>
<a href="http://focalinux.cipsga.org.br">FOCA GNU/Linux</a>
<br>
<a href="mailto:gleydson@debian.org">Gleydson Mazioli da Silva</a>
<br><br>
N�o �, na verdade, um documento espec�fico do Debian, apesar de ter
sido escrito com base nele. Esse documento est� se tornando refer�ncia
nacional para sistemas GNU/Linux e tr�s desde informa��es b�sicas at�
configura��es mais pesadas.

</td></tr>
<tr><td>

<a href="../view.php?doc=apt-howto-pt_BR">Como usar o APT</a>
<br>
<a href="mailto:kov@debian.org">Gustavo Noronha Silva</a>
<br><br>
Manual de refer�ncia sobre as facilidades do sistema APT, documenta��o
oficial do Debian, re�ne praticidade e conte�do.

</td></tr><tr><td>

<a href="../view.php?doc=pratico">Guia Pr�tico para o Debian GNU/Linux</a>
<br>
<a href="mailto:kov@debian.org">Gustavo Noronha Silva</a> 
<br><br>
Manual sobre fun��es b�sicas do Debian que s�o �teis e nem sempre muito
conhecidas.

</td></tr><tr><td>

<a href="../view.php?doc=grub">Como usar o GRUB</a>
<br>
<a href="mailto:kov@debian.org">Gustavo Noronha Silva</a> e
<a href="mailto:vitor@vitoria.org.br">Vitor Silva Souza</a>
<br><br>
Um guia r�pido sobre como instalar e usar o Grub como inicializador
do sistema no dia a dia.

</td></tr><tr><td>

<a href="../view.php?doc=transicao22-30">Transi��o Potato - Woody</a>
<br>
<a href="mailto:gleydson@debian.org">Gleydson Mazioli da Silva</a>
<br><br>
Tem como objetivo facilitar a atualiza��o dos sistemas potato para o
woody guiando os usu�rios pelo caminho.

</td></tr>
<tr><td>

<a href="../view.php?doc=manual-deb">Manual DEB</a>
<br>
<a href="mailto:kov@debian.org">Gustavo Noronha Silva</a>
<br><br>
Manual que visa auxiliar de um modo mais 'direito' a forma de se criar
seus pacotes .deb.

</td></tr><tr><td>

<a href="../view.php?doc=bash-intl">Como Internacionalizar Scripts Bash</a>
<br>
<a href="mailto:kov@debian.org">Gustavo Noronha Silva</a>
<br><br>
Um curto e direto guia sobre como tornar scripts bash internacionaliz�veis
(traduz�veis).

</td></tr><tr><td>

<a href="../view.php?doc=modelo-sgml">Modelo SGML</a>
<br>
<a href="mailto:gleydson@debian.org">Gleydson Mazioli da Silva</a>
<br><br>
Modelo SGML autodid�tico e pr�tico para aprendizado e cria��o de
documentos usando SGML.

</td></tr><tr><td>

<a href="../view.php?doc=kdebian">Administrando Kernel no Debian</a>
<br>
<a href="mailto:kov@debian.org">Gustavo Noronha Silva</a>
<br><br>
Guia r�pido documentando as principais fun��es do sistema de
compila��o de kernel do Debian e explicando os v�rios pacotes
relacionados a kernel do Debian.

</td></tr><tr><td>

<a href="../docs/textos/PCTel-PCI-Potato.HOWTO-pt_BR.txt">PCTel HSP56 no Debian (potato)</a>
<br>
V�tor Souza <a href="vitor@vitoria.org.br">vitor@vitoria.org.br</a>
<br><br>
Pode vir a ser a solu��o para o grande problema dos posuidores de
Winmodems. Nas palavras do autor: "O prop�sito deste documento � ser
um guia para aqueles que est�o tendo problemas ao tentar instalar o
modem HSP56 Micromodem da PCTel num sistema Debian GNU/Linux 2.2r3
(potato).". � muito poss�vel que este documento tamb�m ajude usu�rios
de Woody e SID.
</font>

</td></tr>
</table>

<hr noshade>

<h1>Documentos Traduzidos</h1>
<p>
<table width="100%" border="0" cellspacing="2" cellpadding="3">
<tr>
<td bgcolor="ededed" width="75%"><font face="lucida" size="2">Manual de instala��o do Debian 3.0 (Woody) - Intel x86</td>
<td width="25%"><font face="lucida" size="2"> <a href="../view.php?doc=debian-install-i386">debian-install-i386</a>
</td>
</tr>
<tr>
<td bgcolor="ededed" width="75%"><font face="lucida" size="2">Manual de instala��o do Debian 3.0 (Woody) - PowerPC</td>
<td width="25%"><font face="lucida" size="2"> <a href="../view.php?doc=debian-install-powerpc">debian-install-powerpc</a>
</td>
</tr>
<tr>
<td bgcolor="ededed" width="75%"><font face="lucida" size="2">Manual de instala��o do Debian 3.0 (Woody) - ARM</td>
<td width="25%"><font face="lucida" size="2"> <a href="../view.php?doc=debian-install-arm">debian-install-arm</a>
</td>
</tr>
<tr>
<td bgcolor="ededed" width="75%"><font face="lucida" size="2">Manual de instala��o do Debian 3.0 (Woody) - SPARC</td>
<td width="25%"><font face="lucida" size="2"> <a href="../view.php?doc=debian-install-sparc">debian-install-sparc</a>
</td>
</tr>
<tr>
<td bgcolor="ededed" width="75%"><font face="lucida" size="2">Manual de instala��o do Debian 3.0 (Woody) - Motorola 68xx</td>
<td width="25%"><font face="lucida" size="2"> <a href="../view.php?doc=debian-install-m68k">debian-install-m68k</a>
</td>
</tr>
<tr>
<td bgcolor="ededed" width="75%"><font face="lucida" size="2">Manual de instala��o do Debian 3.0 (Woody) - Alpha</td>
<td width="25%"><font face="lucida" size="2"> <a href="../view.php?doc=debian-install-alpha">debian-install-alpha</a>
</td>
</tr>
<tr>
<td bgcolor="ededed" width="75%"><font face="lucida" size="2">Tutorial sobre o dselect</font></td>
<td width="25%"><font face="lucida" size="2"> <a href="../view.php?doc=dselect-beginner">dselect-beginner</a>
</td>
</tr>
<tr>
<td bgcolor="ededed" width="75%"><font face="lucida" size="2">Debian Packaging Manual <strong>[obsoleto]</strong></font></td>
<td width="25%"><font face="lucida" size="2"><a href="../view.php?doc=packaging">packaging-manual</a>
</td>
</tr>
<tr>
<td bgcolor="ededed" width="75%"><font face="lucida" size="2">Guia dos Novos Mantenedores do Debian.</a></font></td>
<td width="25%"><font face="lucida" size="2"><a href="../view.php?doc=maint-guide">maint-guide</a>
</font></td>
<tr>
<td bgcolor="ededed" width="75%"><font face="lucida" size="2">Securing Debian HOWTO</font></td>
<td width="25%"><font face="lucida" size="2"><a href="../view.php?doc=securing-debian-howto">securing-debian-howto</a>
</td>
</tr>
<tr>
<td bgcolor="ededed" width="75%"><font face="lucida" size="2">Debian Meta Menual</font></td>
<td width="25%"><font face="lucida" size="2"><a href="../view.php?doc=meta-manual">meta-manual</a>
</td>
</tr>
<tr>
<td bgcolor="ededed" width="75%"><font face="lucida" size="2">Hist�ria do Projeto Debian</td>
<td width="25%"><font face="lucida" size="2"><a href="../view.php?doc=project-history">project-history</a>
</td>
</tr>
<tr>
<td bgcolor="ededed" width="75%"><font face="lucida" size="2">Debian FAQ</td>
<td width="25%"><font face="lucida" size="2"><a href="../view.php?doc=debian-faq">debian-faq</a>
</td>
</tr>
<tr>
<td bgcolor="ededed" width="75%"><font face="lucida" size="2">Debiandoc-SGML</font></td>
<td width="25%"><font face="lucida" size="2"><a href="../view.php?doc=debiandoc">debiandoc</a>
</font></td>
</tr>
<tr>
<td bgcolor="ededed" width="75%"><font face=lucida size="2">Lintian User Manual</font></td>
<td width="25%"><font face=lucida size="2"><a href="../view.php?doc=lintian-manual">lintian-manual</a>
</td>
</tr>       
</table>
</td>   
</tr>   
</table>
</font>
<hr noshade>
<font face="lucida" size="2"><p align=center><strong>* Todos os cr�ditos aos autores e tradutores est�o contidos nos documentos. *</strong></p></font>
<p>

<hr noshade>

<!-- eof documentacao -->

 <? include ("../end.tpt") ?>

