<? include ('page.tpt') ?>

<h2>Guia Pr�tico para o Desktop Debian GNU/Linux</h2>
<hr noshade>
O grupo Debian-BR, percebendo a falta de documenta��o
voltada ao usu�rio "Desktop", decidiu criar um documento
que ajudasse na configura��o dos sistemas e nas decis�es 
dos usu�rios Desktop.
<p>
Est�o participando dessa empreitada:
<p>
<table border=2>
<tr><td>Nome</td><td>Email</td><td>Nick (IRC)</td><td>Parte do Manual</td></tr>

<tr>
<td>Paulo Henrique Baptista de Oliveira</td>
<td> baptista@linuxsolutions.com.br </td>
<td>baptista</td>
<td>N/A</td>
</tr>

<tr>
<td>Kairo F. de Ara�jo</td>
<td> kairo@jfnet.com.br </td>
<td>N/A</td>
<td>N/A</td>
</tr>

<tr>
<td>Goedson Teixeira Paixao</td>
<td> gopaixao@dcc.ufmg.br </td>
<td>N/A</td>
<td>N/A</td>
</tr>

<tr>
<td>Rafael Sasaki</td>
<td> sasaki@solar.com.br </td>
<td>N/A</td>
<td>N/A</td>
</tr>

<tr>
<td>Michelle Ribeiro</td>
<td> michellersilva@uol.com.br </td>
<td>electra</td>
<td>N/A</td>
</tr>

<tr>
<td>Douglas Correa</td>
<td> douglas.correa@uol.com.br </td>
<td>douglas</td>
<td>Sylpheed, Plug N' Play</td>
</tr>

<tr>
<td>Ricardo Melo</td>
<td> rickmelo@uol.com.br</td>
<td>N/A</td>
<td>X para sis* e trident*, pppconfig, ethernet</td>
</tr>

<tr>
<td>Ederson Roberto Aleixo</td>
<td> eraleixo@terra.com.br </td>
<td>dimi</td>
<td>N/A</td>
</tr>

<tr>
<td>Victor Maida</td>
<td> vmaida@terra.com.br </td>
<td>N/A</td>
<td>N/A</td>
</tr>

<tr>
<td>Rodrigo Morais Araujo</td>
<td>rma@cin.ufpe.br</td>
<td>N/A</td>
<td>ISDN</td>
</tr>

<tr>
<td>Edson Yassuo Fugio</td>
<td>edsonyf@dglnet.com.br</td>
<td>N/A</td>
<td>Impress�o e USB</td>
</tr>

<tr>
<td>Andr� Luis Lopes</td>
<td>andrelop@ig.com.br</td>
<td>andrelop</td>
<td>Aplicativos para Internet</td>
</tr>

<tr>
<td>Luis Alberto Garcia Cipriano</td>
<td>lacipriano@uol.com.br</td>
<td>N/A</td>
<td>Aplicativos de �udio</td>
</tr>

<tr>
<td>Prof. Jo�o Henrique G. Borges</td>
<td>jhgb@uol.com.br</td>
<td>N/A</td>
<td>N/A</td>
</tr>

<tr>
<td>Eduardo Gon�alves</td>
<td>egsilva@prolan.com.br</td>
<td>rot200</td>
<td>Placa de Som, Instala��o</td>
</tr>

</table>

<hr>
Arquivos interessantes podem ser encontrados no diret�rio
<a href="stuff/">stuff</a>, incluindo um �ndice inicial,
proposto por Paulo Henrique.

<? include ('end.tpt') ?>
