#!/bin/sh
# Gera a p�gina

dirprincipal=`pwd`

begin=page.tpt
end=end.tpt
add=secao.tpt
ext=cnt
outext=html

if [ ! -x $1 ];then
 echo "O diret�rio n�o pode ser acessado, verifique as permiss�es!"
 exit 1
fi

if [ ! -w $1 ];then
 echo "N�o � poss�vel gravar no diret�rio, verifique as permiss�es!"
 exit 1
fi

if [ "$3" = "php" ]; then
    ext=cnp
    outext=php
fi

# Muda para o diret�rio especificado como argumento e retorna ao anterior no 
# final da execu��o do programa
DIRATUAL=$(pwd)
if [ ! -z $1 ];then cd $1;fi

ls *.$ext 2>/dev/null|grep ${ext} > /dev/null
if [ ! $? = 0 ];then
 echo "Nenhum arquivo .$ext encontrado em $PWD!"
 exit 1
fi

for ARQUIVO in *.$ext;do

outfile=../`basename ${ARQUIVO} .${ext}`.$outext
tmpfile=/tmp/gerapage-$$$$

    if [ "$2" = "fixlinks" ]; then
	cat $dirprincipal/spool/page.tpt \
	    | sed 's/<a href=\"/<a href=\"..\//g' \
	    > $tmpfile
    elif [ "$2" = "fotos" ]; then
	cat $dirprincipal/spool/page.tpt \
	    | sed 's/<a href=\"/<a href=\"..\/..\/..\//g' \
	    | sed 's/<img src=\"i/<img src=\"..\/..\/..\/i/g' \
	    | sed 's/<img src=i/<img src=..\/..\/..\/i/g' \
	    > $tmpfile
    else
	cat $dirprincipal/spool/page.tpt \
	    > $tmpfile
    fi
    if [ "$2" = "fotos" ]; then
	test -f secao.tpt && cat secao.tpt \
	    | sed 's/<a href=\"/<a href=\"..\/..\//g' \
	    >> $tmpfile
    else
	test -f secao.tpt && cat secao.tpt \
	    >> $tmpfile
    fi

    cat ${ARQUIVO} \
	>> $tmpfile

    cat $dirprincipal/spool/end.tpt \
	>> $tmpfile

    if [ "$4" = "fixbodyimgs" ]; then
	cat $tmpfile \
	| sed 's/<img src=\"i/<img src=\"..\/i/g' \
	| sed 's/<img src=i/<img src=..\/i/g' \
	> $outfile
    else
	cp $tmpfile $outfile
    fi

    rm $tmpfile

done

cd ${DIRATUAL}

exit 0
