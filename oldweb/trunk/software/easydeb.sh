#!/bin/sh -e

# Copryright 2001 - Gustavo Noronha Silva <kov@debian.org>
# Esse script est� dispon�vel sob a GPL (GNU General Public
# License) e pode ser distribu�do sob seus termos.

# easydeb -> um script para criar arquivos .deb comfacilidade
# para rodar esse script voc� antes deve construir (compilar)
# ou deixar pronto para instalar o programa/outro tipo de arquivo
# que vai instalar e criar o arquivo deb.rules
# 
# esse arquivo cont�m os seguintes campos:
#
# Package: nomedopacote
# Version: vers�odopacote
# Maintainer: Nome do Mantenedor <e@mail.org>
# Depends: pacotes, dos, quais, depende
# Description: Descri��o de linha �nica
#
# todo arquivo que for ser instalado tem de ter uma entrada
# do tipo:
# install: nomedoarquivo /diretorio/onde/serah/instalado permissao
# por exemplo:
# install: gklog /usr/bin 775
#
# arquivos de configura��o devem ter uma entrada 
# conffile: /etc/arquivodeconf.conf
#
# Bugs e sugest�es: Gustavo Noronha Silva <kov@debian.org>


if ! [ -f deb.rules ]; then
    echo "O arquivo deb.rules n�o existe, crie-o primeiro..."
    exit 1
fi

if [ -d debian ]; then
    rm -fr debian
fi

# le campos importantes do arquivo deb.rules no diretorio
# atual...

PACK=$(cat deb.rules | grep Package: | awk '{print $2}')
VERSION=$(cat deb.rules | grep Version: | awk '{print $2}')
ARCH=$(cat deb.rules | grep Arch: | awk '{print $2}')
DESC=$(cat deb.rules | grep Description: | sed s/Description:/\ /g)
DEPS=$(cat deb.rules | grep Depends: | sed s/Description:/\ /g)
MAINT=$(cat deb.rules | grep Maintainer: | sed s/Maintainer:/\ /g)

# arquivos a serem instalados

FILES=$(cat deb.rules | grep ^install: | awk '{print $2}' | tr "\n" " " )
DIRS=$(cat deb.rules | grep ^install: | awk '{print $3}' | tr "\n" " ")
MODS=$(cat deb.rules | grep ^install: | awk '{print $4}' | tr "\n" " ")

CFF=$(cat deb.rules | grep ^conffile: | awk '{print $4}' | tr "\n" " ")

# cria os diretorios...
for dir in $DIRS; do
    install -d debian/$PACK/$dir
done

# copia os arquivos...
i=1;
while [ 1 ]; do

file=$(echo $FILES | awk "{print \$$i}")
dir=$(echo $DIRS | awk "{print \$$i}")
mod=$(echo $MODS | awk "{print \$$i}")
if [ -z $file ];then
    break
fi
    install -m $mod $file debian/$PACK/$dir;

i=$(expr $i + 1)
done

# cria o diretorio de controle...
install -d debian/$PACK/DEBIAN

# escreve o control
echo -e \
"Package: $PACK\n\
Version: $VERSION\n\
Architecture: $ARCH\n\
Maintainer: $MAINT\n\
Depends: $DEPS\n\
Description: $DESC\n" > debian/$PACK/DEBIAN/control

# escreve o changelog
echo -e \
"$PACK ($VERSION) unstable; urgency=low\n\
\n\
\t* Criado pelo easydeb\n\
\n\
 -- $MAINT  `date -R`\n\
\n" > debian/$PACK/DEBIAN/changelog


# gera o deb
fakeroot dpkg --build debian/$PACK ../$PACK\_$VERSION\_$ARCH.deb

echo -e "\nALL DONE?! +)=)\n"
