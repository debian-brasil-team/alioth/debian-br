<? include ('page.tpt') ?>

<!-- software -->
<h1>Pegue o Debian!</h1>
<font face="lucida" size="2">
H� v�rias maneiras pelas quais voc� pode conseguir o 
Debian. A melhor seja, talvez, copiar de um amigo o
CD. No entanto, h� como pegar o Debian atrav�s da
rede.
</font>

<hr noshade>

<p>
<h1>Mirrors brasileiros</h1>
<font face="lucida" size="2">
Veja aqui a <a href="mirrors.php">lista de mirrors brasileiros</a>
do Debian. Voc� pode us�-los para atualizar seu Debian usando o
<a href="view.php?doc=apt-howto-pt_BR">APT</a>.
</font>

<hr noshade>

<p>
<h1>Imagens dos CDs para download</h1>
<font face="lucida" size="2">
Voc� pode tamb�m consultar o site 
<a href="http://cdimage.debian.org">http://cdimage.debian.org</a>
e pegar as imagens dos CDs de instala��o.
<p>
Imagens para instala��o do Debian 2.2.r3 em portugu�s est�o
dispon�veis no site 
<a href="ftp://ima.cipsga.org.br">ftp://ima.cipsga.org.br</a>.
<p>
<ul>
<li><a href="ftp://ima.cipsga.org.br/debian/debian-br-3.0r0-iso-1.raw">CD 1</a>
<li><a href="ftp://ima.cipsga.org.br/debian/debian-br-3.0r0-iso-2.raw">CD 2</a>
</ul>
<p>
Aqui est�o os MD5SUMS:
<br>
<ul>
<li>f859820e717653b3699d3bd53f606a72  debian-br-3.0r0-iso-1.raw
<li>906b6b178a75f61b3d34d981387182ec  debian-br-3.0r0-iso-2.raw
</ul>
</font>

<hr noshade>

<p>
<h1>Comprando CDs do Debian</h1>
<font face="lucida" size="2">
Para comprar CDs do Debian veja a <a 
href="http://www.debian.org/distrib/vendors#br">Lista de Lojas Brasileiras
que vendem CDs do Debian</a>. Prefira a <a 
href="http://www.linuxmall.com.br">Linux Mall - Brasil</a>, que permite
contribui��o para o Debian.
<p>
A lista completa e informa��es sobre os CDs do Debian est�o neste 
<a href="http://www.debian.org/distrib/vendors">endere�o</a>.
</font>

<p>
<h1>Capas de CD Debian-BR</h1>
<font face="lucida" size="2">
Se voc� precisa deixar a capa de seu CD Debian mais <i>bonitinha</i>, n�s 
do projeto <b>Debian-BR</b> fizemos duas vers�es de capa para o 
<b>Debian 2.2r3 "Potato" Debian-BR</b>.
<p>
Fa�a o download <a href="arquivos/release.tar.bz2">aqui</a>.
<p>
Veja aqui uma amostra das capas:
<p>
<a href="imagens/capa-cd1.jpg">
<img src="imagens/1.png" alt="Capa: 1" border="1"> &nbsp 
</a>
<a href="imagens/front-cd1.png">
<img src="imagens/2.png" alt="Capa: 2" border="1">
</a>

<h1>Software:</h1>
<p>
<font face="lucida" size="2">
Essa � uma se��o dedica para software do Debian 
GNU/Linux desenvolvido ou traduzido no Brasil, caso voc� deseje traduzir 
algum software espec�fico do Debian envie um e-mail para: 
<a 
href="mailto:debian-l10n-portuguese@lists.debian.org">
debian-l10n-portuguese@lists.debian.org
</a>.</font>

<hr noshade>

<table width="100%" border="0" cellspacing="0" cellpadding="4">
<tr>
<td bgcolor="ededed"><font face="lucida" size="2">Boot-Floppies (Instala��o)</td>
<td><font face="lucida" size="2">
<a href="software/boot-floppies.tgz">Discos de Instala��o em Portugu�s
</font></a>
</td>
</tr>

<tr>
<td bgcolor="ededed"><font face="lucida" size="2">Bot apt-br</td>
<td><font face="lucida" size="2"><a href="software/apt-br-0.45-3-Debian-BR_1.tar.bz2">apt-br-0.45-3-Debian-BR_1.tar.bz2</a></td>
</tr>
<tr>
<td bgcolor="ededed"><font face="lucida" size="2">Bot dpkg-br</td>
<td><font face="lucida" size="2"><a href="software/eggdrop-1.4.4-pt_BR.tar.gz">eggdrop-1.4.4-pt_BR.tar.gz</a></td></font>
</tr>
</table>

<!-- end-software -->

<? include ('end.tpt') ?>
