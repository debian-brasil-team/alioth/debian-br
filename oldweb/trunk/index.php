<? include ('page.tpt') ?>

<!-- index -->

<!-- intro -->

<h1>Introdu��o:</h1>
<font face="lucida" size="2">

Ol�, <strong>bem-vindo(a)</strong> � p�gina do Projeto Debian-BR, nosso objetivo � fazer dessa p�gina e suas dep�ncias um ponto de refer�ncia para a tradu��o e desenvolvimento do <a href="http://www.debian.org"><strong>Debian</strong></a> <a href="http://www.gnu.org/gnu/why-gnu-linux.html">GNU</a>/<a href="http://www.linux.org">Linux</a> no Brasil.</font>

<!-- end-intro -->

<!-- what da fuck -->

<h1>�timo! Mas o que � Debian?</h1>
<font face="lucida" size="2">

O <strong>Debian</strong> � uma distribui��o de GNU/Linux que tem como caracter�stica principal a <strong>universalidade</strong>, ou seja, tentamos fazer com que ele seja um Sistema Operacional de car�ter <strong>livre</strong> que possa ser usado em qualquer lugar do mundo por qualquer pessoa.
<br>O <strong>Debian</strong> � feito por mais de 500 volunt�rios ao redor de todo o <a href="http://www.debian.org/devel/developers.loc">globo</a>. Cada pacote seja ele um programa ou aplicativo tem como respons�vel o chamado <a href="http://www.debian.org/devel/">desenvolvedor</a>, cujas tarefas incluem a compila��o e a corre��o de <a href="http://bugs.debian.org">bugs</a> do pacote. � tamb�m reconhecido como uma das melhores e sen�o a melhor das distribui��es GNU/Linux numa vis�o mais fan�tica :-)</font>

<!-- end-what da fuck -->

<!-- dbr -->

<h1>...e o Debian-BR?</h1>
<font face="lucida" size="2">

Como todos n�s sabemos a coisa nunca foi nada f�cil aqui no Brasil, por isso 
o trabalho do Debian-BR se torna um pouco d�ficil at� mesmo por falta de 
aten��o de partes que deveriam ser mais interessadas no bem-estar (intelectual 
e de vida) do povo, tudo oqu� � feito no Projeto � totalmente volunt�rio como 
o pr�prio prop�sito do <strong>Debian</strong>, contamos com a ajuda de algumas
entidades como o <a href="http://www.cipsga.org.br">CIPSGA</a> que nos d�o 
muita for�a para seguir a caminhada rumo a uma distribui��o de 
<strong>GNU/Linux</strong> de alt�ssima qualidade como a <strong>Debian
</strong> para nossa l�ngua portuguesa. A <a 
href="suporte/documentacao.php">tradu��o </a> e o <a 
href="software.php">desenvolvimento</a> do <strong>Debian</strong>
no Brasil est�o em ritmo acelerado e o Projeto Debian-BR hoje comemora o feliz 
lan�amento do <strong>Debian GNU/Linux 3.0r0 "Woody" Debian-BR</strong> 
totalmente em Portugu�s do Brasil! Confira em 
<a href="software.php">Pegue o Debian</a>.
<br><br>
Ficamos muito agradecidos por sua visita e esperamos sua colabora��o!
</font>

<!-- end-dbr -->

<hr noshade>

<center>
<!-- webring -->
<script language=javascript src="http://www.quilombodigital.org/webring/script.php?id=24"></script>
</center>

<!-- end-index -->
<? include ('end.tpt') ?>
