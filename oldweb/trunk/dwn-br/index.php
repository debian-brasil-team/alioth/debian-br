<? include ('../page.tpt') ?>

<h1>Novidades</h1>
<hr noshade>
Atualmente o Debian-BR trabalha em tr�s newsletters
semanais:
<p>
A <a href="http://www.debian.org/News/weekly">Debian
Weekly News</a> � traduzida todas as semanas por
Henrique Pedroni Neto &lt;
<a href="mailto:henrique@ital.org.br">henrique@ital.org.br</a>&gt
e Michelle Ribeiro &lt;
<a href="mailto:michelle@focalinux.org">michelle@focalinux.org</a>&gt; 
e postada na lista <a 
href="http://lists.debian.org/debian-news-portuguese">debian-news-portuguese
</a>
al�m de aparecer como tradu��o oficial no site.

<p>

A Debian Weekly News Brasil � criada por Michelle Ribeiro
<a href="mailto:michelle@focalinux.org">michelle@focalinux.org</a>, 
e � tamb�m postada na lista <a 
href="http://lists.debian.org/debian-news-portuguese">debian-news-portuguese
</a>. Veja a vers�o online:
<ul>
<li><a href="2002/">2002</a></li>
</ul>

<p>

O <a href="../resumo-gnome">Resumo Gnome</a> � uma tradu��o
da newsletter semanal do Gnome, o Gnome Summary, feita por
Luis Alberto Garcia Cipriano &lt;
<a href="mailto:lacipriano@uol.com.br">lacipriano@uol.com.br</a>.
Ela � tamb�m postada na lista <a 
href="http://lists.debian.org/debian-news-portuguese">debian-news-portuguese
</a>.

<hr noshade>

<? include ("../end.tpt") ?>
