<? include ('../../../page.tpt') ?>

<h1>Debian Weekly News Brasil - 08 de Julho de 2002</h1>

<p>
Bem-vindo � quarta edi��o da DWN, a newsletter semanal para a comunidade Debian no Brasil. Estamos felizes em ver que nossas a��es est�o tomando grandes propor��es e que v�rios membros j� est�o recebendo o justo reconhecimento. 
<p>

<b>M�rio Teza � nomeado vice-presidente da Procergs.</b> No dia 04 de Julho de 2002, um dos grandes defensores do Software Livre tomou pose do cargo de vice-presidente da Procergs, o que muito alegra a comunidade Debian no Brasil, j� que M�rio muito tem nos apoiado. Foi junto com ele que os mantenedores brasileiros <i>Gustavo Noronha (kov)</i>, <i>Gleydson Mazioli</i> e <i>Henrique Holschuh</i> no in�cio do ano criaram a distribui��o Liberdade, baseada em Debian. Atrav�s dele tamb�m foi cedido o stand que utilizamos durante o III F�rum Internacional de Software Livre e v�rios outros trabalhos realizados em conjunto. S� temos muito que parabenizar, agradec�-lo a ajuda e continuar disponibilizando todo o apoio poss�vel.
<p>

<b>Almo�o de Miguel de Icaza com Grupo de Usu�rios Debian do RS.</b> Ap�s o lan�amento do Gnome 2 em Porto Alegre, o presidente da <a href="http://www.gnome.org/">Gnome Foundation</a> e CTO e co-fundador da <a href="http://www.ximian.com/">Ximian, Inc.</a>, almo�ou no dia 28 de Junho com Andr� Franciosi, Pablo Lorenzzoni, Ronaldo Lages e Marcelo Branco. Segundo Andr�, conversaram sobre os rumos do Gnome, que conta hoje com grande apoio da <a href="http://www.sun.com/">Sun</a> e como ajudar os usu�rios brasileiros, o que j� gerou alguns projetos no Sul utilizando o Gnome 2, iniciativas de Marcelo Branco. As fotos do encontro ser�o publicadas em breve pelo pr�prio Miguel em seu <a href="http://primates.ximian.com/~miguel/">site pessoal</a>. 
<p>

<b>Tradu��o dos Alertas de Seguran�a Debian</b>. <i>Michelle Ribeiro</i> anunciou nesta semana que passar� a ser respons�vel pela tradu��o e atualiza��o da <a href="http://www.debian.org.br/security/">p�gina de seguran�a</a> do Projeto Debian. Muito obrigado a <i>Philippe Gaspar</i>, que fazia este trabalho anteriormente e que tem auxiliado muito a comunidade. Michelle come�ou tamb�m o trabalho de tradu��o dos <a href="http://www.debian.org.br/security/2002/">Alertas de Seguran�a</a> que s�o enviados originalmente para a lista debian-security. A vers�o traduzida est� sendo enviada para a lista debian-news-portuguese e o <a href="http://lists.debian.org/debian-news-portuguese/2002/debian-news-portuguese-200207/msg00001.html">DSA sobre a vulnerabilidade da libapache-mod-ssl</a> foi o primeiro aviso a ser traduzido. 
<p>

<b>Campanha para motivar o uso do Debian Rau-Tu.</b> <i>Andr� L. Oliveira</i>, tamb�m conhecido como ph_, prop�s uma campanha para motivar o uso do <a href="http://rautu.cipsga.org.br/">Debian Rau-Tu</a>, sistema de perguntas e respostas sobre Debian, disponibilizado no <a href="http://www.cipsga.org.br/">CIPSGA</a>. Para participar da campanha, basta adicionar a seu arquivo de assinatura o seguinte texto: "D�vidas sobre Debian? Visite o Rau-Tu: http://rautu.cipsga.org.br".
<p>

<b>Artigos sobre Software Livre.</b> <i>Pablo Lorenzzoni</i> disponibilizou em seu 
<a href="http://people.debian.org/~spectra/files.html">site pessoal</a> uma s�rie de artigos sobre software livre. Muitos deles foram escritos pelo pr�prio Pablo e encontram-se em portugu�s.<p> 


<b>Vers�o do Debian para Novos Usu�rios.</b> <i>Alexandre Puhl</i> questiona qual vers�o � indicada para novos usu�rios e qual a diferen�a entre a woody e as demais. <i>Marcio Teixeira</i> ajuda a esclarecer, informando que cada release ganha um apelido, sendo a woody a vers�o em teste (testing) e a potato, a vers�o est�vel (stable). Como a woody j� se encontra bastante est�vel, conta agora com o suporte do Time de Seguran�a e ser� lan�ada em breve, novos usu�rios podem utiliz�-la tranq�ilamente. 
<p>

<b>Onde baixar imagens ISO do Debian. </b> Perguntas sobre onde baixar imagens de instala��o do Debian s�o freq�entes na lista debian-user-portuguese. Por isto, mais uma vez lembramos que voc� poder� obt�-las em <a href="http://www.debian.org.br/CD/">http://www.debian.org.br/CD/</a>. Vers�es m�nimas do Debian 3.0 (woody), tamb�m est�o dispon�veis. Se preferir, poder� utilizar a vers�o beta da woody com instala��o gr�fica, <a href="http://hackers.progeny.com/pgi">disponibilizada</a> pela Progeny. Uma outra op��o � o <a href="http://www.linorg.usp.br/">site do projeto Linorg</a>, da USP, que traz tamb�m imagens do LDP, OpenOffice e do kernel.
<p>

<b>Woody sem banda larga.</b> Muitos usu�rios n�o t�m como efetuar o download de uma ISO e por isso recorrem a lista para saber onde comprar ou se alguma revista j� trouxe o CD encartado. <i>Michelle Ribeiro</i> e <i>Otavio Salvador</i> j� ajudaram alguns, disponibilizando-se para gravar uma c�pia. Se voc� tamb�m pode faz�-lo, ajude-nos a disseminar o uso do Debian, acompanhando as solicita��es na lista debian-user-portuguese. 
<p>


<b>Status de pacotes.</b> Se assim como o <i>Rafael Jannone</i>, voc� tamb�m quer checar se determinado programa est� sendo empacotado ou se foi abandonado, poder� faz�-lo atrav�s da URL <a href="http://bugs.debian.org/wnpp">http://bugs.debian.org/wnpp</a>.
<p>

<b>Spam nas listas Debian.</b> <i>Adilson Junior</i> questionou se, diante dos spams constantes enviados para as listas Debian, estas n�o deveriam aceitar o envio apenas daqueles que est�o inscritos na mesma. O desenvolvedor <i>Carlos Laviola</i> esclarece que isto j� foi discutido v�rias vezes e que a melhor raz�o para que n�o funcione desta forma s�o os novos usu�rios que n�o sabem ou n�o querem inscrever para enviar apenas uma d�vida. 
<p>
<hr>

<? include ('../../../end.tpt') ?>
