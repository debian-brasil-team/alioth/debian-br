<? include ('../../../page.tpt') ?>
<h1>Debian Weekly News - 03 de Agosto de 2002</h1>

<i>Bem vindo(a) � sexta edi��o da DWN Brasil, a newsletter semanal para a comunidade Debian no Brasil.</i>
<p>

<b>Novos Tradutores da DWN.</b> Devido a sua falta de tempo, Gustavo Noronha da Silva, o kov, <a href="http://lists.debian.org/debian-l10n-portuguese/2002/debian-l10n-portuguese-200207/msg00286.html">esteve a procura</a> de um novo volunt�rio para a realiza��o dessa tarefa. Henrique Pedroni Neto, o kirkham, e Michelle Ribeiro passam a realiz�-la, atrav�s de um rod�zio em que cada semana um deles ser� o respons�vel pela tradu��o. Com o apoio do Time de Imprensa, passa a ser realizado uma pr�via revis�o na lista <a href="http://lists.debian.org/debian-l10n-portuguese/2002/debian-l10n-portuguese-200207/msg00286.html">debian-br-imprensa@listas.cipsga.org.br</a>, diminuindo assim a possibilidade de erros. 
<p>

<b>Nova Commiter para o www.debian.org.</b> Ap�s v�rias contribui��es para o <a href="http://debian-br.cipsga.org.br/projetos/webwml.php">projeto de Tradu��o das P�ginas Web do Debian</a>, Michelle Ribeiro foi <a href="http://lists.debian.org/debian-l10n-portuguese/2002/debian-l10n-portuguese-200207/msg00315.html">adicionada</a> ao grupo de commiter no CVS e agora poder� tamb�m auxiliar enviando as contribui��es de outros tradutores. A tradu��o da p�gina do Debian � mais um dos projetos importantes, j� que muitas vezes � a porta de entrada para novos usu�rios e desenvolvedores, que aguarda a doa��o de seu esfor�o. Para saber como colaborar, acesse a <a href="http://debian-br.cipsga.org.br/projetos/webwml.php">p�gina do projeto</a> no site do Debian-BR.
<p>

<b>O Servidor IBM para a Comunidade.</b> Cristiano Anderson anunciou na lista debian-rs
que a IBM est� disponibilizando maquinas virtuais Linux, rodando em plataforma S/390 (Mainframe) para desenvolvimento volunt�rio. Qualquer desenvolvedor interessado em portar/testar alguma aplica��o em Linux S390 poder� criar a pr�pria m�quina. O �nico problema � que ainda n�o h� m�quinas Debian (atualmente pode-se escolher entre SuSE, Red Hat e TurboLinux), mas segundo alguns contatos, isso est� sendo providenciado, afirma Cristiano, que est� participando de alguns testes, entre eles testes com kernel, m�dulos de QDIO (interface de rede utilizada no s390) e performance. Para quem quiser conhecer o projeto, basta acessar o <a href="
http://www-1.ibm.com/servers/eserver/zseries/os/linux/lcds/">site da IBM</a>.
<p>

<b>Pacotes do Projeto �gypten.</b> Renato Martini <a href="http://www.cipsga.org.br/article.php?sid=3507&mode=&order=0&thold=0">anunciou</a> no <a href="http://www.cipsga.org.br/">site do CIPSGA</a> que Marcus Brinkmann organizou <a href="ftp://ftp.gnupg.org/GnuPG/alpha/">pacotes</a> para o Debian da atual release do Projeto �gypten, que tem como objetivo garantir a seguran�a em correios eletr�nicos que usam como base os protocolos TeleTrust e.V. MailTrusT Version 2, inclui os padr�es S/ MIME, X.509v3, OpenPGP e outros. Os softwares s�o mantidos pelas empresas de software livre Intevation, g10 Code e Klar�lvdalens Datakonsult AB, que foram contratadas pela Ag�ncia federal alem� Bundesamt f�r Sicherheit in der Informationstechnik (BSI).
<p>


<b>Debate sobre a Experi�ncia de kov na ONU.</b> Ocorreu no �ltimo s�bado, �s 14h, no Audit�rio da Secret�ria de Educa��o, em Porto Alegre - RS, um debate com Gustavo Noronha da Silva sobre as experi�ncias vividas durante sua estadia em Genebra, em um evento da ONU, <a href="http://debian-br.cipsga.org.br/dwn-br/2002/05/">noticiado na edi��o anterior</a> da DWN-BR. 
<p>

<b>Origem do logotipo do Debian.</b> Claudio Ferreira Filho questionou sobre a origem do logo do Debian. O logo surgiu da realiza��o de um <a href="http://www.debian.org/News/1999/19990204">concurso</a> realizado em 1999, pois o projeto precisava de dois novos logotipos, um de uso livre (a espiral) e outro com uma licen�a mais restrita (a garrafa). A escolha foi <a href="http://www2.educ.umu.se/~bjorn/mhonarc-files/debian-announce/msg00134.html">anunciada</a> em 26/08/1999 na lista debian-announce com a vit�ria de <a href="http://www.silva.com">Ra�l M. Silva</a>, esclarece Victor Maida. Vale adicionar que Silva tamb�m � o criador do logo do <a href="http://www.maconlinux.org/">Projeto Mac on Linux</a>. <p>


<b>Atualizando Sistemas Muito Antigos.</b> Andr� Luiz Lins da Silva enviou-nos uma mensagem, procurando por usu�rios que j� tivessem passado pela experi�ncia de atualizar um sistema antigo, como o <a href="http://www.debian.org/distrib/archive">Debian GNU/Linux 1.3</a> para uma vers�o mais atual da distribui��o. Andr� vem utilizando essa m�quina como servidor de e-mail e gateway e foi instruido n�o realizar a atualiza��o, j� que in�meras configura��es foram alteradas desde aquela �poca e que, com certeza, o procedimento de atualiza��o n�o seria bem-sucedido, devendo realizar uma nova instala��o. 
<p>

<b>Voc� conhece o magpie?</b> Rildo Taveira de Oliveira deu uma �tima dica sobre o pacote <a href="http://packages.debian.org/stable/text/magpie.html">magpie</a>. Esse programa cria um �ndice com informa��es sobre todos os pacotes conhecidos pelo apt e pelo dpkg. Os pacotes est�o divididos e voc� pode navegar por grupos como "Se��o" e "Categoria". Uma das divis�es mais interessantes � a por Prioridade. Atrav�s dela, voc� poder� estudar quais pacotes s�o realmente necess�rios em seu sistema e realizar uma opera��o limpeza no mesmo. 
<p>

Viu mais not�cias? Por favor mantenha-nos informados! N�s estamos sempre buscando hist�rias interessantes, principalmente as escritas por volunt�rios para adicionar. N�s n�o vemos tudo, infelizmente. Esperamos seu e-mail em <a href="mailto:debian-br-imprensa@debian-br@cipsga.org.br">debian-br-imprensa@cipsga.org.br</a>.
<hr>
<? include ('../../../end.tpt') ?>
