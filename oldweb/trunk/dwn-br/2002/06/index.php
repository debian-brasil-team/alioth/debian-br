<? include ('../../../page.tpt') ?>
<h1>Debian Weekly News - 25 de Julho de 2002</h1>

<i>Bem vindo(a) � sexta edi��o da DWN Brasil, a newsletter semanal para a comunidade Debian no Brasil. Esta � uma edi��o muito especial para n�s pois, ap�s mais de um ano de espera, podemos comemorar o lan�amento da Debian Woody.</i>
<p>

<b>Debian GNU/Linux 3.0 lan�ado</b>. No �ltimo s�bado recebemos a t�o esperada <a href="http://lists.debian.org/debian-news-portuguese/2002/debian-news-portuguese-200207/msg00010.html">not�cia</a>. Feitas as devidas comemora��es, a discuss�o agora gira em torno do nome das releases. A distribui��o testing atual est� sendo chamada de sarge, mas quais nomes de personagens ser�o usados quando acabarem os do filme Toy Story? Os brasileiros j� t�m v�rias sugest�es, como Monsters Inc., filme que tamb�m foi criado pela Pixxar, South Park e Pokemon. Aproveitando este debate, Henrique Pedroni Neto, o kirkham, nos lembra de um <a href="http://www.revistadolinux.com.br/ed/013/debian.php3">artigo</a> escrito por Eduardo Ma�an para a <a href="http://www.revistadolinux.com.br/">Revista do Linux</a> onde o desenvolvedor comenta sobre aspectos pol�ticos e sociais do Projeto Debian e conta um pouco sobre a hist�ria dos codenomes das releases.
.<p>

<b>Onde comprar a Debian Woody em CD</b>. At� o momento, a <a href="http://www.linuxsecurity.com.br/">Linux Security</a> � a �nica loja online que tem o CD para venda. Lembramos, no entanto, que todos podem recorrer a lista <a href="http://lists.debian.org/debian-user-portuguese/">debian-user-portuguese</a> ou ao grupo de usu�rios de sua regi�o e e checar se algu�m pode gravar uma c�pia para voc�. Ao Renato Langona, nossos agradecimentos pela divulga��o do lan�amento do Debian GNU/Linux 3.0 na newsletter di�ria da Linux Security.
<p>
 
<b>Gnome 2 para Woody</b>. Gustavo Noronha, o kov, realizou o backport de todos os pacotes b�sicos do Gnome 2, do metatheme e do gkdial para a distribui��o Woody. Para aqueles que querem utilizar o Gnome, mas n�o pretendem usar a distribui��o unstable, basta agora apontar o apt para deb http://people.debian.org/~kov/debian woody gnome2. H� uma <a href="http://www.debianplanet.org/node.php?id=751">nota</a> sobre o assunto no site <a href="http://www.debianplanet.org">, com alguns screenshots e coment�rios.
<p>

<b>Novo commiter com acesso ao CVS de www.debian.org</b>. Carlos A P Gomes agora tem acesso ao CVS e <a href="http://lists.debian.org/debian-l10n-portuguese/2002/debian-l10n-portuguese-200207/msg00258.html">pode</a> atualizar wmls do site do debian diretamente. Atrav�s da <a href="http://lists.debian.org/debian-l10n-portuguese/">lista de localiza��o para o portugu�s</a> do Debian, ele agora � mais um commiter que recebe as atualiza��es dos volunt�rios e as publica, al�m dos esfor�os not�veis que ele pr�prio tem feito para manter o site traduzido. Para saber como colaborar, veja o <a href="http://debian-br.cipsga.org.br/projetos/webwml.php">guia</a> dispon�vel no site do Debian-BR.
<p>

<b>Debian-MG</b>. Foi <a href="http://lists.debian.org/debian-user-portuguese/2002/debian-user-portuguese-200207/msg00433.html">criada</a> no CIPSGA a lista de discuss�o Debian-MG para os usu�rios de Minas Gerais. Todos est�o convidados a inscrever-se e ajudar o kov a agitar por aquelas bandas. A inscri��o pode ser realizada no endere�o <a href="http://listas.cipsga.org.br/cgi-bin/mailman/listinfo/debian-mg">http://listas.cipsga.org.br/cgi-bin/mailman/listinfo/debian-mg</a>.
<p>

<b>Estrutura do Debian no Brasil</b>. Depois da cria��o dos documentos formalizando as regras de conduta no canal #debian-br e nas listas de discuss�o, est� em discuss�o agora a estrutura do Debian no Brasil. Quest�es como a exist�ncia de um grupo de usu�rios nacional, qual a rela��o entre os grupos de usu�rios locais, presen�a em eventos e etc, est�o sendo debatidas e sua participa��o � de muita import�ncia. Algumas propostas, como a <a href="http://listas.cipsga.org.br/pipermail/debian-br/2002-July/001204.html">Pablo</a> aguardam a aprova��o dos demais.
<p>

<b>Textos sobre software livre para as massas</b>. Elaine Silva, da empresa p�blica de TI de Campinas, a IMA, <a href="http://listas.cipsga.org.br/pipermail/debian-br/2002-July/001130.html">esteve</a> em busca de material sobre sofware livre em linguagem popular e para o p�blico leigo. <a href="http://listas.cipsga.org.br/pipermail/debian-br/2002-July/001135.html">kov</a> opinou que ainda n�o existe muito material com esse perfil pois a maioria sempre exige conhecimento pr�vio razo�vel, e que isso nos tem mantido longe do grande p�blico n�o especializado. No entanto, foram lembrados alguns textos e refer�ncias, por <a href="http://listas.cipsga.org.br/pipermail/debian-br/2002-July/001131.html">Gleydson</a>, <a href="http://listas.cipsga.org.br/pipermail/debian-br/2002-July/001171.html">kov</a> e <a href="http://listas.cipsga.org.br/pipermail/debian-br/2002-July/001139.html">lagc</a>.
<p>

<b>Eduardo Ma�an no Mato Grosso do Sul</b>. Em uma iniciativa da OAB-MS, Comiss�o Especial de Direito de Inform�tica e Internet-OAB/MS, Escola Superior de Advocacia-OAB/MS em parceria com a <a href="http://www.mileniuminformatica.com.br/">Milenium Inform�tica</a>, foi realizado no dia 20 de Julho um workshop sobre software livre no audit�rio da OAB-MS em Campo Grande. Durante o evento, foram discutidos os motivos que levaram empresas de destaque no estado a utilizarem o software livre em seus neg�cios. As <a href="http://www.mileniuminformatica.com.br/eventos/fotos/Fotos%20Workshop%20Software%20Livre-00.html">fotos</a> do evento j� est�o dispon�veis.
<p>

<b>Usu�rio Debian na O'Reilly Open Source Conference</b>. O engenheiro de software Leonardo Almeida far� um estudo de caso sobre a tecnologia Zope no evento que ocorre de 22 a 26 de Julho na Calif�rnia, EUA.
A apresenta��o ter� o t�tulo "From Oracle to ZODB" ("De Oracle para ZODB"), 
e descreve uma experi�ncia bem-sucedida de convers�o de um site que dependia 
de um banco de dados relacional Oracle para uma solu��o 100% orientada a objetos 
utilizando apenas o ZODB, banco de dados OO integrado ao Zope. Maiores informa��es no
<a href="http://conferences.oreillynet.com/os2002/">site oficial</a> da confer�ncia. 
<p>

<hr>
<? include ('../../../end.tpt') ?>
