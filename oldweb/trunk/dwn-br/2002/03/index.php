<? include ('../../../page.tpt') ?>

<h1>Debian Weekly News Brasil - 25 de Junho de 2002</h1>

<p>
<i>Bem-vindo(a) � terceira edi��o da Debian Week News Brasil, a newsletter semanal para a comunidade Debian no Brasil. Nesta edi��o, not�cias das duas �ltimas semanas (09 � 22 de Junho), que trazem importantes novidades. 
</i>

<p>
<b>Woody com instala��o gr�fica</b> - A <a href="http://www.progeny.com/">Progeny Linux Systems</a> disponibilizou para a comunidade uma vers�o beta da instala��o gr�fica da Woody, baseada no software da empresa, o <a href="http://hackers.progeny.com/pgi/">PGI (Progeny Graphic Installer)</a>. <i>Thadeu Penna</i> levou a not�cia at� a lista <a href="debian-user-portuguese@lists.debian.org">debian-user-portuguese@lists.debian.org</a> e um grupo demonstrou interesse em realizar a localiza��o, j� que pode ser um grande incentivo para um maior uso de Debian no Brasil. Como a instala��o n�o est� internacionalizada ainda e haver� a necessidade de faz�-lo, tudo isto est� sendo discutido na <a href="http://lists.debian.org/debian-devel-portuguese/2002/debian-devel-portuguese-200206/msg00010.html">debian-devel-portuguese@lists.debian.org</a>.  
<p>

<b>Problemas com o Apache</b> - O <a href="http://lists.debian.org/debian-security-announce/debian-security-announce-2002/msg00042.html">bug</a> encontrado no Apache, que deixa o servidor suscet�vel a ataques DoS, foi corrigido na vers�o 1.3.9-14 do pacote Apache do Debian e � altamente recomendado que voc� atualize para este caso esteja utilizando a Potato. Ainda n�o h� corre��es para a Woody, que ainda est� em processo de freeze, e para corrigir o problema em seu sistema, h� a op��o de instalar o pacote unstable do software. Lembramos que � recomendado sempre utilizar a distribui��o est�vel (que hoje � a Potato) em m�quinas de produ��o para que casos como este n�o comprometam os servi�os. 
<p>

<b>Pacotes Debian do Gnome 2</b>- J� est�o dispon�veis para download os pacotes da vers�o 2 do Gnome Desktop. Basta adicionar a seguinte linha � sua sources.list: <i>deb ftp://ftp.debian.org/debian ../project/experimental main</i>. Vale lembrar que estes s�o pacotes experimentais e que ser� necess�rio configurar o apt para utilizar pacotes da vers�o unstable. Para obter um sistema misto (testing + pacotes da unstable), basta checar como proceder no documento <a href="http://debian-br.cipsga.org.br/view.php?doc=apt-howto-pt_BR">Como usar o Apt</a>.
<p>

<b>Material sobre o Debian-BR</b> - Animados com o material sobre Debian criado pelo grupo europeu para uso em eventos como a LinuxTag, o grupo brasileiro quer <a href="http://listas.cipsga.org.br/pipermail/debian-br/2002-June/000956.html">criar seus pr�prios textos de divulga��o</a>, tendo como base o projeto no Brasil. Como sempre, colabora��es s�o muito bem-vindas, tanto para cria��o de novos textos, como para o design do material e podem ser encaminhadas para lista<a href="mailto:debian-br@listas.cipsga.org.br">debian-br@listas.cipsga.org.br</a>. Assim como no caso dos folders traduzidos, estamos em busca de empresas que usam Debian e queiram apoiar a impress�o destes materiais. 
<p>

<b>Documenta��o feita pelo Projeto Debian-BR torna-se refer�ncia</b> - O documento <a href="http://debian-br.cipsga.org.br/view.php?doc=apt-howto-pt_BR">Como Usar o APT</a>, criado pelo Projeto Debian-BR, vem se tornando refer�ncia dentro do Projeto Debian. Depois de ser traduzido para o ingl�s, por Steven Langasek, o guia j� foi traduzido em mais seis idiomas, incluindo japon�s e coreano. Existe ainda a possibilidade de ele ser <a href="http://lists.debian.org/debian-devel/2002/debian-devel-200206/msg00759.html">citado</a> no an�ncio de lan�amento do Woody (Debian 3.0). Isso mostra que os brasileiros devem confiar no seu potencial e, mais do que traduzir, produzir conhecimento, se tornando uma grande for�a no mundo do software livre.
<p>

<b>Palestra virtual com o maintainer Pablo Lorenzzoni (spectra) </b>- A convite da <a href="http://www.ufla.br/">Universidade Federal de Lavras</a>, o desenvolvedor ministrou nos dias 11 e 12 palestras virtuais via IRC sobre <i>Culturas Hacker</i>. Se voc� n�o pode acompanhar, ainda poder� ler o bel�ssimo <a href="http://people.debian.org/~spectra/files/culturas_hacker.txt">texto</a> que foi criado por Pablo como base para a apresenta��o e que, entre outros assuntos, fala sobre a import�ncia da cultura hacker para o software livre e a diferen�a entre hackers e crackers.
<p>

<b>Eduardo Ma�an realiza palestra sobre Debian em Cerquilho</b> - O evento organizado por <i>Alex Laner</i> teve in�cio com a palestra do tamb�m colaborador da Revista do Linux, <i>PiterPunk</i>, sobre o uso de GNU/Linux no mundo real e contou com a presen�a de 30 pessoas. Alex diz que a apresenta��o do Debian feita por Ma�an foi vital para o esclarecimento daqueles que estavam presentes: <i>"Acho que foi muito produtiva pois algumas pessoas pensavam que s� existia Conectiva. Acabaram sabendo que h� um pouco mais."</i> Fotos das palestras ser�o disponibilizadas em breve, mas Alex j� disponibilizou algumas <a href="http://www.rootshell.be/~rootsh/gulc/domingo/">fotos</a> do passeio p�s evento que o grupo fez. 
<p>

Viu mais not�cias? Por favor mantenha-nos informados! N�s estamos sempre buscando hist�rias interessantes, principalmente as escritas por volunt�rios para adicionar. N�s n�o vemos tudo, infelizmente. Esperamos seu e-mail em <a href="mailto:imprensa.debian-br@cipsga.org.br">imprensa.debian-br@cipsga.org.br</a>.

<hr>

<? include ('../../../end.tpt') ?>
