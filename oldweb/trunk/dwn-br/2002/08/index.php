<? include ('../../../page.tpt') ?>

<h1>Debian Weekly News - 16 de Agosto de 2002</h1>

<p><i>Bem-vindo � oitava edi��o da DWN-BR, a newsletter semanal para a comunidade Debian no Brasil. Depois de duas semanas sem newsletter, n�o poder�amos deixar que mais uma passasse, principalmente essa, j� que ter�a-feira <a href="http://listas.cipsga.org.br/pipermail/debian-br/2002-August/001331.html">comemoramos</a> 2 anos da cria��o do canal #debian-br, que � uma das pe�as chaves para o projeto Debian no Brasil. Junte os amigos, fa�a a festa e ajude-nos cada vez mais. Se voc� ainda n�o sabe o que comemorar, n�s temos uma <a href="http://debian-br.cipsga.org.br/oquecomemorar.html">pequena lista para voc�</a>.
</i>
<p>

<b>Novo pacote brasileiro - abntex</b>. Atendendo ao <a href="http://lists.debian.org/debian-devel-portuguese/2002/debian-devel-portuguese-200207/msg00002.html">pedido</a> do nosso amigo Daniel Martins por um pacote do <a href="http://codigolivre.org.br/projects/abntex/">abnTeX</a>, norma ABNT para LaTeX, Otavio Salvador <A href="http://lists.debian.org/debian-devel-portuguese/2002/debian-devel-portuguese-200207/msg00004.html">criou</a> e j� disponilizou o mesmo para testes. Para instal�-lo, adicione a linha "deb http://www.freedom.ind.br/otavio debian woody" (sem aspas) ao seu arquivo /etc/apt/sources.list. Otavio avisa que essa � a vers�o 0.5-0+. 0+ deve-se ao fato de haver muita coisa a ser melhorada, como a inclus�o de uma descri��o e da licen�a. Vale ressaltar que esse ainda n�o � um pacote oficial da distribui��o Debian, mas que Otavio j� fez o ITP.
<p>

<b>Teste de pacotes da libgcode.</b> Goedson Teixeira Paix�o <a href="http://lists.debian.org/debian-devel-portuguese/2002/debian-devel-portuguese-200208/msg00002.html">solicita</a> que os usu�rios testem as <a href="http://www.dcc.ufmg.br/~gopaixao/debian/packages/">vers�es preliminares</a> dos pacotes da <a href="http://gcode.sourceforge.net/">libgcode</a> nos quais ele est� trabalhando. Quem estiver disposto a fazer os testes, deve instalar o pacote gcode-demo (uma vers�o gcode do gtk-demo) e execut�-lo. Goedson aguarda cr�ticas sobre o empacotamento. 
<p>

<b>Sistema Livre para Gerenciamento de PABX.</b> Tiago de Lima Bueno <a href="http://lists.debian.org/debian-user-portuguese/2002/debian-user-portuguese-200208/msg00217.html">solicitou</a> uma indica��o de software livre para gerenciamento de PABX, j� que o programa que utiliza atualmente � propriet�rio e n�o tem suprido todas as necessidades da empresa. Como outros usu�rios apresentaram a mesma reclama��o, iniciou-se um pequeno projeto para desenvolver um novo sistema livre. Uma op��o que j� existe � o <a href="http://pblogan.sourceforge.net/">PBLogAn</a>.
<p>

<b>Tradu��o do tutorial do Jigdo</b>. Elcio Mello <a href="http://lists.debian.org/debian-l10n-portuguese/2002/debian-l10n-portuguese-200208/msg00020.html">entrou</a> em contato com Peter Jay Salzman, criador do mini-tutorial sobre o Debian Jigdo que foi recentemente adicionado ao 
<a href="http://www.tldp.org/">Projeto de Documenta��o do Linux</a> e realizou a tradu��o do documento, que estar� dispon�vel em breve. Como informado na DWN de 30 de Julho, este tutorial descreve por que voc� deve usar o jigdo, adiciona uma pequena explana��o sobre como este trabalha e como ele � usado para pegar e atualizar as imagens iso do Debian. O original em ingl�s j� pode ser <a href="http://tldp.org/HOWTO/mini/Debian-Jigdo/">acessado.</a>
<p>


<b>Como Traduzir Man Pages</b>. Depois de um per�odo de f�rias, Paulo Ormenese retornou a lista debian-l10n-portuguese e <a href="http://lists.debian.org/debian-l10n-portuguese/2002/debian-l10n-portuguese-200208/msg00035.html">questiona</a> qual o procedimento para a tradu��o de p�ginas de manuais, uma vez que est� disposto a traduzir as p�ginas do dpkg e do apt. Kov <a href="http://lists.debian.org/debian-l10n-portuguese/2002/debian-l10n-portuguese-200208/msg00039.html">esclarece</a> que basta traduzir o arquivo sgml ou tex. Caso nenhum desses arquivos esteja presente no pacote, traduza a pr�pria man page e envie ao autor, com c�pia para a lista. 
<p>

<b>Que pacote Debian voc� �?</b> Leandro Ferreira <a href="http://lists.debian.org/debian-user-portuguese/2002/debian-user-portuguese-200208/msg00449.html">enviou-nos</a> o link para uma <a href="http://pigdog.org/features/dpn.html">p�gina</a> muito divertida que "debianiza" seu nome/nick. Os resultados s�o algo como "acidx-i18n", "xfonts-biznet-claviola", "gleydson-common" e "macan-curses".
<p>


<b>Cria��o das listas de debian-ce e debian-df</b>. Gleydson Mazioli <a href="http://listas.cipsga.org.br/pipermail/debian-br/2002-August/001302.html">anunciou</a> a cria��o das listas de discuss�es para os grupos locais do Cear� e Distrito Federal. Se voc� vive em uma dessas regi�es, j� pode <a href="http://listas.cipsga.org.br/pipermail/debian-br/2002-August/001302.html">
inscrever-se</a> e ajudar os demais usu�rios a promover o Debian em seu Estado.
<p>

<b>Debate sobre Cultura Hacker.</b> Aconteceu ontem, 15 de Agosto, no audit�rio da UERGS, em Porto Alegre, o primeiro debate sobre Cultura  Hacker, apresentado pelos desenvolvedores Debian Pablo Lorenzzoni e Gustavo Noronha, o Kov. A palestra, foi aberta por Pablo Lorenzzoni que falou sobre a hist�ria dos hackers, arquiteturas que se trabalhavam na �poca do surgimento, sua cultura, etc. Gustavo Noronha Silva, mais conhecido na comunidade como Kov, deu seguimento ao evento, falando sobre o contrato social da Debian e como essa institui��o se coloca em rela��o �s licen�as existentes de software no mercado. Foi explicado tamb�m como funciona o sistema de vota��o da Debian, assim como s�o feitas as tomadas de decis�es. Fotos do evento est�o <a href="http://debian-br.cipsga.org.br/comunidade/fotos/cultura-hacker-uergs/">dispon�veis</a> no site do Debian-BR.
<p>

<b>Agenda de Palestras com Desenvolvedores Debian:</b>
<ul>
<li>
<b>Desenvolvimento em Debian - Kov</b><br>
19/08, 19h30<br>
Faculdade Ritter dos Reis<br>
R. Orfanotr�fio, 555<br>
Porto Alegre - RS

<li>
<b>Contrato Social do Debian - Kov</b><br>
23/08, 20hs, na 2a. Semana do Ping�im<br>
Univates<br>
Tel.: 0800-7070809

<li>
<b>Infra Estrutura do Debian - Henrique (hmh)</b><br>
29/08, 19h30<br>
Faculdade Ritter dos Reis<br>
R. Orfanotr�fio, 555<br>
Porto Alegre - RS
</ul>
<hr>

<? include ('../../../end.tpt') ?>
