<? include ('../../../page.tpt') ?>
<h1>Debian Weekly News Brasil - 16 de Julho de 2002</h1>

<i>Bem vindo(a) � quinta edi��o da DWN Brasil, a newsletter semanal para a comunidade Debian no Brasil. Nessa semana come�aram as discuss�es sobre os rumos do projeto no Brasil e a participa��o de todos � muito importante para que a democracia esteja sempre presente.</i>
<p>

<b>Regras de Conduta para as Listas e o Canal #debian-br.</b> Gustavo Noronha, colocou em discuss�o na lista do <a href="http://listas.cipsga.org.br/cgi-bin/mailman/listinfo/debian-br">Debian-BR</a> documentos com regras de conduta para as <a href="http://listas.cipsga.org.br/pipermail/debian-br/2002-July/001011.html">listas</a> e o canal <A href="http://listas.cipsga.org.br/pipermail/debian-br/2002-July/001013.html">#debian-br</a>. V�rios pontos foram debatidos, foram sugeridas diversas altera��es e levantadas muitas quest�es. Considerou-se que esses documentos ser�o �teis para proteger as listas e o canal quando for necess�rio impedir que o mau uso de alguns prejudique a comunidade e os objetivos do projeto. Com as altera��es aprovadas nas discuss�es, chegou-se �s primeiras vers�es dos documentos, que j� est�o publicados no site do Debian-BR (<a href="http://debian-br.cipsga.org.br/view.php?doc=lista">listas</a> e <a href="http://debian-br.cipsga.org.br/view.php?doc=canal">canal</a>). N�o deixe de enviar sugest�es para melhoraria dos documentos. Tamb�m surgiram outros temas, como definir quem � membro do Debian-BR, debian.org.br deixar de ser um mirror de debian.org, vota��o para aprova��o de propostas, cria��o de novas listas, etc e est�o sendo abertas threads apropriadas para discut�-los. N�o deixe de assinar a lista e participar das discuss�es.
<p>

<b>kov - um jovem hacker na ONU.</b> Gustavo Noronha Silva, o kov, se aventurou por terras estrangeiras a convite da Organiza��o N�o-Governamental canadense <a href="http://www.takingitglobal.org">TakingITGlobal</a>. Ele foi um dos jovens do mundo escolhidos para representar a ONG no primeiro comit� preparat�rio do <a href="http://www.itu.int/wsis/">World Summit on the Information Society</a>. O convite foi feito atrav�s do pessoal do <a href="http://www.juventudefsm.org.br">acampamento da juventude do FSM</a>. O <a href="http://www.softwarelivre.rs.gov.br">Projeto Software Livre RS</a> publicou o <a href="http://www.softwarelivre.rs.gov.br/index.php?menu=mais_noticias2&cod=1025813011&tab=1">di�rio</a> escrito por ele na Su��a e h� tamb�m algumas <a
href="http://www.picturetrail.com/angel_on_broomstick">fotos</a>.

<p>

<b>Debian Install Fest no Rio de Janeiro.</b> Todo m�s � realizado pelo <a href="http://www.cipsga.org.br/">CIPSGA</a>, na cidade do Rio de Janeiro um Install Fest no qual � utilizado a distribui��o Debian. Desde sua primeira edi��o, o mesmo ocorre no domingo seguinte ao curso de instala��o do sistema operacional, tamb�m organizado pelo CIPSGA. Assim, os alunos e demais interessados podem levar suas m�quinas para que seja instalado o Debian. A quinta edi��o ocorreu no domingo passado e foi um sucesso, como as anteriores, segundo o Djalma Valois, presidente do comit�. Para saber as datas dos pr�ximos Install Fests, basta acompanhar o <a href="http://www.cipsga.org.br/sections.php?op=viewarticle&artid=414">calend�rios dos treinamentos</a>. 
<p>


<b>Novos volunt�rios para ajudar no Debian Rau-Tu. </b> Depois de v�rios meses como colaborador em destaque, Henrique Pedroni Neto, o kirkham, <a href="http://listas.cipsga.org.br/pipermail/debian-br/2002-July/001014.html">passa</a> a administrar o <a href="http://rautu.cipsga.org.br/read.php?tid=1">t�pico Instala��o</a>, do <a href="http://rautu.cipsga.org.br/">Debian Rau-Tu</a>, sistema de perguntas e respostas sobre a distribui��o Debian. H� v�rios outros t�picos que precisam de novos volunt�rios. Interessados em colaborar devem enviar uma mensagem para a lista <a href="http://listas.cipsga.org.br/cgi-bin/mailman/listinfo/debian-br">do Projeto Debian-BR.
<p>


<b>Atualiza��o do gerenciador de listas Mailman do CIPSGA.</b> Depois de internacionalizar as vers�es 2.0 e 2.1 do gerenciador de listas de discuss�o Mailman, Gleydson Mazioli <a href="http://listas.cipsga.org.br/pipermail/debian-br/2002-July/001006.html">atualizou</a> e migrou a base de dados de listas do servidor do <a href="http://www.cipsga.org.br/">CIPSGA</a> (Comit� de Incentivo � Produ��o de Software GNU e Alternativo). As mensagens enviadas pelo programa (como por exemplo, no ato do cadastramento) e a interface de gerenciamento encontram-se totalmente em portugu�s, o que facilitar� o acesso de um maior n�mero de usu�rios. Lembramos que nesse <a href="http://www.cipsga.org.br/">servidor</a> est�o hospedadas as listas de discuss�o dos grupos de usu�rios Debian nacional e dos demais grupos regionais. 
<p>


<b>Sistema doc-agent.</b> Gleydson Mazioli <a href="http://lists.debian.org/debian-l10n-portuguese/2002/debian-l10n-portuguese-200207/msg00080.html">corrigiu</a> os erros que existiam no doc-agente e o sistema voltou a notificar os tradutores sobre atualiza��es e erros em documentos. Dessa forma, os volunt�rios saber�o o que foi modificado e poder�o realizar rapidamente as corre��es. No entanto, como o sistema tamb�m notifica a lista <a href="http://lists.debian.org/debian-l10n-portuguese/">debian-l10n-portuguese@lists.debian.org</a> sobre atualiza��es e erros cometidos pelos tradutores e esses t�m trabalho constamente, o tr�fego da lista aumentou bastante e tem gerado algumas reclama��es e solicita��es de mudan�a. Gleydson <a href="http://lists.debian.org/debian-l10n-portuguese/2002/debian-l10n-portuguese-200207/msg00231.html">promete</a> melhorar o sistema e diminuir o n�mero de mensagens enviadas.
<p>

<b>Vocabul�rio Padr�o.</b> Devido a constantes quest�es sobre tradu��o de algumas palavras, realizadas na lista <a href="http://lists.debian.org/debian-l10n-portuguese/">debian-l10n-portuguese@lists.debian.org</a>, Luis Alberto Garcia Cipriano, o lagc, <a href="http://lists.debian.org/debian-l10n-portuguese/2002/debian-l10n-portuguese-200207/msg00122.html">levanta</a> a possibilidade de adotarmos o <a href="http://br.tldp.org/ferramentas/vp/vpinfo.html">Vocabul�rio Padr�o</a>, do <a href="http://br.tldp.org/">projeto LDP-BR</a>, w caso o adotemos, sugere a separa��o dos termos gerais dos espec�ficos do Debian para facilitar a discuss�o das melhores tradu��es. O uso de um vocabul�rio comum pode facilitar o trabalho dos colaboradores e
tornar o resultado final mais consistente.<p>

<b>Sylpheed-claws ainda mais r�pido. </b> Gustavo Noronha, o kov, <a href="http://lists.debian.org/debian-user-portuguese/2002/debian-user-portuguese-200207/msg00299.html">anunciou</a> que os autores do Sylpheed-Claws prometem uma melhora significativa na performance do programa de correio eletr�nico. A pr�xima vers�o provavelmente ter� cache de messages, que possibilitar� uma 
troca mais r�pida entre pastas.  O pacote Debian do sylpheed-claws � mantido por kov e tem muitos usu�rios na comunidade Debian do Brasil. 
<p>

<b>Mirrors mais r�pidos para serem usados com o apt.</b> Quer atualizar mais r�pido seu sistema Debian? Use o <a href="http://packages.debian.org/testing/admin/apt-spy.html">apt-spy</a>. Em resposta a Eduardo Fidelis, que <a href="http://lists.debian.org/debian-user-portuguese/2002/debian-user-portuguese-200207/msg00118.html">solicitou-nos</a> uma lista de bons mirrors para serem usados em seu /etc/apt/sources.list, foi recomendado o uso do apt-spy, programa que cria esse arquivo a partir das melhores respostas ao teste realizado com os atuais mirrors do Debian. 
<p>

<b>D�vidas sobre instala��o do Debian em outras arquiteturas</b>. T�m sido mais freq�entes as quest�es sobre o uso de Debian em outras arquiteturas. Nessa semana, Rodrigo Resende e Renato L. Sousa, da Unesp, relataram suas <a href="http://lists.debian.org/debian-user-portuguese/2002/debian-user-portuguese-200207/msg00238.html">d�vidas</a> de instala��o em uma m�quina IBM PowerPC. Se voc� tem alguma experi�ncia com outras arquiteturas e gostaria de relatar suas experi�ncias, junte-se a lista <a href="http://lists.debian.org/debian-user-portuguese/">http://lists.debian.org/debian-user-portuguese/</a>.
<p>

<b>Novas tradu��es do Manual de Instala��o no site do Debian-BR.</b> Aproveitando o aumento de quest�es como as descritas acima, Gleydson Mazioli <a href="http://lists.debian.org/debian-l10n-portuguese/2002/debian-l10n-portuguese-200207/msg00112.html">adicionou</a> ao <a href="http://debian-br.cipsga.org.br/">site do projeto Debian-BR</a> mais algumas tradu��es do Manual de Instala��o da Debian para outras arquiteturas, entre elas, <a href="http://debian-br.cipsga.org.br/view.php?doc=debian-install-s390">s390</a>, da IBM e <a href="http://debian-br.cipsga.org.br/view.php?doc=debian-install-ia64">ia64</a>, arquitetura de 64Bits Intel. 
<p>

<b>Hardware compat�vel com Debian.</b> Ainda sobre o Manual de Instala��o, que � um documento de leitura obrigat�ria: quer saber se sua placa de som funcionar� se migrar para o Debian? Consulte em <a href="http://www.debian.org/releases/stable/i386/ch-hardware-req.pt.html#s-hardwar">http://www.debian.org/releases/stable/i386/ch-hardware-req.pt.html#s-hardwar</a>. Lembramos, mais uma vez, que h� manuais para outras arquiteturas.<p>
<hr>
<? include ('../../../end.tpt') ?>