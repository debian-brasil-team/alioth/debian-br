<? include ('../../../page.tpt') ?>

<h1>Debian Weekly News Brasil - 04 de Junho de 2002</h1>

<p>
<i>Bem-vindo � primeira edi��o da DWN Brasil, a newsletter semanal para a comunidade Debian no Brasil, que
cresce cada vez mais. Exigente e ativa, novos projetos s�o gerados por ela e a DWN Brasil � um deles. </i>
<p>

<b>Cria��o da lista debian-br@listas.cipsga.org.br.</b> Com o n�mero de colaboradores e grupos de usu�rios
regionais aumentando, a press�o para que se crie uma estrutura que possa evitar segmenta��o, 
difundir melhor as informa��es e organizar melhor as a��es de n�vel local ou nacional aumentou. 
Por isso est� sendo criada a lista debian-br@listas.cipsga.org.br, com o prop�sito de organizar
as discuss�es sobre o grupo de usu�rios e projeto Debian-BR. � importante ressaltar que a lista
de suporte a usu�rios, onde as d�vidas t�cnicas e de organiza��o do sistema Debian continua sendo
a debian-user-portuguese@lists.debian.org.br. Para se inscrever na nova lista visite: 
<a href="http://listas.cispga.org.br">http://listas.cispga.org.br</a>.
<p>


<b>Precisa-se de volunt�rios para o projeto DDTP.</b> O <a href="http://debian-br.cipsga.org.br/projetos/ddtp.html">DDTP</a>
(ou Debian Description Translation Project) � um projeto internacional para traduzir as descri��es
dos pacotes Debian. Conforme <i>Luis Alberto Garcia Cipriano</i>. coordenador do projeto, quase 50% do objetivo j� foi alcan�ado,
mas � preciso que mais pessoas se envolvam, para que possamos voltar nossos esfor�os para a implementa��o de um
processo de qualidade. Para se juntar ao grupo, basta seguir as instru��es contidas no <a href="http://ddtp.debian.org/~grisu/ddts/guides/guide-pt_BR.txt">
Guia do Tradutor do DDTP</a>
<p>


<b>Tradu��o das p�ginas web do Debian.</b> Com a coordena��o de <i>Philipe Gaspar</i> e <i>Gustavo Noronha (kov)</i>,
os tradutores t�m trabalhado em ritmo acelerado e n�o poder�amos deixar de parabeniz�-los por isso! Vale lembrar que, 
assim como nos demais esfor�os de localiza��o, novos volunt�rios s�o muito bem-vindos e que para juntar-se ao grupo 
basta seguir os procedimentos descritos na <a href="http://debian-br.cipsga.org.br/projetos/webwml.html">p�gina do projeto</a>.
Recomendamos tamb�m a leitura da <a href="http://lists.debian.org/debian-l10n-portuguese/2002/debian-l10n-portuguese-200205/msg00074.html">
mensagem</a> enviada pelo <i>kov</i> durante esta semana, na qual ele explica detalhadamente os m�todos para quem quer ajudar sem causar
problemas de duplica��o de esfor�os.


<b>Documentos que solucionam d�vidas b�sicas.</b> Apesar do grande n�mero de documentos em portugu�s
disponibilizados na p�gina do <i>Projeto Debian-BR</i>, ainda existem d�vidas b�sicas como por exemplo,
sobre o uso do apt ou permiss�es de arquivos aparecendo na lista. A voc� que est� come�ando agora com Debian,
indicamos a visita ao <a href="http://debian-br.cipsga.org.br/">site do Projeto</a> e a leitura dos
seguintes artigos, que s�o essenciais para uma boa base de conhecimentos:
<p>
<a href="http://debian-br.cipsga.org.br/suporte/faq.html">Debian FAQ</a>, que foi atualizado na �ltima semana.<br>
<a href="http://debian-br.cipsga.org.br/view.php?doc=pratico">Guia Pr�tico para o Debian GNU/Linux</a><br>
<a href="http://debian-br.cipsga.org.br/view.php?doc=apt-howto-pt_BR">Como usar o APT</a><br>
<a href="http://debian-br.cipsga.org.br/view.php?doc=dselect-beginner">Tutorial sobre o dselect</a><br>
<a href="http://debian-br.cipsga.org.br/view.php?doc=kdebian">Administrando Kernel no Debian</a><br>
<a href="http://debian-br.cipsga.org.br/view.php?doc=debian-history">Hist�ria do Projeto Debian</a><br>
<p>

<b>OpenOffice.org 1.0 em portugu�s empacotado.</b> Cansado dos problemas de acentua��o no StarOffice e da
pol�tica comercial da Sun? N�s temos a solu��o para seus problemas: J� est�o dispon�veis o pacote deb
da vers�o 1.0 do OpenOffice e pacotes de internacionaliza��o separados do pacote bin�rio principal. Basta adicionar 
as linhas informadas em <a href="http://www.linux-debian.de/openoffice/">http://www.linux-debian.de/openoffice/</a>
e executar o comando <i># apt-get install openoffice.org openoffice.org-l10n-pt</i>. Nossos parab�ns a equipe coordenada
por <i>Claudio Ferreira Filho</i>, que est� trabalhando na localiza��o do Open Office. Para aqueles que estiverem dispostos a ajudar, 
maiores informa��es podem ser encontradas em <a href="www.narede.hpg.com.br/oo_trad.html">www.narede.hpg.com.br/oo_trad.html</a>.
<p>


<b>Fotos do III F�rum Internacional de Software Livre.</b> <i>Gustavo Noronha da Silva (kov)</i>, disponibilizou no site do
Projeto Debian-BR as <a href="http://debian-br.cipsga.org.br/comunidade/forum2002.html">fotos</a>
tiradas por v�rios integrantes da comunidade que estiveram presente no <a href="http://www.softwarelivre.rs.gov.br/forum/">
III FISL</a>, realizado em Porto Alegre, nos dias 02, 03 e 04 de Maio. Destaque para as 
<a href="http://debian-br.cipsga.org.br/comunidade/fotos/forum2002-maddog/">fotos</a>
retiradas por <i>John "Maddog" Hal</i>, que foi t�o bem recebido pelos integrantes do grupo de usu�rios 
Debian do Rio Grande do Sul.
<p>


<b>Palestra com Eduardo Ma�an.</b> Nosso desenvolvedor-embaixador far� uma palestra em Cerquilho, interior
de S�o Paulo, a convite de <i>Alex Laner</i>. A palestra, que tem como objetivo apresentar a Debian ao <i>GULC - 
Grupo de Usu�rios Linux de Cerquilho</i>, ser� realizada no dia 15 de Junho, �s 17h. Maiores informa��es e 
o cadastramento no site <a href="http://www.terlizzi.com.br/gulc/">http://www.terlizzi.com.br/gulc/</a>
<p>



Viu mais not�cias? Por favor mantenha-nos informados! N�s estamos
sempre buscando hist�rias interessantes, principalmente as escritas
por volunt�rios para adicionar. N�s n�o vemos tudo, infelizmente.
Esperamos seu email em <a 
href="mailto:imprensa.debian-br@cipsga.org.br">
imprensa.debian-br@cipsga.org.br</a>.
<p>

<hr>
Michelle Ribeiro<br>

<? include ('../../../spool/end.tpt') ?>
