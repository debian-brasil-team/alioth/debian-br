<? include ("../page.tpt") ?>

<? include ("secao.tpt") ?>

<b>Projeto:</b> Tradu��o das P�ginas de Manual do Debian<br>
<b>Coordenador:</b> 
Philipe Gaspar
<a href="mailto:philipegaspar@terra.com.br">philipegaspar@terra.com.br</a><br>
<b>Descri��o:</b><br>
<p>
Este projeto visa a tradu��o de p�ginas de manual de pacptes espec�ficos do
Debian, tais como debconf, fakeroot, dpkg e kernel-package. Pois j� existe na
ldp-br um <a href="http://br.tldp.org/projetos/man/man.html">projeto de 
tradu��o de p�ginas de manual</a> mais comuns.
<p>
O sistema de coordena��o das tradu��es � bastante parecido com o de p�ginas 
wml, exceto que n�o podemos modificar as p�ginas originais para adicionar o
n�mero de revis�o do CVS. Ent�o os arquivos INFOS foram criados, estes 
aqruivos cont�m informa��es que devem atualizadas. Os arquivo INFOS t�m
os seguintes campos:
<p>Manpage:   nome do documento no reposit�rio CVS, talvez sejam diferentes daqueles
que est�o no pacote fonte; este � o ID do documento.
<p>Encoding:   codifica��o do documento
<p>Location:   localiza��o do documento no pacote fonte (somente utiliza-o 
quando o pacote fonte estiver o documento).
<p>Original:   ID original no diret�rio english/.
<p>Original-CVS-Revision:   n�mero de revis�o do documento original no qual esta
tradu��o � baseada.
<p>Translator:   nome do tradutor, ser� usado para enviar mensagens autom�ticas
quando uma p�gina de manual traduzidas est� desatualizada.

<p>

<b>Ajudando:</b>
<p>
Se quiser ajudar, comece baixando o arquivo abaixo:

<p>
<ul>
<li> <a href="man/man-cvs">man-cvs</a>
</ul>

<p>
Voc� deve come�ar tornando o <i>man-cvs</i> execut�vel
e ent�o rod�-lo enquanto estiver conectado � Internet
da seguinte forma:
<p>
<pre>
$ ./man-cvs init
</pre>
<p>
Se ele mostrar erros quanto a um arquivo <i>~/.cvspass</i>
simplesmente execute:
<p>
<pre>
$ touch ~/.cvspass
</pre>
<p>
E rode ele novamente. Ele vai pedir senha algumas vezes, mas 
o acesso que voc� est� fazendo � an�nimo, portanto basta teclar
enter quando ela for requisitada.
<p>
O script vai ent�o criar o ambiente de produ��o. Vai criar um
diret�rio <i>manpages</i> e dentro dele v�rios outros diret�rios, que 
ser�o os nomes do pacote fonte. 
<p>
Agora voc� est� pronto para come�ar a trabalhar. Sempre antes
de mudar alguma coisa no cvs rode o script <i>man-cvs</i>,
agora sem o par�metro 'init' e sempre avise � lista
<a href="mailto:debian-l10n-portuguese@lists.debian.org">
debian-l10n-portuguese@lists.debian.org</a> no que vai trabalhar. 
<p>

Quando acabar uma tradu��o ou atualiza��o, envie a p�gina de manual
anexado para a lista <a href="mailto:debian-l10n-portuguese@lists.debian.org">
debian-l10n-portuguese@lists.debian.org</a> com '[manpage]' no assunto.
O '[manpage]' � simplesmente para facilitar a administra��o, n�o
esque�a de us�-lo. Lembre-se de deixar o campo Location em branco, pois o 
documento ainda n�o foi adicionado ao pacote fonte. Para fazer isso voc� 
tamb�m precisa mandar para o BTS, para que o mantenedor do pacote adicione sua
p�gina de manual traduzida ao pacote. 
<p>
Lembre-se alguns termos nas p�ginas de manual s�o padronizados e devem ser
traduzidos da seguinte forma:

NAME - NOME
SYNOPSIS - RESUMO
DESCRIPTION - DESCRI��O
OPTIONS - OP��ES
ENVIRONMENT - AMBIENTE
AUTHOR - AUTOR
CAVEATS - PROBLEMAS
FILES - ARQUIVOS

<p>
Qualquer d�vida entre em contato com a mesma lista ou com o
coordenador (a lista � sempre prefer�vel).

 <? include ("../end.tpt") ?>
