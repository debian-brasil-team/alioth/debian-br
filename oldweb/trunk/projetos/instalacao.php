<? include ("../page.tpt") ?>

<? include ("secao.tpt") ?>

<b>Projeto:</b> Tradu��o da Instala��o do Debian<br>
<b>Coordenadores:</b> 
Gleydson Maziolli da Silva
<a href="mailto:gleydson@debian.org">gleydson@debian.org</a><br>
Luis Alberto Garcia Cipriano
<a href="mailto:lacipriano@uol.com.br">lacipriano@uol.com.br</a><br>
<b>Descri��o:</b><br>
<p>
Esse projeto tem sido levado principalmente pelo Gleydson
com a ajuda de um desenvolvedor brasileiro que mora bem
longe: na Fran�a, Rafael Laboissiere. Agora mais pessoas
est�o se juntando para ajudar nos esfor�os.
<p>
Basicamente, esse projeto tem como objetivo traduzir os
'potfiles' do processo de instala��o do Debian e sua
documenta��o. Todo o trabalho � feito pelo cvs e �
coordenado na lista <a 
href="http://lists.debian.org/debian-l10n-portuguese">
debian-l10n-portuguese</a>.
<p>
Para obter os arquivos dos 'boot-floppies', como � conhecido
nosso sistema de instala��o, basta usar os comandos:
<p>
<pre>
$ export CVSROOT=:pserver:anonymous@cvs.debian.org:/cvs/debian-boot
$ cvs login
(a senha � vazia, basta pressionar enter)
$ cvs co boot-floppies
</pre>
<p>
N�o fa�a nada sem antes consultar a lista para evitar duplica��o
de trabalho e para saber como proceder caso venha a traduzir ou
atualizar alguma coisa.
 <? include ("../end.tpt") ?>

