<? include ("../page.tpt") ?>

<? include ("secao.tpt") ?>

<b>Projeto:</b> Tradu��o das Descri��es de Pacotes<br>
<b>Coordenador:</b> 
Luis Alberto Garcia Cipriano
<a href="mailto:lacipriano@uol.com.br">lacipriano@uol.com.br</a><br>
<b>Descri��o:</b><br>
<p>
O Projeto conhecido como DDTP (ou Debian Description Translation
Project) � um projeto internacional para traduzir as descri��es
dos pacotes Debian. Dessa forma os usu�rios n�o falantes do
ingl�s ter�o mais facilidade em descobrir quais pacotes s�o �teis
a eles.
<p>
O Projeto Debian-BR foi o segundo a aderir ao DDTP e mant�m um
alto n�vel de progresso gra�as ao trabalho do seu coordenador.
� muito f�cil colaborar com esse projeto j� que o processo se
d� por trocas de emails com um servidor e os textos a serem
traduzidos s�o normalmente pequenos. Um curto espa�o de tempo
por dia pode ajudar muito o projeto.
<p>
Existe um <a 
href="http://gluck.debian.org/~grisu/ddts/guides/guide-pt_BR.txt">Guia 
para o Tradutor (em portugu�s)</a> e o <a 
href="http://ddtp.debian.org/">Site do projeto (em ingl�s)</a>, que
cont�m links para o FAQ e as estat�sticas. Existe uma 
<a href="http://ddtp.debian.org/packages.debian.org/">interface web
de procura de pacotes experimental</a> que usa as descri��es traduzidas
e que funciona como a <a 
href="http://packages.debian.org/">http://packages.debian.org/</a>.
<p>
Aqui voc� pode ver alguns shots (clique para aumentar):
<p>
<table border=0 cellpadding=5>
<tr>

<td>
<center>
<a href="ddtp-shots/2002_03_21_023659_shot.jpg">
<img src="ddtp-shots/thumb-2002_03_21_023659_shot.jpg">
<br>
<b>apt-cache e dselect</b>
</center>
</td>

<td>
<center>
<a href="ddtp-shots/2002_03_21_024830_shot.jpg">
<img src="ddtp-shots/thumb-2002_03_21_024830_shot.jpg">
<br>
<b>apt-cache show</b>
</center>
</td>

<td>
<center>
<a href="ddtp-shots/2002_03_21_025933_shot.jpg">
<img src="ddtp-shots/thumb-2002_03_21_025933_shot.jpg">
<br>
<b>gnome-apt</b>
</center>
</td>

</tr>
</table>
<p>
Para usar as descri��es traduzidas desde j� voc� pode trocar sua
linha do /etc/apt/sources.list para:
<p>
<pre>
deb http://ddtp.debian.org/aptable pt_BR/&lt;distribui��o&gt; main
deb-src http://ddtp.debian.org/aptable pt_BR/&lt;distribui��o&gt; main
</pre>
<p>
Troque &lt;distribui��o&gt; pela distribui��o que voc� usa.
(Exemplos: stable, testing, unstable, ou potato, woody, sid).
<p>
Note que voc� precisa de uma linha separada para non-free, contrib
e outra ainda para non-US. Essas partes do reposit�rio Debian ainda
n�o s�o suportadas pelo DDTP.
 <? include ("../end.tpt") ?>

