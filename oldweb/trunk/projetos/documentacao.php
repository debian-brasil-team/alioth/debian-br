<? include ("../page.tpt") ?>

<? include ("secao.tpt") ?>

<b>Projeto:</b> Tradu��o de Documenta��o<br>
<b>Coordenador:</b> 
Gustavo Noronha Silva
<a href="mailto:kov@debian.org">kov@debian.org</a><br>
<b>Descri��o:</b><br>
<h1>Tradu��o de Documenta��o</h1>
<font face="lucida" size="2">
<p>
Para que a tradu��o ocorra de uma maneira organizada e sem duplica��o de
esfor�os, aqui v�o algumas instru��es sobre como iniciar a tradu��o de
qualquer dos documentos ou outro tipo de material. Antes de mais nada,
tenha certeza de que <strong>*realmente*</strong> tem vontade de ajudar 
(e tempo!).
Depois olhe a lista abaixo e escolha o documento que voc� deseja traduzir.
A maioria dos links levam a p�ginas <strong>HTML</strong> que *n�o* devem ser traduzidas,
voc� deve pegar o documento em formato <strong>SGML</strong>, normalmente do
<i>CVS</i>.<p>
P�ginas HTML <strong>*n�o*</strong> devem ser traduzidas, voc� deve pegar o documento em
formato <strong>SGML</strong> clicando no nome do documento. Ap�s feita a escolha, envie um email para o coordenador dizendo seu <strong>nome 
completo</strong>, <strong>e-mail que deseja ter
relacionado � tradu��o e qual documento ir� traduzir.</strong><p>
Caso seu interesse na tradu��o acabe, envie novamente um email para
<a href=mailto:kov@debian.org>&lt;kov@debian.org&gt;</a> comunicando sua decis�o e enviando a parte
que j� est� pronta, caso algo tenha sido feito.<p>
Por favor, n�o deixe
um documento muito tempo parado, caso n�o esteja tendo tempo, desista
dele, o ideal seria o envio de um relat�rio de tradu��o mensal na lista
<a href=mailto:debian-user-portuguese@lists.debian.org>&lt;debian-user-portuguese@lists.debian.org&gt;</a> (veja a <a href="suporte/documentacao.php">se��o</a> relacionada).<br>
</font>
	
<p>

<small><i>* Documentos em fase de tradu��o</i>.</small>

<p>

<table summary=""><tr valign="top">
<td width="50%">
<h1>Manuais para Usu�rios:</h1>
<font face="lucida" size="2">
<ul>
<li><a href="http://www.debian.org/doc/user-manuals#quick-reference">Debian Reference</a> <strong><font color="#a04545">*</font> Alex Fernandes Rosa, <a href="mailto:alex@neurix.com.br">e-mail</a></strong>
<li><a href="http://www.debian.org/doc/user-manuals#guide">Debian Guide</a>
<li><a href="http://www.debian.org/doc/user-manuals#userref">Debian User Reference Manual</a> <strong><font color="#a04545">*</font> Francisco Mariano Neto, <a href="mailto:fmneto@yahoo.com">e-mail</a></strong>w
<li><a href="http://www.debian.org/doc/user-manuals#tips">Debian Tips</a>
<li><a href="http://www.debian.org/doc/user-manuals#usersguide">The Debian Linux User's Guide</a>
<li><a href="http://www.debian.org/doc/user-manuals#java-faq">Debian GNU/Linux and Java FAQ</a>
</ul></font>

<p>

<h1>Manuais para Administradores:</h1>
<font face="lucida" size="2">
<ul>
<li><a href="http://www.debian.org/doc/admin-manuals#relnotes">Debian Release Notes</a>
<li><a href="http://www.debian.org/doc/admin-manuals#system">Debian System Administrator's Manual</a>
<li><a href="http://www.debian.org/doc/admin-manuals#network">Debian Network Administrator's Manual</a> <strong><font color="#a04545">*</font> H�lio Loureiro, <a href=mailto:helio@loureiro.eng.br>e-mail</a></strong>
<li><a href="http://www.debian.org/doc/admin-manuals#hardware">Hardware Compatibility List</a> <strong>[obsoleto]</strong>
</ul></font>
</td>

<p>

<td width="50%">
<h1>Manuais para Desenvolvedores:</h1>
<font face="lucida" size="2">
<ul>
<li><a href="http://www.debian.org/doc/devel-manuals#policy">Debian Policy Manual</a> <strong><font color="a04545">*</font> Vinicius Kursancew, 
<a href="mailto:linux@vkcorp.org">linux@vkcorp.org</a></strong>
<li><a href="http://www.debian.org/doc/devel-manuals#devref">Debian Developer's Reference</a> <strong><font color="a04545">*</font> Carlos Laviola, <a href=mailto:claviola@ajato.com.br>e-mail</a></strong>
<li><a href="http://www.debian.org/doc/devel-manuals#dpkgint">dpkg Internals Manual</a>
<li><a href="http://www.debian.org/doc/devel-manuals#menu">Debian Menu System</a> <strong><font color="a04545">*</font> F�bio Quintal, <a href=mailto:info@ferasoft.com.br>e-mail</a></strong>
<li><a href="http://www.debian.org/doc/devel-manuals#i18n">Introduction to i18n</a> <strong><font color="a04545">*</font> Elcio F. de Mello, <a href=mailto:mello@ajato.com.br>e-mail</a></strong>
<li><a href="http://www.debian.org/doc/devel-manuals#swprod">How Software Producers can distribute their products directly in .deb format</a>
<li><a href="http://www.debian.org/doc/devel-manuals#makeadeb">Introduction: Making a Debian Package</a> <strong>[obsoleto]</strong>
<li><a href="http://www.debian.org/doc/devel-manuals#programmers">Debian Programmers' Manual</a> <strong>[obsoleto]</strong>
</ul></font>
</td>
</tr></table>

<p>

<h1>Manuais Variados:</h1>
<font face="lucida" size="2">
<ul>
<li><a href="http://www.debian.org/doc/misc-manuals#linuxmag">Linux Magazines</a>
<li><a href="http://www.debian.org/doc/misc-manuals#books">Debian Book Suggestions</a>
<li><a href="http://www.debian.org/doc/misc-manuals#dict">Debian Dictionary</a>
</ul></font>
 <? include ("../end.tpt") ?>

