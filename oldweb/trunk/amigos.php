<? include ('page.tpt') ?>

<h1>Sites Relacionados</h1>
<hr noshade>
<font face="lucida" size="2"><center><b>Sites Oficiais</b></center>
<hr noshade>
<a href="http://www.debian.org">Projeto Debian</a>: Site oficial do Projeto.
<p>
<a href="http://www.debian.org/doc/ddp">Projeto de Documenta��o Debian</a>:
Projeto que cuida da documenta��o do Debian.
<p>
<a href="http://www.debian.org/devel">Desenvolvimento Debian</a>:
P�gina de interesse para desenvolvedores e interessados em desenvolvimento
Debian. Com manuais e pol�ticas de desenvolvimento.
<p>
<a href="http://nm.debian.org">Novos Mantenedores Debian</a>:
P�gina onde se cadastram candidatos a mantenedores Debian. Pode-se
tamb�m fazer consultas sobre a situa��o dos inscritos.
<p>
<a href="http://db.debian.org">Banco de Dados de Desenvolvedores
Debian</a>: Nesse endere�o pode-se conseguir informa��es sobre
as m�quinas dispon�veis para o Debian e sobre os desenvolvedores.
</font>

<hr noshade>
<font face="lucida" size="2"><center><b>Servi�os Debian</b></center>
<hr noshade>
<a href="http://bugs.debian.org">Sistema de Busca de Erros</a>:
Aqui voc� pode acessar os relat�rios de erros enviados sobre
pacotes Debian e aprender como enviar novos.
<p>
<a href="http://packages.debian.org">Pacotes Debian</a>:
Aqui voc� encontra informa��es sobre pacotes Debian e pode
baixar pacotes e pacotes fontes facilmente.
<p>
<a href="http://buildd.debian.org">Sistema de Constru��o Autom�tica
de Pacotes</a>: P�gina dos famosos "buildds", que constr�em os
pacotes para todas as arquiteturas automaticamente. Pode-se conseguir
logs e ver gr�ficos para saber como as diversas arquiteturas est�o
conseguindo se manter na luta para chegar a uma situa��o razo�vel
para o lan�amento.
</font>

<hr noshade>
<font face="lucida" size="2"><center><b>Sites de Not�cias/Ajuda Debian</b></center>
<hr noshade>
<a href="http://www.debianplanet.org">DebianPlanet</a>: Site de
not�cias sobre Debian, em ingl�s.
<p>
<a href="http://www.debianhelp.org">DebianHelp</a>: Site de ajuda
para usu�rios de Debian, em ingl�s.
</font>

<hr noshade>
<font face="lucida" size="2"><center><b>Miscel�nea</b></center>
<hr noshade>
<a href="http://www.apt-get.org">Linhas
n�o-oficiais para o APT</a>: Linhas para serem adicionadas ao arquivo
/etc/apt/sources.list, que apontam para reposit�rios com pacotes diversos.
�timo se voc� quer um pacote que n�o est� dispon�vel para seu release
do Debian.
<p>
<a href="http://www.sfu.ca/~cth/ltmodem/debian">Christoph Hebeisen Homepage</a>:
Pacotes DEB para o driver do modem Luncent Winmodem.

<hr noshade>

<font face="lucida" size="2"><center><b>Links Diversos</b></center>
<p align=justify>
<ul>
<li>
<a href="http://www.gnu.org">Projeto GNU</a> - Site oficial do Projeto GNU.<br>
</li>
<li>
<a href="http://savannah.gnu.org">GNU Savannah</a> - Portal que abriga e oferece apoio � novos Projetos de Software Livre.<br>
</li>
<li>
<a href="http://www.cipsga.org.br">CIPSGA</a> - Comit� de Incentivo a Produ��o de Software GNU e Alternativo.<br>
</li>
<li>
<a href="http://www.quilombodigital.org">Quilombo Digital</a> - ONG que discute aspectos �ticos, pol�ticos e legais do Software Livre.<br>
</li>
<li>
<a href="http://www.narede.hpg.com.br/oo_trad.html">OpenOffice.org Brasil</a> - Projeto de tradu��o do OpenOffice.org para o portugu�s brasileiro.
</li>
<li>
<a href="http://www.militux.poli.org">MILITUX</a> - Site da campanha MILITUX por uma comunidade mais atuante.
</li>
<li>
<a href="http://www.softwarelivre.rs.gov.br">Software Livre RS</a> - Projeto de Apoio ao Software Livre do governo do Rio Grande do Sul.
</li>
<li>
<a href="http://www.olinux.com.br">O Linux</a> - Site refer�ncia em GNU/Linux no Brasil.<br>
<li>
<a href="http://www.poli.org">Projeto Poli</a> -  Projeto Portugu�s de Tradu��o.<br>
</li>
<li>
<a href="http://ldp-br.conectiva.com.br">Linux Documentation Project Brasil (LDP-BR)</a> - Projeto de Documenta��o do Linux no Brasil.
</li>
<li>
<a href="http://lie-br.conectiva.com.br">LIE-BR</a> - Esfor�o de Internacionaliza��o do Linux no Brasil.
</li>
<li>
<a href="http://www.linuxdoc.org">Linux Documentation Project (LDP)</a> - Reune documentos e apoia a cria��o de novos.
</li>
<li>
<a href="http://pontobr.org">PontoBR</a> - Site de not�cias no estilo Slashdot, falando de GNU/Linux al�m de muitos outros assuntos.
</li>
<li>
<a href="http://www.osef.org">OSEF</a> - Funda��o Open Source de Educa��o.
</ul>

<? include ('end.tpt') ?>
