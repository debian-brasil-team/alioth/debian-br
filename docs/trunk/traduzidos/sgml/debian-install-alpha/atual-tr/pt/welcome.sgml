<!-- mantenha este coment�rio para controle de revis�es -->
<!-- $Id: welcome.sgml,v 1.1.1.1 2003/06/14 19:40:55 kov Exp $ -->
<!-- original version: 1.34 -->

<chapt id="welcome">Bem vindo ao Debian
    <p>
N�s estamos felizes em ver que voc� decidiu utilizar o Debian e estamos
certos de que voc� ver� que a distribui��o Debian GNU/Linux � �nica.
O &debian; re�ne software livre de alta qualidade de todo o mundo,
integrado em um sistema coerente como um todo. Acreditamos que voc�
comprovar� que o resultado � verdadeiramente mais do que a soma das
partes.
 <p>
Este cap�tulo oferece uma vis�o do Projeto Debian e &debian;. Se voc� j� 
sabe a hist�ria do Projeto Debian e sobre a distribui��o &debian;,
sinta-se livre para seguir at� o pr�ximo cap�tulo. 


  <sect id="debian">O que � o Debian?
    <p>
Debian � uma organiza��o totalmente volunt�ria, dedicada ao
desenvolvimento de software livre e a promover os ideais da Funda��o do
Software Livre (Free Software Foundation). O projeto Debian foi iniciado
em 1993, quando Ian Murdock lan�ou um convite aberto para desenvolvedores
de software para que eles contrbu�ssem para uma distribui��o de software
completa e coerente baseada no relativamente novo kernel Linux. Essa
relativamente pequena associa��o de entusiastas dedicados, fundada
originalmente pela
<url id="&url-fsf-intro;" name="Free Software Foundation"> e influenciada
pela filosofia <url id="&url-gnu-intro;" name="GNU"> evoluiu com o passar
dos anos para uma organiza��o que possui em torno de 500
<em>Desenvolvedores Debian</em>. 
    <p>
Os Desenvolvedores Debian est�o envolvidos em uma variedade de atividades,
incluindo administra��o de sites <url id="&url-debian-home;" name="Web">
e <url id="&url-debian-ftp;" name="FTP">, design de gr�ficos, an�lise
legal de licen�as de softwares, cria��o de documenta��o e, � claro,
manuten��o de pacotes de software. 
   <p>
No interesse de comunicar nossa filosofia e atrair desenvolvedores que
acreditam nos objetivos do Debian, o projeto Debian publicou diversos
documentos que exp�em nossos valores e servem de guia para definir o que
significa ser um Desenvolvedor Debian. 
      <list compact>
        <item>
O <url id="&url-social-contract;" name="Contrato Social Debian"> � um
relato do comprometimento do Debian para com a Comunidade do Software
Livre. Qualquer pessoa que aceite obedecer ao Contrato Social pode se
tornar um <url id="&url-new-maintainer;" name="mantenedor">. Qualquer
mantenedor pode incluir software novo no Debian &mdash; contanto que o
software atenda os crit�rios de ser livre e o pacote siga nossos padr�es
de qualidade.
        <item>
A <url id="&url-dfsg;" name="Debian Free Software Guidelines"> s�o 
um conjunto de regras claras e consisas de crit�rios da Debian para 
o free software. O DFSG � um documento muito influente no movimento
Software Livre e foi a fun��o da <url id="&url-osd;" name="The Open
Source Definition">.
        <item>
O <url id="&url-debian-policy;" name="Manual de Pol�ticas Debian"> � uma
extensiva especifica��o dos padr�e de qualidade do Projeto Debian.
    </list>
  <p>
Os Desenvolvedores Debian tamb�m est�o envolvidos em diversos outros 
projetos; alguns espec�ficos do Debian, outros envolvendo alguns ou toda
a comunidade Linux. Alguns exemplos s�o:
    <list compact>
      <item>
O <url id="&url-lsb-org;" name="Linux Standard Base"> (LSB) � um projeto
que almeja padronizar o sistema b�sico GNU/Linux, o que permitir� que
desenvolvedores de software e hardware facilmente criem software e
controladores de dispositivos para Linux em geral, ao inv�s de somente
para uma distribui��o GNU/Linux espec�fica.
      <item>
O <url id="&url-fhs-home;" name="Filesystem Hierarchy Standard"> (FHS) 
� um esfor�o para padronizar a estrutura do sistema de arquivos do Linux.
O FHS permitir� que desenvolvedores de software concentrem seus esfor�os
no design de programas, sem se preocupar com detalhes como a maneira que
seu pacote ser� instalado nas diversas distribui��es GNU/Linux.
    <item>
O <url id="&url-debian-jr;" name="Debian Jr."> � um projeto interno, o
qual t�m por objetivo certificar-se que o Debian tenha algo para oferecer
para nossos jovens usu�rios.
    </list>
  <p>
Para maiores informa��es gerais sobre o Debian, veja o <url
id="&url-debian-faq;" name="Debian FAQ">.


  <sect id="linux">O que � GNU/Linux?
    <p>
O projeto GNU desenvolveu um conjunto compreensivo de ferramentas 
de software livre para uso com o Unix&trade; e outros sistemas 
operacionais semelhantes ao Unix, como o Linux. Estas ferramentas
permitem que usu�rios executem tarefas das mais simples (como copiar ou
remover arquivos do sistema) at� tarefas mais complicadas (como escrever
e compilar programas e fazer edi��es sofisticadas em uma variedade de 
formatos de documentos). 
    <p>
Um sistema operacional consiste de v�rios programas fundamentais que s�o
necess�rios para seu computador para que ele possa se comunicar e receber
instru��es dos usu�rios, ler e gravar dados no disco r�gido, fitas e
impressoras, controlar o uso da mem�ria e executar outros softwares. A
parte mais importante de um sistema operacional � o kernel. Em um sistema
GNU/Linux, o Linux � componente kernel. O restante do sistema consiste de
outros programas, muitos dos quais foram escritos por ou para o projeto
GNU. Devido ao kernel Linux sozinho n�o constituir um sistema operacional
utiliz�vel, n�s preferimos usar o termo ``GNU/Linux'' quando nos referimos
ao sistema que muitas pessoas casualmente se referem como ``Linux''.
   <p>
O <url id="&url-kernel-org;" name="kernel Linux"> apareceu pela primeira
vez em 1991, quando um estudante finland�s de Ci�ncia da Computa��o
chamado Linux Torvalds anunciou uma vers�o preliminar de um substituto
para o kernel Minix no grupo e not�ciasda Usenet <tt>comp.os.minix</tt>.
Veja a <url id="&url-linux-history;" name="P�gina da Hist�ria do Linux">
da Linux International.
   <p>
Linux Torvalds continua a coordenar o trabalho de centenas de
desenvolvedores com a ajuda de alguns eleitos confi�veis. Um excelente
sum�rio semanal das discuss�es na lista de discuss�o <tt>linux-kernel</tt>
� a <url id="&url-kernel-traffic;" name="Kernel Traffic">. Maiores
informa��es sobre a lista de discuss�o <tt>linux-kernel</tt> pode ser
encontrada no <url id="&url-linux-kernel-list-faq;" name="FAQ da lista
linux-kernel">.


  <sect id="dgl">O que � &debian;?
    <p>
A combina��o da filosofia e metodologia Debian com as ferramentas GNU, o
kernel Linux, e outros importantes softwares livres, formam uma
distribui��o de software �nica chamada &debian;. Esta distribui��o �
constitu�da de um grande n�mero de <em>pacotes</em> de software. Cada
pacote na distribui��o cont�m execut�veis, scripts, documenta��o,
informa��o de configura��o e possui um <em>mantenedor</em> que �
primariamente respons�vel por manter o pacote atualizado, receber os
relat�rios de bugs e se comunicar com o autor(es) original(is) do software
empacotado. Nossa base de usu�rios extremamente grande, combinada com
nosso sistema de gerenciamento de bugs garante que os problemas sejam
encontrados e corrigidos rapidamente.
    <p>
A aten��o do Debian aos detalhes nos permite construir uma distribui��o
de alta qualidade, est�vel e escal�vel. Instala��es podem ser facilmente
configuradas para servir muitos prop�sitos, desde firewalls compactos
passando por esta��es de trabalhos desktop cient�ficas at� servidores de
redes de alto n�vel.
    <p>
A caracter�stica que mais distingue o Debian de outras distribui��es
GNU/Linux � seu sistema de gerenciamento de pacotes. Estas ferramentas
d�o ao administrador de um sistema Debian o controle completo sobre os
pacotes instalados no sistema, incluindo a habilidade de instalar um �nico
pacote ou automaticamente atualizar o sistema operacional inteiro. Pacotes
individuais pode tamb�m ser mantidos e n�o atualizados. Voc� pode at�
mesmo dizer ao sistema de gerenciamento de pacotes sobre o software que
voc� compilou manualmente e quais depend�ncias ele resolve.
    <p>
Para proteger seu sistema contra ``cavalos de tr�ia'' de outros softwares
mal intencionados, o Debian verifica se os pacotes tiveram origem de seus
mantenedores Debian registrados. Empacotadores Debian tamb�m t�m um grande
cuidado ao configurar seus pacotes de uma maneira segura. Quando problemas
de seguran�a em pacotes fornecidos aparecem, consertos s�o geralmente
colocados a disposi��o muit rapidamente. Com as simples op��es de
atualiza��o Debian, consertos de seguran�a pode ser obtidos e instalados
automaticamente atrav�s da Internet.
    <p>
O m�todo prim�rio, e o melhor, de se obter suporte para seu sistema
&debian; e de se comunicar com os Desenvolvedores Debian � atrav�s das
muitas listas de discuss�o mantidas pelo Projeto Debian (existem mais de
90 listas at� o momento). A maneira mais f�cil de se inscrever em uma ou
mais destas listas � visitar a <url id="&url-debian-lists-subscribe;"
name="p�gina de inscri��o nas listas de discuss�o Debian"> e preencher o
formul�rio que voc� encontrar� nesta p�gina.


  <sect id="hurd">O que � Debiab GNU/Hurd?
    <p>
Debian GNU/Hurd � um sistema Debian GNU que substitui o kernel Linux
monol�tico com um kenrle GNU Hurd &mdash; um conjunto de servidores
sendo executados em cima de um microkernel GNU Mach. O Hurd ainda n�o est�
finalizado e n�o � indicado para o uso do dia-a-dia, mas o trabalho est�
continuando. O Hurd est� sendo desenvolvido atualmente somente para a
arquitetura i386, por�m ports para outras arquiteturas ser�o feitos uma
vez que o sistema torne-se mais est�vel.
    <p>
Para maiores informa��es, veja a <url id="&url-debian-home;/ports/hurd/"
name="p�gina de ports Debian GNU/Hurd"> e a lista de discuss�o
<email>debian-hurd@lists.debian.org</email>.


<sect id="getting-newest-inst">
<heading>Obtendo o Debian</heading>
    <p>
Para informa��o sobre como fazer o download do &debian; a partir da
Internet ou de quem CDs oficiais Debian podem ser comprados, veja a
<url id="&url-debian-distrib;" name="p�gina web de distribui��o">. A
<url id="&url-debian-mirrors;" name="lista de espelhos Debian"> cont�m um
conjunto completo de espelhos oficiais Debian.
    <p>
A Debian pode ser atualizada ap�s a instala��o de forma muito f�cil. 
O procedimento de instala��o ajudar� a configurar o sistema assim 
voc� poder� fazer estas atualiza��es assim que o sistema de instala��o
for finalizado, se precisar fazer.


  <sect id="getting-newest-doc">
    <heading>Obtendo a vers�o mais nova deste documento</heading>
    <p>
Este documento est� constantemente sendo revisado. Certifique-se de checar
as <url id="&url-release-area;" name="p�ginas Debian &release;"> para
quaisquer informa��es de �ltimo minuto aobre a vers�o &release; do sistema
&debian;. Vers�es atualizadas deste manual de instala��o est�o tamb�m
dispon�veis  a partir das <url id="&url-install-manual;" name="p�ginas
oficiais do Manual de Instala��o">.


<sect id="organization">Organiza��o deste documento
    <p>
Este documento foi criado para servir como um manual para os usu�rios
Debian iniciantes. Ele tenta assumir o m�imo poss�vel sobre seu n�vel de
conhecimento. No entanto, � assumido que voc� possui conhecimentos gerais
sobre como o hardware de seu computador funciona.
    <p>
    
Usu�rios experientes podem encontrar refer�ncias interessantes neste
documento, incluindo o m�nimo de espa�o ocupado pela instala��o, detalhes
sobre o hardwares suportado pelo sistema de instala��o Debian e muito
mais. N�s encorajamos usu�rios experientes a ler o restante deste
documento.
    <p>
Em geral, este manual � organizado de forma linear, seguindo seus passos
atrav�s do processo de instala��o do in�cio ao fim. Aqui est�o os passos
sobre a instala��o do &debian; e as se��es deste documento as quais se
relacionam com cada passo:

  <enumlist>
        <item>
Determine se seu hardwares atende aos requerimentos necess�rios para usar
o sistema de instala��o em Requerimentos do Sistema,
<ref id="hardware-req">.
        <item>
Fa�a uma c�pia de seguran�a (backup) do seu sistema, execute qualquer
configura��o de planejamento antes de instalar o Debian, em Antes de Voc�
Iniciar, em <ref id="preparing">. Caso voc� esteja preparando um sistema
de inicializa��o m�ltipla (multi-boot), voc� precisa criar espa�o
particion�vel em seu disco r�gido para que o Debian o utilize.
        <item>
Em m�todos de instala��o, <ref id="install-methods">, voc� obter� os
arquivos de instala��o necess�rios par aseu m�todo de instala��o.
        <item>
<ref id="rescue-boot"> descreve a inicializa��o no sistema de instala��o.
Este cap�tulo tamb�m discute procedimentos de como agir quando ocorre um
problema caso voc� tenha problemas neste passo.
        <item>	
A configura��o das parti��es Linux para seu sistema Debian � explicada em
Particionamento , <ref id="partitioning">.
        <item>
A instala��o do kernel e a configura��o dos m�dulos dos controladores de
perif�ricos � explicada em Instalando o Sistema,
<ref id="install-system">. Configure sua conex�o de rede para que os
arquivos de instala��o restantes possam ser obtidos diretamente do
servidor Debian caso voc� n�o esteja instalando a partir do CD.
        <item>
Inicie o donwload/instala��o/configura��o de um sistema funcional m�nimo
em Instala��o do Sistema B�sico, <ref id="install-base">.
        <item>
Inicie seu novo sistema b�sico instalado e passe por algumas tarefas de
configura��o adicionais em Configura��es Iniciais, <ref id="init-config">.
        <item>
Instale software adicional em Instala��o de Pacotes,
<ref id="install-packages">. Use o <em>tasksel</em> para instalar grupos
de pacotes que formam uma `tarefa' de computador, ou o
<prgn>dselect</prgn> para selecionar pacotes individuais a partir de uma
longa lista ou o <prgn>apt-get</prgn> para instalar pacotes individuais
quando voc� j� conhece os nomes dos pacotes que voc� quer.
  </enumlist>

  <p>
Uma vez que voc� tenha seu sistema instalado voc� pode ler as informa��es
sobre P�s Instala��o, <ref id="post-install">. Este cap�tulo explica onde
procurar para encontrar maiores informa��es sobre Unix e Debian e como
substituir seu kernel. Se voc� quer construir seu pr�prio sistema de
instala��o a partir dos fontes, certifique-se de ler as Informa��es
T�cnicas sobre os Disquetes de Instala��o,
<ref id="boot-floppy-techinfo">.
  <p>
Finalmente, informa��es sobre esse documento e como contribuir com o mesmo
podem ser encontradas em <ref id="administrivia">.
<!-- FIXME for pt language only - Find out a good translation for
administrivia -->


<![ %FIXME [
<sect>
  <heading>Este documento possui problemas conhecidos</heading>
  <p>
Este documento est� ainda em sua forma inicial. � conhecido que ele
� incompleto e provavelmente tamb�m cont�m erros, problemas gramaticais
e muito mais. Se voc� ver palavras ``FIXME'' ou ``TODO'', voc� pode estar
certo que esta se��o est� imcompleta. Tenha cuidado. Qualquer ajuda,
sugest�o, e especialmente patches, ser�o muito apreciados.
    <p>
Vers�es em desenvolvimento deste documento podem ser encontradas em
<url id="&url-install-manual;">. Neste local voc� encontrar� uma lista
de todas as diferentes arquitetutas e idiomas para os quais este documento
est� dispon�vel.
    <p>
Os fontes est�o dispon�veis publicamente; procure por maiores informa��es
sobre como contribuir em <ref id="administrivia">. N�s aceitamos
sugest�es, coment�rios, patches e relat�rios de bugs (reporte o bug no
pacote <package>boot-floppies</package>, mas primeiro cheque para
verificar se o problema j� est� reportado).

]]>


  <sect>Sobre Copyrights e Licen�as de Software
    <p>
Estamos certos de voc� leu algumas das licen�as que acompanham a maioria
dos softwares comerciais &mdash; elas normalmente dizem que voc� pode
somente usar uma c�pia do software em um �nico computador. A licen�a do
sistema &debian; n�o � completamente como essas licen�as. N�s o
encorajamos a colocar uma c�pia do &debian; em cada computador em sua
escola ou local de trabalho. Empreste sua m�dia de instala��o para seus
amigos e ajude-os a instal�-la em seus computadores! Voc� pode at� mesmo
fazer milhares de c�pias e <em>vend�-las</em> &mdash; embora com algumas
restri��es. Sua liberdade de instalar e usar o sistema vem diretamente
do motivo do Debian ser baseado em <em>software livre</em>.
    <p>
Chamar o software de ``livre'' n�o significa que o software n�o tenha
copyright e n�o significa que os CDs contendo o software devam ser
distribu�dos sem custo. Software livre, em parte, significa que as
licen�as dos programas individuais n�o requerem que voc� pague pelo
privil�gio de distribuir ou usar estes programas. Software livre tamb�m
significa que n�o somente qualquer um pode estender, adaptar e modificar
o software, mas que os resultados de seu trabalho devem ser distribu�dos
da mesma forma.<footnote>Note que o projeto Debian, como uma forma de
concess�o pragm�tica para seus usu�iors, disponibiliza alguns pacotes que
n�o cumprem nossos crit�rios de serem livres. Esses pacotes n�o s�o por�m
parte da distribui��o oficial e est�o dispon�veis somente a partir das
�reas <tt>contrib</tt> e <tt>non-free</tt> dos espelhos Debian ou em
CD-ROMs de terceiros; veja o <url id="&url-debian-faq;" name="Debian FAQ">
na se��o ``Os reposit�rios FTP Debian'' para maiores informa��es sobre o
layout e conte�do dos reposit�rios.</footnote>
    <p>
Muitos dos programas no sistema s�o licenciados sob os termos da
<em>Licen�a P�blica Geral</em> <em>GNU</em>, frequentemente conhecida
como ``a GPL''. A GPL requer que voc� fa�a com que o <em>c�digo fonte</em>
dos programas esteja dispon�vel sempre que voc� distribuir uma c�pia
bin�ria do programa; esta condi��o da licen�a garante que qualquer usu�rio
ser� capaz de modificar o software. Devido a esta condi��o, o c�digo fonte
para todos os programas est� dispon�vel no sistema Debian.<footnote>Para
informa��es sobre como localizar, desempacotar e construir bin�rios a
partir dos pacotes fontes, veja a
<url id="&url-debian-faq;" name="Debian FAQ">, se��o ``Fundamentos do
Sistema de Gerenciamento de Pacotes Debian''.</footnote>
    <p>
Existem diversas outras formas de copyright e licen�as de software usadas
em programas no Debian. Voc� pode encontrar os copyrights e as licen�as
para cada pacote instalado em seu sistema verificando o arquivo
<tt>/usr/share/doc/<var>nome-do-pacote</var>/copyright</tt> uma vez que
voc� possua um pacote instalado em seus sistema.
    <p>
Para maiores informa��es sobre licen�as e como o Debian determina se um
software � livre o bastante para ser inclu�do na distribui��o principal,
veja as <url id="&url-dfsg;"
name="As linhas Guias Debian para o Software Libre">.
    <p>
O aviso legal mais importante � que este software � distribu�do sem
<em>nenhuma garantia</em>. Os programas que criaram este software o
fizeram para o benef�cio da comunidade. Nenhuma garantia � feita sobre
a adequa��o deste software par aqualquer dado prop�sito. Por�m, uma vez
que o software � livre, voc� t�m a possibilidade de modificar este
software para que o mesmo se adeque as suas necessidades &mdash; e
aproveitar os benef�cios das mudan�as feitas por outros que estenderam
o software desta forma.

<!-- Deixe este coment�rio no final do arquivo
Local variables:
mode: sgml
sgml-omittag:t
sgml-shorttag:t
sgml-namecase-general:t
sgml-general-insert-case:lower
sgml-minimize-attributes:max
sgml-always-quote-attributes:t
sgml-indent-step:2
sgml-indent-data:nil
sgml-parent-document:("../install.pt.sgml" "book" "chapt")
sgml-declaration:nil
sgml-exposed-tags:nil
sgml-local-catalogs:nil
sgml-local-ecat-files:nil
End:
-->
