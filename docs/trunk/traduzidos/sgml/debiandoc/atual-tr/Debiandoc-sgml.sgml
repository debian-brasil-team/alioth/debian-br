<!doctype debiandoc public "-//DebianDoc//DTD DebianDoc//EN">

<debiandoc>

  <book id="debiandoc-sgml">

    <titlepag>

      <title>Manual Debiandoc-SGML</title>

      <author>
        <name>Ian Jackson</name>
        <email>ijackson@gnu.ai.mit.edu</email>
      </author>
 
      <author>
        <name>Ardo van Rangelrooij</name>
        <email>ardo@debian.org</email>
      </author>  
      <author>
       <name>Grupo de Revisores Debian-BR</name>
       <email>grd-br@listas.cipsga.org.br</email>
      </author>
      <version><date></version>

      <abstract>
      </abstract>

      <copyright>
        <copyrightsummary>
          Copyright &copy; 1998 Ardo van Rangelrooij
        </copyrightsummary>
        <copyrightsummary> 
          Copyright &copy; 1996 Ian Jackson
        </copyrightsummary>
        <p>
        Debiandoc-SGML, inclu�ndo este manual, � software livre; voc�
        pode redistribui-lo e/ou modifica-l� dentro dos termos da
        GNU General Public Licence como publicada pela Free Software
        Foundation; vers�o 2, ou (sobre sua escolha) qualquer vers�o
        futura.
        </p>
        <p> 
        Este � distribu�do esperando que sej� �til ao seu
        utilizador, mas <em>sem nenhuma garantia</em>; sem at� 
        mesmo para garantia em particular ou determinado prop�sito. 
        Vej� a GNU General Public License para mais detalhes.
        </p>
        <p> 
 Voc� deve ter recebido uma c�pia da Licen�a GNU com seuM
        sistema Debian GNU/Linux, em <file>/usr/doc/copyright/GPL</file>,
        ou com o pacote fonte <prgn>debiandoc-sgml</prgn> como o arquivo 
        <tt>COPYING</tt>. Se n�o recebeu, escreva para a Free Software   
        Foundation, Inc., 59 Temple Place, Suite 330, Boston, MAM
        02111-1307, USA.
        </p>

      </copyright>

    </titlepag>

    <toc detail="sect">

    <chapt id="markup">
      <heading>Marca��o SGML e metacaracteres</heading>

      <p>
      Um documento SGML consiste em um texto entre tags marcando
      o in�cio e final de elementos.
      </p>

      <p>
      Tags tem a forma <tt>&lt;<var>elemento</var>&gt;</tt> para
        in�ciar o <var>elemento</var> e
<tt>&lt;<var>/elemento</var>&gt;</tt>
      para finaliz�-la.
      </p>

      <p>
      Existem alguns atalhos que se pode usar:
        <tt>&lt;<var>elemento/</var><var>conte�do/</var></tt> espec�fica
      um elemento <var>elemento</var> com conte�do <var>conte�do</var>, 
      mas o <var>conte�do</var> n�o pode cont�r uma barra <tt>/</tt>.   
      <tt>&lt;/&gt;</tt> fecha o elemento atualmente aberto.
      </p>

      <p>
      Muitos tipos de elementos in�ciais de tags podem cont�r
      atributos; estes aparecem dentro do par�ntese, e s�o   
      separados do elemento por um espa�o em branco. Os atributos
      permitidos em um elemento particular de inicio de tag s�o  
      descritos ao longo do elemento.
      </p>

      <p>
      Se voc� deseja incluir car�cteres de marca��o SGML
      (sinal de menor e maior <tt>&lt;</tt> <tt>&gt;</tt> e 
      <tt>&amp;</tt>) como texto voc� deve se referir a eles pelo
      nome (isto � chamado uma refer�ncia na linguagem SGML).    
      <tt>&amp;<var>nome</var>;</tt> produz o car�cter que
      o nome � <var>nome</var>.
      </p>

      <p>
        
      Alguns caracteres �teis s�o:
        <taglist compact="compact">
          <tag>
            <tt>lt</tt>
          </tag>
          <item>
            <p> 
              sinal de menor (esquerdo) <tt>&lt;</tt>
            </p>
          </item>
          <tag>  
            <tt>gt</tt>
          </tag>
          <item>
            <p> 
              sinal de maior (direito) <tt>&gt;</tt>
            </p>
          </item>
          <tag>  
            <tt>amp</tt>
          </tag>
          <item>
            <p> 
              <tt>&amp;</tt>
            </p>
          </item>
          <tag>  
            <tt>copy</tt>
          </tag>
          <item>
            <p> 
              s�mbolo de copyright <tt>&copy;</tt>
            </p>
          </item>
        </taglist>
      A lista completa de caracteres est� dispon�vel em
        <prgn>linuxdoc-sgml</prgn>, em <prgn>linuxdoc-sgml</prgn>'s
        no arquivo <file>rep/latin1/general</file>.
      </p>

      <p>
      Voc� pode tamb�m usar <tt>&amp;#<var>n�mero</var>;</tt> para se
      referir ao car�cter que o n�mero � <var>n�mero</var> (em
        ISO-LATIN-1). <var>n�mero</var> deve estar em valor decimal.
      </p>

    </chapt>


    <chapt id="structure">
      <heading>Estrutura Global</heading>

      <p>
      A primeira linha do documento deve ser
        <example>
&lt!doctype debiandoc public "-//DebianDoc//DTD DebianDoc//EN"&gt
        </example>
        ou
        <example>
&lt!doctype debiandoc system&gt
        </example>
      para indicar que o documento � um arquivo Debiandoc-SGML.
      </p>

      <p>
      O documento deve iniciar com a tag <tt>&lt;book&gt;</tt> tag
        e finalizar com <tt>&lt;/book&gt;</tt>.
      </p>

      <p>
      Isto pode ser seguido de <tt>&lt;title&gt;</tt>, um ou mais
        tags <tt>&lt;author&gt;</tt>-<tt>&lt;email&gt;</tt>, e 
      opcionalmente <tt>&lt;version&gt;</tt>.  Cada uma destas 
      � uma pega da marca��o de texto inline - vej� <ref id="inline">.
      A <tt>&lt;version&gt;</tt> pode tamb�m cont�r <tt>&lt;date&gt;</tt> 
      que mostra a data e hora em que o documento foi gerado.
      </p>

      <p>
      Ent�o pode vir uma <tt>&lt;abstract&gt;</tt>, um aviso de
        <tt>&lt;copyright&gt;</tt>, e um marcador <tt>&lt;toc&gt;</tt>.
      </p>

      <p>
      <tt>&lt;abstract&gt;</tt> cont�m um par�grafo simples que
      cont�m um resumo do documento.
      </p>

      <p>
      A tag <tt>&lt;copyright&gt;</tt> cont�m um aviso de copyright
      (isto � usado, por exemplo, no rodap� de cada pagina HTML)   
      seguida de um ou mais par�grafos, o primeiro deve ser
      indicado pela tag <tt>&lt;p&gt;</tt> para distinguir ela
      do sum�rio.
      </p>
  <p>
      O marcador <tt>&lt;toc&gt;</tt> espec�fica que a tabela de
      conte�do ser� produzida. A tag <tt>&lt;toc&gt;</tt> n�o   
      cont�m nada no fonte SGML - seu conte�do � gerado pelo    
      sistema de processamento. A tag <tt>&lt;toc&gt;</tt> pode 
      cont�r um atributo dizendo em detalhes como ele pode ser, 
      por exemplo, <tt>&lt;toc sect1&gt;</tt> diz que as
      subsegues devem ser inclu�das, considerando que a tag 
      <tt>&lt;toc chapt&gt;</tt> diz que somente cap�tulos e ap�ndices
      devem ser inclu�dos. Os valores permitidos s�o <tt>chapt</tt>,  
      <tt>sect</tt>, <tt>sect1</tt> e <tt>sect2</tt>.
      </p>

      <p>
      Seguinte a estas partes vem o corpo do documento - um ou mais
      cap�tulos <tt>&lt;chapt&gt;</tt>, opcionalmente seguido de um ou
      mais ap�ndices <tt>&lt;appendix&gt;</tt>.
      </p>

      <p>
      N�o � necessario marcar o final dos elementos <tt>&lt;title&gt;</tt>,
      <tt>&lt;author&gt;</tt>, <tt>&lt;version&gt;</tt>, 
      <tt>&lt;abstract&gt;</tt> e <tt>&lt;copyright&gt;</tt> - eles s�o
      automaticamente fechados no inicio do pr�ximo elemento.
      </p>

    </chapt>
 
    <chapt id="chaptsectpar">
      <heading>Cap�tulos, ap�ndices, segu�s, e par�grafos</heading>

      <p>
      Cada cap�tulo inicia com a tag <tt>&lt;chapt&gt;</tt>,
      seguida do titulo do cap�tulo. O titulo pode conter  
      texto marcado inline, mas sem refer�ncias cruzes (veja
      <ref id="inline">). O mesmo se aplica para um ap�ndice, 
      exceto que ele inicia com uma tag <tt>&lt;appendix&gt;</tt>.
      </p>

      <p>
      O corpo de um cap�tulo ou um ap�ndice cont�m zero ou mais
      par�grafos, o primeiro deve se indicado por uma tag
      <tt>&lt;p&gt</tt> para distingui-l� do t�tulo, e   
      ent�o zero ou mais segu�s.
      </p>

      <p>
      Subsegu�s s�o <tt>&lt;sect1&gt;</tt>; existe tamb�m pequenas
      divis�es <tt>&lt;sect2&gt;</tt>, <tt>&lt;sect3&gt;</tt> e
        <tt>&lt;sect4&gt;</tt>.
      </p>

      <p>
      Par�grafos s�o inclu�dos com <tt>&lt;p&gt;</tt>.  Algumas vezes
      a tag de inicio de par�grafo pode ser omitida, mas � obrigat�rio
      ap�s <tt>&lt;chapt&gt;</tt>, <tt>&lt;sect&gt;</tt> e assim 
      por diante. N�o � necess�rio marcar o final de um par�grafo com 
      <tt>&lt;/p&gt;</tt>.
      </p>

      <p>
      Par�grafos podem contar texto marcados inline (veja <ref
      id="inline">) e tamb�m listas e exemplos (<ref id="listexamp">).
      </p>

    </chapt>

    <chapt id="inline">
      <heading>Texto marcado inline e marca��o do estilo de
caracteres</heading

      <p>
      Texto ordin�rio (chamado "texto inline" neste documento) pode
      cont�r elementos para forma��o especial e refer�ncia cruz.   
      Texto inline aparece em cap�tulo, ap�ndice e t�tulos de      
      segu�s, em aviso de copyright, dentro de par�grafos 
      e em outros lugares parecidos.
      </p>

      <sect>
        <heading>Estilo do Car�cter</heading>

        <p>
        Existem um n�mero de elementos para denotar certas
        partes especiais de texto. Para todos eles um texto
        especial pode ser marcado explicitamente, usando   
        uma tag final <tt>&lt;<var>element</var>&gt;</tt>, 
        a tag final para fechamento para elementos <tt>&lt;/&gt;</tt> 
        ou barra <tt>/</tt> que finaliza a forma mais abreviada
        de marca��o de elementos (see <ref id="markup">).
        </p>

        <p>
          <taglist>

            <tag>
              <tt>&lt;em&gt;</tt> - jnfase
            </tag>
            <item>
              <p> 
            Indica que o texto contido � mais importante ou
            mais significante que o resto.
              </p>
              <p> 
            Normalmente isto ser� representado por italicos se
            dispon�vel, ou texto icentivado ou sublinhado, ou 
            em formatos de texto planos colocando o texto
            entre ast�riscos como *este*.  
              </p>
            </item>

            <tag>
              <tt>&lt;strong&gt;</tt> - mais jnfase
            </tag>
            <item>
              <p> 
            Indica que o texto contido � ainda mais 
            importante ou ainda mais significante que o resto.
              </p>
              <p> 
            Tipicamente isto ser� representado por negrito
            se dispon�vel ou em formato texto plano com   
            texto entre ast�risco como *este*.
              </p>
            </item>

            <tag>
              <tt>&lt;var&gt;</tt> - vari�vel metasint�tica
            </tag>
            <item>
              <p> 
            Indica que o texto contido � uma vari�vel matasint�tica.
            Ou sej�, ele � uma pega de objeto da sintaxe que � usado
            para ter uma valor real substitu�do.
              </p>
              <p> 
            Normalmente isto ser� representado por italicos, em
            em formato texto plano com sinais de maior e menor 
            como &lt;este&gt;. Se muitas vari�veis metasint�ticas
            s�o mostradas uma ap�s as outras eles devem cada uma 
            ser seguidas de seu pr�prio elemento <tt>&lt;var&gt;</tt>.
              </p>
            </item>

            <tag>
              <tt>&lt;package&gt;</tt> - nome do pacote
            </tag>
            <item>
              <p> 
            Indica que o texto contido � o nome de um pacote Debian.
              </p>
              <p> 
            Isto � normalmente feito usando uma fonte com largura
            fixa; em formato texto plano quotas s�o usadas em torno
            do elemento.
              </p>
            </item>

            <tag>
              <tt>&lt;prgn&gt;</tt> - nome do programa ou arquivo conhecido
            </tag>
            <item>
              <p> 
            Indica que o texto contido � o nome de um programa,
            um arquivo conhecido (normalmente sem o caminho),  
            uma fun��o ou alguma coisa parecida que tem um nome
            no computador.
              </p>
              <p> 
            Em formatos de sa�da onde o sublinhado de car�cter
            e v�rios estilos de fontes est�o dispon�veis este �
            normalmente representado com uma fonte de largura  
            fixa. Em texto plano, estes nomes n�o s�o especialmente
            marcados, ocorre a introdu��o de quotas no fluxo do texto.
              </p>
            </item>

            <tag>
              <tt>&lt;file&gt;</tt> - arquivo completo ou nome de diret�rio
            </tag>
            <item>
              <p> 
            Indica que o texto contido � o caminho completo de um
            arquivo, buffer, diret�rio, etc.
              </p>
                   <p> 
            Isto � normalmente feito usando uma fonte com larguraM
            fixa; em formato texto plano, quotas podem ser usadasM
            em torno do elemento.M
              </p>
            </item>

            <tag>
              <tt>&lt;tt&gt;</tt> - c�digo ou fragmento de sa�da, comando
              string
            </tag>  
            <item>  
              <p>   
            Indica que o texto contido � uma string geral que 
            v�m de fora do computador. Ele deve ser usado para
            comandos ou fragmentos de c�digo que devem ser  
            mostrados na linha de comando ou palavras (vej�
            <ref id="examples"> para uma alternativa), e assim 
            por diante. Ele � frequentemente necess�rio para  
            introduzir vari�veis metasint�ticas nestas strings,
            onde elas devem fazer parte do elemento <tt>&lt;tt&gt;</tt> 
            ao inv�s de elementos dentro dela.
              </p>
              <p> 
            Isto � normalmente feito usando uma fonte com largura
            fixa; em formatos texto plano quotas podem ser usadas
            ao redor do elemento.
              </p>
            </item>

            <tag>
              <tt>&lt;qref id="<var>refid</var>"&gt;</tt> - Refer�ncia
quieta
          </tag>
            <item>
              <p> 
            Produz uma "refer�ncia quieta" para identifica��o de 
            refer�ncia nomeada (veja <ref id="xref">). Isto deve ser
            usado onde uma refer�ncia cruzada pode ser �til mas n�o
            intrusiva, mas onde n�o � essencial.
              </p>
              <p> 
            Em formatos onde refer�ncias cruzadas podem ser feitas
            n�o intrusivamente fazendo uma regi�o do texto um
            hyperlink sem introduzir textos inline, este elemento
            far� com que seu texto contido se torne um hyperlink para
            o alvo da refer�ncia cruzada. Em outros formatos
            este elemento n�o tem nenhum efeito.
              </p>
            </item>

          </taglist>
        </p>

      </sect>

      <sect id="xref">
        <heading>Refer�ncias Cruzadas</heading>

        <p>
        Existem um n�mero de elementos para introduzir
        refer�ncias cruzadas ou para outras partes do mesmo
        documento ou para outros documentos.
        </p>

        <p>
        As refer�ncias cruzadas para documentos internos s�o baseadas
        no esquema de identifica��o de refer�ncias. Cada cap�tulo, 
        ap�ndice, se��o, subse��o, etc, pode ter um atributo <tt>id</tt> 
        passando sua refer�ncia de identifica��o - por exemplo 
        <tt>&lt;chapt id="spong"&gt;</tt> especifica que o cap�tulo ou
        ap�ndice iniciado tem a refer�ncia de identifica��o <tt>spong</tt>.
        Esta refer�ncia de identifica��o pode ent�o ser referida em outras  
        partes do documento usando elementos especiais de refer�ncia
cruzada.
        </p>

        <p>
        O identificador de refer�ncia poder� tamb�m ser usado para
        gerar nomes de arquivos e refer�ncias para formatos tal
        como HTML, que produzem diversos arquivos de sa�da; se nenhuma
        identifica��o de refer�ncia � especificada ent�o o cap�tulo,
        ap�ndice ou n�meros de se��o ser�o usados. � uma boa id�ia
        identificar no m�nimo suas refer�ncias de ap�ndice ou 
        cap�tulos. Assim, aqueles nomes de arquivos n�o ser�o alterados
        se voc� alterar a ordem daqueles cap�tulos, se��es ou
        ap�ndices em seu documento.
        </p>

        <p>
          <taglist>

            <tag>
              <tt>&lt;ref id="<var>refid</var>"&gt;</tt> -
              refer�ncias cruzadas no documento completo  
            </tag>
            <item>
              <p> 
            Isto gera uma refer�ncia cruzada dentro do mesmo documento
            para o cap�tulo, ap�ndice ou se��o com id <var>refid</var>.
              </p>
              <p> 
            O elemento <tt>&lt;ref&gt;</tt> n�o tem nenhum conte�do;
            o cap�tulo, ap�ndice ou n�mero de se��o e t�tulo e seu 
            n�mero de p�gina ou tudo que � apropriado para o formato
            de sa�da ser� inserido no texto no ponto onde a tag
            aparece.
              </p>
              <p> 
            Sintaticamente a refer�ncia cruzada � um substantivo de frase,
            satisfat�rio para usu�rios como <tt>(veja &lt;ref
                id="<var>...</var>"&gt;)</tt> ou <tt>informa��es mais
            adiante em &lt;ref id="<var>...</var>"&gt;.</tt>.
              </p>
            </item>

            <tag>
             <tt>&lt;manref name="<var>name</var>"
              section="<var>section</var>"&gt;</tt> - manpage
            </tag>
            <item>
              <p> 
            Gera uma refer�ncia cruzada para uma manpage para
            <var>name</var> na se��o <var>se��o</var>. Esta tag
            n�o possui nenhum conte�do; texto descrevendo a p�gina,
            tipicamente <tt><var>name</var>(<var>section</var>)</tt>, 
            ser� inserido no ponto onde <tt>&lt;manref&gt;</tt>
            aparece.
              </p>  
            </item> 

            <tag>
              <tt>&lt;email&gt;</tt> - endere�o de email
            </tag>
            <item>
              <p> 
            Indica que o texto contido � um endere�o de email.
            O conte�do da tag deve ser simplesmente o texto do 
            endere�o de email; marca��o de estilo de carecteres e
            refer�ncias cruzadas s�o pro�bidas. Normalmente a tag
            final <tt>&lt;/email&gt;</tt> n�o pode ser omitida, mas
            ela pode ser deixado fora quando aparece em uma
                <tt>&lt;author&gt;</tt> como o final de
                <tt>&lt;author&gt;</tt>, implicado pelo in�cio do pr�ximo
            elemento, implicando no final do endere�o de email.
              </p>
              <p> 
            Em muitos formatos isto ir� gerar uma refer�ncia cruzada
            que pode, por exemplo, ser usada para enviar 
            endere�o de e-mail para o endere�o citado. Em outros
            ser� simplesmente uma marca��o para um texto especial,
            inclu�ndo sinais de maior e menor <tt>&lt;</tt> 
            <tt>&gt;</tt> em torno dele.
              </p>
            </item>

            <tag>
              <tt>&lt;ftpsite&gt;</tt> - nome de site FTP an�nimo
            </tag>
            <tag> 
              <tt>&lt;ftppath&gt;</tt> - caminho dos arquivos
              de site FTP
            </tag>
           <item>
              <p> 
                <tt>&lt;ftpsite&gt;</tt> indica que o conte�do do 
            elemento � um nome DNS de um site FTP an�nimo, e
                <tt>&lt;ftppath&gt;</tt> que � um caminho no site.
            Ambos elementos n�o podem cont�r qualquer marca��o
            de estilo de car�cteres ou refer�ncia cruzada.
              </p>
              <p> 
            Tipicamente, ambos elementos ser�o feitos em fonte fixa;
            se poss�vel, o <tt>&lt;ftppath&gt;</tt> ser� feito em um
            hyperlink funcional para o site <tt>&lt;ftpsite&gt;</tt>.
              </p>
              <p> 
                <tt>&lt;ftppath&gt;</tt> deve sempre ser 
            precedido por um <tt>&lt;ftpsite&gt;</tt> no mesmo cap�tulo,
            ou ap�ndice, mas uma vez que um site foi nomeado,
            v�rios caminhos podem ser definidos.
              </p>
            </item>

            <tag>
              <tt>&lt;httpsite&gt;</tt> - Nome de site HTTP
            </tag>
            <tag> 
              <tt>&lt;httppath&gt;</tt> - caminho para o site HTTP
            </tag>
            <item>
              <p> 
                <tt>&lt;httpsite&gt;</tt> indica que o conte�do
            do elemento � um nome DNS de um site HTTP, e 
                <tt>&lt;httppath&gt;</tt> que � o caminho naquele
            site. Ambos elementos n�o podem cont�r qualquer
            estilo de car�cter ou refer�ncia cruzada.
              </p>
              <p> 
            Normalmente ambos elementos ser�o feitos em fonte fixa;
            Se poss�vel, <tt>&lt;httppath&gt;</tt> ser� feito em
            um hyperlink funcional para os arquivos nomeados
            no diret�rio mais recente.
                <tt>&lt;httpsite&gt;</tt>.
              </p>

       <p>
            Normalmente ambos elementos ser�o feitos em fonte fixa;
            Se poss�vel, <tt>&lt;httppath&gt;</tt> ser� feita em
            um hyperlink funcional para os arquivos nomeados
            no diret�rio mais recente.
                <tt>&lt;httpsite&gt;</tt>.
              </p>
              <p>
                <tt>&lt;httppath&gt;</tt> deve sempre ser procedida
            de um <tt>&lt;httpsite&gt;</tt> no mesmo cap�tulo
            ou ap�ndice, mas uma vez que o site foi nomeado 
            v�rios caminhos podem aparecer.
              </p>
            </item>

            <tag>
              <tt>&lt;url id="<var>id</var>"
              name="<var>name</var>"&gt;</tt> - URL
            </tag>
            <item>
              <p>
            Gera uma refer�ncia cruz para a URL com a 
            identifica��o <var>id</var> e usa o <var>name</var> 
            para ser mostrado no documento ao inv�s da id.  
            Est� tag nco cont�m qualquer conte�do.
              </p>
              <p>
            Normalmente este elemento ser� criado em fonte fixa;
            Se poss�vel, <var>id</var> ser� feito em um hyperlink
            funcional usando <var>name</var> como identifica��o.
              </p>
            </item>

          </taglist>
        </p>

      </sect>

      <sect id="footnotes">
        <heading>Footnotes</heading>

        <p>
        Footnotes pode aparecer em muitos textos inline, e s�o
        indicados por <tt>&lt;footnote&gt;<var>...
        </var>&lt;/footnote&gt</tt>.
        O texto de footnote ser� removido e colocado em outro 
        lugar (onde depende do formato), e troca com uma refer�ncia 
        ou hyperlink para footnote.
        </p>

        <p>
        O conte�do do footnote deve cont�r um ou mais par�grafos;
        o inicio do primeiro par�grafo n�o precisa ser marcado
        explicitamente. Elementos de marca��o inline como
        um estilo de car�cter n�o fazem efeito no conte�do do
        footnotes definido dentro dele - o footnote � um
        trecho limpo.
        </p>

        <p>
        Footnote pode ser aninhado, mas � raramente uma boa id�ia.
        </p>

      </sect>

    </chapt>
 
    <chapt id="listexamp">
      <heading>Lists e exemplos</heading>

      <sect id="lists">
        <heading>Listas</heading>
          
        <p>
        Existem tr�s tipos de listas:
          <list compact="compact">
            <item>
              <p> 
                <tt>&lt;list&gt;</tt> - lista ordin�ria (bola)
              </p>
            </item>
            <item> 
              <p>  
                <tt>&lt;enumlist&gt;</tt> - lista n�merada
              </p>
           </item>
            <item> 
              <p>
                <tt>&lt;taglist&gt;</tt> - lista com tags
              </p> 
            </item>
          </list>
        </p>

        <p>
        Cada entrada em uma lisa ordin�ria ou numerada � um item
        introduzido por <tt>&lt;item&gt;</tt>.  Cada entrada em uma
        lista � uma ou mais tags <tt>&lt;tag&gt;</tt>s seguida de
        um <tt>&lt;item&gt;</tt>.
        </p>

        <p>
        N�o � necess�rio marcar o final de <tt>&lt;tag&gt;</tt>
        ou elementos <tt>&lt;item&gt;</tt>.
        </p>

        <p>
        Todos os tr�s tipos de lista vem em dois tipos, dependendo
        se voc� especificar o atributo <tt>compact</tt> (eg,
          <tt>&lt;taglist compact&gt;</tt>) ou n�o (eg,
          <tt>&lt;enumlist&gt;</tt>).  A <tt>&lt;tag&gt;</tt> pode
        cont�r apenas texto marcado inline.
        </p>

        <p>
        As vers�es compactas s�o usadas dentro de par�grafos.
        O formatador n�o colocara "buracos" em torno da lista ou 
        entre os it�ns.<footnote><p>Existe um problema com isto 
        em HTML.  O formatador HTML n�o tenta, usando listas
        HTML com atributo <tt>compact</tt> que gerou, mas nem todos
        os navegadores o entendem corretamente. </p></footnote> As
        entradas em cada lista compacta devem ser um par�grafo simples
        (n�o � necess�rio para marcar o inicio de um par�grafo).
        Quaisquer listas dentro de uma lista compacta deve ser
        marcadas como compactas (embora os formatadores de fato deduzem
        que elas devem ser compactas).
        </p>


        <p>
        As vers�es n�o compactas s�o entendidas como par�grafos. 
        Cada entrada em tal lista pode cont�r mais que um 
        par�grafo (denovo, o inico do primeiro par�grafo n�o
        precisa ser marcado). A lista � separada do texto cercado,
        e as entradas de cada outro, por linhas em branco como devem
        ser esperadas para quebras de par�grafo.
        </p>

      </sect>
 
      <sect id="examples">
        <heading>Exemplos</heading>

        <p>
          Exemplos - fragmentos de c�digo multi linhas, scripts, e pegasM
        similares de um texto de computador s�o inclu�dos com
          <tt>&lt;example&gt;</tt> e finalizados com
          <tt>&lt;/example&gt;</tt>.
        </p>

        <p>
        O exemplo ser� formatado exatamente com ele � digitado,
        com espa�os e novas linhas reproduzidas. Ele ser� mostrado
        em uma fonte fixa, normalmente � usada pelo estilo de
        car�cteres <tt>&lt;tt&gt;</tt>, at� mesmo se o formatador
        usa uma fonte proporcional.  Qualquer identa��o que 
        pode ser inclu�da apropriadamente pelo formatador; o
        exemplo pode ser inclu�do iniciando na coluna da esquerda.
        </p>

        <p>
        Exemplos pode cont�r texto de car�cteres marcados mas n�o
        podem contar refer�ncias cruzadas ou elementos de estilo de 
        car�cteres <tt>&lt;em&gt;</tt>, <tt>&lt;strong&gt;</tt>,
          <tt>&lt;package&gt;</tt>, <tt>&lt;prgn&gt;</tt>,
          <tt>&lt;file&gt;</tt>, e <tt>&lt;tt&gt;</tt>. Vej� <ref
          id="inline">.
        </p>

        <p>
        Um exemplo n�o produz uma quebra de par�grafo; exemplos s�o
        considerados parte do par�grafo. Se um exemplo ser� um
        par�grafo ent�o seu pr�prio par�grafo inicia tags que 
        devem ser adicionadas como apropriadas.
        </p>

        <p>
        Qualquer car�cter de marca��o SGML no exemplo deve usual - 
        vej� <ref id="markup">. <tt>&lt;example&gt;</tt> 
        n�o funciona como o ambiente TeX's <tt>verbatim</tt>.
        </p>

      </sect>

    </chapt>

    <chapt id="unsupported">
      <heading>Car�cter�sticas SGML ainda n�o suportadas</heading>

      <p>
      O pacote <prgn>debiandoc-sgml</prgn> n�o suporta a car�cter�stica
        SUBDOC do SGML.  Embora � poss�vel declarar uma entity SUBDOC no
      documento e ent�o se referir a est� entity no corpo do documento, 
      a sa�da gerada pelos conversores do pacote n�o s�o t�o
      parecidas como o esperado.
      </p>

    </chapt>

  </book>

</debiandoc>
 

  


