<!-- 
  These are standard URLs for well-known resources on the Internet.
  Feel free to wrap some of these in linguistic conditionals if 
  versions for other languages are available.  The Debian URLs
  do content negotiation so it's not necessary there.
 -->


<!-- ******************************************
  Standard Debian URLs
  -->

<!-- set this to refer to a different Debian web site -->
<!-- these are disabled
<![ %lang-de [ <!entity www-debian-org "www.debian.de">
<![ %lang-es [ <!entity www-debian-org "www.es.debian.org"> ]]>
<![ %lang-fr [ <!entity www-debian-org "www.fr.debian.org"> ]]>
<![ %lang-ja [ <!entity www-debian-org "www.jp.debian.org"> ]]>
<![ %lang-pt [ <!entity www-debian-org "www.br.debian.org"> ]]>
<![ %lang-pt [ <!entity ftp-debian-org "ftp.br.debian.org"> ]]>
    -->
<!entity www-debian-org "www.debian.org">

<!entity url-debian-home "http://&www-debian-org;/">

<!entity ftp-debian-org "ftp.debian.org">
<!entity url-debian-ftp "ftp://&ftp-debian-org;/">

<!entity url-social-contract "http://&www-debian-org;/social_contract">

<!entity url-dfsg "&url-social-contract;#guidelines">

<![ %lang-ja [ <!entity url-debian-policy
  "http://www.debian.or.jp/Documents/debian-policy-ja/policy.ja.html/"> ]]>
<!entity url-debian-policy "http://&www-debian-org;/doc/debian-policy/">

<!entity url-new-maintainer "http://&www-debian-org;/doc/maint-guide/">

<!-- Debian Jr. Project -->
<!entity url-debian-jr "http://&www-debian-org;/devel/debian-jr/">

<!entity url-debian-faq "http://&www-debian-org;/doc/FAQ/">

<!entity url-bts "http://bugs.debian.org/">
<!entity url-bts-boot-floppies "&url-bts;boot-floppies">

<!-- broken for i386 (from the website, `i386 is not a port') -->
<!entity url-ports "http://&www-debian-org;/ports/&architecture;/">

<!entity email-boot-floppies-list "debian-boot@lists.debian.org">

<!-- base for the current distribution -->
<!entity disturlftp "ftp://&ftp-debian-org;/debian/dists/&releasename;/">
<!entity disturl    "http://http.us.debian.org/debian/dists/&releasename;/">

<!-- base for unstable -->
<!entity url-dist-unstable "ftp://ftp.debian.org/debian/dists/unstable/">

<!-- where to get the current boot-floppies -->
<!entity url-boot-floppies "&disturl;main/disks-&architecture;/current/">

<!-- boot-floppies CVSweb -->
<!entity url-bf-cvsweb "http://cvs.debian.org/boot-floppies/">

<!-- URL for README-CVS from CVS -->
<!entity url-bf-readme-cvs "http://cvs.debian.org/cgi-bin/viewcvs.cgi/~checkout~/boot-floppies/README-CVS?tag=HEAD%26content-type=text/plain">

<!-- instructions on where to get Debian -->
<!entity url-debian-distrib "http://&www-debian-org;/distrib/">

<!-- vendors that sell Debian CDs -->
<!entity url-debian-cd-vendors "http://&www-debian-org;/CD/vendors/">

<!-- Debian mirrors -->
<!entity url-debian-mirrors "http://&www-debian-org;/distrib/ftplist">

<!-- an example mirror w/o trailing slash -->
<!entity url-debian-mirror-eg "http://mirrors.kernel.org/debian">

<!-- release area off the Debian web site -->
<!entity url-release-area "http://&www-debian-org;/releases/&releasename;/">

<!-- WNPP -->
<!entity url-wnpp "http://&www-debian-org;/devel/wnpp/">

<!-- X strike force -->
<!entity url-x-strike-force "http://&www-debian-org;/~branden/">

<!-- release notes for this release -->
<!entity url-release-notes "&url-release-area;&architecture;/release-notes/">
<!-- older, deprecated name -->
<!entity url-upgrading "&url-release-notes;">

<!-- official www.debian.org location of the install manual -->
<!entity url-install-manual "&url-release-area;&architecture;/install">
<!entity url-dselect-beginner "&url-release-area;&architecture;/dselect-beginner">

<!-- relative locations of the manuals; this is tricky because in the -->
<!-- web build we can rely on content negotiation, whereas off a CD or -->
<!-- whathaveyou, we cannot -->
<![ %official-web-build [
  <!entity url-local-install-manual   "install">
  <!entity url-local-dselect-beginner "dselect-beginner">
]]>
  <!entity url-local-install-manual   "install&langext;.html">
  <!entity url-local-dselect-beginner "dselect-beginner&langext;.html">
  

<!entity url-debian-list-archives "http://lists.debian.org/">
<!-- older, deprecated name -->
<!entity url-list-archives "&url-debian-list-archives;">

<!entity url-debian-lists-subscribe "http://&www-debian-org;/MailingLists/subscribe">
<!entity url-list-subscribe "&url-debian-lists-subscribe;">

<!-- DDP project page -->
<!entity url-ddp "http://&www-debian-org;/doc/ddp">

<!-- Debian Jr. project -->
<!entity url-debian-jr "http://&www-debian-org;/devel/debian-jr/">

<!-- Debian Med project -->
<!entity url-debian-med "http://&www-debian-org;/devel/debian-med/">

<!-- APT pinning instructions -->
<!entity url-apt-pin-howto "http://&www-debian-org;/doc/manuals/apt-howto/ch-apt-get">

<!-- Securing Debian manual -->
<!entity url-securing-debian "http://&www-debian-org;/doc/manuals/securing-debian-howto/">

<!-- i18n coordination and info -->
<!entity url-debian-i18n "http://&www-debian-org;/international/">



<!--
  misc
  -->
<![ %lang-ja [ <!entity url-osd
  "http://www.geocities.co.jp/SiliconValley-PaloAlto/9803/osd-ja/osd-ja.html"> ]]>
<!entity url-osd "http://opensource.org/docs/definition_plain.html">

<!-- possibly use talking about bundled Debian -->
<!entity url-va-research "http://www.varesearch.com/">

<!-- GPL -->
<!entity url-gnu-copyleft "http://www.gnu.org/copyleft/gpl.html">

<!entity url-linux-journal "http://www.linuxjournal.com/">

<!-- Linux Kernel entities -->
<!entity url-linux-kernel-list-faq "http://www.tux.org/lkml/">
<!entity url-kernel-traffic "http://kt.zork.net/kernel-traffic/">

<!-- Introductions to the FSF and GNU Project -->

<!-- Linux History page -->
<!entity url-linux-history "http://www.li.org/linuxhistory.php">

<!-- Introductions to the FSF and GNU Project -->
<!entity url-fsf-intro "http://www.fsf.org/fsf/fsf.html">
<!entity url-gnu-intro "http://www.gnu.org/gnu/the-gnu-project.html">

<!-- Kernel archives -->
<!entity url-kernel-org "http://www.kernel.org/">

<!-- Linux Standard Base -->
<!entity url-lsb-org "http://www.linuxbase.org/">

<!-- Filesystem Hierarchy Standard -->
<!entity url-fhs-home "http://www.pathname.com/fhs/">

<!-- OPN -->
<!entity url-opn "http://www.openprojects.net/">
<!entity opn-irc-server "irc.openprojects.net">


<!-- ******************************************
  HOWTOs and FAQs (general)
  -->

<![ %lang-fr [
    <!entity url-ldp "http://www.freenix.org/unix/linux/">
    <!entity url-ldp-vo "http://www.tldp.org/"> ]]>
<!entity url-ldp "http://www.tldp.org/">

<![ %lang-ja [  <!entity url-jf "http://www.linux.or.jp/JF/"> ]]>

<![ %lang-ja [ <!entity url-net-howto
  "&url-jf;JFdocs/NET-3-HOWTO.html"> ]]>
<!entity url-net-howto "&url-ldp;HOWTO/NET-3-HOWTO.html">

<![ %lang-ja [ <!entity url-ethernet-howto
  "&url-jf;JFdocs/Ethernet-HOWTO.html"> ]]>
<!entity url-ethernet-howto "&url-ldp;HOWTO/Ethernet-HOWTO.html">

<![ %lang-ja [ <!entity url-ppp-howto
  "&url-jf;JFdocs/PPP-HOWTO.html"> ]]>
<!entity url-ppp-howto "&url-ldp;HOWTO/PPP-HOWTO.html">

<![ %lang-ja [ <!entity url-multidisk-howto
   "&url-jf;JFdocs/Multi-Disk-HOWTO.html"> ]]>
<!entity url-multidisk-howto "&url-ldp;HOWTO/Multi-Disk-HOWTO.html">

<![ %lang-ja [ <!entity url-boot-prompt-howto
  "&url-jf;JFdocs/BootPrompt-HOWTO.html"> ]]>
<!entity url-boot-prompt-howto "&url-ldp;HOWTO/BootPrompt-HOWTO.html">

<!entity url-linux-freebsd "&url-ldp;HOWTO/mini/Linux+FreeBSD-2.html">

<!entity url-unix-user-friendly
  "http://www.camelcity.com/~noel/usenet/cuuf-FAQ.htm">

<!entity url-unix-faq
  "ftp://rtfm.mit.edu/pub/usenet/news.answers/unix-faq/faq/">

<!entity url-scsi-faq
  "http://www.cis.ohio-state.edu/hypertext/faq/usenet/scsi-faq/top.html">

<!entity url-removed-packages
  "http://ftp-master.debian.org/removals.txt">

<!entity url-removed-nonus
  "http://non-us.debian.org/~troup/removals.txt">


<!entity url-linux-org-help "http://www.linux.org/help/">

<!-- possibly useful
	http://metalab.unc.edu/LDP/HOWTO/Access-HOWTO.html
        http://www.netbsd.org/Ports/i386/faq.html - multiboot
  -->

<!-- German specific -->
<![ %lang-de [
  <!entity url-german-ldp "http://www.tu-harburg.de/dlhp/">
  <!entity url-enhanced-german-howto "http://www.guug.de/~winni/linux/">
  <!entity url-lunetix "http://lhb.lunetix.de/LHB/">

  <!entity email-installmanual-de "installmanual-de@packages.debian.org">

  <!-- mirrors -->
  <!entity mirror-linux-tu-darmstadt-de
    "ftp://linux.mathematik.tu-darmstadt.de/pub/linux/distributions/debian/">
  <!entity mirror-ftp-rz-uni-karlsruhe-de
    "ftp://ftp.rz.uni-karlsruhe.de/pub/linux/mirror.debian/">
  <!entity mirror-ftp-tu-clausthal-de
    "ftp://ftp.tu-clausthal.de/pub/linux/debian/">
  <!entity mirror-ftp-uni-erlangen-de
    "ftp://ftp.uni-erlangen.de/pub/Linux/debian/">
  <!entity mirror-ftp-uni-mainz-de "ftp://ftp.uni-mainz.de/pub/Linux/">
  <!entity mirror-sunsite-cnlab-switch-ch
    "ftp://sunsite.cnlab-switch.ch/mirror/">
  <!entity mirror-gd-tuwien-ac-at
    "ftp://gd.tuwien.ac.at/opsys/linux/">
  <!entity mirror-debian-mur-at
    "ftp://debian.mur.at/">

  <!entity installmanual-de-base "http://www.infodrom.de/Debian/&release;">
  <!entity installmanual-de-html "&installmanual-de-base;/install-de.html">
  <!entity installmanual-de-ps "&installmanual-de-base;/install-de.ps">
  <!entity installmanual-de-ps-gz "&installmanual-de-base;/install-de.ps.gz">
  <!entity installmanual-de-txt "&installmanual-de-base;/install-de.txt">
  <!entity installmanual-de-txt-gz "&installmanual-de-base;/install-de.txt.gz">
  <!entity installmanual-de-tar-gz "&installmanual-de-base;/install-de.tar.gz">

  <!entity ldp-0 "http://pc1.chemie.uni-bielefeld.de/LDP/">
  <!entity ldp-1 "http://ldp.atnet.at/">
  <!entity ldp-2 "http://metalab.unc.edu/pub/Linux/docs/HOWTO/translations/de/">
  <!entity ldp-3 "http://www.mordor.ask.fh-furtwangen.de/LDP/">
  <!entity ldp-4 "http://www.go.dlr.de/linux/LDP/">
  <!entity ldp-5 "http://mailer.wiwi.uni-marburg.de/linux/LDP/">
  <!entity ldp-6 "http://www.tu-harburg.de/~semb2204/dlhp/">
  <!entity ldp-7 "http://www.asta.va.fh-ulm.de/LDP/">
]]>

<!-- ******************************************
  HOWTOs and FAQs (x86)
  -->

<!entity url-installation-guide "&url-ldp;LDP/gs/gs.html">

<!entity url-installation-howto "&url-ldp;HOWTO/Installation-HOWTO.html">

<![ %lang-ja [ <!entity url-cd-howto
  "&url-jf;JFdocs/CDROM-HOWTO.html"> ]]>
<!entity url-cd-howto "&url-ldp;HOWTO/CDROM-HOWTO.html">
<!entity url-cdrom-howto "&url-cd-howto;">

<![ %lang-fr [ <!entity url-partition-howto
  "&url-ldp-vo;HOWTO/mini/Partition/"> ]]>
<![ %lang-hr [ <!entity url-partition-howto
  "http://dokumentacija.linux.hr/Particije.html"> ]]>
<![ %lang-ja [ <!entity url-partition-howto
  "&url-jf;JFdocs/Partition.html"> ]]>
<!entity url-partition-howto "&url-ldp;HOWTO/mini/Partition/">

<![ %lang-fr [ <!entity url-partition-examples
  "&url-ldp-vo;HOWTO/mini/Partition/partition-5.html#SUBMITTED"> ]]>
<!entity url-partition-examples
  "&url-ldp;HOWTO/mini/Partition/partition-5.html#SUBMITTED">

<![ %lang-ja [ <!entity url-lilo-howto
  "&url-jf;JFdocs/LILO.html"> ]]>
<!entity url-lilo-howto "&url-ldp;HOWTO/mini/LILO.html">

<![ %lang-ja [ <!entity url-large-disk-howto
  "&url-jf;JFdocs/Large-Disk-HOWTO.html"> ]]>
<!entity url-large-disk-howto "&url-ldp;HOWTO/Large-Disk-HOWTO.html">

<!entity url-phoenix-bios-faq-large-disk "http://www.phoenix.com/pcuser/BIOS/biosfaq2.htm">

<![ %lang-fr [ <!entity url-invoking-bios-info
  "&url-ldp-vo;HOWTO/mini/Hard-Disk-Upgrade/install.html"> ]]>
<!entity url-invoking-bios-info "&url-ldp;HOWTO/mini/Hard-Disk-Upgrade/install.html">

<![ %lang-ja [ <!entity url-hardware-howto
   "&url-jf;JFdocs/Hardware-HOWTO.html"> ]]>
<!entity url-hardware-howto "&url-ldp;HOWTO/Hardware-HOWTO.html">

<![ %lang-fr [ <!entity url-pcmcia-howto
  "&url-ldp-vo;HOWTO/PCMCIA-HOWTO.html"> ]]>
<![ %lang-ja [ <!entity url-pcmcia-howto
  "&url-jf;JFdocs/PCMCIA-HOWTO.html"> ]]>
<!entity url-pcmcia-howto "&url-ldp;HOWTO/PCMCIA-HOWTO.html">

<!entity url-x86-laptop	"http://www.linux-laptop.net/">

<!-- this link is bad and not sure how to replace it! -->
<!entity url-linux-mca "http://www.dgmicro.com/mca/general-goods.html">
<!entity url-linux-mca-discussion
  "http://www.dgmicro.com/linux_frm.htm">

<!-- "the link is valid but information is stale. The archive has been
removed and text documention at link directs one to a URL that is broken." -->
<!entity url-pc-hw-faq
  "http://www.faqs.org/faqs/pc-hardware-faq/part1/">
<!--
  "http://www.cis.ohio-state.edu/hypertext/faq/usenet/pc-hardware-faq/top.html"
  -->

<!-- there are a ton of other multiboot-oriented HOWTOs; are they useful?
     http://metalab.unc.edu/LDP/HOWTO/mini/Linux+DOS+Win95+OS2.html
     http://metalab.unc.edu/LDP/HOWTO/mini/Linux+FreeBSD.html
     http://metalab.unc.edu/LDP/HOWTO/mini/Linux+NT-Loader.html
     http://metalab.unc.edu/LDP/HOWTO/mini/Linux+Win95.html
     http://metalab.unc.edu/LDP/HOWTO/mini/Loadlin+Win95.html
     http://metalab.unc.edu/LDP/HOWTO/mini/Multiboot-with-LILO.html
  -->     

<!entity url-windows-refund "http://www.linuxmall.com/refund/">

<!entity url-xfree86 "http://www.xfree86.org/">
<!-- older, deprecated name -->
<!entity url-xfree-support "&url-xfree86;">

<!entity url-xfree86-cardlist "&url-xfree86;cardlist.html">

<!entity url-simtel "ftp://ftp.simtel.net/pub/simtelnet/msdos/">


<!-- ******************************************
  m68k links
   urls that I need to absorb or point to:
	http://www.informatik.uni-oldenburg.de/~amigo/debian_inst.html
	http://www.linux-m68k.org/debian-atari.html
	http://www.linux-m68k.org/debian-mac.html
        http://www.mac.linux-m68k.org/docs/debian-mac68k-install.html
        http://www.mac.linux-m68k.org/docs/debian-mac-install.html
	ftp://baltimore.wwaves.com/pub/MacLinux/debian-support/Debian-2.0-Mac-Install.html (outdated by above?)
        http://www.sleepie.demon.co.uk/linuxvme/

   NetBSD has some nice documentation:
	http://www.netbsd.org/Ports/atari/faq.html
	ftp://ftp.NetBSD.ORG/pub/NetBSD/NetBSD-1.3.3/atari/INSTALL
	http://www.netbsd.org/Ports/amiga/index.html (sup/unsup hardware)
	ftp://ftp.netbsd.org/pub/NetBSD/NetBSD-1.3.3/mvme68k/INSTALL
  -->

<!entity url-m68k-faq "http://www.linux-m68k.org/faq/faq.html">

<!entity url-m68k-mac "http://www.mac.linux-m68k.org/"> 

<!entity url-m68k-install-guide
  "http://www.informatik.uni-oldenburg.de/~amigo/InstGuide/">

<!-- old amiga install guide, for credits only -->
<!entity url-m68k-old-amiga-install "http://www.informatik.uni-oldenburg.de/~amigo/debian_inst.html">

<!-- not used
  <!entity url-m68k-port "http://debian-m68k.nocrew.org/">
 -->



<!-- ******************************************
  alpha links
   urls that I need to absorb or point to:
         [No longer]
   also interesting
	http://classnet.med.miami.edu:8080/alpha/
	http://www.redhat.com/support/docs/rhl/alpha/rh52-hardware-alpha.html
	http://www.azstarnet.com/~axplinux/alpha-sys.html
  -->

<![ %lang-ja [ <!entity url-alpha-faq
  "&url-jf;JFdocs/LinuxAlpha-FAQ.html"> ]]>
<!entity url-alpha-faq "http://linux.iol.unh.edu/linux/alpha/faq/">

<!entity url-alpha-debian "http://&www-debian-org;/ports/alpha/">

<!entity url-alpha-loic "http://lhpca.univ-lyon1.fr/axp/debian.html">

<!entity url-alpha-loic-guide "http://lhpca.univ-lyon1.fr/axp/README">

<!entity url-alpha-marc "http://www.bard.org.il/~marc/html/DebAlphaHOWTO.html/">

<!entity url-milo-howto "&url-ldp;HOWTO/MILO-HOWTO.html">

<!entity url-alpha-howto "&url-ldp;HOWTO/Alpha-HOWTO.html">

<!entity url-srm-howto "&url-ldp;HOWTO/SRM-HOWTO.html">

<!entity url-alpha-firmware "http://ftp.digital.com/pub/DEC/Alpha/firmware/">

<!entity url-jensen-howto "http://www.linuxalpha.org/faq/FAQ-9.html">

<!entity url-lightweight-rtc-patch "ftp://genie.ucd.ie/pub/alpha/patches-2.2.x/rtclight-2.2.12.diff">

<!-- ******************************************
  sparc links
  http://www.netbsd.org/Documentation/network/netboot/index.html -
    tftp setup for NetBSD
  http://www.geog.ubc.ca/s_linux/howto/netboot.html
  http://users.ai-lab.fh-furtwangen.de/~eble/sparclinux/installation-howto.html
  -->

<!entity url-sparc-linux "http://www.ultralinux.org/">

<![ %lang-ja [
  <!entity url-sparc-linux-faq "http://home.att.ne.jp/red/world/sparc/"> ]]>
<!entity url-sparc-linux-faq "&url-sparc-linux;faq.html">

<!entity url-sun-faqs
  "http://metalab.unc.edu/pub/sun-info/sun-faq/FAQs/">

<!entity url-openboot
  "http://docs.sun.com/?p=/coll/216.2">

<!entity url-sun-nvram-faq
  "http://www.squirrel.com/sun-nvram-hostid.faq.html">


<!-- ******************************************
  powerpc links
  -->

<!entity url-powerpc-linux-faq 
  "http://www.dartmouth.edu/cgi-bin/cgiwrap/jonh/lppc/faq.pl">

<!entity url-powerpc-yaboot-faq
  "http://penguinppc.org/projects/yaboot/doc/yaboot-howto.shtml/">

<!entity url-powerpc-quik-faq
  "http://penguinppc.org/projects/quik/">

<!entity url-powerpc-debian 
   "http://&www-debian-org;/ports/powerpc/">

<!entity url-netbsd-powerpc-faq 
   "http://www.netbsd.org/Ports/macppc/faq.html">

<!entity url-mac-fdisk-tutorial
  "http://penguinppc.org/projects/yaboot/doc/mac-fdisk-basics.shtml">

<!entity url-powerpc-of 
   "http://prdownloads.sourceforge.net/debian-imac/">

<!entity url-powerpc-suntar 
   "http://hyperarchive.lcs.mit.edu/HyperArchive/Archive/cmp/suntar-223.hqx">

<!entity url-powerpc-resedit 
   "http://www.resexcellence.com/resedit.shtml">

<!entity url-powerpc-creator-changer 
   "ftp://uiarchive.uiuc.edu/mirrors/ftp/ftp.info-mac.org/info-mac/disk/creator-changer-284.hqx">

<!entity url-powerpc-diskcopy 
   "http://download.info.apple.com/Apple_Support_Area/Apple_Software_Updates/English-North_American/Macintosh/Utilities/Disk_Copy/Disk_Copy_6.3.3.smi.bin">

<!entity url-powerpc-ppcbug "no URL">

<!entity url-powerpc-bootx
   "http://penguinppc.org/projects/bootx/">


<!-- ******************************************
  mips links
    -->

<!entity url-mips-howto 
   "http://oss.sgi.com/mips/mips-howto.html">

 <!entity url-indigo2-howto 
   "http://oss.sgi.com/mips/i2-howto.html">


<!-- OBSOLETE DE STUFF -->
<![ %lang-de [ 

  <!entity url-debian-home-de "http://www.debian.de/">
  <!entity url-debian-home-at "http://www.debian.at/">
  <!entity url-lunetix "http://www.lunetix.de/docs/LHB/">
  <!entity url-german-ldp "http://www.tu-harburg.de/dlhp/">
  <!entity url-enhanced-german-howto "http://www.guug.de/~winni/linux/">

  <!entity email-installmanual-de "installmanual-de@packages.debian.org">

<!-- There is absolutely no reason why these appear, except that the
     documentation is broken. -->
  <!entity url-lunetix "DE DOCUMENTATION NEEDS SYNCING">
  <!entity mirror-linux-tu-darmstadt-de "DE DOCUMENTATION NEEDS SYNCING">
  <!entity mirror-ftp-rz-uni-karlsruhe-de "DE DOCUMENTATION NEEDS SYNCING">
  <!entity mirror-ftp-tu-clausthal-de "DE DOCUMENTATION NEEDS SYNCING">
  <!entity mirror-ftp-uni-erlangen-de "DE DOCUMENTATION NEEDS SYNCING">
  <!entity mirror-ftp-uni-mainz-de "DE DOCUMENTATION NEEDS SYNCING">
  <!entity mirror-sunsite-cnlab-switch-ch "DE DOCUMENTATION NEEDS SYNCING">
  <!entity ldp-0 "DE DOCUMENTATION NEEDS SYNCING">
  <!entity ldp-1 "DE DOCUMENTATION NEEDS SYNCING">
  <!entity ldp-2 "DE DOCUMENTATION NEEDS SYNCING">
  <!entity ldp-3 "DE DOCUMENTATION NEEDS SYNCING">
  <!entity ldp-4 "DE DOCUMENTATION NEEDS SYNCING">
  <!entity ldp-5 "DE DOCUMENTATION NEEDS SYNCING">
  <!entity ldp-6 "DE DOCUMENTATION NEEDS SYNCING">
  <!entity ldp-7 "DE DOCUMENTATION NEEDS SYNCING">
  <!entity ldp-8 "DE DOCUMENTATION NEEDS SYNCING">

]]> 

<!-- ****************************************** s390 links -->
  
<!entity url-s390-distributions
   "http://www.redbooks.ibm.com/pubs/pdfs/redbooks/sg246264.pdf">

<!entity url-s390-linux
   "http://www.redbooks.ibm.com/pubs/pdfs/redbooks/sg244987.pdf">

<!entity url-s390-devices
   "http://oss.software.ibm.com/developerworks/opensource/linux390/docu/l390dd08.pdf">

<!entity url-s390-tech
   "http://oss.software.ibm.com/developerworks/opensource/linux390/documentation-2.4.shtml">

<!entity url-s390-developerworks
   "http://oss.software.ibm.com/developerworks/opensource/linux390/index.shtml">

<!-- Keep this comment at the end of the file
Local variables:
mode: sgml
End:
-->
