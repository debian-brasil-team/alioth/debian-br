<!-- mantenha estes coment�rios para controle de revis�o de tradu��es -->
<!-- original version: 1.92 -->
<!DOCTYPE debiandoc PUBLIC "-//DebianDoc//DTD DebianDoc//EN" [
 <!entity % versiondata  SYSTEM "dynamic.ent" > %versiondata;
 <!entity % messagesdata SYSTEM "messages.ent"> %messagesdata;
 <!entity % pt-specific  SYSTEM "pt/pt.ent"   > %pt-specific;
 <!entity % defaultsdata SYSTEM "defaults.ent"> %defaultsdata;
 <!entity % urlsdata     SYSTEM "urls.ent"    > %urlsdata;

 <!-- Dados textuais das entities -->
 <!entity ch-welcome       SYSTEM "pt/welcome.sgml">
 <!entity ch-hardware      SYSTEM "pt/hardware.sgml">
 <!entity ch-preparing     SYSTEM "pt/preparing.sgml">
 <!entity ch-inst-methods  SYSTEM "pt/inst-methods.sgml">
 <!entity ch-rescue-boot   SYSTEM "pt/rescue-boot.sgml">
 <!entity ch-partitioning  SYSTEM "pt/partitioning.sgml">
 <!entity ch-kernel        SYSTEM "pt/kernel.sgml">
 <!entity ch-boot-new      SYSTEM "pt/boot-new.sgml">
 <!entity ch-post-install  SYSTEM "pt/post-install.sgml">
 <!entity ch-tech-info     SYSTEM "pt/tech-info.sgml">
 <!entity ch-appendix      SYSTEM "pt/appendix.sgml">
 <!entity ch-administrivia SYSTEM "pt/administrivia.sgml">

 <!entity FIXME "<![ %FIXME [ <p><em>A Documenta��o ainda n�o 
  est� completa, texto faltando.</em> ]]>">

 <!-- se essa arquitetura possui ou n�o um disquete -->
 <![ %s390   [ <!entity floppies "fitas">   ]]>
               <!entity floppies "disquetes">

 <!entity cvsrev "$Revision: 1.1.1.1 $">
 <!-- 
        conven��es do documento:
          arquivo,diret�rio,dispositivo == file
          programa,comando == prgn
          vari�vel == var
          pacote == package
          sa�da do programa == tt
          entrada pelo teclado == em, um exemplo longo
          entrada de comando == tt

          corre��es de itens: <![ %FIXME [ ... ]]>
          corre��es de plataformas:   <![ %architecture; [ FIXME: ...]]>
  

   NOTAS SOBRE ESTE DOCUMENTO:
   
A tradu��o deste documento para o Idioma Portugu�s foi realizada por:
   Gleydson Mazioli da Silva (gleydson@debian.org / gleydson@focalinux.org)

As atualiza��es desta tradu��o feita pela task force pt_BR foram feitas por:
   Gleydson Mazioli da Silva (gleydson@debian.org)
    index.pt.sgml
    inst-methods.sgml
    post-install.sgml
    appendix.sgml

   Andre Luiz Lopes (andrelop@ig.com.br)
    hardware.sgml
    welcome.sgml

   Luis Alberto Garcia Cipriano (lacipriano@uol.com.br)
    administrivia.sgml
    boot-new.sgml

   Paulo Rog�rio Ormenese (pormenese@uol.com.br)
    kernel.sgml
    preparing.sgml

   Marcio Roberto Teixeira (marciotex.l1@pro.via-rs.com.br)
    partitioning.sgml


	 
 -->
]>

<debiandoc>
  <book>
   <titlepag>
    <title>
     Instalando &debian; &release; para &arch-title;
    <author>
      <name>Bruce Perens</name>
      <email></email>
    </author>
    <author>
      <name>Sven Rudolph</name>
      <email></email>
    </author>
    <author>
      <name>Igor Grobman</name>
      <email></email>
    </author>
    <author>
      <name>James Treacy</name>
      <email></email>
    </author>
    <author>
      <name>Adam Di Carlo</name>
      <email></email>
    </author>
    <version>vers�o &docversion;, &docdate;</version>
   <abstract>
Este documento cont�m instru��es de instala��o do sistema Debian GNU/Linux 
&release;, para arquiteturas &arch-title; ("&architecture;"). Tamb�m 
contem apontadores para maiores informa��es e instru��es de como obter
mais de seu novo sistema Debian.
<![ %not-powerpc [
<![ %not-arm [
<![ %not-hppa [
Os procedimentos neste documento <em>n�o</em> s�o indicados para serem
usados por usu�rios atualizando sistemas existentes; se voc� est�
atualizando, veja o documento <url id="&url-release-notes;" 
name="Notas de Lan�amento para o Debian &release;">.
]]>
]]>
]]>
      </abstract>
      <copyright>
  <copyrightsummary>
Este documento pode ser distribu�do ou modificado sobre os termos da
Licen�a P�blica Geral GNU.
Public Licence.
  <copyrightsummary>
&copy; 1996 Bruce Perens
  <copyrightsummary>
&copy; 1996, 1997 Sven Rudolph
  <copyrightsummary>
&copy; 1998 Igor Grobman, James Treacy
  <copyrightsummary>
&copy; 1998-2002 Adam Di Carlo 
</copyrightsummary>
  <p>
Este manual � software livre; voc� pode redistribui-lo e/ou modifica-lo de 
acordo com os termos da Licen�a P�blica Geral GNU como publicada pela Free 
Software Foundation; , vers�o 2 da licen�a ou (a crit�rio do autor) qualquer
vers�o posterior.
  <p>
Este manual � distribu�do com a iten��o de ser �til ao seu utilizador, no 
entanto <em>N�O TEM NENHUMA GARANTIA</em>, EXPL�CITAS OU IMPL�CITAS,
COMERCIAIS OU DE ATENDIMENTO A UMA DETERMINADA FINALIDADE. Consulte a
Licen�a P�blica Geral GNU para maiores detalhes. 
  <p>
Uma c�pia da Licen�a P�blica Geral GNU est� dispon�vel em
<file>/usr/share/common-licenses/GPL</file> na distribui��o &debian; ou
no <url id="http://www.gnu.org/copyleft/gpl.html" name="website da GNU">
na Web. Voce tamb�m pode obter uma c�pia escrevendo para a Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
  <p>
N�s requeremos que voc� atribua qualquer material derivado deste documento
� Debian e seus autores. Se voc� modificar e melhorar este documento, n�s
requisitamos que os autores sejam notificados, pelo e-mail
<email>&email-boot-floppies-list;</email>.
  <p>
Tradu��o inicial feita integralmente para o idioma portugu�s por:
Gleydson Mazioli da Silva <email>gleydson@debian.org</email>.
 <p>
A atualiza��o deste documento para o lan�amento da woody foi feita 
pela equipe task force do Debian l10n:
A tradu��o deste documento para o Idioma Portugu�s foi realizada por:
   Gleydson Mazioli da Silva (gleydson@cipsga.org.br / gleydson@focalinux.org)

As atualiza��es desta tradu��o feita pela task force pt_BR foram feitas por:
<list compact>
  <item>Andre Luiz Lopes <email>andrelop@ig.com.br</email>
  <item>Gleydson Mazioli da Silva <email>gleydson@debian.org</email>
  <item>Luis Alberto Garcia Cipriano <email>lacipriano@uol.com.br</email>
  <item>Marcio Roberto Teixeira <email>marciotex.l1@pro.via-rs.com.br</email>
  <item>Paulo Rog�rio Ormenese <email>pormenese@uol.com.br</email>
</list>

<![ %alpha [
	<p>
A tabela da arquitetura Alpha foi derivada de informa��es
contribuidas de Jay Estabrook, com a devida permiss�o. ]]>

    <toc detail="sect">

&ch-welcome;
 
&ch-hardware;

&ch-preparing;

&ch-inst-methods;

&ch-rescue-boot;

&ch-partitioning;

&ch-kernel;

&ch-boot-new;

&ch-post-install;

&ch-tech-info;

&ch-appendix;

&ch-administrivia;


  </book>
</debiandoc>

<!-- Deixe este coment�rio no final do arquivo
Local variables:
mode: sgml
sgml-omittag:t
sgml-shorttag:t
sgml-namecase-general:t
sgml-general-insert-case:lower
sgml-minimize-attributes:max
sgml-always-quote-attributes:t
sgml-indent-step:2
sgml-indent-data:nil
sgml-parent-document:nil
sgml-declaration:nil
sgml-exposed-tags:nil
sgml-local-catalogs:nil
sgml-local-ecat-files:nil
End:
-->
<!--  LocalWords:  scalably Macintoshes Mainboards VLB VL EC Ansel CBFF Di XL
 -->
<!--  LocalWords:  versiondata messagesdata defaultsdata urlsdata Estabrook xlt
 -->
<!--  LocalWords:  ALPHAbook ALCOR alcor AVANTI avanti EB eb AlphaPC pc LX lx
 -->
<!--  LocalWords:  SX sx cabriolet AlphaPCI DECpc jensen MIKASA xxx mikasa UDB
 -->
<!--  LocalWords:  NONAME AXPpci noname NORITAKE noritake PWS au miata RPX UX
 -->
<!--  LocalWords:  Deskstation Samsung BX TAKARA takara xl MILO SRM SmartCache
 -->
<!--  LocalWords:  SmartRAID DPT GB MFM RLL IDE ATA FastRAM GVP Zorro amiboot
 -->
<!--  LocalWords:  NICs ThunderLAN HiSax Spellcaster ThinkPad WinModem SIMMs KB
 -->
<!--  LocalWords:  SIMM TODO APM Turbo Cyrix LFB MacOS spammed LBA ISPs fips fs
 -->
<!--  LocalWords:  Unzip RESTORRB EXE TXT defrag defragmenter dbootstrap cfdisk
 -->
<!--  LocalWords:  dselect ext Minix resc dd bs conv sync vold rawrite Alt DCLs
 -->
<!--  LocalWords:  unbootable linux thinkpad screenful ramdisk fdisk ls sbin cd
 -->
<!--  LocalWords:  tcic MSG insmod rmmod GMT ppp ISP pppconfig zmore ATDT pon
 -->
<!--  LocalWords:  plog poff perl firewalling xzf xconfig menuconfig kpkg lilo
 -->
<!--  LocalWords:  rdev Administrivia browsable
 -->
