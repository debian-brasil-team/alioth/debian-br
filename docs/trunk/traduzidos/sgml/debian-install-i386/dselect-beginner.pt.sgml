<!DOCTYPE debiandoc PUBLIC "-//DebianDoc//DTD DebianDoc//EN" [
<!entity % versiondata  SYSTEM "dynamic.ent" > %versiondata;
<!entity % messagesdata SYSTEM "messages.ent"> %messagesdata;
<!entity % defaultsdata SYSTEM "defaults.ent"> %defaultsdata;
<!entity % urlsdata     SYSTEM "urls.ent"    > %urlsdata;
<!--
        Conven��es do documento:
          arquivo, diret�rio, parti��o == file
          programa, comando == prgn
          sa�da do comando == tt
          entrada de teclas == em
          entrada de comando == tt
          vari�vel == var
          pacote == package

   -->
]>

<debiandoc>
  <book>
    <titlepag>
      <title>
       <prgn>dselect</prgn> Documenta��o para Iniciantes
      <author>
        <name>St�phane Bortzmeyer</name>
        <email>bortzmeyer@debian.org</email>
      </author>
      <author>
        <name>Gleydson Mazioli da Silva (Tradutor)</name>
        <email>gleydson@cipsga.org.br</email>
      </author>
    </titlepag>


    <chapt id="dselect-intro">Introdu��o
    <p>
Este arquivo documenta o <prgn>dselect</prgn> para usu�rios que n�o
tiveram contato com este programa, e tem a iten��o de ajudar a
instalar a Debian com sucesso. Ele n�o tenta explicar tudo, assim 
quando voc� entrar pela primeira vez no <prgn>dselect</prgn>, veja
as telas de ajuda.
      <p>
<prgn>dselect</prgn> � usado para selecionar quais pacotes voc� deseja
instalar (estes est�o atualmente em torno de &num-of-distrib-pkgs; 
pacotes na Debian &release;). Ele ser� executado durante a instala��o 
e como ele � muito poderoso e um pouco complexo coisa que pode ser 
usada para bem ou para mal; algum conhecimento sobre ele � altamente 
recomendado. O uso descuidado do <prgn>dselect</prgn> pode bagun�ar 
seu sistema.
      <p>
<prgn>dselect</prgn> guiar� voc� durante o processo de instala��o de
pacotes seguindo:

<list>
	  <item>Choose the access method to use (escolher o m�todo de 
              acesso a ser utilizado).
	  <item>Update list of available packages, if possible. (atualizar
              a lista de pacotes dispon�veis, se poss�vel).
	  <item>Request which packages you want on your system. (Pedir quais
              pacotes quer em seu sistema).
	  <item>Install and upgrade wanted packages. (instalar e atualizar
              pacotes procurados).
	  <item>Configure any packages that are unconfigured. (configurar
              qualquer pacote que estiver desconfigurado).
	  <item>Remove unwanted software. (remover programas n�o desejados).
</list>
Como cada passo � completado com sucesso ele o conduzir� para o pr�ximo.
Siga-os em ordem sem pular qualquer passo.
      <p>
Aqui e l� neste documento n�s falamos de inicializar outro shell.
Linux tem 6 se��es de terminais ou shells dispon�veis a qualquer
hora. Voc� pode alternar entre um e outro pressionando 
<em>Alt-esquerdo-F1</em> at� <em>Alt-esquerdo-F6</em>, depois disso
voc� deve se logar em seu novo shell e continuar. O console usado 
pelo programa de instala��o � o primeiro, a.k.a., tty1, assim 
pressione <em>Alt-esquerdo-F1</em> quando desejar retornar para 
aquele console.


    <chapt id="dselect-main">Ap�s o <prgn>dselect</prgn> ser carregado
      <p>
Uma vez no <prgn>dselect</prgn> voc� ver� esta tela:

<example>
Debian Linux "dselect" package handling frontend.

0.  [A]ccess  Choose the access method to use.
1.  [U]pdate  Update list of available packages, if possible. 
2   [S]elect  Request which packages you want on your system.
3.  [I]nstall Install and upgrade wanted packages. 
4.  [C]onfig  Configure any packages that are unconfigured. 
5.  [R]emove  Remove unwanted software.
6.  [Q]uit    Quit dselect.
</example>

D� uma olhada nestas op��es uma a uma.


      <sect id="access">``Access'' (acesso)
	<p>
Esta � a tela Access:

<example>
dselect - list of access methods
  Abbrev.        Description
  cdrom          Install from a CD-ROM.
* multi_cd       Install from a CD-ROM set.
  nfs            Install from an NFS server (not yet mounted).
  multi_nfs      Install from an NFS server (using the CD-ROM set) (not yet mounted).
  harddisk       Install from a hard disk partition (not yet mounted).
  mounted        Install from a filesystem which is already mounted.
  multi_mount    Install from a mounted partition with changing contents.
  floppy         Install from a pile of floppy disks.
  apt            APT Acquisition [file,http,ftp]
</example>

	<p>
Aqui n�s dizemos onde os pacotes do <prgn>dselect</prgn> est�o.
Por favor ignore a ordem que eles aparecem. � muito importante
que voc� selecione o m�todo apropriado para a instala��o.
Voc� pode ter listado alguns m�todos a mais, ou alguns a menos, ou 
pode ve-los listados em uma ordem diferente; apenas n�o tema isto.
Na lista seguinte, n�s descrevemos os diferentes m�todos.

<taglist>
	    <tag>multi_cd</tag>
	    <item>
Bastante grande e poderoso, este m�todo complexo � o meio recomendado
para instalar uma vers�o recente da Debian de diversos CDs bin�rios.
Cada um destes CDs devem conter em si pr�prios informa��es sobre seus 
pacotes e todos CDs anteriores (no arquivo <file>Packages.cd</file>).
Quando voc� selecionar primeiro este m�todo, tenha certeza que o CD-Rom
que est� usando n�o esta montado. Coloque o �ltimo disco <em>bin�rio</em> 
da configura��o (n�s n�o precisamos dos CDs dos fontes) na unidade
e responda as quest�es que for peguntado:

<list>
 <item>
  CD-ROM drive location (localiza��o da unidade de CD-Rom)
 <item>
  Confirmation that you are using a multi-cd set (confirma��o que est� usando
  uma configura��o com m�ltiplos CDs)
 <item>
  The location of the Debian distribution on the disk(s) (a localiza��o
  da distribui��o debian no disco).
 <item>
  [ Possibly ] the location(s) of the Packages file(s) (a localiza��o dos
  arquivos de pacotes).
</list>
    <p>
Uma vez que voc� atualizar a lista dispon�vel e selecionar os pacotes
para serem instalados, o m�todo multi-cd far� o procedimento normal.
Voc� pode precisar executar o passo "install" para cada um dos CDs
que for inserido. Infelizmente devido as limita��es do dselect ele
n�o pode perguntar a voc� por um novo disco em cada est�gio; o 
meio para trabalhar com cada disco �:

<list>
 <item>
  Insira o CD na sua unidade de CD-Rom.
 <item>
  Atrav�s do menu principal do dselect, selecione "install" (instalar).
 <item>
  Aguarde at� que o dpkg finalize a instala��o atrav�s deste CD (ele pode
  relatar sucesso na instala��o, ou poss�velmente erros na instala��o.
  N�o se preocupe com estes erros a n�o ser depois)
 <item>
  Pressione [Enter] para voltar para o menu principal do dselect.
 <item>
  Repita com o pr�ximo CD da configura��o...
</list>
    <p>
Pode ser necess�rio executar o passo de instala��o mais que uma
vez para obter a ordem da instala��o dos pacotes - alguns pacotes
j� instalados podem precisar ter outros pacotes instalados antes
de serem configurados corretamente.
	      <p>
� recomendado executar o passo "Configure" (configurar), para ajudar
a corrigir qualquer pacote que foi terminado nesta etapa.


	    <tag>multi_nfs, multi_mount</tag>
	    <item>
Estes s�o muito parecidos com o m�todo multi-cd acima, e s�o
refinamentos do tema de copia com altera��o de m�dia, por exemplo se
estiver instalando atrav�s de uma configura��o multi-cd exportada
via NFS do CD-Rom de outra m�quina.


	    <tag>apt</tag>
	    <item>
Uma das melhores op��es para a instala��o atrav�s um mirror local dos
arquivos da Debian, ou da rede. Este m�todo usa o sistema "apt" 
para completar a an�lise de depend�ncias e ordem, assim � prov�vel
que ele instale os pacotes em uma �tima ordem.
	      <p>
A configura��o deste m�todo � diretamente-progressiva; voc� pode
selecionar qualquer n�mero de diferentes localiza��es, misturando
e unindo URLs <tt>file:</tt> (discos locais ou discos NFS montados),
URLs <tt>http:</tt>, ou URLs <tt>ftp:</tt>. Note no entanto que as
op��es HTTP e FTP n�o suportam autentica��o local em proxies.
	      <p>
Se voc� tem um servidor proxy para http ou ftp (ou ambos), tenha
certeza que voc� configurou as vari�veis de ambiente <tt>http_proxy</tt> 
ou <tt>ftp_proxy</tt> respectivamente. Configure-as atrav�s de seu shell
antes de iniciar o dselect, i.e.:
<example>
 # export http_proxy=http://gateway:3128/
 # dselect
</example>


	    <tag>floppy (disquete)</tag>
	    <item>
Recomendado para as pessoas sem CD-Rom ou acesso a rede. N�o 
recomendado como uma op��o vi�vel de instala��o se voc� est�
usando disquetes com tamanhos tradicionais, mas pode funcionar
bem com LS/120 ou Zip drives. Especifique a localiza��o de sua
unidade de discos flex�veis, ent�o insira os disquetes. O
primeiro cont�m o arquivo de pacotes. Este m�todo � lento e 
podem ocorrer problemas devido a problemas de m�dia.


<![ IGNORE [ <tag>ftp</tag>
	    <item>
<strong>M�TODO N�O RECOMENDADO -- use o apt ao inv�s deste.
Somente tente-o se todos os outros falham.</strong>
	      <p>
Voc� ser� perguntdo para entrar com o endere�o do site ftp, se
voc� deseja usar o modo passivo (para ftp proxy), uma combina��o
nome_do_usuario/senha, o caminho do diret�rio da Debian, a lista de
distribui��es que est� interessado em incluir para copiar os
arquivos bin�rios dos pacotes (relativo a /var/lib/dpkg/methods/ftp)
	      <p>
O script de configura��o tentar� imediatamente se conectar ao
servidor remoto para obter os arquivos de Pacotes, etc, ent�o o
dselect se reconectar� depois quando voc� iniciar a instala��o
dos pacotes.
	      <p>
Se precisar trabalhar com um firewall, este m�todo funcionar�
bem, e � ideal para pessoas sem muito espa�o no disco local.]]>


<![ IGNORE [ <tag>http</tag>
	    <item>
<strong>M�TODO N�O RECOMENDADO -- use o apt ao inv�s deste. Somente tente 
este m�todo se os outros falham</strong>
	      <p>
Similar ao ftp, mas usar� uma mistura de URLs <tt>http:</tt>,
<tt>ftp:</tt> e <tt>file:</tt> para procurar os arquivos da Debian.
Voc� pode configurar, se necess�rio, o uso de diferentes proxies para 
URLs <tt>http:</tt> e <tt>ftp:</tt>. Muito �til se um proxy local pode
fazer o cache de pacotes para m�ltiplos usu�rios, ou se o mirror
somente permite o acesso http e n�o o ftp. ]]>


	    <tag>nfs</tag>
	    <item>
<strong>M�TODO N�O RECOMENDADO -- use o apt ou multi_nfs ao inv�s deste.  
Somente tente este m�todo se todos os outros falham.</strong>
	      <p>
Este � um m�todo de instala��o simples, com requerimentos simples: 
passe a ele o endere�o do servidor NFS, a localiza��o da distribui��o
Debian no servidor e (talvez) o(s) arquivo(s) de pacote(s). Ent�o o
dselect instalar� as v�rias se��es atrav�s servidor. Lento mas f�cil;
n�o usa a ordem pr�pria, assim ele pode executar muitas vezes o passo
"Install" e/ou "Configure". Obviamente somente apropriado para
instala��es baseadas na NFS.


	    <tag>harddisk</tag>
	    <item>
<strong>M�TODO N�O RECOMENDADO -- use o apt ou multi_mount ao inv�s deste. 
Somente tente este m�todo se os outros falham</strong>
	      <p>
Forne�a o dispositivo de bloco da parti��o do disco r�gido a ser usado,
bem como a localiza��o dos arquivos da Debian naquela parti��o. Lento
e f�cil. N�o usa a ordem correta, assim ele poder� executar v�rias 
vezes os passos "Install" e/ou "Configure". N�o recomendado, desde
que o m�todo "apt" suporta esta funcionalidade, com a ordem correta.


	    <tag>mounted</tag>
	    <item>
<strong>M�TODO N�O RECOMENDADO -- use o apt ou multi_mount ao inv�s deste. 
Somente tente este m�todo se todos os outros falham</strong>
	      <p>
Simplesmente especifique a localiza��o dos arquivos da Debian em
seu sistema de arquivos. Poss�velmente o m�todo mas f�cil, mas lento.
N�o usa a ordem correta, assim ele pode executar v�rias vezes os passos
"Install" e/ou "Configure".


<![ IGNORE [ <tag>mountable</tag>
	    <item>
<strong>M�TODO IMPLORADO -- use o apt ou multi_mount ao inv�s deste. 
Somente tente este m�todo de todos os outros falham</strong>
	      <p>
Um pouco mais complexo que o m�todo "mounted", e bem mais r�pido -- ele
procurar� somente os pacotes necess�rios. Isto permite um melhor
controle sobre as op��es de instala��o do que os outros m�todos, mas
requer mais configura��o. ]]>


	    <tag>cdrom</tag>
	    <item>
<strong>M�TODO N�O RECOMENDADO -- use multi_cd ao inv�s deste. Este m�todo
simplesmente n�o funciona com diversos CDs, como os inclu�dos na
Debian %release;.</strong>
	      <p>
Preparado para instala��o com apenas um CD, este simples m�todo 
somente pergunta a localiza��o de sua unidade de CD-Rom, a
localiza��o da distribui��o Debian naquele disco e ent�o (se necess�rio)
a localiza��o dos arquivos de pacotes no. Simples mas totalmente lento. 
N�o usa a ordem correta, assim ele executar� diversas vezes os passos 
"Install" e/ou "Configure". N�o recomendado, porque ele assume que a 
distribui��o est� em um CD-Rom simples, que n�o � o caso. Use o m�todo 
"multi_cd" ao inv�s deste
	  </taglist>
	<p>
Se voc� tem algum problema -- talvez o Linux n�o veja sua unidade de
CD-Rom, seu NFS montado n�o esta funcionando ou tem esquecido onde
a parti��o dos pacotes est� -- voc� tem algumas op��es:

<list>
 <item>
  Inicie outro shell. Corrija o problema e retorne ao shell principal.
 <item>
  Saia do <prgn>dselect</prgn> e execute-o novamente mais tarde. Voc�
  pode at� mesmo precisar desligar o computador para resolver algum 
  problema. Isto � totalmente certo mas quando voc� retornar ao <prgn>
  dselect</prgn> execute-o como root. Ele n�o pode ser executado 
  autom�ticamente na primeira vez.
</list>
	<p>
Ap�s escolher o m�todo de acesso do <prgn>dselect</prgn>, voc� 
dever� indicar a localiza��o precisa dos pacotes. Se voc� n�o 
conseguir na primeira tentativa, pressione <em>Control-C</em> 
e retorne ao item "Access".
	<p>
Uma vez que terminar o programa retornar� � tela principal.


      <sect id="update">"Update" (atualizar)
	<p>
<prgn>dselect</prgn> ler� os arquivos <file>Packages</file> ou
<file>Packages.gz</file> do mirror e criar� um banco de dados em seu 
sistema com todos os pacotes dispon�veis. Isto pode demorar um pouco
enquando ele copia e processa os arquivos.


      <sect id="select">"Select" (selecionar)
	<p>
Segure seu chap�u. Aqui � onde tudo acontece. O objetivo do exerc�cio
� selecionar apenas os pacotes que deseja instalar.
	<p>
Pressione a tecla &enterkey;. Se voc� tem um computador lento tenha
certeza que a tela continuar� apagada por 15 segundos, assim n�o
digite teclas no bash neste ponto.
	<p>
A primeira coisa que vem na tela � a p�gina 1 do arquivo de ajuda.
Voc� pode obter esta ajuda pressionando <em>?</em> em qualquer 
ponto da tela "Select" e voc� pode paginar entre as telas de ajuda
pressionando a tecla <em>.</em> (parada completa).
	<p>
Antes de voc� continuar verifique estes pontos:

<list>
 <item>
  Para sair da tela "Select" ap�s todas sele��es estaram completas,
  pressione a tecla &enterkey;. Isto retornar� para a tela principal
  se n�o existirem problemas com sua sele��o. Ou ent�o, voc� ser�
  perguntado para tentar corrigir aquele problema. Quando voc� estiver
  satisfeito com qualquer tela de ajuda pressione a tecla &enterkey;
  para sair.
 <item>
  Problemas s�o muito normais e devem ser esperados. Se voc� selecionar
  o pacote <var>A</var> e aquele pacote requer o pacote <var>B</var> para
  executar, ent�o o <prgn>dselect</prgn> alertar� sobre o problema e
  poder� ainda sugerir uma solu��o. Se o pacote <var>A</var> entra em
  conflito com o pacote <var>B</var> (i.e., se eles s�o ambos exclusivos) 
  voc� ser� perguntado para decidir entre eles.
</list>

<p>D� uma olhada nas duas primeiras linhas do topo da tela "Select".

<example>
dselect - main package listing (avail., priority)          mark:+/=/- verbose:v help:?
EIOM Pri Section  Package      Inst.ver    Avail.ver   Description
</example>

	<p>
Este cabe�alho nos relembra de algumas das teclas especiais:

<taglist>
	    <tag><tt>+</tt>
	    <item>
Selecione um pacote para instala��o.

	    <tag><tt>=</tt>
	    <item>
Coloca um pacote em hold -- �til em um pacote corrompido. Voc� pode
reinstalar uma vers�o antiga e coloca-la em hold enquando voc� aguarda
o aparecimento de uma nova vers�o.

	    <tag><tt>-</tt>
Remove um pacote.

	    <tag><tt>_</tt>
	    <item>
Remove um pacote e ele configura arquivos.

	    <tag><tt>i,I</tt>
	    <item>
Muda/Ciclo entre telas de informa��es.

	    <tag><tt>o,O</tt>
	    <item>
Ciclo entre as op��es de ordena��o.

	    <tag><tt>v,V</tt>
	    <item>
Uma alterna��o simples/completa. Use esta tecla para destravar os
significados de EIOM na linha dois, mais ser� mostado assim mesmo o 
resumo aqui. Note que teclas em mai�sculas e min�sculas s�o muito
diferentes no efeito.

<example>
Valor  Significado          Valores poss�veis
E      Erro                 Espa�o, R, I
I      Estado de Instala��o Espa�o, *, -, U, C, I 
O      Marca Antiga         *, -, =, _, n
M      Marca                *, -, =, _, n 
</example>

</taglist>

	<p>
Verifique bastante isto antes de sair ou veja a tela de ajuda onde os
significados s�o sempre mostrados. Um exemplo:
	<p>
Se voc� entrar no <prgn>dselect</prgn> e encontra uma linha como esta:

<example>
EIOM Pri  Section  Package   Description 
  ** Opt  misc     loadlin   a loader (running under DOS) for LINUX kernel
</example>

Isto est� dizendo que o loadlin foi selecionado quando voc� executou o 
<prgn>dselect</prgn>, mas ele ainda n�o foi instalado. Porque n�o? A 
resposta deve ser porque o pacote loadlin n�o est� fisicamente 
dispon�vel. Ele pode estar faltando em seu mirror.
	<p>
A informa��o que o <prgn>dselect</prgn> usa para ter todos os 
pacotes corretamente instalados � colocado nos pacotes. Nada no
mundo � perfeito e �s vezes acontece que as depend�ncias
incluidas no pacote est�o incorretas, com o resultado que o
<prgn>dselect</prgn> simplesmente n�o resolver� esta situa��o.
Um meio provido onde o usu�rio pode retomar o controle s�o os 
comandos <em>Q</em> e <em>X</em> que est�o dispon�veis na
tela "Select".

<taglist>
 <tag><em>Q</em>
 <item>
Como substitui��o. For�a o <prgn>dselect</prgn> ignorar as constru��es
em depend�ncias e fazer o que voc� especificou. Os resultados, � claro, 
dependem de sua pr�pria cabe�a.

 <tag><em>X</em>
 <item>
Use o <em>X</em> se voc� estiver totalmente perdido. Ele retorna
as coisas do modo que estavam e sai.
</taglist>

  <p>
Teclas que te ajudam a <em>n�o</em> se perder (!) s�o <em>R</em>,
<em>U</em> e <em>D</em>.

<taglist>
 <tag><em>R</em>
 <item>
Cancela todas sele��es neste n�vel. N�o afeta sele��es feitas em um
n�vel anterior.

 <tag><em>U</em>
 <item>
Se o <prgn>dselect</prgn> prop�s altera��es e voc� fez mais adianda
U pode restaurar as pr�-sele��es do <prgn>dselect</prgn>

 <tag><em>D</em>
 <item>
Remove as sele��es feitas pelo <prgn>dselect</prgn> deixando somente
as suas.
</taglist>

	<p>
<!-- FIXME: este exemplo n�o � somente um mau exemplo como tamb�m errado -->
<!-- factually now -->

Como o exemplo que segue. O pacote <package>boot-floppies</package> 
(n�o um exemplo para iniciantes, eu acho, mas ele foi escolhido por
causa que ele possui algumas depend�ncias) depende destes pacotes:

<list>
 <item>
  <package>libc6-pic</package>
 <item>
  <package>slang1-pic</package>
 <item>
  <package>sysutils</package>
 <item>
  <package>makedev </package>
 <item>
  <package>newt0.25</package>
 <item>
  <package>newt0.25-dev</package>
 <item>
  <package>popt</package>
 <item>
  <package>zlib1g</package>
 <item>
  <package>zlib1g-dev</package>
 <item>
  <package>recode</package>
</list>

	<p>
A pessoa mantendo o pacote <package>boot-floppies</package> tamb�m
pensa que os seguintes pacotes est�o instalados. Estes n�o s�o,
no entando, essenciais:

<list>
 <item>
  <package>lynx</package>
 <item>
  <package>debiandoc-sgml</package>
 <item>
  <package>unzip</package>
</list>

    <p>
Assim quando selecionar o pacote <package>boot-floppies</package>, ser�
mostrada a seguinte tela:

<example>
dselect - recursive package listing mark:             +/=/- verbose:v help:?
EIOM Pri Section Package Description

dselect - recursive package listing                         mark:+/=/- verbose:v help:?
EIOM Pri Section  Package      Description
  ** Opt admin    boot-floppie Scripts to create the Debian installation floppy set.   
  _* Opt devel    newt0.25-dev Developer's toolkit for newt windowing library
  _* Opt devel    slang1-dev   The S-Lang programming library, development version.
  _* Opt devel    slang1-pic   The S-Lang programming library, shared library subset ki
</example>

(Outros pacotes podem ou n�o aparecer, dependendo do que estiver 
instalado em seu sistema). Voc� ser� notificado que os pacotes 
requeridos foram selecionados.
	<p>
A tecla <em>R</em> volta ao ponto de partida.

<example>
dselect - recursive package listing mark:             +/=/- verbose:v help:?
EIOM Pri Section Package Description

dselect - recursive package listing                         mark:+/=/- verbose:v help:?
EIOM Pri Section  Package      Description
  __ Opt admin    boot-floppie Scripts to create the Debian installation floppy set.   
  __ Opt devel    newt0.25-dev Developer's toolkit for newt windowing library
  __ Opt devel    slang1-dev   The S-Lang programming library, development version.
  __ Opt devel    slang1-pic   The S-Lang programming library, shared library subset ki
</example>

Para decidir agora o que voc� n�o deseja no boot-floppies, apenas 
tecle &enterkey;.

<!-- FIXME: A frase abaixo pode precisar de corre��es -->
	<p>
A tecla <em>D</em> coloca as coisas no modo que foi selecionado na
primeira etapa.

<example>
dselect - recursive package listing mark:             +/=/- verbose:v help:?
EIOM Pri Section Package Description

dselect - recursive package listing                         mark:+/=/- verbose:v help:?
EIOM Pri Section  Package      Description
  _* Opt admin    boot-floppie Scripts to create the Debian installation floppy set.   
  __ Opt devel    newt0.25-dev Developer's toolkit for newt windowing library
  __ Opt devel    slang1-dev   The S-Lang programming library, development version.
  __ Opt devel    slang1-pic   The S-Lang programming library, shared library subset ki
</example>

	<p>
A tecla <em>U</em> restaura a sele��o do <prgn>dselect</prgn>:

<example>
dselect - recursive package listing mark:             +/=/- verbose:v help:?
EIOM Pri Section Package Description

dselect - recursive package listing                         mark:+/=/- verbose:v help:?
EIOM Pri Section  Package      Description
  _* Opt admin    boot-floppie Scripts to create the Debian installation floppy set.   
  _* Opt devel    newt0.25-dev Developer's toolkit for newt windowing library
  _* Opt devel    slang1-dev   The S-Lang programming library, development version.
  _* Opt devel    slang1-pic   The S-Lang programming library, shared library subset ki
</example>


	<p>
� sugerido executar com os valoes padr�es inicialmente -- voc� tem uma
ampla opurtunidade de adicionar mais depois.
	<p>
Tudo que voc� decidir, pressione &enterkey; para aceitar e retornar
para a tela principal. Se isto resultar em problemas n�o resolvidos
voc� pode ir retornando para outra tela de resolu��o de problemas.
	<p>
Assim as teclas <em>R</em>, <em>U</em>, e <em>D</em> s�o muito �teis em
diversas situa��es. Voc� pode experimentar a vontade e ent�o restaurar
tudo e iniciar outra vez. <em>N�o</em> olhe eles como sendo uma
caixa de vidro escrito "quebre em caso de Emerg�ncia".
	<p>
Ap�s fazer suas sele��es na tela "Select", pressione <em>I</em> para
voc� ter uma janela maior, <em>t</em> para te levar ao come�o e
ent�o use a tecla <em>Page-Down</em> para verificar r�pidamente
suas configura��es. Deste modo voc� pode conferir os resultados de
seu trabalho e encontrar alguns erros superficiais. Muitas pessoas tem
desmarcado grupos completos de pacotes por engano e notaram o
erro bem mais tarde. <prgn>dselect</prgn> � uma ferramenta 
<em>muito</em> poderosa assim n�o abuse dele.

	<p>
Agora voc� deve ter esta situa��o:

<example>
package category     status

required             all selected
important            all selected
standard             mostly selected
optional             mostly deselected
extra                mostly deselected
</example>

	<p>
Feliz? Pressione &enterkey; para sair do processo "Select". Voc� pode
voltar e executar "Select" denovo se desejar.


      <sect id="install">"Install" (instalar)
	<p>
O <prgn>dselect</prgn> � executado na configura��o completa dos
&num-of-distrib-pkgs; pacotes e instala aqueles selecionados. Espere
ser perguntado para decidir o que fazer. Isto � frequentemente �til
para mudar para um shell diferente para comparar, digo, uma configura��o
antiga com uma nova. Se o arquivo antigo � <file>conf.modules</file>
o novo ser� <file>conf.modules.dpkg-new</file>
	<p>
A tela rola passando razoavelmente r�pida em uma m�quina r�pida. 
Voc� pode iniciar/parar ela com <em>Control-s</em>/<em>Control-q</em> e
no fim da execu��o voc� pode obter uma lista de quaisquer pacotes 
desinstalados. Se voc� deseja ter um registro de tudo que aconteceu,
use caracter�sticas normais do Unix como <prgn>tee</prgn> ou <prgn>
script</prgn>
	<p>
Pode acontecer que um pacote n�o seja instalado porque ele depente de
outros pacotes que est�o listados para instala��o mas n�o est�o 
instalados. A resposta aqui � executar "Install" denovo. Tem sido
relatado casos onde foi necess�rio executar 4 vezes antes de tudo 
ser instalado. Isto pode variar de acordo com seu m�todo de
aquisi��o.


      <sect id="configure">"Configure" (configurar)
	<p>
Muitos pacotes foram configurados no passo 3, mas qualquer coisa 
que passou pode ser configurada aqui.


      <sect id="remove">"Remove" (remover)
	<p>
Remove pacotes que est�o instalados mas n�o s�o requeridos.


      <sect id="quit">"Quit" (Sair)
	<p>
� recomend�vel executar neste ponto <file>/etc/cron.daily/find</file> 
porque voc� teve diversos arquivos novos instalados em seu sistema.
Assim voc� pode usar <prgn>locate</prgn> para obter a localiza��o
de qualquer arquivo dado.

    <chapt id="dselect-conclusion">Algumas dicas na conclus�o
      <p>
Quando o processo de instala��o executa o <prgn>dselect</prgn> para voc�,
voc� estar� ansioso para ter a Debian sendo executada o mais
r�pido poss�vel. Bem, por favor se prepare para levar uma hora, assim 
voc� deve aprender seu m�todo de fazer isto corretamente. Quando
voc� entrar pela primeira vez na tela "Select" n�o fa�a nenhuma
sele��o -- apenas pressione &enterkey; e veja quais problemas de
depend�ncias existem. Tente corrigi-las. Se retornar para
a tela principar execute novamente <ref id="select">.
      <p>
Voc� pode ter uma id�ia do tamanho do pacote pressionando <em>i</em>
e olhando para a figura "Size". Este � o tamanho do pacote compactado,
assim os arquivos descompactados ocupar�o mais espa�o (veja 
"Installed-Size", que � em kilo-bytes, para conhece-lo).
      <p>
Instalar um novo sistema Debian � uma coisa mais complexa, mas
o <prgn>dselect</prgn> pode fazer isto para voc� t�o facilmente 
quanto pode ser. Assim gaste algum tempo aprendendo a utiliza-lo.
Leia as telas de ajuda e esperimente com <em>i, I. o,</em> e <em>
O</em>. Use a tecla <em>R</em>. Est� tudo l�, mas est� dispon�vel
para utiliza-lo efetivamente.</p>


    <chapt id="dselect-glossary">Gloss�rio
      <p>
Os seguintes termos s�o �teis para voc� neste documento e em geral, quando
estiver falando sobre a Debian.

<taglist>
 <tag>Pacote
  <item> 
   Um arquivo que cont�m tudo necess�rio para instala��o, desinstala��o
   e executa��o de um programa em particular.
   <p>
   O programa que controla os pacotes �
   <prgn>dpkg</prgn>. <prgn>dselect</prgn> � uma interface para
   <prgn>dpkg</prgn>. Usu�rios experi�ntes frequentemente usam 
   o <prgn>dpkg</prgn> para instalar e remover pacotes.

 <tag>Nomes dos pacotes
 <item>
  Todos nomes dos pacote tem a forma<var>xxxxxxxxxxx.deb</var>.
  <p>
  Alguns nomes de pacotes s�o:

  <list>
   <item>
    <file>efax_08a-1.deb</file>
   <item>
    <file>lrzsz_0.12b-1.deb</file>
   <item>
  <file>mgetty_0.99.2-6.deb</file>
   <item>
  <file>minicom_1.75-1.deb</file>
   <item>
  <file>term_2.3.5-5.deb</file>
   <item>
  <file>uucp_1.06.1-2.deb</file>
   <item>
  <file>uutraf_1.1-1.deb</file>
   <item>
  <file>xringd_1.10-2.deb</file>
   <item>
  <file>xtel_3.1-2.deb</file>
  </list>

</taglist>

</book>


<!-- Deixe este coment�rio no final do arquivo 
Local variables:
mode: sgml
sgml-omittag:nil
sgml-shorttag:t
sgml-namecase-general:t
sgml-general-insert-case:lower
sgml-minimize-attributes:nil
sgml-always-quote-attributes:t
sgml-indent-step:2
sgml-indent-data:nil
sgml-parent-document:nil
sgml-exposed-tags:nil
sgml-local-catalogs:nil
sgml-local-ecat-files:nil
End:
-->



