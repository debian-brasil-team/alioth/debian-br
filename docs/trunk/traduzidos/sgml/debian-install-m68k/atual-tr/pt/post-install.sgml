<!-- mantenha este coment�rio para controle de revis�es -->
<!-- $Id: post-install.sgml,v 1.1.1.1 2003/06/14 19:40:56 kov Exp $ -->
<!-- original version: 1.25 -->

<chapt id="post-install">Pr�ximos passos e para onde ir a partir daqui

<sect id="unix-intro">Se voc� � novo no Unix
    <p>
Se voc� � novo no Unix, voc� provavelmente dever� comprar muitos livros e 
ler muito. O <url id="&url-unix-faq;" name="Unix FAQ"> cont�m um n�meros 
de refer�ncia a livros e news groups na Usenet que podem lhe ajudar. Voc� 
tamb�m pode dar uma olhada em <url id="&url-unix-user-friendly;" 
name="User-Friendly Unix FAQ">..
    <p>
O Linux � uma implementa��o do Unix. O <url id="&url-ldp;" name="Projeto
de documenta��o do Linux (LDP)">  tem um grande n�mero de HOWTOs e livros 
online relacionados com o Linux. 
Muitos destes documentos podem ser instalados localmente; tente instalar o 
pacote <package>doc-linux-html</package> (vers�es HTML) ou o pacote 
<package>doc-linux-text</package> (vers�es ASCII), ent�o veja estes 
documentos em <file>/usr/doc/HOWTO</file>. Vers�es internacionais dos 
HOWTOs da LDP tamb�m est�o dispon�veis como pacotes Debian.
    <p>
Informa��es espec�ficas a Debian podem ser encontradas abaixo.

  <sect id="shutdown">Desligando o Sistema
   <p>
Para desligar um sistema Linux em execu��o voc� n�o deve reiniciar com
o bot�o de reset na frente ou na traseira de seu computador ou
simplesmente desligar o computador. O Linux deve ser desligado de uma
maneira controlada ou de outra forma arquvos podem ser perdidos e discos
podem ser danificados. Voc� pode pressionar a combina��o de teclas
Ctrl-Alt-Del <![ %powerpc %m68k [ ou Control-Shift-Power em sistemas
Macintosh]]>. Voc� pode tamb�m autenticar-se no sistema como root e
digitar <tt>shutdown -h now</tt>, <tt>reboot</tt> ou <tt>halt</tt> caso
nenhuma das combina��es de teclas funcione ou se voc� prefere digitar
comandos.

<sect id="debian-orientation">Orientando-se com a Debian
    <p>
A Debian � um pouco diferente das outras distribui��es. At� mesmo se voc� 
estiver familiar com outras distribui��es do Linux, voce dever� conhecer certas
coisas sobre a Debian para ajudar a deixar seu sistema em perfeito estado. 
Este cap�tulo cont�m materiais para ajuda-lo a se orientar; a inten��o dele n�o 
� ser um tutorial de como usar a Debian, mas serve como um guia r�pido para o 
mais apressado.

  <sect1>Sistema de Empacotamento Debian
<p>
O conceito mais importante a entender � o sistema de pacotes da Debian. Em 
essencial, grande parte do seu sistema pode ser considerado sobre o controle 
do sistema de pacotes. Isto inclui:
<list>
 <item>
  <file>/usr</file> (excluindo <file>/usr/local</file>)
 <item>
  <file>/var</file> (voc� poderia criar <file>/var/local</file> com seguran�a aqui)  
 <item>
  <file>/bin</file>
 <item>
  <file>/sbin</file>
 <item>
  <file>/lib</file>
</list>
Por exemplo, se voc� trocar <file>/usr/bin/perl</file>, ele trabalhar�, mas 
quando for atualizar seu pacote <package>perl</package>, o arquivo que 
colocou aqui ser� substitu�do. Usu�rios experientes podem contornar este 
problema colocando pacotes em "hold" no <prgn>dselect</prgn>.
    <p>
Um dos melhores m�todos de instala��o � o apt. Voc� pode us�-lo como um
m�todo de dentro do dselect ou voc� pode usar a vers�o de linha de
comando (info apt-get). Note que o apt lhe permite juntar as se��es main,
contrib e non-free e assim voc� pode ter paotes com restri��o de
exporta��o e tamb�m vers�es padr�es.


 <sect1>Gerenciamento de Vers�es de Aplica��es
   <p>
Vers�es alternativas de aplica��es s�o gerenciadas pelo
update-alternatives. Caso voc� esteja mantendo m�ltiplas vers�es de sua
aplica��o, leia a p�gina de manual do update-alternatives.

 <sect1>Gerenciamento de tarefas do Cron
   <p>
Quaisquer jobs sob o alcance do administrador de sistemas dever� estar
em <file>/etc</file>, uma vez que eles s�o arquivos de configura��o. Caso
voc� possua um cron job do usu�rio root para execu��es di�rias, semanais
ou noturnas, coloque-os em <file>/etc/cron.[daily,weekly,monthly}</file>.
Estes s�o invocados a partir de <file>/etc/crontab</file> e ser�o
executados em ordem alfab�tica, o que os serializa.
   <p>
Por outro lado, caso voc� possua um job cron que (a) precisa ser executado
com um usu�rio especial ou (b)precisa ser executado em um momento
espec�fico ou frequentemente, voc� pode usar <file>/etc/crontab</file> ou,
melhor ainda, <file>/etc/cron.d/whatever</file>. Esses arquivos em
particular possuem um campo extra que lhe permite estipular o usu�rio sob
o qual o job cron ser� executado.
   <p>
Em qualquer caso, voc� somente edita os arquivos e o cron ir� not�-los
automaticamente. N�o existe a necessidade de executar um comando especial.
Para maiores informa��es veja cron(8), crontab(5) e
<file>/usr/share/doc/cron/README.Debian</file>.


<![ %i386 [
  <sect id="reactivating-win">Reativando o DOS e Windows
    <p>
Ap�s instalar o sistema b�sico e gravar o <em>Master Boot
Record</em>, voc� ser� capaz de inicializar o Linux, mas 
provavelmente nada mais.
Isto depende do que escolheu durante a instala��o. Este cap�tulo 
descrever� como voc� pode reativar seus sistemas antigos, assim 
ser� capaz de inicializar novamente no DOS ou Windows. 
    <p>
O <prgn>LILO</prgn> � um gerenciador de partida que lhe permite 
inicializar outros sistemas operacionais al�m do Linux, que 
est� de acordo com os padr�es PC. O gerenciador de partida � 
configurado atrav�s do arquivo <file>/etc/lilo.conf</file>. 
Ser� necess�rio re-executar o comando <prgn>lilo</prgn> ap�s 
qualquer modifica��o neste arquivo. A raz�o disto � que as 
altera��es ser�o gravadas somente ap�s executar o programa. 
    <p>
As partes importantes do arquivo <file>lilo.conf</file> s�o as 
linhas contendo as palavras <tt>image</tt> e <tt>other</tt>, tamb�m 
como linhas contendo estas. Elas podem ser usadas para descrever 
um sistema que ser� inicializado pelo <prgn>LILO</prgn>. Tal 
sistema pode incluir um kernel (<tt>image</tt>), 
uma parti��o ra�z, par�metros adicionais do kernel, etc. Tabm�m 
configura��es para inicializar um outro sistema operacional 
n�o-Linux. Estas palavras tamb�m podem ser usadas mais de uma 
vez. A ordem destes sistemas no arquivo de configura��o � 
importante pois determina que sistema ser�o inicializado ap�s, 
por exemplo, um per�odo de tempo (<tt>delay</tt>) presumindo que o 
<prgn>LILO</prgn> n�o foi interrompido pelo pressionamento da 
tecla <em>shift</em>. 
    <p>
Ap�s a instala��o do Deiban, apenas o sistema atual est� 
configurado para inicializa��o atrav�s do <prgn>LILO</prgn>. Se 
desejar inicializar outro kernel do Linux, voc� ter� que editar o 
arquivo de configura��o <file>/etc/lilo.conf</file> e 
adicionar as seguintes linhas:

<example>
&additional-lilo-image;
</example>

Para uma configura��o b�sica, apenas as primeiras duas linhas s�o 
necess�rias. Se desejar conhecer mais sobre as outras duas 
op��es, d� uma olhada na documenta��o do <prgn>LILO</prgn>. 
Ela pode ser encontrada em <file>/usr/share/doc/lilo/</file>. 
O arquivo que deve ler � <file>Manual.txt</file>. Para ter 
uma inicializa��o r�pida do seu sistema, tamb�m d� uma 
olhada nas manpages do <prgn>LILO</prgn> <manref name="lilo.conf" 
section="5"> para uma vis�o das op��es de configura��o e 
<manref name="lilo" section="8"> para a descri��o de instala��o 
da nova configura��o no setor de inicializa��o do disco r�gido. 
    <p>
Existem outros gerenciadores de inicializa��o dispon�veis na 
&debian;, como o GRUB (do pacote <package/grub/), CHOS (do pacote 
<package/chos/),Extended-IPL (no pacote <package/extipl/), loadlin 
(no pacote <package/loadlin/) etc.
]]>


<sect id="further-reading">Futuras leituras e informa��es
    <p>
Se voc� precisa saber mais sobre um programa em particular, voc� pode tentar 
primeiro o comando <tt>man <var>programa</var></tt> ou <tt>info <var>
programa</var></tt>.
    <p>
Existem documentos muito �teis em <file>/usr/doc</file>. Em particular, 
<file>/usr/doc/HOWTO</file> e <file>/usr/doc/FAQ</file> cont�m diversas 
informa��es interessantes.
    <p>
O <url id="http://&www-debian-org;/" name="web site da Debian"> cont�m 
larga quantidade de documenta��o. Em particular, veja <url id="&url-debian-faq;" 
name="Debian FAQ"> e o <url id="&url-list-archives;" name="Debian Mailing List
Archives">. A comunidade Debian far�o seu suporte; para se inscrever em uma ou 
mais das listas de discuss�o da Debian, veja <url id="&url-list-subscribe;" 
name="Mail List Subscription">.


<sect id="kernel-baking">Compilando um novo Kernel
    <p>
Porque alguem deseja compilar um novo kernel? Isto n�o � freq�entemente 
necess�rio desde que o kernel padr�o que acompanha a Debian trabalha com muitas 
configura��es. No entanto, � �til compilar um novo kernel com o objetivo de:
<list>
  <item>
gerenciar hardware especial necess�rio ou gerenciar conflitos com kernels
pr�-fornecidos
  <item>
gerenciar hardware ou op��es n�o inclu�das no kernel padr�o, como APM ou SMP

<![ %i386 [ <item>
Os tipos compact e idepci n�o s�o fornecidos com suporte a som. Por�m o
tipo vanilla sim, o mesmo pode n�o n�o funcionar por outras raz�es.  ]]>

  <item>
Otimizar o kernel removendo drivers desnecess�rios, que diminui tempo de 
inicializa��o.
  <item>
Utilizar op��es do kernel que n�o est�o dispon�veis no kernel padr�o (como o 
firewall da rede).
  <item>
Executar um kernel atualizado ou em desenvolvimento
  <item>
Impressionar seus amigos, testar coisas novas.
</list>

 <sect1>Gerenciamento da Imagem do Kernel
    <p>
N�o tenha nenhum medo em tentar compilar o kernel. � divertido e lucrativo.
    <p>
Para compilar um kernel para a Debian trabalhar, voc� precisar� de v�rios 
pacotes: <package>kernel-package</package>, <package>kernel-source-&kernelversion;
</package> (a vers�o mais recente quando este documento foi escrito), 
<package>fakeroot</package> e alguns outros programas que provavelmente j� 
est�o instalados (veja <file>/usr/doc/kernel-package/README.gz</file> para 
a lista completa).
    <p>
Este m�todo produzir� um .deb de seu kernel fonte e, caso voc� possua
m�dulos n�o-padr�o, produzir um .deb dependente sincronizado deles tamb�m.
Este � uma maneira melhor de gerenciar imagens de krnel;
<file>/boot</file> ir� conter o kernel, o System.map e um log do arquivo
de configura��o ativo para a costru��o.
    <p>
Note que voc� n�o <em>precisa</em> compilar o kernel usando o
"m�todo da Debian"; mas n�s achamos que utilizar um sistema de pacotes para 
administrar o kernel � realmente mais seguro e mais f�cil. De fato, voc� pode 
obter os fontes do kernel corrigidos por Linus ao inv�s do <package>
kernel-source-&kernelversion;</package>, contudo utilize o m�todo de 
compila��o do kernel-package.
    <p>
Note que voc� encontrar� a documenta��o completa sobre o uso do <package>
kernel-package</package> em <file>/usr/doc/kernel-package</file>. Esta 
se��o cont�m um pequeno tutorial.
<![ %sparc [
    <p>
Se estiver compilando um kernel para o UltraSPARC, voc� precisar� 
ter certeza de ter o pacote <package>egcs64</package> instalado. 
Este � o compilador preferido para os kernels SPARC 64bits. O 
<prgn>gcc</prgn> padr�o tamb�m compilar� kernels 64 bits, mas 
ele n�o � est�vel. Ainda se n�u usar o pacote 
<package>egcs64</package> e encontrar problemas no kernel, voc� 
provavelmente ser� recomendado a compilar o kernel usando 
<package>egcs64</package> para verificar se o problema ainda 
persiste. Ap�s instalar o <package>egcs64</package>, tenha 
certeza de executar <tt>update-alternatives --config sparc64-linux-gcc</tt> 
como root e que o pacote <package>egcs64</package> est� sendo usado 
para este programa. ]]>
    <p>
A partir de agora, n�s assumimos que seus fontes do kernel est�o localizados em
<file>/usr/local/src</file> e que sua vers�o do kernel � &kernelversion;. 
Como root, crie um diret�rio em <file>/usr/local/src</file> e altere o 
dono daquele diret�rio para a conta n�o-root que utiliza. Com sua conta 
normal, altere seu diret�rio para onde voc� deseja descompactar os fontes 
do kernel (<tt>cd /usr/local/src</tt>), descompacte os fontes do kernel 
(<tt>tar Ixvf /usr/src/kernel-source-&kernelversion;.tar.bz2</tt>), altere 
seu diret�rio para ele (<tt>cd kernel-source-&kernelversion;</tt>). Agora, 
voc� pode configurar o seu kernel, Execute o <tt>make xconfig</tt> se o X11 
estiver instalado, configurado e rodando, <tt>make menuconfig</tt> em caso 
contr�rio (voc� precisar� do pacote <package>ncurses-dev</package> instalado). 
Leve um tempo lendo a documenta��o online e escolha cuidadosamente as op��es. 
Quando estiver em d�vida, � tipicamente melhor incluir o controlador 
de dispositivo (o software que ger�ncia perif�ricos de hardware, como 
placas Ethernet, controladores SCSI, e muitos outros). Tenha cuidado: 
outras op��es, que n�o est�o relacionadas com hardwares espec�ficos, devem 
ser deixadas em seus valores padr�es caso n�o entende-las. N�o se esque�a 
de selecionar "Kernel daemon support" (e.g. auto-inicializa��o de m�dulos) 
em "Loadable module support" 
<![ %alpha [
e "Enhanced Real Time Clock Support" em "Character devices"
(eles n�o s�o
]]>
<![ %not-alpha [ (Ele n�o � ]]>
selecionados por padr�o). Se n�o estiver incluido, a sua instala��o da Debian 
ter� problemas. 
    <p>
Limpe a �rvore dos fontes e resete os par�metros do <package>kernel-package
</package>. Para fazer isto, digite <tt>make-kpkg clean</tt>.
    <p>
Agora, compile o kernel: <tt>fakeroot make-kpkg --revivion=custom.1.0 
kernel-image</tt>. O n�mero da vers�o "1.0" pode ser alterada a vontade; isto � um 
n�mero de vers�o para localizar suas constru��es do kernel. Igualmente, voc� 
pode colocar qualquer palavra que quiser substituindo "custom" (i.e., o nome do 
host). A compila��o do kernel poder� demorar um pouco, dependendo da pot�ncia do 
seu computador.
<![ %supports-pcmcia [
    <p>
Se voc� precisar do suporte PCMCIA, voc� tamb�m dever� instalar o pacote 
<package>pcmcia-source</package>. Descompacte o arquivo compactado como 
root no diret�rio <file>/usr/src</file> (� importante que estes m�dulos 
estejam localizados aqui, onde eles devem ser encontrados, isto �, 
<file>/usr/src/modules</file>). Ent�o, como root, digite <tt>make -kpkg 
modules_image</tt>. 
]]>
    <p>
Ap�s a compila��o estar completa, voc� poder� instalar seu kernel personalizado 
como qualquer pacote. Como root, digite <tt>dpkg -i 
../kernel-image-&kernelversion;-<var>subarch</var>_custom.1.0_&architecture;.deb</tt>. 
A parte <var>subarch</var> � uma subarquitetura opcional, 
<![ %i386 [ como um "i586", ]]> 
dependendo de que op��es do kernel utilizou. O comando <tt>dpkg -i 
kernel-image...</tt> instalar� o kernel, junto com outros arquivos de 
suporte. Por instante, o <file>system.map</file> ser� apropriadamente 
instalado (�til para problemas de depura��o do kernel), e 
/boot/config-&kernelversion; ser� instalado, contendo as suas configura��es 
atuais do sistema. 
Seu novo pacote <package>kernel-image-&kernelversion;
</package> � inteligente o bastante para utilizar o gerenciador de 
inicializa��o de sua plataforma para executar uma atualiza��o na 
inicializa��o, lhe permitindo inicializar sem re-executar o 
gerenciador de inicializa��o. Se voc� criou um pacote de m�dulos, 
e.g., se tiver PCMCIA, ser� necess�rio instalar aquele pacote tamb�m.
    <p>
Esta � a hora de reiniciar seu computador: Leia qualquer alerta que o passo 
acima tenha produzido, ent�o digite <tt>shutdown -r now</tt>
    <p>
Para mais informa��es sobre o <package>kernel-package</package>, leia 
<file>/usr/doc/kernel-package</file>.


<!-- Deixe este coment�rio no final do arquivo
Local variables:
mode: sgml
sgml-omittag:t
sgml-shorttag:t
sgml-namecase-general:t
sgml-general-insert-case:lower
sgml-minimize-attributes:max
sgml-always-quote-attributes:t
sgml-indent-step:2
sgml-indent-data:nil
sgml-parent-document:("../install.pt.sgml" "book" "chapt")
sgml-declaration:nil
sgml-exposed-tags:nil
sgml-local-catalogs:nil
sgml-local-ecat-files:nil
End:
-->
