<!doctype debiandoc system>
<!--
Debian META Manual
Copyright (C) 1997 Ardo van Rangelrooij
Feito sobre os termos da Licen�a P�blica Geral GNU,
vers�o 2 ou (por sua op��o) outra qualquer.
-->
<debiandoc>
<book>
<titlepag>
<title>Debian Meta Manual</title>
<author>
<name>Ardo van Rangelrooij</name>
<email>ardo.van.rangelrooij@tip.nl</email>
</author>
<author>
<name>Oliver Elphick - Maintainer</name>
<email>olly@lfix.co.uk</email>
</author>
<author>
<name>Traduzido por: F�bio C�sar Colombo</name>
<email>streptococus@ieg.com.br</email>
</author>
<author>
<name>Revisado por: Wendell Martins Borges</name>
<email>wendell@centrodecitricultura.br</email>
</author>
<author>
<name>Revisado por: Philipe Gaspar</name>
<email>linux-l@uol.com.br</email>
</author>
<version><date></version>
<abstract>
O Debian Meta Manual da um visco de todos os manuais
dispon�veis na distribui��o Debian GNU/Linux. Est�
visco inclu� documenta��o escrita especificadamente para o
Debian GNU/Linux (como parte de documenta��o do Debian)
como as documenta��es escritas para Linux em geral
(p.ej. v�rias revistas sobre Linux).
</abstract>
<copyright>
<copyrightsummary>
Copyright &copy; 1997 Ardo van Rangelrooij
</copyrightsummary>
<p>
Este manual � software livre; pode ser redistribuido e/ou
modificado sobe os termos da Licen�a P�blica Geral GNU,
p�blicada pela Free Software Foundation; Uma outra ver��o
2, ou (se quiser) qualquer ver��o posterior.
</p>
<p>
Este documento se distribui com a esperan�a de que seja util,
pois vem
<em>sem nenhuma garantia</em>; nem se quer a �mplicita garantia
de comerciabilidade ou conveni�ncia para um fim em particular. 
Ver a Licen�a P�blica Geral GNU para mais detalhes.
</p>
<p> 
Voc� deveria receber uma c�pia da Licen�a
P�blica Geral GNU com seu sistema Debian GNU/Linux, em
<tt>/usr/doc/copyright/GPL</tt>, com o pacotes
<prgn>debiandoc-sgml</prgn> no  arquivo
<tt>COPYING</tt>. Se n�o, escreva  para a  Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
</p>
</copyright>
</titlepag> 
<toc sect>  
<chapt>     
<heading>Introdu��o</heading>
<p>
"Sobre este Manual"
</p>
<p> 
"Onde encontrar novas ver��es"
</p>
<p> 
"Como aparece este manual"
</p>
<p> 
Por favor, envie todos seus comentarios, cr�ticas, sugest�es,
bugs, etc. para <email>olly@lfix.co.uk<;/email>.
</p>
</chapt>
<chapt> 
<heading>Manuais gerais</heading>
<p>
Os seguites manuais gerais est�o dispon�veis:
<list compact>
<item>
<p>Manual do projeto</p>
</item>
<item> 
<p>META-Manual</p>
</item>
<item> 
<p>FAQ</p>
</item>   
<item>    
<p>Dicionario</p>
</item>
<item> 
<p>Livros Recomendados</p>
</item>
<item> 
<p>Revistas Online</p>
</item>
</list>
</p>   
<p>    
O <em>Manual do projeto</em> explica a hist�ria, as
metas, e outros temas relacionados com o Projeto Debian.
</p>
<p> 
O <em>META-Manual</em> explica ao usu�rio onde buscar
os documentos que cobram suas d�vidas.
</p>
<p> 
O <em>FAQ</em> � uma lista de perguntas frequentes de
usu�rios de um sistema Debian GNU/Linux e as respostas a
essas perguntas.
</p>
<p> 
O <em>Dicionario</em> descreve e define termos
utilizados nos manuais do Debian.
</p>
<p> 
Os <em>Livros Recomendados</em> s�o uma lista de livros que
se sugerem e s�o lidos para documentar-se sobre v�rios
temas que s�o cobertos nos manuais de Debian.
</p>
<p> 
A distribui��o Debian GNU/Linux vem com
as seguites <em>Revistas Online</em>:  
<list compact>
<item>
<p>Linux Gazette (Ingl�s)</p>
</item>
<item> 
<p>PLUTO Journal (Italiano)</p>
</item>
<item> 
<p>L'Echo de Linux (Franc�s)</p>
</item>
</list>
</p>   
</chapt>
<chapt> 
<heading>Manuais para o usu�rio</heading>
<p>
Os seguintes manuais para o usu�rio est�o dispon�veis:
<list compact>
<item>
<p>Manual do Usu�rio</p>
</item>
<item> 
<p>Dicas</p>
</item>
</list>
</p>   
<p>    
O <em>Manual do Usu�rio</em> cobre todo o que um usu�rio
deveria saber sobre um sistema Debian GNU/Linux desde o ponto
de vista do usu�rio.
</p>
<p> 
As <em>Dicas</em> s�o uma cole��o de truques sobre como fazer
certas coisas em um sistema Debian GNU/Linux.
</p>
</chapt>
<chapt> 
<heading>Manuais do Administrador</heading>
<p>
Os seguintes manuais do administrador est�o dispon�veis:
<list compact>
<item>
<p>Lista de Compatibilidade de Hardware</p>
</item>
<item> 
<p>Notas da ver��o</p>
</item>
<item> 
<p>Manual de instala��o</p>
</item>
<item> 
<p>Manual do Administrador de Sistema</p>
</item>
<item> 
<p>Manual do Administrador de Redes</p>
</item>
<item> 
<p>Manual de Configura��o do Kernel</p>
</item>
</list>
</p>   
<p>    
A <em>Lista de Compatibilidade de Hardware</em> lista todas as
pegas de hardware que se sabe que funcionam com a distribui��o
Debian GNU/Linux. Est� lista tamb�m inclu� conselhos para fazer que o
Debian funcione com certas pegas de hardware, por ex. configura��es  
do X para diferentes adaptadores de v�deo.
</p>
<p> 
As <em>Notas da ver��o</em> descrevem  toda a informa��o
especifica sobre a ver�ao da distribui��o Debian GNU/Linux.
</p>
<p> 
O<em>Manual de Instala��o</em> cont�m instru��es sobre
como instalar a distribui��o Debian GNU/Linux.
</p>
<p> 
O <em>Manual do Administrador de Sistema</em> cobre todos os
aspectos sobre a administra��o de um sistema Debian GNU/Linux.
</p>
<p> 
O <em>Manual do Administrador de Rede</em> cobre todos os
aspectos sobre a administra��o da rede de um sistema Debian
GNU/Linux.
</p>
<p> 
O <em>Manual de Configura��o do Kernel</em> descreve todos os
aspectos de configura��o do kernel em um sistema Debian GNU/Linux.
</p>
</chapt>
 <chapt>

      <heading>Manuais para os Desenvolvedores</heading>

      <p>
        Os seguintes manuais est�o dispon�veis para o desenvolvedor:
        <list>
          <item>
            <p>Refer�ncia para o Desenvolvedor</p>
          </item>
          <item> 
            <p>Manual de Pol�ticas</p>
          </item>
          <item> 
            <p>Introdu��o: Fazer um Pacote Debian</p>
          </item>
          <item> 
            <p>Empacotando-HOWTO</p>
          </item>
          <item> 
            <p>Manual de Empacotamento</p>
          </item>
          <item> 
            <p>Manual do interior do dpkg</p>
          </item>
          <item> 
            <p>Manual do Sistema de Menus</p>
          </item>
        </list>  
      </p>

      <p>
        A  <em>Refer�ncia para o Desenvolvedor</em> descreve como contribuir
        na distribui��o Debian GNU/Linux. Inclui uma introdu��o de como se
	tornar novo desenvolvedor, o processo de carga de arquivos ao servidor,
	como manejar o sistema de seguimentos de bugs, listas de discuss�o, 
	um servidor de Internet, etc.  Este manual foi feito para ser um 
	<em>manual de referencia</em> para todos os desenvolvedores do Debian,
	tanto os novatos quanto os mas veteranos. 
      </p>

      <p>
        O <em>Manual de Pol�ticas</em> descreve os requerimentos para as
	regras da distribui��o Debian GNU/Linux. Isto inclui a estrutura
        e conte�dos de um arquivo Debian, algumas quest�es de designe
        do sistema operacional, como os requerimentos t�cnicos que
        cada pacote deve cumprir para ser inclu�do na distribui��o.
      </p>

      <p>
        A <em>Introdu��o: Fazendo um pacote Debian</em> � uma
        introdu��o sobre como criar um pacote para Debian GNU/Linux.
      </p>

      <p>
        O <em>Empacotando-HOWTO</em> explica como criar um
        pacote para o sistema Debian GNU/Linux. Este manual � 
	centralizado no uso do <prgn>debmake</prgn>. 
      </p>

      <p>
        O <em>Manual de Empacotamento</em> explica como criar um pacote
        para o sistema Debian GNU/Linux.  Este manual � centralizado no
	uso do <prgn>dpkg-dev</prgn>. 
      </p>

      <p>
        O <em>Manual do interior de dpkg</em> descreve o funcionamento
	interior do <prgn>dpkg</prgn> e como escrever novas ferramentas
	para manejar pacotes usando as bibliotecas do <prgn>dpkg</prgn>. 
      </p>

      <p>
        O <em>Manual do Sistema de Menus</em> descreve o Sistema de 
	Menus do Debian GNU/Linux e o pacote <prgn>menu</prgn>.
      </p>

    </chapt>

    <chapt>

      <heading>Manuais de Documenta��o</heading>

      <p>
        Os seguintes manuais de documenta��o est�o dispon�veis:
        <list>
          <item>
            <p>Como come�ar um novo manual baseado no SGML</p>
          </item>
          <item> 
            <p>Diretrizes de Documenta��o</p>
          </item>
          <item> 
            <p>Debiandoc-SGML Markup Manual</p>
          </item>
        </list>  
      </p>

      <p>
        <em>Como come�ar um Novo Manual baseado em SGML</em> � um exemplo
	de documento SGML para Debian.  Este exemplo pode ser usado como
	ponto de partida para gente que deseja escrever um novo manual
        para Debian. Inclu� um c�digo fluente SGML como exemplo para
	um manual, como alguns scripts para a shell e para make para gerar
	os diferentes formatos de sa�da.
      </p>

      <p>
        As <em>Diretrizes de Documenta��o</em> e o "Manual de Pol�ticas"
        para a documenta��o do Debian. 
      </p>

      <p>
        O <em>Debiandoc-SGML Markup Manual</em> descreve como usar os
	comandos SGML markup do pacote Debiandoc-SGML. 
      </p>

      </chapt>

  </book>

</debiandoc>

<!-- Keep this comment at the end of the file
Local variables:
mode: sgml
sgml-omittag:nil
sgml-shorttag:t 
sgml-minimize-attributes:nil
sgml-always-quote-attributes:t
sgml-indent-step:2
sgml-indent-data:t
sgml-parent-document:nil
sgml-exposed-tags:nil   
sgml-local-catalogs:nil 
sgml-local-ecat-files:nil
End:
--> 
