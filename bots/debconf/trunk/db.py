from os import getcwd

from sqlobject import connectionForURI, SQLObject, StringCol, \
     IntCol, DateTimeCol

connection_uri = 'sqlite://%s/debconf.db' % (getcwd ())

__connection__ = connectionForURI (connection_uri)

class Factoids (SQLObject):
    name = StringCol ()
    definition = StringCol ()
    who = StringCol ()
    channel = StringCol ()
    time = IntCol ()

class Seen (SQLObject):
    nick = StringCol ()
    epoch = IntCol ()
    channel = StringCol ()
    msg = StringCol ()

class Privileged (SQLObject):
    nick = StringCol ()
    hostname = StringCol ()

if __name__ == '__main__':
    Factoids.createTable ()
    Seen.createTable ()
    Privileged.createTable ()
