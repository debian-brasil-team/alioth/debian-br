import time

def duration (past_secs):
    now_secs = time.time ()

    result_hours = result_days = result_mins = result_secs = days_msg = hours_msg = mins_msg = secs_msg = 0

    since_past = now_secs - past_secs
    
    if since_past >= 60:
        result_secs = since_past % 60
        result_mins = (since_past / 60) % 60
    else:
        result_secs = since_past

    if since_past / 60 >= 60:
        result_hours = (since_past / 60 / 60) % 24

    if since_past / 60 / 60 >= 24:
        result_days = since_past / 60 / 60 / 24

    result_days = int (result_days)
    result_hours = int (result_hours)
    result_mins = int (result_mins)
    result_secs = int (result_secs)

    if result_days:
        str = 'dia'
        if result_days > 1:
            str += 's'	
        days_msg = '%d %s' % (result_days, str)
    if result_hours:
        str = 'hora'
        if result_hours > 1:
            str += 's'
        hours_msg = '%d %s' % (result_hours, str)
    if result_mins:
        str = 'minuto'
        if result_mins > 1:
            str += 's'
        mins_msg = '%d %s' % (result_mins, str)
    if result_secs:
        str = 'segundo'
        if result_secs > 1:
            str += 's'
        secs_msg = '%d %s' % (result_secs, str)

    result_msg = ''

    if days_msg:
        result_msg += days_msg
        if (hours_msg and mins_msg) or (hours_msg and secs_msg) or (mins_msg and secs_msg):
            result_msg += ', '
        else:
            result_msg += ' e '

    if hours_msg:
        result_msg += hours_msg
        if mins_msg:
            if secs_msg:
                result_msg += ', ' + mins_msg + ' e ' + secs_msg
            else:
                result_msg += ' e ' + mins_msg
        else:
            result_msg += ' e ' + secs_msg

    elif mins_msg:
        result_msg += mins_msg
        if secs_msg:
            result_msg += ' e ' + secs_msg

    elif secs_msg:
        result_msg += secs_msg
    
    return result_msg
