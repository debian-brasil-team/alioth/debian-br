#!/usr/bin/python
# -*- coding: UTF-8 -*-

# AptBRv2 -- A replacement for #debian-br's apt-br bot
# Copyright (C) 2003 Gustavo Noronha Silva
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

import os
import sys
import time
import re
import signal

from sqlobject import AND
from db import __connection__, Factoids, Seen, Privileged

import irclib

import config
import misc

class AptBR (irclib.IRC):
    awaited_ping = None

    def __init__ (self):
        irclib.IRC.__init__ (self)

        self.channellog = {}

        self.connection = self.server ()
        self.connect ()

        self.add_global_handler ('pubmsg', self.pubmsg_cb)
        self.add_global_handler ('privmsg', self.privmsg_cb)
        self.add_global_handler ('action', self.action_cb)
        self.add_global_handler ('pong', self.pong_cb)

        self.schedule_rotate ()

    def connect (self):
        self.connection.connect (config.network, config.port,
                                 config.mynick, ircname = config.realname)
	self.connection.send_raw ('NICKSERV IDENTIFY %s'  % (config.mypass))

# Wait for NickServ authentication
        time.sleep (2)

        for channel in config.channels:
            self.join_channel (channel)

        self.connection.execute_delayed (10*60, self.ping_server)

    def disconnect (self, message = 'Reconectando'):
        self.connection.quit (message)

        try:
            self.connection.disconnect ()
        except ValueError:
            pass
        self.awaited_ping = None

    def join_channel (self, channel):
        self.connection.join (channel)

        try:
            if self.channellog[channel].closed:
                return
        except KeyError:
            pass

        self.channellog[channel] = open ('logs/%s.log' % (channel), 'a')

	self.connection.send_raw ('ChanServ OP %s %s' % (channel,
                                                         self.connection.get_nickname))

    def log_general (self, msg):
        flog = open ('logs/general.log', 'a')
        flog.write ('%s\t%s\n' % (time.strftime ('%d/%m/%Y %H:%M:%S'), msg))
        flog.close ()

    def log_private (self, msg):
        flog = open ('private_logs/0_private.log', 'a')
        flog.write ('%s\t%s\n' % (time.strftime ('%d/%m/%Y %H:%M:%S'), msg))
        flog.close ()

    def log_message (self, target, from_nick, msg, is_action = False):
        target = target.lower ()

        if irclib.is_channel (target):
            flog = self.channellog[target]
            flush_cmd = flog.flush
        else:
            flog = open ('private_logs/%s.log' % (target), 'a')
            flush_cmd = flog.close
            
        if not is_action:
            flog.write ('%s\t%s\t%s\n' %
                        (time.strftime ('%d/%m/%Y %H:%M:%S'),
                         from_nick, msg))
        else:
            flog.write ('%s\t* %s\t%s\n' %
                        (time.strftime ('%d/%m/%Y %H:%M:%S'),
                         from_nick, msg))
        flush_cmd ()

    def log_factoid (self, factoid, action, who):
        flog = open ('logs/factoid.log', 'a')

        if action == 0:
            action_text = 'ADD'
        elif action == 1:
            action_text = 'DEL'
        else:
            action_text = 'REP'

        flog.write ('%s\t[%s] %s\t(por %s)\n' % (time.strftime ('%d/%m/%Y %H:%M:%S'),
                                                 action_text, factoid, who))
        flog.write ('\tFactoid: %s\n\n' % (self.get_factoid (factoid)))
        flog.close ()

    def schedule_rotate (self):
        localtime = list (time.localtime ())

        localtime[3] = 0
        localtime[4] = 0
        localtime[5] = 1

        at = 86400 + time.mktime (tuple(localtime))

        self.connection.execute_at (at, self.rotate_logs)
        self.log_general ('nova rotação de logs agendada para %s' % \
                          (time.ctime (at)))

    def rotate_logs (self):
        self.log_general ('rotacionando logs...')

        log_date = time.localtime (time.time () - 60*60*24)
        log_month = time.strftime ('%Y-%m', log_date)
        log_day = time.strftime ('%Y-%m-%d', log_date)

        if not os.path.isdir ('logs/%s' % log_month):
            os.mkdir ('logs/%s' % log_month)

        if os.path.isfile ('logs/factoid.log'):
            os.rename ('logs/factoid.log',
                       'logs/%s/factoid.log.%s' % (log_month, log_day))

        else:
            self.log_general ('factoid.log ausente durante rotacionamento de logs')
            
        for channel in self.channellog:
            fd = self.channellog[channel]

            if os.fstat(fd.fileno())[6]:
                logfile = fd.name
                fd.close ()

                os.rename (logfile,
                           'logs/%s/%s.%s' % (log_month,
                                              os.path.basename (logfile),
                                              log_day))

                self.channellog[channel] = open (logfile, 'a')

        self.schedule_rotate ()

    def record_seen_nick_cb (self, nick, channel, msg):
        # NOT NEEDED FOR SQLOBJECT
        # quotes the quotes (hehehe) for the sql query
        # msg = msg.replace ('"', '""')

        seen_entry = Seen.select (Seen.q.nick == nick)
        if seen_entry.count () > 0:
            seen_entry = seen_entry[0]
            seen_entry.epoch = time.time ()
            seen_entry.channel = channel
            seen_entry.msg = msg
        else:
            Seen (nick = nick, epoch = time.time (),
                  channel = channel, msg = msg)

    def pubmsg_cb (self, connection, event):
        source = event.source ()
        from_nick = source.split ('!')[0]
        from_ident = source.split ('!')[1].split ('@')[0]
        from_host = source.split ('@')[1]

        channel = event.target ()
        msg = event.arguments ()[0].split (' ')
        
        self.log_message (channel, from_nick,
                          event.arguments ()[0])
        self.record_seen_nick_cb (from_nick.lower (), channel,
                                  event.arguments ()[0])

        if msg[0][:-1] == self.connection.get_nickname ():
            self.handle_direct_msg (msg[1:], from_nick, from_ident, from_host, channel)

    def action_cb (self, connection, event):
        target = event.target ()
        from_nick = event.source ().split ('!')[0]

        self.log_message (target, from_nick, event.arguments ()[0], True)

    def handle_direct_msg (self, msg, from_nick, from_ident, from_host, channel):
        if not len(msg):
            sys.stderr.write ('handle_direct_msg: received empty msg from %s!%s@%s on channel %s\n' % (from_nick, from_ident, from_host, channel))
            return

        if from_nick in ignored:
            return

        try:
            index = msg.index ('eh')
            msg[index] = 'é'
        except ValueError:
            pass

        msgstring = ''
        for part in msg:
            msgstring = "%s %s" % (msgstring, part)
        msgstring = msgstring.strip ()

        if msg[0] == 'privexec' and len(msg) > 1:
            result = Privileged.select (Privileged.q.hostname == from_host)

            if not result.count ():
                self.log_private ('Tentativa de uso do privexec por host não autorizado: %s!%s@%s' % (from_nick, from_ident, from_host))

            else:
                command = msgstring[msgstring.find (' ') + 1:]

                try:
                    privcom = compile (command, 'private_logs/1_private.log', 'single')
                    exec (privcom)
                except:
                    self.connection.privmsg (from_nick,
                                             'Um erro ocorreu ao tentar executar %s' % command)
            return

        if re.match ('quem (te|lhe) ensinou .+\?', msgstring.lower()):
            factoid = ''
            for part in msg[3:]:
                factoid = '%s %s' % (factoid, part)

            factoid = factoid[:-1].strip ().lower ()
           
            teacher = self.get_teacher (factoid)
            if teacher == None:
                amsg = '%s: desculpe, não me lembro =/' % (from_nick)
            else:
                if teacher == from_nick:
                    ateacher = 'você'
                else:
                    ateacher = teacher
                    
                amsg = '%s: quem me ensinou %s foi %s' % \
                       (from_nick, factoid, ateacher)
                
            self.say (amsg, channel)

        elif re.match ('(você |voce )?viu .+', msgstring.lower ()):
            try:
                seen_nick = msg[msg.index ('viu') + 1]
            except ValueError:
                self.say ('%s: hein?' % (from_nick), channel)
                return
            if seen_nick.find ('?') != -1:
                seen_nick = seen_nick[:seen_nick.find('?')]

            if seen_nick == from_nick:
                self.say ('%s: tá de sacanagem, é?' % (from_nick),
                          channel)
            elif seen_nick == self.connection.get_nickname ():
                self.say ('%s: eu tô aqui, ora!' % (from_nick),
                          channel)
            else:
                result = Seen.select (Seen.q.nick == seen_nick.lower ())

                if not result.count ():
                    self.say ('%s: não me lembro de ter visto %s por aqui...' % \
                              (from_nick, seen_nick), channel)
                else:
                    result = result[0]
                    seen_time = time.localtime (result.epoch)
                    fmt_time = time.strftime ('%H:%M:%S %d %Y', seen_time).split (' ')
                    seen_channel = result.channel
                    seen_msg = result.msg

                    week_days = [ 'segunda-feira', 'terça-feira', 'quarta-feira', 'quinta-feira', 'sexta-feira', 'sábado', 'domingo' ]
                    months = [ 'janeiro', 'fevereiro', 'março', 'abril', 'maio', 'junho', 'julho', 'agosto', 'setembro', 'outubro', 'novembro', 'dezembro' ]

                    self.say ('%s: eu vi %s pela última vez há %s, às %s de %s, %s de %s de ' \
                              '%s, no canal %s, dizendo "%s".' \
                              % (from_nick, seen_nick, misc.duration (result.epoch), \
                                 fmt_time [0], week_days [seen_time [6]], fmt_time [1], \
                                 months [seen_time [1] - 1], fmt_time [2], \
                                 seen_channel, seen_msg), channel)

        elif re.match ('(que )?hora(s)?(sao|são|é)?', msgstring):
            if time.localtime ()[3] <= 1:
                now_verb = 'é'
            else:
                now_verb = 'são'

            self.say ('%s: agora %s %s!' % (from_nick, now_verb, time.strftime ('%H:%M:%S')),
                      channel)

        elif re.match ('(o que é|quem é)? ?.+\?$', msgstring):
            factoid = ''
            try:
                for part in msg[msg.index ('é') + 1:]:
                    factoid = '%s %s' % (factoid, part)
            except ValueError:
                factoid = msgstring

            factoid = factoid[:-1].strip ().lower ()
            if factoid == 'você' or factoid == 'voce':
                factoid = self.connection.get_nickname ()

            definition = self.get_factoid (factoid)
            if definition == None:
                amsg = '%s: eu não sei =(' % (from_nick)
            else:
                try:
                    definition.index ('<redirecionar>')
                    ind = definition.index ('>')
                    definition = self.get_factoid (definition[ind+1:])
                except ValueError:
                    pass
                    
                if factoid == from_nick:
                    afactoid = 'você'
                    verb = 'é'
                elif factoid == self.connection.get_nickname ():
                    afactoid = 'eu'
                    verb = 'sou'
                else:
                    afactoid = factoid
                    verb = 'é'
                    
                definition = definition.replace ('$interlocutor', from_nick)

                try:
                    definition.index ('<resposta>')
                    ind = definition.index ('>')
                    amsg = '%s' % (definition[ind+1:])
                except ValueError:
                    amsg = '%s: %s %s %s' % (from_nick, afactoid,
                                             verb, definition)
            self.say (amsg, channel)

        elif re.match ('.+ é .+', msgstring):
            if msg[0] == 'nao,' or msg[0] == 'naum,':
                msg[0] = 'não,'

            definition_starts = msg.index ('é') + 1

            defined_obj = ''
            for part in msg:
                if part == 'não,':
                    continue
                if part == 'é':
                    break
                defined_obj = '%s %s' % (defined_obj, part)
            defined_obj = defined_obj.strip ().lower ()
            if defined_obj == 'você' or defined_obj == 'voce':
                defined_obj = self.connection.get_nickname ()

            factoid = self.get_factoid (defined_obj)
            if factoid:
                try:
                    factoid.index ('<redirecionar>')
                    ind = factoid.index ('>')

                    self.say ('%s: %s é um redirecionamento para %s, impossível sobrescrever' % \
                              (from_nick, defined_obj, factoid[ind+1:]), channel)

                    return
                except ValueError:
                    pass

                if msg[0] != 'não,':
                    self.say ('%s: eu já conheço %s...' % (from_nick, defined_obj),
                              channel)
                    return

            definition = ''
            for part in msg[definition_starts:]:
                definition = '%s %s' % (definition, part)
            definition = definition.strip ()
            
            # saves this at table factoids
            self.save_factoid (defined_obj, definition,
                               from_nick, channel)

            self.say ('%s: peguei!' % (from_nick), channel)

        elif re.match ('(esquecer|esqueça) .+', msgstring):
            factoid = ''
            for part in msg[1:]:
                factoid = '%s %s' % (factoid, part)
            factoid = factoid.strip ().lower ()

            if factoid == 'você' or factoid == 'voce':
                factoid = self.connection.get_nickname ()

            if self.get_factoid (factoid) == None:
                self.say ('%s: eu já não sabia o que era %s...' % \
                          (from_nick, factoid), channel)
                return

            self.forget_factoid (factoid, from_nick)
            self.say ('%s: eu esqueci %s!' % (from_nick, factoid),
                      channel)
        else:
            tell = False

            match = re.match ('(contar|dizer|falar|conte|diga|fale)( sobre)? (?P<what>.+) (a|à|ao|para|pro|pra) (?P<whom>.+)', msgstring)
            if not match:
                match = re.match ('(contar|dizer|falar|conte|diga|fale) (a|à|ao|para|pro|pra) (?P<whom>.+) sobre (?P<what>.+)', msgstring)

            if match:
                tell = True
                gdict = match.groupdict ()
                what = gdict['what']
                whom = gdict['whom']

                if irclib.is_channel (whom):
                    self.log_general ('tentativa de uso do "tell" para canal (%s) por %s' % \
                                      (whom, from_nick))
                    return
                what = what.strip ().lower ()
                if what == 'você' or what == 'voce':
                    what = self.connection.get_nickname ()

                definition = self.get_factoid (what)
                
                if definition == None:
                    self.say ('%s: eu não sei o que é %s =(' % (from_nick, what), channel)
                    return
                else:
                    # we should check that the target definition does
                    # not exist!
                    try:
                        definition.index ('<redirecionar>')
                        ind = definition.index ('>')
                        definition = self.get_factoid (definition[ind+1:])
                    except ValueError:
                        pass

                if what == from_nick:
                    afactoid = 'você'
                    verb = 'é'
                elif what == self.connection.get_nickname ():
                    afactoid = 'eu'
                    verb = 'sou'
                else:
                    afactoid = what
                    verb = 'é'

                definition = definition.replace ('$interlocutor', whom)

                try:
                    definition.index ('<resposta>')
                    ind = definition.index ('>')
                    amsg = '%s' % (definition[ind+1:])
                except ValueError:
                    amsg = '%s %s %s' % (afactoid, verb, definition)

                self.say ('%s: %s' % (whom, amsg), channel)

    def save_factoid (self, name, definition, nick, channel):
        subst = None
        #name = name.replace ('"', '""')
        #definition = definition.replace ('"', '""')

        result = Factoids.select (Factoids.q.name == name)

        if result.count ():
            subst = 1
            result = result[0]
            result.name = name
            result.definition = definition
            result.who = nick
            result.channel = channel
            result.time = time.time ()
        else:
            Factoids (name = name, definition = definition, who = nick,
                      channel = channel, time = time.time ())

        if subst:
            self.log_factoid (name, 2, nick)
        else:
            self.log_factoid (name, 0, nick)

    def forget_factoid (self, factoid, who):
        #factoid = factoid.replace ('"', '""')

        row = Factoids.select (Factoids.q.name == factoid)
        if row.count ():
            row = row[0]
            row.delete (row.id)
            self.log_factoid (factoid, 1, who)

    def get_factoid (self, factoid):
        #factoid = factoid.replace ('"', '""')
        result = Factoids.select (Factoids.q.name == factoid)

        if result.count ():
            return result[0].definition
        else:
            return None

    def get_teacher (self, factoid):
        #factoid = factoid.replace ('"', '""')
        result = Factoids.select (Factoids.q.name == factoid)

        if result.count ():
            return result[0].who
        else:
            return None

    def say (self, msg, channel):
        self.log_message (channel, self.connection.get_nickname (), msg)
        self.connection.privmsg (channel, msg)

    def privmsg_cb (self, connection, event):
        source = event.source ()
        from_nick = source.split ('!')[0]
        from_ident = source.split ('!')[1].split ('@')[0]
        from_host = source.split ('@')[1]

        channel = event.target ()
        msg = event.arguments ()[0].split (' ')
        my_nick = self.connection.get_nickname ()

        self.log_message (from_nick, from_nick, event.arguments ()[0])
        
        if msg[0][:-1] == my_nick:
            self.handle_direct_msg (msg[1:], from_nick, from_ident, from_host, channel)
        else:
            self.handle_direct_msg (msg, from_nick, from_ident, from_host, from_nick)

    def ping_server (self):
        if self.connection.is_connected ():
            self.awaited_ping = time.time ()

            self.connection.send_raw ('PING :%s' % (self.awaited_ping))

    def pong_cb (self, connection, event):
        # For now, this won't be checked, but could be, in the future
        server = event.target ()
        time = event.arguments ()[0]

        if str (self.awaited_ping) != time:
            self.log_general ('PONG recebido (%s) diferente do esperado (%s), desconectando' % (time, str (self.awaited_ping)))
            self.disconnect ()
            return

        self.awaited_ping = None
        self.connection.execute_delayed (10*60, self.ping_server)
        
    def process_forever (self):
        while True:
            if not self.connection.is_connected ():
                self.connect ()
            elif self.awaited_ping:
                if time.time () - self.awaited_ping > 10*60:
                    self.disconnect ()
            try:
                self.process_once (0.1)
            except ValueError:
                pass

def handle_signal (signumber, isf):
    if aptbr.connection.is_connected ():
        aptbr.disconnect ('Me fizeram sair, mas eu já volto. Só um instante!')
        
signal.signal (signal.SIGTERM, handle_signal)

try:
    handle = open ('bot.pid')
    pid = int(handle.read ().strip ())
    handle.close ()
    proc_info = open ('/proc/%d/cmdline' % (pid))
    info = proc_info.read ().replace ('\x00', ' ').strip ()
    if info.endswith ('main.py'):
        sys.exit (0)
    proc_info.close ()
except IOError:
    pass

handle = open ('bot.pid', 'w')
handle.write (str(os.getpid ()))
handle.close ()

os.environ['TZ'] = 'America/Sao_Paulo'
time.tzset ()

ignored = open ('ignore.list').read ().strip ('\n')

aptbr = AptBR ()
aptbr.process_forever ()
