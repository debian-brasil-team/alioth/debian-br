#!/bin/bash
# 
# Script de conversão simples de conversão do arquivos do apt-br de ISO-8859-1 
# para UTF-8. 
# Óbvio que o conteúdo é GPL.
# Escrito em 17/04/2005.

for i in `find -type f`;
do
	iconv -f ISO-8859-1 -t UTF-8 $i > $i.utf
	rm $i && mv $i.utf $i
done
