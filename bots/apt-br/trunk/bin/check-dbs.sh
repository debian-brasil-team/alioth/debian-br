#!/bin/sh
# Este script tem a fun��o de verificar por inconsist�ncias e problemas
# nos bancos de dados is e are em texto plano, relatando erros cometidos 
# com frequencia durante a edi��o destes arquivos

if [ -z $1 ];then
 echo Especifique um arquivo a ser lido. Por exemplo apt-br-is.txt
 exit 1
fi

if [ ! -f $1 ];then
 echo O arquivo especificado n�o existe!
 exit
fi

echo "[------------ Iniciando a verifica��o no arquivo ${1} ------------]"
echo;echo
echo "[-------------------------- Falta do  => ---------------------------]"
cat $1|grep -v '=>'|grep -v '^#'

echo;echo
echo "[--------------------- Espa�o duplo antes do => ---------------------]"
cat $1|grep -i '  => '

echo;echo
echo "[-------------------- Espa�o duplo depois do => ---------------------]"
cat $1|grep -i ' =>  '

echo;echo
echo "[------------------ Espa�o duplo depois do <reply> ------------------]"
cat $1|grep -i ' <reply>  '

echo;echo
echo "[------------------ Espa�o duplo depois do <reply> ------------------]"
cat $1|grep -i '  <reply> '

echo;echo
echo "[------------------ Falta de espa�o depois do <reply> ---------------]"
cat $1|grep -i '<reply>[a-zA-Z^$]'

echo;echo





exit 0
