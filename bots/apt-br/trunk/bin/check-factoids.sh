#!/bin/sh
# Este Script verifica quais foram as novas inclus�es no banco de dados do 
# infobot analisando o arquivos de log e tamb�m dizendo qual foi o usu�rio
# que fez o cadastro
#
# Gleydson Mazioli da Silva <gleydson@escelsanet.com.br>

if [ -z $1 ];then
 echo Voce precisa especificar o arquivo de log a ser lido... ele pode 
 echo ser especificado como um arquivo compactado .gz
 exit 1
fi

file $1|grep gzip &>/dev/null 
if [ $? = 0 ]; then
  COMPACTADO="1"
 else
  COMPACTADO="0"
fi


echo;echo
echo "[------------------------ NOVOS FACTOIDS ---------------------------- ]"
if [ "${COMPACTADO}" = "1" ] ; then
  zcat $1|grep 'enter:'|grep 'eh'|grep -v 'nao'|grep -v 'tambem'|cut -c 25-
 else
  cat $1|grep 'enter:'|grep 'eh'|grep -v 'nao'|grep -v 'tambem'|cut -c 25-
fi

if [ "${COMPACTADO}" = "1" ] ; then
  zcat $1|grep 'enter:'|grep 'sao'|grep -v 'nao'|grep -v 'tambem'|cut -c 25-
 else
  cat $1|grep 'enter:'|grep 'sao'|grep -v 'nao'|grep -v 'tambem'|cut -c 25-
fi

echo;echo 
echo "[ --------------------- REMO��O DE FACTOIDS ------------------------ ]"
if [ "${COMPACTADO}" = "1" ] ; then
  zcat $1|grep 'esquecer:'|grep '=>'|cut -c 16-
 else
  cat $1|grep 'esquecer:'|grep '=>'|cut -c 16-
fi

echo;echo
echo "[ ------------------- MODIFICA��O DE FACTOIDS ----------------------- ]"
if [ "${COMPACTADO}" = "1" ] ; then
  zcat $1|grep 'update:'|cut -c 26- 
  zcat $1|grep 'statement:'|grep 'tambem'|cut -c 26-
 else
  cat $1|grep 'update:'|cut -c 26-
  cat $1|grep 'statement:'|grep 'tambem'|cut -c 26-
fi

echo;echo
echo "[ ---------------------------- FIM --------------------------------- ]"

exit 0

