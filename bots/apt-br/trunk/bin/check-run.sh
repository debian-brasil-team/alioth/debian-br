#!/bin/sh
# A fun��o deste shell script � verificar se o apt-br est� rodando e 
# reinicia-lo caso n�o estiver. Um email � enviado aos usu�rios 
# especificados em SEND_TO avisando sobre o reinicio do apt-br. 
#
# Gleydson Mazioli da Silva <gleydson@escelsanet.com.br>

RUN_UID="infobot"
INFOBOT_DIR=~/infobot
LOG_FILE=apt-br.log
SEND_TO="gleydson"

ps aux|grep ${RUN_UID}|grep 'perl infobot'| \
grep -v 'grep perl infobot' &>/dev/null

if [ $? != 0 ]
then
 FILE_TIME=`ls -la ${INFOBOT_DIR}/${LOG_FILE}|cut -c 50-55`

 for EMAIL in $SEND_TO; do
  cat <<EOF|mail -s "Reinicio do apt-br: $(date +%T-%d/%m)" $EMAIL
  Foi verificada a interrup��o no funcionamento do apt-br as $FILE_TIME
e reinicio do bot as $(date +%T). 
  Verifique o log do apt-br procurando por qualquer a��o que tenha 
ocorrido pouco antes do hor�rio da interrup��o para determinar a 
causa da interrup��o (como flood, nukes, etc).

OBS: Quando o bot � reiniciado, a numera��o das linhas no arquivo 
     de log reinicia a partir da 1. 
EOF
 done
cd ${INFOBOT_DIR}
perl infobot &

fi
exit 0
