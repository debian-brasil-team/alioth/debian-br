#!/bin/sh
# Este Script verifica quais foram as novas inclus�es no banco de dados do 
# infobot analisando o arquivos de log e tamb�m dizendo qual foi o usu�rio
# que fez o cadastro
#
# Gleydson Mazioli da Silva <gleydson@escelsanet.com.br>

if [ -z $1 ];then
 echo Voce precisa especificar o arquivo de log a ser lido... ele pode 
 echo ser especificado como um arquivo compactado .gz
 exit 1
fi

file $1|grep gzip &>/dev/null
if [ $? = 0 ] ; then
  COMPACTADO="1"
 else
  COMPACTADO="0"
fi


echo;echo
echo "[ --------------- URLs HTTP e FTP Mencionadas no canal --------------- ]"
if [ "${COMPACTADO}" = "1" ] ; then
  zcat $1|grep 'http:'|grep -v 'apt-br'|grep -v 'type'|grep -v 'exact:'|grep -v 'statement:'|grep -v 'update'|grep -v 'match:'|grep -v 'enter:'|grep -v 'esquecer:'|grep -v 'deixou'|grep -v 'notfound'|grep -v 'question'|grep -v 'ajustou o topic'|grep -v 'topic for'|cut -c 11-
  zcat $1|grep 'www.'|grep -v 'http'|grep -v 'apt-br'|grep -v 'type'|grep -v 'exact:'|grep -v 'statement:'|grep -v 'update'|grep -v 'match:'|grep -v 'enter:'|grep -v 'esquecer:'|grep -v 'deixou'|grep -v 'notfound'|grep -v 'question'|grep -v 'ajustou o topic'|grep -v 'topic for'|cut -c 11-
 else
  cat $1|grep 'http:'|grep -v 'apt-br'|grep -v 'type'|grep -v 'exact:'|grep -v 'statement:'|grep -v 'update'|grep -v 'match:'|grep -v 'enter:'|grep -v 'esquecer'|grep -v 'deixou'|grep -v 'notfound'|grep -v 'question'|grep -v 'ajustou o topic'|grep -v 'topic for'|cut -c 11-
  cat $1|grep 'www.'|grep -v 'http'|grep -v 'apt-br'|grep -v 'type'|grep -v 'exact:'|grep -v 'statement:'|grep -v 'update'|grep -v 'match:'|grep -v 'enter:'|grep -v 'esquecer'|grep -v 'deixou'|grep -v 'notfound'|grep -v 'question'|grep -v 'ajustou o topic'|grep -v 'topic for'|cut -c 11-
fi

if [ "${COMPACTADO}" = "1" ] ; then
  zcat $1|grep 'ftp:'|grep -v 'apt-br'|grep -v 'type'|grep -v 'exact:'|grep -v 'statement:'|grep -v 'update'|grep -v 'match:'|grep -v 'enter:'|grep -v 'esquecer'|grep -v 'deixou'|grep -v 'notfound'|grep -v 'question'|grep -v 'ajustou o topic'|grep 'topic for'|cut -c 11-
  zcat $1|grep 'ftp.'|grep -v 'ftp:'|grep -v 'apt-br'|grep -v 'type'|grep -v 'exact:'|grep -v 'statement:'|grep -v 'update'|grep -v 'match:'|grep -v 'enter:'|grep -v 'esquecer'|grep -v 'deixou'|grep -v 'notfound'|grep -v 'question'|grep -v 'ajustou o topic'|grep 'topic for'|cut -c 11-
 else
  cat $1|grep 'ftp:'|grep -v 'apt-br'|grep -v 'type'|grep -v 'exact:'|grep -v 'statement:'|grep -v 'update'|grep -v 'match:'|grep -v 'enter:'|grep -v 'esquecer'|grep -v 'deixou'|grep -v 'notfound'|grep -v 'question'|grep -v 'ajustou o topic'|grep 'topic for'|cut -c 11-
  cat $1|grep 'ftp.'|grep -v 'ftp:'|grep -v 'apt-br'|grep -v 'type'|grep -v 'exact:'|grep -v 'statement:'|grep -v 'update'|grep -v 'match:'|grep -v 'enter:'|grep -v 'esquecer'|grep -v 'deixou'|grep -v 'notfound'|grep -v 'question'|grep -v 'ajustou o topic'|grep 'topic for'|cut -c 11-

fi

echo;echo 
echo "[ ---------------- MUDAN�AS DE TOPIC  ----------------- ]"
if [ "${COMPACTADO}" = "1" ] ; then
  zcat $1|grep -i 'mudan�a de topic'|cut -c 11-  
  zcat $1|grep -i 'ajustou o topic'|cut -c 11-
 else
  cat $1|grep -i 'mudan�a de topic'|cut -c 11-
  cat $1|grep -i 'ajustou o topic'|cut -c 11-
fi

echo;echo 
echo "[ ---------------- PACOTES CITADOS PARA INSTALA��O  ----------------- ]"
if [ "${COMPACTADO}" = "1" ] ; then
  zcat $1|grep -i 'apt-get install'|cut -c 11-
  zcat $1|grep -i 'dpkg -i'|cut -c 11-
 else
  cat $1|grep -i 'apt-get install'|cut -c 11-
  cat $1|grep -i 'dpkg -i'|cut -c 11-
fi

echo;echo 
echo "[ ------------------- VITIMAS BANIDAS NO CANAL  ---------------------- ]"
if [ "${COMPACTADO}" = "1" ] ; then
  zcat $1|grep -v 'apt-br'|grep 'mode'|grep '+b'|cut -c 11-
 else
  cat $1|grep -v 'apt-br'|grep 'mode'|grep '+b'|cut -c 11-
fi

echo;echo 
echo "[ ------------------ VITIMAS CHUTADAS NO CANAL  ---------------------- ]"
if [ "${COMPACTADO}" = "1" ] ; then
  zcat $1|grep -v 'apt-br'|grep 'foi chutado'|cut -c 11-
 else
  cat $1|grep -v 'apt-br'|grep 'foi chutado'|cut -c 11-
fi

echo;echo 
echo "[ ------------------ USO INCORRETO DE NICKS ---------------------- ]"
if [ "${COMPACTADO}" = "1" ] ; then
  zcat $1|grep -i 'Killed'|grep -i 'This nick is reserved'|cut -c 11-
 else
  cat $1|grep -i 'Killed'|grep -i 'This nick is reserved'|cut -c 11-
fi

echo;echo 
echo "[ ------------------ USO da conta root no IRC -------------------- ]"
if [ "${COMPACTADO}" = "1" ] ; then
  zcat $1|grep -i 'root@'|grep -i 'entrou'|cut -c 11-
 else
  cat $1|grep -i 'root@'|grep -i 'entrou'|cut -c 11-
fi


echo;echo
echo "[ --------------- PALAVR�ES/OFENSAS FALADOS NO CANAL ----------------- ]"
if [ "${COMPACTADO}" = "1" ] ; then
  zcat $1|grep -i 'porra '|cut -c 11- 
  zcat $1|grep -i ' puta'|cut -c 11-
  zcat $1|grep -i 'caralho'|cut -c 11-
  zcat $1|grep -i 'buceta'|cut -c 11-
  zcat $1|grep -i ' viad'|cut -c 11-
  zcat $1|grep -i ' demen[tecia]'|cut -c 11-
  zcat $1|grep -i ' froxo'|cut -c 11-
  zcat $1|grep -i ' c[u�] '|cut -c 11-
  zcat $1|grep -i ' �gua '|cut -c 11-
  zcat $1|grep -i 'quatro'|grep 'olho'|cut -c 11-
  zcat $1|grep -i ' negr[ao] '|cut -c 11-
  zcat $1|grep -i 'retardad[ao]'|cut -c 11-
  zcat $1|grep -i 'bicha'|cut -c 11-
  zcat $1|grep -i 'sapat[�a]o'|cut -c 11-
 else
  cat $1|grep -i 'porra '|cut -c 11-
  cat $1|grep -i ' puta'|cut -c 11-
  cat $1|grep -i 'caralho'|cut -c 11-
  cat $1|grep -i 'buceta'|cut -c 11-
  cat $1|grep -i ' viad'|cut -c 11-
  cat $1|grep -i ' demen[tecia]'|cut -c 11-
  cat $1|grep -i ' froxo'|cut -c 11-
  cat $1|grep -i ' c[u�] '|cut -c 11-
  cat $1|grep -i ' �gua '|cut -c 11-
  cat $1|grep -i 'quatro'|grep 'olho'|cut -c 11-
  cat $1|grep -i ' negr[ao] '|cut -c 11-
  cat $1|grep -i 'retardad[ao]'|cut -c 11-
  cat $1|grep -i 'bicha'|cut -c 11-
  cat $1|grep -i 'sapat[�a]o'|cut -c 11-
fi
echo;echo

echo;echo 
echo "[ ---------------------- A��ES CONTRA O BOT  ------------------------ ]"
if [ "${COMPACTADO}" = "1" ] ; then
  zcat $1|grep 'apt-br'|grep 'mode'|grep '+b'|cut -c 11-
  zcat $1|grep 'apt-br\[0m foi chutado'|cut -c 11-
  zcat $1|grep 'Cannot join channel (+b)'|cut -c 11-
  zcat $1|grep 'Tentativa de kick do pr�prio bot'|cut -c 11-
 else
  cat $1|grep 'apt-br'|grep 'mode'|grep '+b'|cut -c 11-
  cat $1|grep 'apt-br\[0m foi chutado'|cut -c 11-
  cat $1|grep 'Cannot join channel (+b)'|cut -c 11-
  cat $1|grep 'Tentativa de kick do pr�prio bot'|cut -c 11-
fi


echo;echo
echo "[ ------------------- REQUISI��O DE OPERADOR  ----------------------- ]"
if [ "${COMPACTADO}" = "1" ] ; then
  zcat $1|grep 'tentando dar OP na'|cut -c 11-
 else
  cat $1|grep 'tentando dar OP na'|cut -c 11-
fi

echo;echo
echo "[ ---------------------------- FIM --------------------------------- ]"

exit 0

