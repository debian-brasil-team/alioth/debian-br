#!/bin/sh
# A função deste shell script é verificar se o dpkg-br está rodando e 
# reinicia-lo caso não estiver. Um email é enviado aos usuários 
# especificados em SEND_TO avisando sobre o reinicio do dpkg-br. 
#
# Gleydson Mazioli da Silva <gleydson@escelsanet.com.br>

RUN_UID="infobot"
DPKG_DIR=~/dpkg-br
LOG_FILE=eggdrop.log
SEND_TO="gleydson"

ps aux|grep ${RUN_UID}|grep 'eggdrop egg.config'| \
grep -v 'grep eggdrop egg.config' &>/dev/null

if [ $? != 0 ]
then
 FILE_TIME=`ls -la ${DPKG_DIR}/${LOG_FILE}|cut -c 50-55`

 for EMAIL in $SEND_TO; do
  cat <<EOF|mail -s "Reinicio do dpkg-br: $(date +%T-%d/%m)" $EMAIL
  Foi verificada a interrupção no funcionamento do dpkg-br as $FILE_TIME
e reinicio do bot as $(date +%T). 
  Verifique o log do dpkg-br procurando por qualquer ação que tenha 
ocorrido pouco antes do horário da interrupção para determinar a 
causa da interrupção (como flood, nukes, etc).

OBS: Quando o bot é reiniciado, a numeração das linhas no arquivo 
     de log reinicia a partir da 1. 
EOF
 done
cd ${DPKG_DIR}
./eggdrop egg.config

fi
exit 0
