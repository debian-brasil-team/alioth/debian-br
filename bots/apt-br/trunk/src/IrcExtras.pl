# infobot :: Kevin Lenzo (c) 1999

use Socket;

$| = 1;

$SIG{'INT'}  = 'killed'; 
$SIG{'KILL'} = 'killed';
$SIG{'TERM'} = 'killed';

$updateCount = 0;
$questionCount = 0;
$autorecon = 0;

$label = "(?:[a-zA-Z\d](?:(?:[a-zA-Z\d\-]+)?[a-zA-Z\d])?)";
$dmatch = "(?:(?:$label\.?)*$label)";
$ipmatch = "\d+\.\d+\.\d+\.\d";
$ischan = "[\#\&].*?";
$isnick = "[a-zA-Z]{1}[a-zA-Z0-9\_\-]+";

sub TimerAlarm {
    &status("$TimerWho's timer finalizado, enviando despertador");
    &say("$TimerWho: Esta é sua chamada para acordar cara.");
}

sub killed {
    my $quitMsg = $param{'quitMsg'} || "regrouping";
    &quit($quitMsg);
    &closeDBMAll();
	# MUHAHAHAHA.
    exit(1);
}

sub topic {
    my ($chan, $msg) = @_;
    &status("CANAL: Mudança do topic do $chan para $msg");
    rawout("TOPIC $chan :$msg");
}

sub kick {
    my ($nick,$chan,$msg) = @_;
    my (@chans) = ($chan eq "") ? (keys %channels) : lc($chan);

    $nick =~ tr/A-Z/a-z/;

    foreach $chan (@chans) {

	&status("$nick foi chutado do canal $chan.");
	if ($msg eq "") {
	    &rawout("KICK $chan $nick");
	  } else {
	    &rawout("KICK $chan $nick :$msg");
	}
    }
}

sub ban {
    my ($mask,$chan) = @_;
    my (@chans) = ($chan eq "") ? (keys %channels) : lc($chan);

    $nick =~ tr/A-Z/a-z/;

    foreach $chan (@chans) {


        &status("$mask foi banido do canal $chan.");
	&rawout("MODE $chan +b $mask");
    }
}

# Adicionado para checar se o bot está usando o nick especificado, caso 
# não estiver, recupera o nick e se identifica no nickserv
sub check_nick {
if ($param{'wantNick'} !~ /$ident/) {
    &status("Tentando reaver o nick $param{'wantNick'}...");
    $ident = $param{'wantNick'};
    $param{nick} = $ident; 
    &nick($ident);
    &status("Reidentificando no nickServ");
    rawout("PRIVMSG NickServ :IDENTIFY $param{'nickServ_pass'}");
 }
}

sub joinChan {
    foreach (@_) {
	&status("entrando no canal $_");
	rawout("JOIN $_");
    }
}

sub invite {
    my($who, $chan) = @_;
    rawout("INVITE $who $chan");
}

sub notice {
    my($who, $msg) = @_;
    foreach (split(/\n/, $msg)) {
	rawout("NOTICE $who :$_");
    }
}

sub say {
    my @what = @_;
    my ($line, $each, $count);

    foreach $line (@what) {
	for $each (split /\n/, $line) {
	    sleep 1 if $count++;
	    if (getparam("msgonly")) {
		&msg ($who, $each);
	    } else {
		&status("</$talkchannel> $each");
		rawout("PRIVMSG $talkchannel :$each");
	    }
	}
    }
}

sub msg {
    my $nick = shift;
    my @what = @_;

    my ($line, $each, $count);

    foreach $line (@what) {
	for $each (split /\n/, $line) {
	    sleep 1 if $count++;
	    &status(">$nick< $each");
	    rawout("PRIVMSG $nick :$each");
	}
    }

}

sub quit {
    my $quitmsg = $_[0];
    rawout("QUIT :$quitmsg");
    &status("QUIT $param{nick} saiu do IRC ($quitmsg)");
    close(SOCK);
}

sub nick {
    $nick = $_[0];
    rawout("NICK ".$nick);
}

sub part {
    foreach (@_) {
	status("left $_");
	rawout("PART $_");
	delete $channels{$_};
    }
}

sub mode {
    my ($chan, @modes) = @_;
    my $modes = join(" ", @modes);
    rawout("MODE $chan $modes");
}

sub op {
    my ($chan, $arg) = @_;
    $arg =~ s/^\s+//;
    $arg =~ s/\s+$//;
    $arg =~ s/\s+/ /;
    my @parts = split(/\s+/, $arg); 
    my $os = "o" x scalar(@parts);
    mode($chan, "+$os $arg");
}

sub deop {
    my ($chan, $arg) = @_;
    $arg =~ s/^\s+//;
    $arg =~ s/\s+$//;
    $arg =~ s/\s+/ /;
    my @parts = split(/\s+/, $arg); 
    my $os = "o" x scalar(@parts);
    &mode($chan, "-$os $arg");
}

sub timer {
    ($t, $timerStuff) = @_;
    # alarm($t);
}

$SIG{"ALRM"} = \&doTimer;

sub doTimer {
    rawout($timerStuff);
}

sub channel {
    if (scalar(@_) > 0) {
	$talkchannel = $_[0];
    }
    $talkchannel;
}

sub rawout {
    $buf = $_[0];
    $buf =~ s/\n//gi;
    select(SOCK); $| = 1;
    print SOCK "$buf\n";
    select(STDOUT);
}

1;
