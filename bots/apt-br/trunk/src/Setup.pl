
# infobot :: Kevin Lenzo   (c) 1997-2000

sub setup {
# param setup should stay after most of the requires
# so that it overrides anything they might set.
    &paramSetup();

    if ($param{VERBOSITY} > 1) {
	my $params = "Parametros sao:\n";
	foreach (sort keys %param) {
	    $params .= "   $_ -> $param{$_}\n";
	}
	&status($params);
    }

    die "dbname eh nulo" if (!$param{'dbname'});

    %dbs = ("is" => "$param{basedir}/$param{dbname}-is",
	    "are" => "$param{basedir}/$param{dbname}-are");
    srand();

    $setup_time = scalar(localtime());
    $setup_time =~ s/\n//g;

    $startTime = time();

    &setup_help;
    &openDBM(%dbs);

    $qCount = &get("is", "the qCount");
    $qEpochTime = &get("is", "the qEpochTime");

    # things to say when people thank me
    @welcomes = ('sem problemas', 'o prazer eh meu', 'coisa certa',
		 'sem preocupação', 'de nada', 'falo', ':-)', 'disponha');

    # when i'm cofused and I have to reply
    @confused = ("huh?", 
		 "O que?", 
		 "Como?",
		 "Ham?",
		 "???",
		 "Pode ser mais especifico?",
		 "nao entendi...",
		 "nao entendi... voce pode ser mais especifico?",
		 "desculpe...", 
		 "Eu nao estou te entendendo...",
		 "me desculpe?");

    # when i recognize a query but can't answer it
    @dunno = ('Eu nao sei', 
	      'gostaria de saber',
	      'nao tenho pistas',
	      'nunca ouvi falar',
	      'nao conheço',
	      'eu heim!', 
	      'sem ideias',
	      'sei lah');



    # check the ignore parameter for a filename containing the
    # ignore list

    if ($param{ignore}) {
	&openDBMx('ignore');
    }
	
    if ($param{sanePrefix}) {
	for $d (qw/is are/) {
	    my $dbname = $DBprefix.$d;
	    my $sane = "$param{confdir}/$param{sanePrefix}";
	    $sane .= "-$d.txt";
	    if (-e $sane) {
		&status("carregando definições do sane $sane");
		&insertFile($dbname, $sane);
	    } else {
		&status("nao foi possível encontrar o arquivo $sane do sane.");
	    }
	}

	if (! open IGNORE, "$param{'confdir'}/$param{sanePrefix}-ignore.txt") {
	    &status("No fallback ignore file $param{'confdir'}/$param{sanePrefix}-ignore.txt");
	} else {
	    while (<IGNORE>) {
		s/^\s+//;
		s/\s+\#.*//;
		chomp;
		/\S/ && do {
		    &postInc(ignore => $_);
		    if ($param{'VERBOSITY'} > 0) {
			&status("Adicionando $_ na lista de ignorados (do sane).");
		    }
		};
	    }
	    close IGNORE;
	}
    }

    if ($param{'plusplus'}) {
	&openDBMx('plusplus');
    }

    if ($param{'seen'}) {
	&openDBMx('seen');
    }

    # set up the users and ops
	&status("Processando Arquivo de Usuário");
    &parseUserfile();

	&status("Processando Arquivo de Canal");
	# set up the channel file
	&parseChannelfile();

    # ways to say hello
    @hello = ('diga',     
	      'ola',
	      'opa',
	      'oi',
	      'ei',
	      'fala',
	      'hau',
	      'e ai, tudo em cima',
	      'beleza',
	      'tudo beleza');

    $param{'maxKeySize'}  ||= 30; # maximum LHS length
    $param{'maxDataSize'} ||= 200; # maximum total length

    if (!defined(@verb)) {
	@verb = split(" ", "eh sao");
	#  am was were does has can wants needs feels
	#  handle s-v agreement for non-being verbs later
    }

    if (!defined(@qWord)) {
	@qWord = split(" ", "que onde quem"); # why how when
    }

    # do this ONCE per startup to amortize.  Still too much mem.
    #&getAllKeys;
    $isCount = &getDBMKeys('is'); 
    $areCount = &getDBMKeys('are');
    $factoidCount = $isCount + $areCount;

    &status("setup: $factoidCount factoids; $isCount EH; $areCount SAO");
}


sub paramSetup {
    my $initdebug = 1;
    $param{'DEBUG'} = $initdebug;

    if (!@paramfiles) {
	# if there is no list of param files, just go for the default
	# (usually ./files/infobot.config)

	@paramfiles = ("$param{confdir}/infobot.config");
    }

    # now read in the parameter files
    &loadParamFiles(@paramfiles);
}


1;
