# infobot :: Kevin Lenzo   (c) 1997

sub getReply {
    my($msgType, $message, $msgFilter) = @_;
    my($theMsg) = "";
    my($locMsg) = $message;

    # x is y

    # x    is the lhs (left hand side)
    # 'is' is the mhs ("middle hand side".. the "head", or verb)
    # y    is the Y (right hand side)

    my($X, $V, $Y, $result);
    my ($theVerb, $orig_Y);

    $locMsg =~ tr/A-Z/a-z/;

    my $literal = ($locMsg =~ s/^literal //);

    if (getparam('rss') and $message =~ m/^perlfaq\'\s+(.*?)\?*$/) {
	# specially defined type.  get and process an RSS (RDF Site Summary)
	eval "use URI::Escape";
	not ($@) and do {
	    my $q = uri_escape($1, '\W');
	    my $result = &get_headlines("http://www.perlfaq.com/cgi-bin/rss/kw?q=$q");
	    if ($result =~ s/^error: //) {
		return "$who: couldn't get the perlfaq: $result";
	    } else {
		return "$who: $result";
	    }
	}
    } elsif ($result = get("is", $locMsg)) {
#	&status("exact: $message =eh=> $result");
	$theVerb = "eh";
	$X = $message;
	$V = $theVerb;
	$Y = $result;
	$orig_Y = $X;

    } elsif ($result = get("are", $locMsg)) {
#	&status("exact: $message =sao=> $result");
	$theVerb = "sao";
	$X = $message;
	$V = $theVerb;
	$Y = $result;
	$orig_Y = $X;

    } else { # no verb
	$y_determiner = '';
	$verbs = join '|', @verb;

	$message = " $message ";

	if ($message =~ / ($verbs) /i) {
	    $X = $`;
	    $V = $1; 
	    $Y = $';

	    $X =~ s/^\s*(.*?)\s*$/$1/;
	    $Y =~ s/^\s*(.*?)\s*$/$1/;
	    $orig_Y = $Y;
	    $Y =~ tr/A-Z/a-z/;

	    $V =~ s/^\s*(.*?)\s*$/$1/;

	    if ($Y =~ s/^(um|uma|o|a|os|as)\s+//) {
		$y_determiner = $1;
	    } else {
		$y_determiner = '';
	    }

	    if ($questionWord !~ /^\s*$/) {
		if ($V eq "eh") {
		    $result = &get("is", $Y);
		} else {
		    if ($V eq "sao") {
			$result = &get("are", $Y);
		    }
		}
	    }
	    $theVerb = $V;
	}

	if ($param{'VERBOSITY'} > 1) {
	    my $debugstring = "\tmsgType:\t$msgType\n";
	    $debugstring .= "\tquestionWord:\t$questionWord\n";
	    $debugstring .= "\taddressed:\t$addressed\n";
	    $debugstring .= "\tfinalQMark:\t$finalQMark\n";
	    $debugstring .= "\tX[$X] verb[$theVerb] det[$y_determiner] Y[$Y]\n";
	    $debugstring .= "\tresult:\t$result\n";
	    &status($debugstring);
	}

	if ($y_determiner) {
	    # put the det back on 
	    $Y = "$y_determiner $Y";
	}

# check "is" tables anyway for lhs alone

	if (!defined($V)) {	# no explicit head had been found
	    my $det;
	    if ($locMsg =~ s/^\s*(um|uma|a|o|as|os)\s+//) {
		$det = $1;
	    }
	    $locMsg =~ s/[.!?]+\s*$//;

	    my($check) = "";

	    $check = &get("is", $locMsg);

	    if ($check ne "") {
		$result = $check;
		$orig_Y = $locMsg;
		$theVerb = "eh";
		$V = "eh";	# artificially set the head to is
	    } else {
		$check = &get("are", $locMsg);
		if ($check ne "") {
		    $result = $check;
		    $V = "sao"; # artificially set the head to are
		    $orig_Y = $locMsg;
		    $theVerb = "sao";
		}
	    }
	    if ($det) {
		$orig_Y = "$det $orig_Y";
	    }
	}
    }

    if ($V ne "") {		# if there was a head...
	if (not $literal) {	# Changed to cope with $msgFilter - 26Jun19100, Masque
	    my(@poss) = split(/(?<!\\)\|/, $result);
	    $poss[0] =~ s/^\s//;
	    $poss[$#poss] =~ s/\s$//;
	    my @filtered  =   grep /\Q$msgFilter\E/, @poss unless $msgFilter eq "NOFILTER";

	    if (@filtered) {
                $theMsg =   $filtered[int(rand(@filtered))];
                $theMsg =~  s/^\s*//;
	    } elsif (@poss > 1 && $msgFilter eq "NOFILTER") {
		$theMsg =   $poss[int(rand(@poss))];
		$theMsg =~  s/^\s*//;
	    } else {
		if ($msgFilter eq "NOFILTER" || $result =~ /\Q$msgFilter\E/) {
			$theMsg = $result;
		} else {
			$theMsg = q!<reply>Hmm.  No matches for that, $who.!;
		}
	    }
            $theMsg =~ s/\\\|/\|/g;
	} else {
	    $theMsg = $result;
	}
    }

    $skipReply = 0;

    if ($theMsg ne "") {
	if ($msgType =~ /public/) {
	    my $interval = time() - $prevTime;
	    if ( ($param{'mode'} eq 'IRC' ) 
		&& getparam('repeatIgnoreInterval')
		&& ($theMsg eq $prevMsg) 
		&& ((time()-$prevTime) < getparam('repeatIgnoreInterval'))) {
		&status("repetição ignorada ($interval secs < ".getparam('repeatIgnoreInterval').")");
		$skipReply = 1;
		$theMsg = "NOREPLY";
		$prevTime = time();
	    } else {
		$skipReply = 0;
		$prevTime = time() unless ($theMsg eq $prevMsg);
		$prevMsg = $theMsg;
	    }
	}


	# by now $theMsg should contain the result, or null

	# this global is nto a great idea
	$shortReply = 0;
        $noReply = 0;
       
	if (0 and $theMsg =~ s/^\s*<noreply>\s*//i) { 
	    # specially defined type. No reply. Experimental.
	    $noReply = 1;
	    return 'NOREPLY';
	}

	if (!$msgType) {
	    $msgType = 'private';
	    &status("NO MSG TYPE / ajuste para private\n");
	}
	
	if ($literal) {
	    $orig_Y =~ s/^literal //;
	    $theMsg = "$who: $orig_Y =$theVerb= $theMsg";
	    return $theMsg;
	}

#	if ($msgType !~ /private/ and $theMsg =~ s/^\s*<reply>\s*//i) {
	if ($theMsg =~ s/^\s*<reply>\s*//i) {
	
	    # specially defined type.  only remove '<reply>'
	    $shortReply = 1;
	} elsif (getparam('rss') and $theMsg =~ m/(<(?:rss|rdf)\s*=\s*(\S+)>)/i) {
	    # specially defined type.  get and process an RSS (RDF Site Summary)
	    my ($replace, $rdf_loc) = ($1,$2);
	    $shortReply = 1;
	    $rdf_loc =~ s/^\"+//;
	    $rdf_loc =~ s/\"+$//;

	    if ($rdf_loc !~ /^(ht|f)tp:/) {
		&msg($who, "$orig_Y: bad RSS [$rdf_loc] (nao é uma url HTTP ou FTP)");
	    } else {
		my $result = &get_headlines($rdf_loc);
		if ($result =~ s/^error: //) {
		    $theMsg = "não foi possível obter as noticias: $result";
		} else {
		    $theMsg =~ s/\Q$replace\E/$result/;
		    $theMsg = "$who: $theMsg";
		}
	    }
	} elsif ($msgType !~ /private/ and 
		 $theMsg =~ s/^\s*<action>\s*(.*)/\cAACTION $1\cA/i) {
	    # specially defined type.  only remove '<action>' and make it an action
	    $shortReply = 1;
	} else {		# not a short reply
	    if (!$infobots{$nuh} and $theVerb =~ /eh/) {
		my($x) = int(rand(18));
		# oh this could be done much better
		if ($x <= 5) {
		    $theMsg= "$orig_Y eh $theMsg";
		}
		if ($x == 6) { 
		    $theMsg= "eu acho que $orig_Y eh $theMsg";
		}
		if ($x == 7) { 
		    $theMsg= "hmmm... $orig_Y eh $theMsg";
		}
		if ($x == 8) { 
		    $theMsg= "eu já disse que $orig_Y eh $theMsg";
		}
		if ($x == 9) { 	
		    $theMsg= "$orig_Y provavelmente eh $theMsg";
		}
		if ($x == 10) { 
		    $theMsg =~ s/[.!?]+$//;
		    $theMsg= "existem rumores que $orig_Y eh $theMsg";
		    # $theMsg .= " dumbass";
		}
		if ($x == 11) { 
		    $theMsg= "eu ouvi dizer que $orig_Y eh $theMsg";
		}
		if ($x == 12) { 
		    $theMsg= "alguem me disse que $orig_Y eh $theMsg";
		}
		if ($x == 13) { 
		    $theMsg= "acredito que $orig_Y eh $theMsg";
		}
		if ($x == 14) { 
		    $theMsg= "bem, $orig_Y eh $theMsg";
		}
		if ($x == 15) { 
		    $theMsg =~ s/[.!?]+$//;
		    $theMsg= "$orig_Y eh, acredito, $theMsg";
		}
		if ($x == 16) { 
		    $theMsg= "segundo fontes seguras, $orig_Y eh $theMsg";
		}
		if ($x == 17) { 
		    $theMsg= "eu ouvi alguem dizendo que $orig_Y eh $theMsg";
		}
	    } else {
		$theMsg = "$orig_Y $theVerb $theMsg" if ($theMsg !~ /^\s*$/);
	    }
	}
    }

    my $safeWho = &purifyNick($who);

    if (!$shortReply) {
	# shouldn't this be in switchPerson?
	# this is fixing the person for going back out

# /^onz!lenzo@lenzo.pc.cs.cmu.edu privmsg rurl :*** noctcp: omega42 is/: nested *?+ in regexp at /usr/users/infobot/infobot-current/src/Reply.pl line 266, <FH> chunk 176.
	
	if ($theMsg =~ s/^$safeWho eh/ele(a) eh/i) { # fix the person 
	} else {
	    $theMsg =~ s/^$param{'nick'} eh /eu sou /ig;
	    $theMsg =~ s/ $param{'nick'} eh / eu sou /ig;
	    $theMsg =~ s/^$param{'nick'} era /eu era /ig;
	    $theMsg =~ s/ $param{'nick'} era / eu era /ig;
	    

	    if ($addressed) {
		$theMsg =~ s/^voces sao (\.*)/eu sou $1/ig;
		$theMsg =~ s/ voces sao (\.*)/ eu sou $1/ig;
		
	    } else {
		if ($theMsg =~ /^voces sao / or $theMsg =~ / voces sao /) {		
		    $theMsg = 'NOREPLY';
		}
	    }
	}

	$theMsg =~ s/ $param{'ident'}\'?s / meu /ig;
	$theMsg =~ s/^$safeWho\'?s /$safeWho, seu /i;
	$theMsg =~ s/ $safeWho\'?s / seu /ig;
	
    }
    

    if (1) {			# $date, $time 
	$curDate = scalar(localtime());
	chomp $curDate;
	$curDate =~ s/\:\d+(\s+\w+)\s+\d+$/$1/;
	$theMsg =~ s/\$date/$curDate/gi;
	$curDate =~ s/\w+\s+\w+\s+\d+\s+//;
	$theMsg =~ s/\$time/$curDate/gi;
    }

    $theMsg =~ s/\$who/$who/gi;

    if (1) {			# variables. like $me or \me
	$theMsg =~ s/(\\){1,}([^\s\\]+)/$1/g;
    }

    $theMsg =~ s/^\s*//;
    $theMsg =~ s/\s+$//;

    if (getparam('filter')) {
	require "src/filter.pl";
	$theMsg = &filter($theMsg);
    }

    if ($theMsg =~ /\S/) {
	return $theMsg;
    } else {
	return undef;
    }
}

1;

