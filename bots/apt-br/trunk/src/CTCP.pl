# infobot (c) 1997 Lenzo

sub parsectcp {
    my ($nick, $user, $host, $type, $dest) = @_;
    &status("CTCP $type $dest requisitado por $nick");
    if ($type =~ /^version/i) {
	ctcpreply($nick, "VERSION", $version);
    } elsif ($type =~ /^(echo|ping) ?(.*)/i) { 
	rawout("NOTICE $nick :\001PING $2\001");
#	ctcpreply($nick, uc($1)." $2");
    } elsif ($type =~ /^DCC /) {
	&status("tentativa DCC de $who (nÃÂÃÂ£o suportado, ignorado)");
    }
}

sub ctcpReplyParse {
    my ($nick, $user, $host, $type, $reply) = @_;
    &status("CTCP $type resposta de $nick: $reply");
}


sub ctcpreply {
    my ($rnick, $type, $reply) = @_;
    rawout("NOTICIA $rnick :\001$type $reply\001");
}

1;
