# infobot :: Kevin Lenzo   (c) 1997

sub normquery {
	my ($in) = @_;

	$in = " $in ";

	# where blah is -> where is blah
#	$in =~ s/ (onde|quem)\s+(\S+)\s+(eh|esta|sao|estao) / $1 $3 $2 /i;	

	# where blah is -> where is blah
#	$in =~ s/ (onde|quem)\s+(.*)\s+(eh|esta|sao|estao) / $1 $3 $2 /i;	

	$in =~ s/^\s*(.*?)\s*/$1/;

	$in =~ s/be tellin\'?g?/tell/i;
#	$in =~ s/ \'?bout/ about/i;

#	$in =~ s/,? any(hoo?w?|ways?)/ /ig;
#	$in =~ s/,?\s*(pretty )*please\??\s*$/\?/i;


	# what country is ...
	if ($in =~ 
	    s/qu(e|al)\s+(endere(ç|c)o?|pa(i|í)s|lugar|rede (suffix|domain))/wh$1 /ig) {	    
	    if ((length($in) == 2) && ($in !~ /^\./)) {
		$in = '.'.$in;
	    }
	    $in .= '?';
	}


# Hacks para adaptação ao Idioma Português e linguagem usada na Internet
# No IRC muitos usam acentuação e outros não, além de outras palavras 
# anternativas (linguagem do IRC mesmo) 
	$in =~ s/aih/ai/ig;
	$in =~ s/aki/aqui/ig;
	$in =~ s/alguém/alguem/ig;
	$in =~ s/almocar/almoçar/ig;
	$in =~ s/área/area/ig;
	$in =~ s/até/ate/ig;
	$in =~ s/baum/bom/ig;
	$in =~ s/blz/beleza/ig;
	$in =~ s/café/cafe/ig;
	$in =~ s/kara/cara/ig;
	$in =~ s/código/codigo/ig;
	$in =~ s/dah/da/ig;
	$in =~ s/difícil/dificil/ig;
	$in =~ s/está/esta/ig;
	$in =~ s/estúpido/estupido/ig;
	$in =~ s/falow|fallow/falo/ig;
	$in =~ s/jóia/joia/ig;
	$in =~ s/naum|nop|não/nao/ig;
	$in =~ s/robô/robo/ig;
	$in =~ s/será/sera/ig;
	$in =~ s/sugestão/sugestao/ig;
	$in =~ s/tah/ta/ig;
	$in =~ s/tb|também/tambem/ig;
	$in =~ s/vc|você/voce/ig;
	$in =~ s/vcs|vocês/voces/ig;

# acorda está contida dentro de acordar e isto pode se tornar um problema se não
# for verificado o espaço (acorda ==> acordarr)
	$in =~ s/acorda(\s|^.)/acordar /ig;
	$in =~ s/almo(ç|c)a(\s|^.)/almoçar /ig;
	$in =~ s/come(\s|^.)/comer /ig;
	$in =~ s/(durmi|durmir|dormi)(\s|^.)/dormir /ig;
	$in =~ s/faze(\s|^.)/fazer /ig;
	$in =~ s/janta(\s|^.)/jantar /ig;
	$in =~ s/(só|soh)(\s|^.)/so /ig;
	$in =~ s/tenta(\s|^.)/tentar /ig;
	$in =~ s/toma(\s|^.)/tomar /ig;
	$in =~ s/(vamo|vamu)(\s|^.)/vamos /ig;

# Estes são casos especial que deve ter os espaços comparados dos dois lados!
# c é normalmente usado para especificar a palavra "voce" no IRC, imagina se 
# tudo que tivesse c sem comparar os espaços em ambos os lados fosse 
# substituido...
	$in =~ s/(\s|^)c(\s|^.)/ voce /ig;
	$in =~ s/(\s|^)d(\s|^.)/ de /ig;
	$in =~ s/(\s|^)q(\s|^.)/ que /ig;
	$in =~ s/(\s|^)já(\s|^.)/ ja /ig;
	$in =~ s/(\s|^)ta(\s|^.)/ esta /ig;
	$in =~ s/(\s|^)(to|esto)/ estou /ig;
	$in =~ s/(\s|^)vo(\s|^.)/ vou /ig;


# filtros de perguntas para garantir que o bot respoda adequadamente. 
# Algumas palavras com acento como "você" são filtradas nas traduções
# anteriores e chegam como "voce" na variável $in atual 
	$in =~ s/como\s?(eu)? configur(o|ar)/como faço pra configurar/gi;
	$in =~ s/como\s?(eu)? instal(o|ar)/como faço pra instalar/gi;
	$in =~ s/como\s?(eu)? (fa(ç|c)o (pra|para))?//gi; 
	$in =~ s/esta coisa? (chamada )?//gi;
	$in =~ s/(tem|possui)\s?(alguma)?\s(pista|id(e|é)ia|dica|sugestao)?\s(d(a|e|o))?\s(como)?//ig;
	$in =~ s/(voce|quem|algu(e|é)m)?\s?(aqui)?\s?(conhece(m)?|sabe usar) //ig;
	$in =~ s/pode (por favor)? (me|nos) (dizer|falar)//ig;
	$in =~ s/pode (procurar|obter|encontrar)( isto|aquilo)?//ig;
	$in =~ s/(o|a|os|as)?(endere(ç|c)o|url) (for|para) //ig;
	$in =~ s/((pra|para) (q|que) serve (o|a|os|as)?)+/o que eh/ig;
	$in =~ s/(algu(m|em) de)?(voc(e|es) conhece(m)? (o|a|os|as)?)+/que eh/ig;
	$in =~ s/qual (é|eh) a fun(ç|c)(a|ã)o d(a|e|o)//ig;
	$in =~ s/que é/que eh/ig;
	$in =~ s/que faz?//ig;


# Retiram espaços em branco extra e fazem checagens envolvendo espaços pra  
# garantir que a frase seja reconhecida corretamente 
 	$in =~ s/\s+/ /g;
 	$in =~ s/^\s+//;
 	if ($in =~ s/\s*[\/?!]*\?+\s*$//) {
 	    $finalQMark = 1;
 	}


# Ocorrem muitos problemas na manipulação do artigo, desativado
#	$in =~ s/\b(o|a|os|as|um?)\s+/ /i; # handle first article in query
	$in =~ s/\s+/ /g;

	$in =~ s/^\s*(.*?)\s*$/$1/;

	$in;
}

# for be-verbs
sub switchPerson {
	my($in) = @_;

	my $safeWho;
	if ($target) {
	    $safeWho = &purifyNick($target);
	} else {
	    $safeWho = &purifyNick($who);
	}

	# $safeWho will cause trouble in nicks with deleted \W's
	$in =~ s/(^|\W)${safeWho}s\s+/$1${who}\'s /ig; # fix genitives
	$in =~ s/(^|\W)${safeWho}s$/$1${who}\'s/ig; # fix genitives
	$in =~ s/(^|\W)${safeWho}\'(\s|$)/$1${who}\'s$2/ig; # fix genitives
	$in =~ s/(^|\s)eu sou(\W|$)/$1$who \eh$2/ig;
	$in =~ s/(^|\s)eu tenho(\W|$)/$1$who tem$2/ig;
	$in =~ s/(^|\s)eu tinha\'?t(\W|$)/$1$who tinha not$2/ig;
	$in =~ s/(^|\s)eu(\W|$)/$1$who$2/ig;
	$in =~ s/ sou\b/ \eh/i;
	$in =~ s/\bam /eh/i;
	$in =~ s/yourself/$param{'ident'}/i if ($addressed);
	$in =~ s/(^|\s)(eu|eu mesmo)(\W|$)/$1$who$3/ig;
#	$in =~ s/(^|\s)meu(\W|$)/$1${who}\eh$2/ig; # turn 'my' into name's

# Muita confusão na manipulação do nick e inserçao dos factoids, desativado
#	if ($addressed > 0) {
#		$in =~ s/(^|\W)voc(e|ê) est(a|á)(\W|$)/$1is $param{'nick'}$2/ig;
#		$in =~ s/(^|\W)voc(e|ê) est(a|á)(\W|$)/$1$param{'nick'} is$2/ig;
#		$in =~ s/(^|\W)voc(e|ê)(\W|$)/$1$param{'nick'}$2/ig; 
#		$in =~ s/(^|\W)voc(e|ê)(\W|$)/$1$param{'nick'}\'s$2/ig;
#		
#	}

	$in;
}   

# ---

1;
