
# infobot :: Kevin Lenzo (c) 1997

if (!$filesep) {
    $filesep = '/';
};

sub loadParamFiles {
    my (@files) = @_;
    my @result;
    my $p;

    if (!@files) {
	# &status("nenhum arquivo de parâmetro para ser carregado.");
	return '';
    }

    foreach $p (@files) {
	if ($p !~ /\S/) {
	    &status("alerta: o nome do arquivo de parâmetros é nulo.");
	    return '';
	}

	if (open(PARAM, $p)) {
	    my $count;
	    while (<PARAM>) {
		chomp;
		next if /^\s*\#/;
		next unless /\S/;
		my ($key, $val) = split(/\s+/, $_, 2);
		$val =~ s/\s+$//;

		# perform variable interpolation

		$val =~ s/(\$(\w+))/$param{$2}/g;
		&status("ajustando $key => $val") 
		    if (exists $param{VERBOSITY} and $param{VERBOSITY} > 2);

		$param{$key} = $val;

		++$count;
	    }
	    &status("arquivo de parâmetros $p carregado ($count itens)");
	    close(PARAM);
	} else {
	    &status("falha ao carregar o arquivo de parâmetros $p");
	}
    }
}

sub writeParamFile {
    my ($filename) = $_[0];
    # write the current parameter set to $filename.
    # returns 1 if successful

    if (open POUT, ">$filename") {
	foreach (sort keys %param) {
	    print POUT "$_ $param{$_}\n";
	}
	close POUT;
	return 1;
    } else {
	# couldn't write the file
	return 0;
    }
}

1;
