
# infobot :: Kevin Lenzo   (c) 1997

sub update {
    my($lhs, $verb, $rhs) = @_;
    my($reply) = $lhs;

    $lhs =~ s/^\s*=?//;		# handle dcc =oznoid and stuff
    $lhs =~ s/^eu (ouvi|pensei) //i;
    $lhs =~ s/^alguem (disse|falou) //i;
    $lhs =~ s/ +/ /g;

    # this really needs cleaning up
    if ($verb eq "eh") {
	$also = ($rhs =~ s/^tambem //i);

	my $also_or = ($also and $rhs =~ s/\s*\|\s*//);

	if ($exists = &get("is", $lhs)) { 
	    chomp $exists;

	    if ($exists eq $rhs and not $main::googling) {
		if ($msgType =~ /public/) {
		    &performSay("eu ja sei disso $who.");
		} else {
		    &msg($who, "ele ja foi $rhs");
		}
		return 'NOREPLY';
	    }

	    $skipReply = 0;	 
	    if ($also) {
		if ($also_or) {
		    $rhs = $exists . '|'.$rhs;
		} else {
		    if ($exists ne $rhs) {
			$rhs = $exists .' ou '.$rhs;
		    }
		}
		    if (length($rhs) > getparam('maxDataSize')) {
			if ($msgType =~ /public/) {
			    if ($addressed) {
				if (rand() > 0.5) {
				    &performSay("é muito longa ".$who);
				} else {
				    &performSay("desculpe, mas é muito longa $who");
				} 
			    }
			} else {
			    &msg($who, "O texto é muito longo");
			}
			return 'NOREPLY';
		}
		if ($msgType =~ /public/) {
		    &performSay("Ok, $who.");
		} else {
		    &msg($who, "okay.");
		}

		$updateCount++;
		&status("update: <$who> \'$lhs =eh=> $rhs\'; era \'$exists\'");
		&set("is", $lhs, $rhs);
	    } else {		# not "also"
		if (($correction_plausible == 0) && ($exists ne $rhs)) {
		    if ($addressed) {
			if (not $main::googling) {
			    if ($msgType =~ /public/) {
				&performSay("...mas $lhs eh $exists...");
			    } else { 
				&msg($who, "...mas $lhs eh $exists..");
			    }
			    &status("FALHA na atualização: <$who> \'$lhs =$verb=> $rhs\'");
			}
		    } else {
			&status("Falha na atualização: <$who> \'$lhs =$verb=> $rhs\' (sem resposta porque não foi endereçada.)");
			# we were not addressed, so just
			# ignore it.  
			return 'NOREPLY';
		    }
		} else {
		    if (IsFlag("m") ne "m") {
			performReply("Você não tem acesso para modificar os factoids");
			return 'NOREPLY';
		    }
		    if ($msgType =~ /public/) {
			&performSay("Ok $who.");
		    } else {
			&msg($who, "okay.");
		    }
		    $updateCount++;
		    &status("update: <$who> '$lhs =eh=> $rhs\'; era \'$exists\'");
		    &set("is", $lhs, $rhs);
		}
	    }
	    $reply = 'NOREPLY';

	} else {
	    &status("enter: <$who> $lhs =$verb=> $rhs");
	    $updateCount++; $factoidCount++;
	    if ($factoidCount == 31337) { # particular count
		$mySaveChannel = &channel();
		&say("Aquilo deve ser um factoid $factoidCount dado em $mySaveChannel por $who.");
		&status("FACTOID NUMBER $factoidCount no canal $mySaveChannel por $who.");
		
		&say("woohoo!");
		&channel($mySaveChannel);
	    }
	    &set("is", $lhs, $rhs);
	    $is{"theCount"}++; 
	}

    } else {			# 'is' failed
	if ($verb eq "sao") {
	    $also = ($rhs =~ s/^tambem //i);
	    if ($exists = &get("are", $lhs)) {
		if ($also) {	
		    if ($exists ne $rhs) {
			$rhs = $exists .' ou '.$rhs;
		    }
		    if ($msgType =~ /public/) {
			&performSay("okay, $who.") unless $rhs eq $exists;
		    } else {
			&msg($who, "okay.");
		    }
		    $updateCount++;
		    &status("update: <$who> \'$lhs =sao=> $rhs\'; era \'$exists\'");
		    &set("are", $lhs, $rhs);
		} else {	# not 'also'
		    if (($correction_plausible == 0) && ($exists ne $rhs)) {
			if ($addressed) {
			    &status("FAILED update: \'$lhs =$verb=> $rhs\'");
			    if ($msgType =~ /public/) {
				&performSay("...mas $lhs eh $exists...");
			    } else { 
				&msg($who, "...mas $lhs eh $exists..");
			    }
			} else {
			    &status("FAILED update: $lhs $verb $rhs (sem endereço, sem resposta)");
			    # we were not addressed, so just
			    # ignore it.  
			    return 'NOREPLY';
			}
			if ($msgType =~ /public/) {
			    &performSay("...mas $lhs sao $exists...");
			} else {
			    &msg($who, "...mas $lhs sao $exists...");
			}
		    } else {
			if ($msgType =~ /public/) {
			    &performSay("Ok, $who.") unless $rhs eq $exists;
			} else {
			    &msg($who, "okay.") 
				unless grep $_ eq $who, split /\s+/, $param{friendlyBots};
			}
			$updateCount++;
			&status("update: <$who> \'$lhs =sao=> $rhs\'; era \'$exists\'");
			&set("are", $lhs, $rhs);
		    }
		    $reply = 'NOREPLY';
		} 
	    } else {
		&status("enter: <$who> $lhs =sao=> $rhs");
		$updateCount++;
		&set("are", $lhs, $rhs);
		$are{"theCount"}++;
	    }
	}
    }

    $lhs .= " $verb $rhs";
    if ($reply ne 'NOREPLY') {	
	$reply = $lhs;
    }

    return $reply;
}

# ---

1;
