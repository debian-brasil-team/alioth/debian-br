# infobot copyright kevin lenzo 1997-1999

sub search {
    return '' unless $addressed;

    my $pattern = shift ;
    $pattern =~ s/^\s*(search|scan|procurar|verificar)(\s+(for|por))?\s+//i;
    my $type = lc($1);

    my $bail_thresh = 5;

    if ($pattern =~ s/^\d+ //) {
	$bail_thresh = $&; 
    } else {
	if ($type eq 'scan') {
	    $bail_thresh = 1;
	}
    }

    $pattern =~ s/\?+\s*$//; # final ? marks

    return '' if ($pattern =~ /^\s*$/);

    my $minlength = 3;

    return "aquela expressão eh muito curta. Tente algo de no mínimo $minlength caracteres."
	if (length($pattern) < $minlength);

    &msg($who,"Procurando por $pattern:");

    my (@response);

    my (@myIsKeys)  = getDBMKeys("is");

    my $t = time();
    my $count = 0;

    foreach (@myIsKeys) {
	if (/$pattern/) {
	    $r = &get("is", $_);
	} else {
	    next ;
	}
	my $t1 = time();
	if ($t1-$t < 2) {
	    sleep 2;
	}
	&msg($who, "$_ eh $r");
	$t = $t1;
	last if ++$count >= $bail_thresh;
    }

    my $last;
    if ($count < $bail_thresh) {
	my (@myAreKeys) = getDBMKeys("are");
	foreach (@myAreKeys) {
	    if (/$pattern/) {
		$r = &get("are", $_);
	    } else {
		next ;
	    }
	    my $t1 = time();
	    if ($t1-$t < 2) {
		sleep 2;
	    }
	    $t = $t1;
	    &msg($who, "$_ sao $r");
	    last if ++$count >= $bail_thresh;
	}
	$last = 1;
    }

    if (!$count) {
	&msg($who, "nada encontrado para $pattern");
    } else {
	my $reply = "  ...mostrando $count itens";
	if ($last) {
	    $reply .= " (todos sendo mostrados).";
	} else {
	    $reply .= " (podem existir mais).";
	}
	
	&msg($who, $reply);
    }
    
    return '';
}

1;
