#
# zippy -- infobot module for Zippy the Pinhead quotes
#          hacked up by Rich Lafferty (mendel) <rich@vax2.concordia.ca> 
#
# Data gratuitously swiped from fortune-mod-9708, the 'fortune' program.
#

package zippy;

my $no_zippy; # Can't think of any situation in which this won't work..

sub zippy::get { 
    my $line = shift;
    unless ($line =~ /^(piada|yow|hahaha)[!? ]*$/i or $line =~ /^seja\s(engraçado|divertido)\?*$/i) {
	return '';
   }

    unless (@yows) { # read data unless it's been read already.
	print "Lendo...\n";
	while (<DATA>) {
	    chomp;
	    push @yows, $_; 
	}
    }
   
    if ($no_zippy) { # ..but just in case :-)
	return "YOW! eu sou um infobot sem ZIPPY!" if $main::addressed;
    }
    
    my $yow = $yows[rand(@yows)];

    return $yow;
}

1;

=pod

=head1 NAME

Zippy.pl - Yow!  Am I having fun yet?

=head1 PREREQUISITES

None.

=head1 PARAMETERS

zippy

=head1 PUBLIC INTERFACE

	[yow|be zippy]

=head1 DESCRIPTION

It's OBVIOUS ... The FURS never reached ISTANBUL ... You were an EXTRA
in the REMAKE of "TOPKAPI" ... Go home to your WIFE ... She's making
FRENCH TOAST!

=head1 AUTHORS

Rich Lafferty (mendel) <rich@vax2.concordia.ca>

=cut

__DATA__
Bill Gates: 640K será o bastante para todos
667: Vizinho do demônio.
A informática chegou para resolver problemas que antes não existiam.
A Inteligência Artificial não funciona por causa da Burrice Natural.
A mais nova tecnologia de transferência de dados entre computadores é o DPL /dpC (Disquete Pra La, Disquete Pra Ca)
Apple (c) - Copyright 1767, Sir Isaac Newton.
A vida começa aos 14.4K, mas a maioridade é com 28.8K.
A vida é um eterno Upgrade.
Backup found: (A)borta (D)eixa Nascer (C)asamento.
Backup not found: (A)bort (R)etry (P)anic
BAHIA SOFTWARES orgulhosamente apresenta: HoloDOOM V.166.
BASIC, é como catapora, a gente pega quando é crianca.
BBS e Internet: Programas para duplicar sua conta telefônica!
BIOS (Bicho Ignorante Operando o Sistema).
Cadê o vaga-lume que entrou no meu modem?
Como reconhecer um excelente programador ?
Compactação de arquivos: ARJa paciência.
Computador de português não tem memória, mas sim, uma leve lembrança.
Computador é que nem carroça, sempre tem um burro na frente.
De onde viemos? Para onde vamos? Lá tem Internet?
Devagar e sempre a 2400 bps.
Dica de micreiro: se seu carro pifar, tente sair e entrar de novo.
Dorme filhinho, senão vem um vírus e te apaga a memória!
Drogas? Pra que? Já tenho um XT.
Elas usam Sempre Livre. Eu uso Modem.
Em matéria de rede, prefiro aquela que vem do Ceara.
ENVIRONMENT ERROR: Chame o Greenpeace, rápido !
Errar é humano. Botar a culpa no computador é mais humano ainda.
Eu cortei meu dedo no meu ZIPer ARJ!
Eu não perdi a memória. Esta em backup, em algum disquete.
Eu também sou multitarefa. Leio no banheiro.
Fiat, Peido e Windows: só os donos gostam.
File not found. Serve alcatra, freguês ?
Função secreta do DOS: adicione BUGS=OFF no seu CONFIG.SYS
Lei de Murphy no micro: Sempre há mais um bug.
Maaanheee. Zezinho quer brincar outra vez de Append.
Meu cachorro se chama SysOp. Ele nunca atende quando chamo.
Modem português funciona a CANECAS em vez de BAUDS.
Modems de 2400, favor transitar na pista da direita.
MS-DOS nunca diz: EXCELENT command or filename. 
MS Viagra: Depois de instalada a ereção, você descobre que não consegue desinstalá-la... 
MS Viagra: Junto com o MSViagra, você receberá um pacote de inúmeros remédios para doenças que você não possui... 
MS Viagra: Na bula vem um cupom para um futuro upgrade para o MSViagra 2... 
MS Viagra: Não virá escrito em lugar nenhum, mas se você reparar bem, você na verdade não terá uma ereção real. Apenas foi convencido de que tem, o que, no fundo no fundo, é a mesma coisa..." 
MS Viagra: O usuário teria a ereção, mas descobriria aterrorizado que ela não poderia proporcionar-lhe um orgasmo...só a partir da versão MSViagra 2...mas o fabricante promete um patch no formato de uma pequena pílula amarela para tomar durante o ato... 
MS Viagra: O usuário teria uma ereção, mas ele misteriosamente terminaria no meio do ato por motivo inexplicado... 
MS Viagra: Provavelmente, o MSViagra ainda estaria em testes...não teria sido lançado! 
MS Viagra: Quando você toma o MSViagra, vários efeitos colaterais surgem: dores de cabeça a, ausência de sentidos, como olfato e paladar e outras coisas. Mas tudo isso é apenas um detalhe, perto do imenso prazer que o MSViagra vai lhe proporcionar... 
MS Viagra: Você compra o MSViagra, mas junto com pacote vem um vidrinho de vitamina C com um gosto horrivel. E o MSViagra só funciona se você tomar essa maldita vitamina... 
MS Viagra: Você descobrirá que só sente atração por determinadas mulheres. Com algumas (normalmente as melhores) você não funcionará... 
MS Viagra: Você toma a pílula, ela não funciona e você liga para o laboratório para reclamar. O laboratório então lhe convence que você não está tomando a pilula direito, ou com muita água, ou com pouca água, ou sem água nenhuma, mas sempre a opção que voce NÃO deu... 
Não confunda modess com modem, o slot é diferente.
Não transmita a 2400 bps. Vá ditando que é mais rápido!
No PARAÍSO dos computadores: o gerenciamento é feito pela Intel, o projeto e a construção pela Apple,o marketing pela Microsoft, o suporte pela IBM, e o preço é dado pela Gateway. No INFERNO dos computadores: o gerenciamento é feito pela Apple, o projeto e a construção pela Microsoft, o marketing pela IBM, o suporte pela Gateway, o preço é dado pela Intel.
+*** Novas piadas rápidas podem ser enviadas para o mantenedor do bot: gleydson@cipsga.org.br ou fernando.ike@gmail.com ***
O Universo por nós conhecido é apenas uma versão beta.
Paiee, o que quer dizer "FORMATTING DRIVE C:" em português ?
Papel higiênico é igual a tempo de acesso: acaba quando você mais precisa.
Para surfar na Internet é preciso uma prancha multimídia?
Perdi minha agenda. Não sei o que fazer.
Pirataria, eu? Não, só faço transferência de tecnologia...
Por falta de criatividade estamos sem tags no momento.
Prefiro ter amigos piratas a ter inimigos registrados.
Programa de computador: Estranha forma de vida que tem a capacidade de transformar comandos em mensagens de erro.
Programador órfão procura placa-mãe.
Sabe por que a Intel não chamou o Pentium de 586? É porque quando eles somaram 486 com 100 deu 585.732691!
Sanduíche preferido do micreiro: Xcopy.
Se emperrar, force; se quebrar, precisava trocar mesmo...
Se não fosse pelo C, estaríamos usando BASI, PASAL e OBOL.
Se o computador atrapalha os estudos, saia da escola!
Se o mundo for uma versão Beta, devemos ser os Bugs.
Teclado: hardware usado para inserir erros no micro.
ida de Programador: Aos 16 anos começa perceber que seus amigos não são tão legais assim... 
Vida de Programador: Com 12 anos de idade fez um programa em cobol para controlar o estoque de bolas de gude.
Vida de Programador: Começa pedir email até para o açougueiro, como se todos tivessem obrigação de ter.
Vida de Programador: Na faculdade seus professores técnicos tem medo de perguntar coisas para você.
Vida de Programador: Qualquer loja que entra e vc não ve um computador, logo critica, e oferece um sistema para o dono.
Vida de Programador: Quando inventaram a internet, você se sentiu programador do mundo.
Vida de Programador: Quando liga para um serviço por telefone e a atendente pede para ligar mais tarde pois o sistema esta com problemas, pede detalhes técnicos, quer falar com o responsável, e diz que se o sistema fosse seu isso não aconteceria. 
Vida de Programador: Quando mais velho, fez uma nova versão daquele programa de receitas para sua mãe, usando multimidia.
Vida de Programador: Quando vai comprar uma calça simples, pergunta ao vendedor se ele tem uma calça default.
Vida de Programador: Se arrepia ao entrar no mappim da praça ramos e ver aqueles terminais burros com tela verde, alias nunca mais voltou lá.
Vida de Programador: Se recusa a conversar com amigos que não tenham pelo menos um pentium 166 mmx.
Vida de Programador: Usa lógica até para dirigir, se for pela 23 de maio e estiver cheia, então corto pela ibirapuera, se não, sigo em frente.
Vida de Programador: Você recebe cartas de tudo que é empresa de software, dizendo que se mandar mais cartas para as revistas reportando bugs encontrados, correra sério risco de vida...
Vírus desamparado procura BBS pra morar.
Você sabe qual o e-mail do Maluf? maluf@mas.faz.
Lei de Murphy: Se alguma coisa pode dar errado, dará.
Lei de Murphy: Nada é tão fácil quanto parece, nem tão complicado quanto a explicação do manual.
Lei de Murphy: Se há possibilidade de várias coisas darem errado, todas darão, ou a que causar mais prejuízo.
Lei de Murphy: Se você perceber que há quatro maneiras de uma coisa dar errado, e driblar as quatros, uma quinta maneira surgirá do nada
Lei de Murphy: Deixadas à sua sorte, a tendência das coisas é de ir de mal a pior.
Lei de Murphy: Toda vez que você decide fazer uma coisa, tem sempre outra coisa pra ser feita antes.
Lei de Murphy: Toda solição cria novo problema.
Lei de Murphy: É impossível criar qualquer coisa à prova de erros - os idiotas são muito iventivos.
Lei de Murphy: A natureza é fogo.
Lei de Murphy: Sorria logo... amanhã será pior.
Lei de Murphy: O objeto é danificado na proporção direta do seu valor.
Lei de Murphy: Se uma experiência funciona, não vai funcionar muito tempo.
Lei de Murphy: Seja wual for o resultado, haverá sempre alguém prá (a) interpretá-lo mal (b) falsificá-lo ou (c) dizer que já tinha previsto tudo em seu último relatório.
Lei de Murphy: Em qualquer compilação de informação de informações, a informação aceita por unamidade, sem qualquer verificação, é a totalmente idiota.
Lei de Murphy: Nenhum supertécnico consultado a peso de outro detectará a idiotice.
Lei de Murphy: Qualquer desses idiotas que vivem em volta de sua mesa dando conselhos idiotas verá o erro imediamente.
Lei de Murphy: Quando um trabalho é malfeito, qualquer tentativa de melhorá-lo piora
Lei de Murphy: É pior a melhor emenda do que o pior soneto.
Lei de Murphy: A melhor maneira de estudar  um assunto é estudá-lo profundamente antes de começar.
Lei de Murphy: Mantenha sempre um registro de seus estudos - isso demonstra quanto você tem trabalhado.
Lei de Murphy: O mais importante na projeção de um quadrado é o traçado de suas curvas.
Lei de Murphy: Em caso de dúvida, seja convicente.
Lei de Murphy: Uma experiência deve resistir à prova da repetição - deve falhar sempre com a mesma precisão.
Lei de Murphy: Não acredite em milagres - baseie-se neles.
Lei de Murphy: Em crises que forçam as pessoas a escolher entre caminhos alternativos a maioria escolherá o pior.
Lei de Murphy: Para acertar em quem é especialista, aposte no que diz que o trabalho vai levar mais tempo e custar mais caro.
Lei de Murphy: Novos sistemas geram novos problemas.
Lei de Murphy: O candidato ideal só aparece um dia depois de preenchido o cargo.
Lei de Murphy: Roubar idéias de uma pessoa é plágio; de muitas; é pesquuisa.
Lei de Murphy: Se você não entende determinada palavra num artigo técnico, retire a palavra. Verá que o artigo fica imediatamente compreensível.
Lei de Murphy: Um ônibus que demora a chegar acaba chegando quando você já andou até um ponto tão próximo do seu destino que não quer mais ônibus porra nenhuma.
Lei de Murphy: Quando você está adiantado pro encontro,, quer fazer hora, pega verde todos os sinais.
