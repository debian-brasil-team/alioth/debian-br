#####################
#                   #
#  brlinux.pl for  #
# brlinux headline #
#     retrival      #
#  tessone@imsa.edu #
#   Chris Tessone   #
#   Licensing:      #
# Artistic License  #
# (as perl itself)  #
#####################
#fixed up to use XML'd /. backdoor 7/31 by richardh@rahga.com
#My only request if this gets included in infobot is that the 
#other header gets trimmed to 2 lines, dump the fluff ;) -rah

#added a status message so people know to install LWP - oznoid
#also simplified the return code because it wasn't working.

use strict;

my $no_brlinuxlines;


BEGIN {
    $no_brlinuxlines = 0;
    eval "use LWP::UserAgent";
    $no_brlinuxlines++ if $@;
}

sub getbrlinuxheads {
    # configure
    if ($no_brlinuxlines) {
	&status("brlinux headlines requer o pacote LWP instalado");
	return '';
    }
    my $ua = new LWP::UserAgent;
    if (my $proxy = main::getparam('httpproxy')) { $ua->proxy('http', $proxy) };
    $ua->timeout(12);
    my $maxheadlines=4;
    my $brlinuxurl='http://br-linux.org/linux/?q=node/feed';
    my $story=0;
    my $brlinuxindex = new HTTP::Request('GET',$brlinuxurl);
    my $response = $ua->request($brlinuxindex);

    if($response->is_success) {
	$response->content =~ /<time>(.*?)<\/time>/;
	my $lastupdate=$1;
	my $headlines = "br-linux - Updated ".$lastupdate;
	my @indexhtml = split(/\n/,$response->content);
	
	# gonna read in this xml stuff.
       	foreach(@indexhtml) {
	    if (/<story>/){$story++;}
	    elsif (/<title>(.*?)<\/title>/){
		$headlines .= " | $1";
	    }
	    elsif (/<url>(.*?)<\/url>/){
		# do nothing
	    }
	    elsif (/<time>(.*?)<\/time>/){
		# do nothing
	    }     
	    last if $story >= $maxheadlines;
	    next;
	}
	
	return $headlines;
    } else {
	return "nao foi possível encontrar as headlines.";
    }
}
1;

__END__

=head1 NAME

br-linux.pl - brlinux headlines grabber 

=head1 PREREQUISITES

	LWP::UserAgent

=head1 PARAMETERS

br-linux

=head1 PUBLIC INTERFACE

	br-linux [headlines]

=head1 DESCRIPTION

Retrieves the headlines from brlinuxdot; probably obsoleted by RDF.

=head1 AUTHORS

Chris Tessone <tessone@imsa.edu>
