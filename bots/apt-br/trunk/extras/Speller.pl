
# infobot :: Kevin Lenzo  (c) 1997

# doce++ for the first version of this!

sub ispell {
    my $in = $_[0];

    $in =~ s/^\s+//;
    $in =~ s/\s+$//;

    return "$in parece divertido" unless $in =~ /^\w+$/;

    #derr@rostrum# ispell -a
    #@(#) International Ispell Version 3.1.20 10/10/95
    #peice
    #& peice 4 0: peace, pence, piece, price

    my @tr = `echo $in | ispell -a -S`;

    if (grep /^\*/, @tr) {
	my $result = "'$in' deve ser digitado corretamente";
	if ($msgType =~ /private/) {
	    &msg($who, $result);
	} else {
	    &say("$who: $result");
	}
    } else {
	@tr = grep /^\s*&/, @tr;
	chomp $tr[0];
	($junk, $word, $junk, $junk, @rest) = split(/\ |\,\ /,$tr[0]);
	my $result = "Possíveis correções para $in: @rest";
	if (scalar(@rest) == 0) {
	    $result = "Não pude encontrar sugestões ortográficas para '$in'";
	}
	if ($msgType =~ /private/) {
	    &msg($who, $result);
	} else {
	    &say($result);
	}
    }
    return '';
}


1;

