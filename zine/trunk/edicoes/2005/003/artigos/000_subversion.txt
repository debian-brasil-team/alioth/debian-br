Titulo: Subversion e Apache2/SSL no Debian 3.1
Autor: Pablo Lorenzzoni <spectra@debian.org>
Revisor0: Raphael Bittencourt S. Costa <raphaelbscosta@yahoo.com.br>
Revisor1: Marco Carvalho <marcoacarvalho@gmail.com>
Revisor2: Felipe Augusto van de Wiel (faw) <felipe@cathedrallabs.org>
Revisor3: Denis Brandl <denisbr@gmail.com>


Subversion + Apache2/SSL no Debian 3.1
======================================

Copyright (c) 2005 by Pablo Lorenzzoni <spectra@debian.org>
Licenciado sob a GPL

Recentemente resolvi aderir à turma do Subversion, depois da insistência
de meu colega desenvolvedor Otávio Salvador. Estava já há algum tempo
usando Arch (tla), no entanto tinha que permanecer usando CVS a maior
parte do tempo (e Subversion outra parte), o que me impedia de aprender
"de verdade" a usar o Arch e todas as suas funcionalidades.

Como usuário de CVS, suas limitações começaram a tornar-se insuportáveis
para meu uso, e resolvi testar o Subversion e aproveitar o know-how de
controle de versões que o CVS já havia me ensinado (considerei a
proximidade de sintaxe do Subversion e do CVS).


Instalação do Subversion e de um repositório
--------------------------------------------

Esta foi a parte mais fácil:

bash# aptitude install subversion subversion-tools

Ao contrário do pacote do CVS (que instala um diretório /var/cvs como
repositório central), o pacote do Subversion não faz o mesmo. Nunca
achei lógico que o CVS instalasse um repositório na instalação (uma vez
que podemos ter quantos repositórios quisermos). Para instalar um
repositório é simples. Eu utilizo um repositório central em /home/svn,
no entanto o usuário pode substituí-lo pela localização que quiser.

bash# mkdir /home/svn
bash# svnadmin create /home/svn/repos
bash# ls /home/svn/repos
conf/  dav/  db/  format  hooks/  locks/  README.txt

Perceba que criei um repositório no diretório "repos" dentro de
/home/svn. Mais sobre isso adiante, quando falarmos sobre a configuração do
Apache2.

O Subversion não usa RCS como o CVS, mas usa um banco de dados como
sistema de arquivos. Existem duas versões desse sistema de arquivos: o
bdb e o fsfs. O bdb nada mais é do que um banco de dados Berkeley DB
(com todas suas virtudes e problemas). O fsfs é um sistema de arquivos
próprio. Versões mais antigas do Subversion só usam bdb (ou o tem como
padrão). Na versão 1.2 o fsfs passou a ser o padrão. Para os objetivos
desse artigo podemos ignorar as diferenças.

Para listar o conteúdo desse banco de dados (e não os arquivos que ele
grava, como fizemos acima com ls), o usuário deve utilizar o Subversion:

bash# svn ls file:///home/svn/repos
bash#

Obviamente não temos nada no repositório ainda.

Vale a pena criar um grupo "subversion" para ser dono do repositório.
Assim os usuários pertencentes ao grupo podem alterá-lo:

bash# addgroup subversion
Adding group `subversion' (1056)...
Concluído
bash# chgrp -R subversion /home/svn
bash# chmod -R 775 /home/svn


O primeiro projeto
------------------

É prática comum, modernamente, utilizar subdiretórios para identificar
porções de um projeto colaborativo. Comumente se utiliza os diretórios
"branches", "tags" e "trunk" para identificar ramificações de um
projeto, seus releases principais e seu repositório comum,
respectivamente. Em "branches" guardamos as versões que se ramificaram
do "trunk" original (não é coincidência que "branches" seja traduzido
para "ramos" e "trunk" para "tronco"). Em "tags" armazenamos releases
pontuais (por exemplo: RELEASE_1.0, COM_PATCH_DO_FULANO, etc). Então é
uma boa ideia organizar nosso projeto dessa forma:

bash$ ls -R /tmp/meu_projeto
/tmp/meu_projeto:
branches/  tags/  trunk/

/tmp/meu_projeto/branches:

/tmp/meu_projeto/tags:

/tmp/meu_projeto/trunk:
Makefile  programa.c  programa.h

bash$ svn import /tmp/meu_projeto file:///home/svn/repos/meu_projeto -m "inicio"
Adding         /tmp/meu_projeto/branches
Adding         /tmp/meu_projeto/tags
Adding         /tmp/meu_projeto/trunk
Adding         /tmp/meu_projeto/trunk/programa.h
Adding         /tmp/meu_projeto/trunk/programa.c
Adding         /tmp/meu_projeto/trunk/Makefile
-
Committed revision 1.
bash$ 

Podemos fazer o primeiro checkout do projeto para um diretório de
trabalho de maneira muito simples:

bash$ mkdir work; cd work
bash$ svn checkout file:///home/svn/repos/meu_projeto/trunk meu_projeto
A  meu_projeto/programa.c
A  meu_projeto/programa.h
A  meu_projeto/Makefile
-
Checked out revision 1.
bash$

Outros comandos similares ao CVS também funcionam: svn diff, svn commit,
svn update.


Apache2, SSL, WebDAV e Subversion
---------------------------------

O Subversion pode ser acessado através de um servidor próprio chamado
svnserve. Pode fazer autenticação e criptografar a conexão com ssh, tal
como o CVS. Se você criou um grupo "subversion" para ser "dono" do
repositório, usuários com conta na sua máquina que pertençam a esse
grupo podem acessar o repositório através de ssh. Não discutirei esses
métodos de acesso, e uma leitura completa pode ser obtida do livro do
Subversion [1].

Entretanto, a melhor diferença em relação ao CVS, talvez, seja a
introdução do WebDAV para acesso ao repositório. Não é objetivo desse
artigo discutir WebDAV. Para uma leitura introdutória, sugiro a
Wikipedia [2].

Para utilizar o WebDAV temos de ativar o Apache2, o módulo SSL e
configurar o local do repositório no apache. Primeiro, instale o Apache2
e o módulo para comunicação com o Subversion:

bash# aptitude install apache2 libapache2-svn

Crie os certificados e a configuração inicial para o seu site:

bash# apache2-ssl-certificate
bash# cd /etc/apache2/sites-available/
bash# cp default ssl
bash# a2ensite ssl
bash# echo "Listen 443" >> /etc/apache2/ports.conf

Certifique-se de que o módulo SSL esteja sendo chamado na inicialização do
Apache2:

bash# a2enmod ssl 
Module ssl installed; run /etc/init.d/apache2 force-reload to enable.
bash#

Edite o arquivo de configuração /etc/apache2/sites-available/ssl para
algo parecido com:

NameVirtualHost *:443
<VirtualHost *:443>
 SSLEngine On
 SSLCertificateFile /etc/apache2/ssl/apache.pem

 ...
 <Location /svn>
    DAV svn
    SVNPath /home/svn/repos
            
    AuthType Basic
    AuthName "Meu Repositório Subversion"
    AuthUserFile /home/svn/dav_svn.passwd
    <LimitExcept GET PROPFIND OPTIONS REPORT>
       Require valid-user
    </LimitExcept>
 </Location>

</VirtualHost>

Perceba, na tag "Location", que podemos utilizar o nome que quisermos.
Eu utilizo /svn, mas poderia utilizar /subversion, /repositorio,
/projeto, /svn/repos, etc. Esse é o sufixo do endereço pelo qual os
usuários poderão acessar seu repositório. No exemplo:

https://meusite.homelinux.net/svn

Além disso, o arquivo dav_svn.passwd com os usuários ficará em /home/svn
e não em /home/svn/repos. Esse arquivo fica escondido do apache, uma vez
que SVNPath dá a localização do repositório. Eis o motivo para termos
criado o repositório em /home/svn/repos e não em /home/svn.

Crie os usuários que terão permissão para mexer no repositório de fora
(perceba que a opção -c só é necessária para a criação do arquivo):

bash# htpasswd2 -c -m /home/svn/dav_svn.passwd spectra
bash# htpasswd2 -m /home/svn/dav_svn.passwd pablo
bash# htpasswd2 -m /home/svn/dav_svn.passwd otavio

Não esqueça de fazer o dono do Apache2 o dono do seu repositório:

bash# chown -R www-data /home/svn

Finalmente, reinicie o apache:

bash# /etc/init.d/apache2 restart

Seu repositório deverá estar acessível em:

https://localhost/svn

Você verá algo como:

Revision 1: /

    * meu_projeto/

Powered by Subversion version 1.1.4 (r13838).

Agora, quaisquer usuários podem acessar o repositório com um simples:

bash$ svn checkout https://meusite.homelinux.net/svn/meu_projeto/trunk

Usuários criados com htpasswd2 terão permissão de escrita no
repositório, e poderão ajudar no projeto diretamente.


Revisões por repositório e futuros artigos
------------------------------------------

As revisões do Subversion são orientadas ao repositório. Portanto, se
importarmos outro projeto dentro do mesmo repositório a revisão do
repositório sobe para 2, mesmo sem mexermos em meu_projeto. Isso pode
parecer meio estranho no início, mas é realmente uma questão de costume.
Se preferir pode criar um repositório para cada projeto, mas isso já
está fora do escopo desse artigo.

Ao mudar de Arch para Subversion, obtive a grande vantagem da sintaxe
muito parecida com a do CVS (ao mesmo tempo que evitava a sintaxe
alienígena do Arch). No entanto perdi a maior vantagem do Arch: a
descentralização. No próximo número do DebianZine vou falar sobre uma
ferramenta que supre essa necessidade: svk [3]. Até lá.


Notas e Referências
-------------------

[1] http://svnbook.red-bean.com/
[2] http://en.wikipedia.org/wiki/WebDAV
[3] http://svk.elixus.org/
