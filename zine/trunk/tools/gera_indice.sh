#!/bin/bash
# Autor: Felipe Augusto van de Wiel (faw) <felipe@cathedrallabs.org>
# Licenciado sob GNU/GPL v2
# Cria o relatório de publicacao

TIMESTAMP=$(date +%Y%m%d%H%M%S);

# Quick and dirty
# FIXME: use a better approach :)
if [ -z $1 ]; then
	echo "Use $0 <dir-com-artigos>";
	exit;
fi

checa_revisores() {
	if [ -z "${revisor0}" ]; then
		revisoes="0"
	else
		revisoes="1"
	fi
	if [ -z "${revisor1}" ]; then
		revisoes=${revisoes}"0"
	else
		revisoes=${revisoes}"1"
	fi
	if [ -z "${revisor2}" ]; then
		revisoes=${revisoes}"0"
	else
		revisoes=${revisoes}"1"
	fi
}

cont=0
for item in $(ls ${1}); do
	autor=$(grep '^Autor:' ${1}/${item} |cut -d':' -f2);
	titulo=$(grep '^Titulo:' ${1}/${item} |cut -d':' -f2);
	revisor0=$(grep '^Revisor0:' ${1}/${item} |cut -d':' -f2);
	revisor1=$(grep '^Revisor1:' ${1}/${item} |cut -d':' -f2);
	revisor2=$(grep '^Revisor2:' ${1}/${item} |cut -d':' -f2);
	id=$(printf "%03d" ${cont});
	diretorio=$(echo "${1}" |cut -c3-)
	arquivo=${diretorio}${item}

	checa_revisores

cat >> artigoszine_${TIMESTAMP}.txt<<EOF
${id}:Titulo:${titulo}
${id}:Link: http://www.debianbrasil.org/zine/${arquivo}
${id}:Autor:${autor}
${id}:Revisor0:${revisor0}
${id}:Revisor1:${revisor1}
${id}:Revisor2:${revisor2}
${id}:Revisoes: ${revisoes}

EOF

cont=$((cont+1))
done
