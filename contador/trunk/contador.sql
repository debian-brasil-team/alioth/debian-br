create table users 
(
	uid int auto_increment unique primary key, 
	name varchar(64), 
	nick varchar(64), 
	email varchar(128) not null unique, 
	password varchar(128) not null,
	url varchar(255), 
	birthdate date, 
	profession tinyint, 
	school tinyint, 
	gender tinyint, 
	photo text, 
	state tinyint, 
	city varchar(120), 
	beganin date, 
	use_at tinyint, 
	flavour varchar(20), 
	public tinyint, 
	regtime int
);

create table preusers 
(
	confirmhash varchar(64) not null unique,
	name varchar(64), 
	nick varchar(64), 
	email varchar(128) not null, 
	password varchar(128) not null,
	url varchar(255), 
	birthdate date, 
	profession tinyint, 
	school tinyint, 
	gender tinyint, 
	photo text, 
	state tinyint, 
	city varchar(120), 
	beganin date, 
	use_at tinyint, 
	flavour varchar(20), 
	public tinyint, 
	regtime int
);
