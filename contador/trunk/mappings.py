#!/usr/bin/python
# -*- coding: utf-8 -*-

state = {}
state[0] = None
state[1] = 'Acre'
state[2] = 'Amapá'
state[3] = 'Amazonas'
state[4] = 'Alagoas'
state[5] = 'Bahia'
state[6] = 'Ceará'
state[7] = 'Distrito Federal'
state[8] = 'Espírito Santo'
state[9] = 'Goiás'
state[10] = 'Maranhão'
state[11] = 'Mato Grosso'
state[12] = 'Mato Grosso do Sul'
state[13] = 'Minas Gerais'
state[14] = 'Pará'
state[15] = 'Paraíba'
state[16] = 'Paraná'
state[17] = 'Pernambuco'
state[18] = 'Piauí'
state[19] = 'Rio de Janeiro'
state[20] = 'Rio Grande do Norte'
state[21] = 'Rio Grande do Sul'
state[22] = 'Rondônia'
state[23] = 'Roraima'
state[24] = 'Santa Catarina'
state[25] = 'São Paulo'
state[26] = 'Sergipe'
state[27] = 'Tocantins'

school = {}
school[0] = None
school[1] = 'Analfabeto'
school[2] = 'Primeiro Grau'
school[3] = 'Segundo Grau'
school[4] = 'Curso Superior'
school[5] = 'Pós-Graduação'
school[6] = 'Mestrado'
school[7] = 'Doutorado'
school[8] = 'Pós-Douturado'

profession = {}
profession[0] = None
profession[1] = 'Administração de Sistemas'
profession[2] = 'Desenvolvimento (Análise/Programação)'
profession[3] = 'Documentação de Sistemas'
profession[4] = 'Suporte a usuários'
profession[5] = 'Tradução de Sistemas'
profession[6] = 'Professor'
profession[7] = 'Acadêmico de Computação'
profession[8] = 'Área de Exatas'
profession[9] = 'Área de Humanas'
profession[10] = 'Área de Sociais'
profession[11] = 'Área de Biológicas'
profession[12] = 'Área de Agrárias'
