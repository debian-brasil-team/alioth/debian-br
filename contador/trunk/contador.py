#!/usr/bin/python
# -*- coding: utf-8 -*-

# contador.py -- simple web system for users to register and to generate
#                simple statistics about them
#
# Copyright (C) 2005 Gustavo Noronha Silva
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

from time import localtime, time, strftime
from sys import stderr
from base64 import encodestring, decodestring
from random import randrange
from md5 import md5
from locale import setlocale, LC_ALL

import MySQLdb
import smtplib

from MySQLdb import cursors
from cherrypy import cpg
from Cheetah.Template import Template

# dicts for state, school and profession
from mappings import *

class Counter (object):
    def __init__ (self):
        # internal configuration
        self.skeleton = 'skeleton.tmpl'

        setlocale (LC_ALL, 'pt_BR.UTF-8')

        import config

        self.dbhost = config.dbhost
        self.dbname = config.dbname
        self.dbuser = config.dbuser
        self.dbpass = config.dbpass

        del config

    def index (self):
        session = cpg.request.sessionMap

        logged = session.get ('logged', None)
        if logged:
            template = Template (file='index.tmpl')
            template.stage = 'LOGGED'
            template.uid = logged
        else:
            template = Template (file='index.tmpl')
            template.stage = 'NOTLOGGED'
        return self.generate_final_output (template)
    index.exposed = True

    def login (self, login = None, password = None):
        session = cpg.request.sessionMap

        if login and password:
            conn, cursor = self.get_dbconn ()
            sql_query = 'select uid,password from users where email="%s"' % \
                        (login)
            cursor.execute (sql_query)
            user = cursor.fetchone ()
            if user:
                if user[1] == password:
                    session['logged'] = user[0]
                    return self._redirect_to_index ()
                else:
                    return '''
                    <font color="red">Senha errada!</font>
                    '''
            else:
                return '''
                <html>
                <head>
                <meta name="content-type" content="text/html; charset=utf-8" />
                </head>
                <body>
                <font color="red">Usuário não existe!</font>
                </body>
                </html>
                '''
        else:
            return '''
            <font color="red">Uh?</font>
            '''
    login.exposed = True

    def logout (self):
        session = cpg.request.sessionMap

        session['logged'] = None
        return self._redirect_to_index ()
    logout.exposed = True

    def password_reminder (self, email = None):
        template = Template (file='password-reminder.tmpl')
        if not email:
            template.stage = 'PASSWORD_REMINDER'
        else:
            template.email = email

            conn, cursor = self.get_dbconn ()
            sql_query = 'select name, password from users '\
                        'where email="%s"' % (email)
            cursor.execute (sql_query)
            result = cursor.fetchone ()
            if result:
                # FIXME: templatization required, maybe a function
                # for sending email based on a template would be good
                try:
                    f = open ('password_reminder-email.tmpl')
                    email_text = f.read ()
                    f.close ()
                except IOError:
                    return '''
                    <font color="red">
                    Não consegui abrir o email de lembrança de senhas!
                    </font>
                    '''
                session = smtplib.SMTP ('localhost')
                try:
                    args = {}
                    args['name'] = result[0]
                    args['email'] = email
                    args['password'] = result[1]
                    session.sendmail ('noreply@debianbrasil.org',
                                      email, email_text % (args))
                except smtplib.SMTPRecipientsRefused, e:
                    return '''
                    <font color="red">
                    SMTP ERROR
                    </font>
                    '''
                template.stage = 'PASSWORD_REMINDER_SENT'
            else:
                template.stage = 'PASSWORD_REMINDER_EMAIL_NOTFOUND'
        return self.generate_final_output (template)
    password_reminder.exposed = True

    def _redirect_to_index (self):
        return '''
        <html>
        <head>
        <meta http-equiv=Refresh content="0;URL=/index">
        </head>
        </html>
        '''

    def register (self):
        template = Template (file='register-form.tmpl')
        template.stage = 'REGISTRATION'
        template.profession = profession
        template.school = school
        template.state = state
        template.curyear = localtime ()[0]
        return self.generate_final_output (template)
    register.exposed = True

    def save (self, **args):
        editing = args.has_key ('uid')
        
        if (not args['name'] or not args['email']) and not editing:
            template = Template (file='errors-register.tmpl')
            template.error = 'MANDATORY_MISSING'
            return self.generate_final_output (template)

        if not args['npassword'] and not editing:
            template = Template (file='errors-register.tmpl')
            template.error = 'NULL_PASSWORD'
            return self.generate_final_output (template)

        if args['npassword'] != args['password_confirm']:
            template = Template (file='errors-register.tmpl')
            template.error = 'PASSWORDS_DO_NOT_MATCH'
            return self.generate_final_output (template)

        if not editing:
            args['confirmhash'] = md5 (str(randrange(1000))).hexdigest ()

        args['email'] = smtplib.rfc822.parseaddr (args['email'])[1]

        # make sure the url begins in http://, as the links may
        # be broken otherwise
        if args['url'][:7] != 'http://' and args['url']:
            args['url'] = 'http://' + args['url']

        args['birthdate'] = '%(birthyear)s-%(birthmonth)s-%(birthday)s' % \
                            (args)
        args['beganin'] = '%(beganinyear)s-%(beganinmonth)s-01' % (args)

        if args['photo']:
            from PIL import Image
            from StringIO import StringIO

            io = StringIO (args['photo'])
            image = Image.open (io)

            width, height = image.size
            target_width = 100
            target_height = 100
            if width > 100 or height > 100:
                if width > height:
                    proportion = 10000 / float(width)
                    target_height = (height * proportion) / 100
                else:
                    proportion = 10000 / float(height)
                    target_width = (width * proportion) / 100
                target_size = (int(target_width), int(target_height))
                image = image.resize (target_size)
            else:
                image = image.copy ()
            io.seek (0)
            io.truncate ()
            image.save (io, 'png')
            args['photo'] = io.getvalue ()
            io.close ()

            args['uuphoto'] = encodestring (args['photo'])
        else:
            args['uuphoto'] = ''

        if args.has_key ('public'):
            args['public'] = '1'
        else:
            args['public'] = '0'

        tmp = 0
        for use_at in ['use_at_work', 'use_at_school', 'use_at_home']:
            if args.has_key (use_at):
                tmp += int(args[use_at])
        args['use_at'] = tmp

        args['regtime'] = time ()

        conn, cursor = self.get_dbconn ()

        if editing:
            # photo and password are fields which there's no simple way
            # of filling the entry with something useful or this should
            # simply not get done for them; we are not going to replace
            # them with '' if nothing is added
            # TODO: a way of removing the photo; maybe a simple checkbox
            if args['uuphoto'] != '':
                args['change_photo'] = ',photo="%s"'\
                                       % (args['uuphoto'])
            else:
                args['change_photo'] = ''

            if args['npassword'] != '':
                args['change_password'] = ',password="%s"' %\
                                          (args['npassword'])
            else:
                args['change_password'] = ''
            sql_query = 'update users set name="%(name)s", '\
                        'nick="%(nick)s", email="%(email)s", '\
                        'url="%(url)s", birthdate="%(birthdate)s", '\
                        'profession="%(profession)s", '\
                        'school="%(school)s", gender="%(gender)s", '\
                        'state="%(state)s", city="%(city)s", '\
                        'beganin="%(beganin)s", use_at="%(use_at)d", '\
                        'flavour="%(flavour)s", public="%(public)s"'\
                        '%(change_photo)s %(change_password)s '\
                        'where uid="%(uid)s"' % (args)
        else:
            sql_query = 'insert into preusers values ("%(confirmhash)s", '\
                        '"%(name)s", "%(nick)s", "%(email)s", "%(npassword)s", '\
                        '"%(url)s", "%(birthdate)s", "%(profession)s", '\
                        '"%(school)s", "%(gender)s", "%(uuphoto)s", '\
                        '"%(state)s", "%(city)s", "%(beganin)s", '\
                        '"%(use_at)d", "%(flavour)s", "%(public)s", '\
                        '%(regtime)d)' % (args)

        cursor.execute (sql_query)

        template = None
        if not editing:
            # FIXME: templatization required
            try:
                f = open ('welcome-email.tmpl')
                welcome_email = f.read ()
                f.close ()
            except IOError:
                return '''
                <font color="red">
                Não consegui abrir o email de boas-vindas!
                </font>
                '''

            # FIXME: templatization required
            session = smtplib.SMTP ('localhost')
            try:
                args['baseurl'] = cpg.request.base
                session.sendmail ('noreply@debianbrasil.org',
                                  args['email'],
                                  welcome_email % (args))
            except smtplib.SMTPRecipientsRefused, e:
                return '''
                <font color="red">
                SMTP ERROR
                </font>
                '''
        
            template = Template (file = 'register-form.tmpl')
            template.stage = 'PENDING'
            template.email = args['email']
        else:
            template = Template (file = 'register-form.tmpl')
            template.stage = 'COMPLETED_EDITING'
        return self.generate_final_output (template)
    save.exposed = True

    def confirm (self, confirmhash):
        conn, cursor = self.get_dbconn ()

        sql_query = 'select * from preusers where confirmhash="%s"' % \
                    (confirmhash)
        cursor.execute (sql_query)

        result = cursor.fetchone ()
        if not result:
            return self.generate_final_output ('''
            <font color="red">
            Hash de confirmação inexistente.
            </font>
            ''')

        result = list (result)
        result[0] = '0'

        sql_query = 'insert into users values (%s, "%s", "%s", "%s", '\
                    '"%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", '\
                    '"%s", "%s", "%s", "%s", "%s", "%s")' % tuple(result)
        try:
            cursor.execute (sql_query)
            conn.commit ()
        except MySQLdb.DatabaseError, e:
            template = Template (file = 'errors-database.tmpl')
            template.erro = str(e)
            return self.generate_final_output (template)

        template = Template (file = 'register-form.tmpl')
        template.stage = 'COMPLETED'
        return self.generate_final_output (template)
    confirm.exposed = True

    def edit (self, uid = 0):
        session = cpg.request.sessionMap
        uid = int(uid)

        logged = session.get ('logged', None)
        if logged != uid:
            return self._redirect_to_index ()
        
        if not uid:
            return '''
            <font color="red">uh?</font>
            '''
        
        conn, cursor = self.get_dbconn (cursors.DictCursor)

        sql_query = 'select uid,name,nick,email,url,birthdate,'\
                    'profession,school,gender,state,city,beganin,'\
                    'use_at,flavour,public from users where uid = %d'\
                    % (uid)
        cursor.execute (sql_query)

        user = cursor.fetchallDict ()[0]

        try:
            year, month, day = str(user['birthdate']).split ('-')
            year = int (year); month = int (month); day = int (day)
        except:
            year = month = day = 0
        user['birthday'] = day
        user['birthmonth'] = month
        user['birthyear'] = year

        try:
            year, month, day = str(user['beganin']).split ('-')
            year = int (year); month = int (month)
        except:
            year = month = 0
        user['beganinmonth'] = month
        user['beganinyear'] = year

        use_at_work, use_at_school, use_at_home = self._get_use_at (user)
        user['use_at_work'] = use_at_work
        user['use_at_school'] = use_at_school
        user['use_at_home'] = use_at_home

        template = Template (file='register-form.tmpl')
        template.stage = 'EDITING'
        template.user = user
        template.profession = profession
        template.school = school
        template.state = state
        template.curyear = localtime ()[0]
        return self.generate_final_output (template)
    edit.exposed = True

    def browse (self, last_shown = 0):
        last_shown = int(last_shown)
        conn, cursor = self.get_dbconn (cursors.DictCursor)

        sql_query = 'select uid,name,nick,email,url,birthdate,'\
                    'profession,school,gender,state,city,beganin,'\
                    'use_at,flavour,public from users where '\
                    'uid > %d order by uid limit 5' % (last_shown)
        cursor.execute (sql_query)

        users = cursor.fetchallDict ()
        result_size = len (users)

        for user in users:
            try:
                year, month, day = str(user['birthdate']).split ('-')
                year = int (year); month = int (month); day = int (day)
                now = localtime ()

                offset = 0
                if now[1] < month:
                    offset = 1
                elif month == now[1]:
                    if now[2] < day:
                        offset = 1
                user['idade'] = '%d anos' % (now[0] - year - offset)

                bdate = (year, month, day, 0, 0, 0, 0, 0, 0)
                user['birthdate'] = strftime ('%d de %B de %Y', bdate)
            except ValueError:
                user['birthdate'] = None
                user['idade'] = None

            try:
                year, month, day = str(user['beganin']).split ('-')
                year = int (year); month = int (month); day = int (day)
                bdate = (year, month, day, 0, 0, 0, 0, 0, 0)
                user['beganin'] = strftime ('%B de %Y', bdate)
            except ValueError:
                user['beganin'] = None

            if user['gender']:
                user['gender'] = 'Masculino'
            else:
                user['gender'] = 'Feminino'

            try:
                user['state'] = state[int(user['state'])]
            except TypeError:
                user['state'] = None
            try:
                user['school'] = school[int(user['school'])]
            except TypeError:
                user['school'] = None
            try:
                user['profession'] = profession[int(user['profession'])]
            except TypeError:
                user['prefession'] = None
 
            # FIXME: ugly, ugly, ugly, but works...
            # TODO: turn this into a mapping for the possible strings
            use_at_work, use_at_school, use_at_home = self._get_use_at (user)
            use_at = ''
            if use_at_work:
                use_at = 'Trabalho, '
            if use_at_school:
                use_at = use_at + 'Escola, '
            if use_at_home:
                use_at = use_at + 'Casa'
            if use_at[-2:] == ', ':
                use_at = use_at[:-2]
            user['use_at'] = use_at

        have_more = True
        if  result_size < 5:
            have_more = False

        last_shown += result_size

        sql_query = 'select uid from users'
        cursor.execute (sql_query)

        if last_shown >= len(cursor.fetchall ()):
            have_more = False
            
        template = Template (file = 'browse.tmpl')
        template.users = users
        template.last_shown = last_shown
        template.have_more = have_more
        template.num_shown = result_size

        return self.generate_final_output (template)
    browse.exposed = True

    def stats (self, what = None):
        if not what or \
           (what != 'state' and what != 'school' and what != 'profession' \
            and what != 'gender'):
            return self._redirect_to_index ()

        conn, cursor = self.get_dbconn ()

        sql_query = 'select %s from users' % (what)
        cursor.execute (sql_query)

        users = cursor.fetchall ()
        values = {}
        for user in users:
            if not values.has_key (user[0]):
                values[user[0]] = 1
            else:
                values[user[0]] += 1

        stats = []

        # grab the stats for those information which have mapping
        if what == 'state' or what == 'school' or what == 'profession':
            mapping = eval (what)
            for s in range(0, len(mapping)):
                if values.has_key (s):
                    stats.append ((mapping[s], values[s]))
                else:
                    stats.append ((mapping[s], 0))

        if what == 'gender':
            if values.has_key (0):
                stats.append (('Feminino', values[0]))
            else:
                stats.append (('Feminino', 0))
            if values.has_key (1):
                stats.append (('Masculino', values[1]))
            else:
                stats.append (('Masculino', 0))

        template = Template (file='stats.tmpl')
        template.stats = stats
        return self.generate_final_output (template)
    stats.exposed = True

    def images (self, uid = '0'):
        uid = int (uid)

        conn, cursor = self.get_dbconn ()

        sql_query = 'select public,photo from users where uid=%d' % (uid)

        cursor.execute (sql_query)
        result = cursor.fetchone ()
        if result:
            cpg.response.headerMap['Content-Type'] = 'image/png'
            if result[0]:
                photo = decodestring(result[1])
                if photo == '':
                    f = open ('broken-image.png')
                    photo = f.read ()
                    f.close ()
            else:
                f = open ('not-public.png')
                photo = f.read ()
                f.close ()
            return photo
        else:
            # FIXME: templatize?
            return self.generate_final_output ('''
            <font color="red">
            Usuário de uid %s não existe!
            </font>
            ''')
    images.exposed = True

    def default (self, method, arg):
        # make sure we only expose exposed methods to default ()
        if eval ('self.%s.exposed' % (method)) == True:
            return eval ('self.%s ("%s")' % (method, arg))
        else:
            return 'duh'
    default.exposed = True

    def _get_use_at (self, user):
        use_at = user['use_at']
        use_at_work = use_at_school = use_at_home = False
        if use_at == 7:
            use_at_work = use_at_school = use_at_home = True
        elif use_at == 6:
            use_at_work = use_at_school = True
        elif use_at == 5:
            use_at_work = use_at_home = True
        elif use_at == 4:
            use_at_work = True
        elif use_at == 3:
            use_at_school = use_at_home = True
        elif use_at == 2:
            use_at_school = True
        else:
            use_at_home = True

        return use_at_work, use_at_school, use_at_home

    def generate_final_output (self, template):
        f = open (self.skeleton)
        output = f.read ()
        f.close ()

        return output.replace ('##BODY##', str(template))

    def get_dbconn (self, cursorclass = None):
        try:
            if cursorclass:            
                conn = MySQLdb.connect (self.dbhost, self.dbuser,
                                        self.dbpass, self.dbname,
                                        cursorclass = cursorclass)
            else:
                conn = MySQLdb.connect (self.dbhost, self.dbuser,
                                        self.dbpass, self.dbname)
            cursor = conn.cursor ()
        except MySQLdb.DatabaseError, e:
            template = Template (file = 'errors-database.tmpl')
            template.erro = str(e)
            return self.generate_final_output (template)

        return conn, cursor

conf = {}
conf['sessionStorageType'] = 'ram'
conf['sessionTimeout'] = 60
conf['staticContentList'] = [('css.css', 'css.css')]

cpg.root = Counter ()
cpg.server.start (configMap = conf)
