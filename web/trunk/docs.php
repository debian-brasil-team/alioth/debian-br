<?
include ('config.php');

include ('cabecalho.inc');
include ('menu.inc');

unset($ewiki_plugins["action"]["calendar"]);       # disables calend�rio
unset($ewiki_plugins["action"]["attachments"]);       # disables anexos =P

$ewiki_page = ewiki_page();
?>

<td valign=top width="80%">

<?
$dorest = true;

if ($t == "nacionais")
{
  echo "<p><h2>Documentos Feitos em Casa</h2></p>

<p>Esta � uma se��o com manuais e tutoriais escritos por membros do Projeto Debian-BR. Se voc� fez algum ou conhece algum que julga �til para inclus�o, entre em contato conosco por e-mail: <a href=\"mailto:debian-l10n-portuguese@lists.debian.org\">debian-l10n-portuguese@lists.debian.org</a>.</p>";
}
elseif ($t == "traduzidos")
{
  echo "<p><h2>Documentos Traduzidos</h2></p>

<p>Esta � uma se��o com todos os documentos traduzidos pelos membros do Projeto Debian-BR com base nos originais do Debian em ingl�s. Caso voc� queira colaborar com a tradu��o de qualquer documento do Debian, ou at� mesmo continuar a tradu��o de algum que n�o tenha sido conclu�do ou estaja desatualizado, entre em contato conosco pelo e-mail <a href=\"mailto:debian-l10n-portuguese@lists.debian.org\">debian-l10n-portuguese@lists.debian.org</a>, para evitar duplica��o de esfor�os.</p>";
}
else
{
  echo "<p><h2>Documenta��o</h2></p>

<p>Esta � uma das se��es de maior import�ncia do nosso s�tio. Aqui est�o todos os documentos, traduzidos e em fase de tradu��o pela Comunidade Debian brasileira.</p>

<p>Os <b>Documentos Feitos em Casa</b> s�o aqueles produzidos por contribuidores do Projeto Debian-BR. H� tamb�m os Documentos Traduzidos. Voc� tamb�m pode encontrar documenta��o sobre o Debian no <a href=\"http://www.debian.org/doc/ddp\">Projeto de Documenta��o do Debian</a>.</p>

<p>
<ul type=\"circle\">
<li><a href=\"{$_SERVER['PHPSELF']}?t=nacionais\">Documentos Feitos em Casa</a></li>
<li><a href=\"{$_SERVER['PHPSELF']}?t=traduzidos\">Documentos Traduzidos</a></li>
</ul>
</p>";

  $dorest = false;
}

if ($dorest) {

$basedir = "docs/$t/sgml";
$dir = dir ($basedir);

while (false !== ($entry = $dir->read ()))
{
  $agentname = "$basedir/$entry/doc-agent";

  if (!file_exists ($agentname))
    continue;

  $agent = fopen ($agentname, "r");
  $conteudo = fread ($agent, filesize ($agentname));
  $campos = explode ("\n", $conteudo);

  $info = explode ("<", $campos[0]);

  printf ("<p><a href=\"view.php?doc=%s%s\">%s</a>: ", $entry, ($t == "traduzidos")?"&translated=yes":"", trim ($info[0]));

  reset ($campos);
  for ($i = 0 ; $i < 6 ; $i++)
    next ($campos);

  while ($line = next ($campos))
    {
      printf ("%s ", trim ($line));
    }
}

$dir->close ();

}
?>

</td>

<?
include ('direita.inc');
include ('fim.inc');
?>

</html>