<?

preg_match("/^([^\/]+)\.debianbrasil\.org/i", $_SERVER["HTTP_HOST"], $matches);

if (($matches[1]) && ($matches[1] != "www"))
{
  header ("Location: http://www.debianbrasil.org/{$matches[1]}");
  exit ();
}

include ('config.php');

$ewiki_page = ewiki_page();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="pt_BR" xml:lang="pt_BR">

<?
include ('cabecalho.inc');
include ('menu.inc');
?>

<!-- p�gina principal -->
<table cellspacing="10" cellpadding="0" border="0" width="100%">
<tr>
<td valign="top" bgcolor="#ffffff" width="80%">

<?
if (!isset($GLOBALS["_REQUEST"])) {
  $_REQUEST = array_merge($HTTP_GET_VARS, $HTTP_POST_VARS);
}
($mod = @$_REQUEST["mod"]) or ($mod = @$_REQUEST["module"]);
if ((strlen($mod)) && (file_exists("modules/$mod.inc"))) {
    include "modules/$mod.inc";
}
else {
  echo $ewiki_page;
}
?>

</td>

<?
include ('direita.inc');
include ('fim.inc');
?>

</body>
</html>
