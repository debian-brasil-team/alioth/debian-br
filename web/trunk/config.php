<?php

 /*
     this include script just opens the database connection;
     it is however an __examplary__ "configuration file" for
     ewiki.php (real config constants can be found in there!)
 */


 #-- OPEN DATABASE for ewiki
/*
 function_exists("mysql_connect") and
 mysql_connect("localhost", $dbuser="root", $pw="") and
 mysql_query("USE test")  // database name
 or
*/
 include("plugins/db_flat_files.php");


 #-- predefine some configuration constants
 define("EWIKI_LIST_LIMIT", 25);
 define("EWIKI_HTML_CHARS", 1);
 define("EWIKI_MPI_AUTOLOADALL", 0);


 #-- plugins
# include("plugins/email_protect.php");
 include("plugins/page_powersearch.php");
 include("plugins/page_pageindex.php");
# include("plugins/page_wordindex.php");
# include("plugins/page_aboutplugins.php");
# include("plugins/page_imagegallery.php");
# include("plugins/page_orphanedpages.php");
# include("plugins/fancy_list_dict.php");
 include("plugins/diff_gnu.php");
# include("plugins/like_pages.php");
 include("plugins/notify_kov.php");
# include("plugins/imgresize_gd.php");
# include("plugins/calendar.php");
 include("plugins/downloads.php");
include("plugins/auth_perm_ring.php");
include("plugins/auth_user_array.php");
include("plugins/auth_method_http.php");
# include("plugins/downloads_view.php");
# include("plugins/markup_footnotes.php");
# include("plugins/more_interwiki.php");
# include("plugins/mpi.php");
# include("plugins/aview_linktree.php");


 #-- library
 if (PHP_VERSION < "4.3") {
    include("fragments/strip_wonderful_slashes.php");
 }
 include("ewiki.php");


?>
