<?
include ('config.php');

include ('cabecalho.inc');
include ('menu.inc');

unset($ewiki_plugins["action"]["calendar"]);       # disables calend�rio
unset($ewiki_plugins["action"]["attachments"]);       # disables anexos =P

$ewiki_page = ewiki_page();

if ($translated)
  $basedir = "docs/traduzidos/sgml";
else
  $basedir = "docs/nacionais/sgml";

function ParseNameMail ($arg)
{
  $info = explode (",", $arg);

  for ($i = 0 ; !empty($info[$i]) ; $i++ )
    {
      $autor = explode ("<", $info[$i]);
      echo  $autor[0];

      $autor = explode (">", $autor[1]);

      echo " <a href=\"mailto:" . $autor[0] . "\">&lt;" . $autor[0] . "&gt;</a>";
	      
      if (!empty($info[$i+1]))
	echo "<br />";
    }
}
?>

<td valign=top width="80%">
       
<?

if ($documento) 
{ 
  $doc = $documento;
}

if ( !$doc )
{
  echo "Voc� precisa selecionar um documento na p�gina de documenta��o!";
}
else
{
  $nomearq = "$basedir/$doc/doc-agent";

  $desc = fopen ($nomearq, "r");
	  
  $conteudo = fread ($desc, filesize ($nomearq));

  $campos = explode ("\n", $conteudo);

  /* A primeira linha eh o nome do documento e o endereco de onde
     o original eh lancado... */

  // Pega o nome do doc
  $info = explode ("<", $campos[0]);

  echo "<h1><b>" . $info[0] . "</b></h1>";

  echo "<center><table valign=top>";

  // Pega a URL do doc original
  $info = explode (">", $info[1]);

  echo "<tr><td><b>Original</b></td><td>";
  echo "<a href=\"" . $info[0] . "\">" . $info[0] . "</a></td></tr>";
	  
  // repositorio CVS
  if ($campos[4])
    {
      echo "<tr><td><b>CVS</b></td><td>";
      echo $campos[4] . "</td></tr>";
    }

  echo "</table><hr><table valign=top>";

  /* A segunda linha eh o nome do(s) autor(es) e seu(s) email(s)... */
  $info = explode (",", $campos[1]);

  printf ("<tr valign=top><td><b>Autor%s</b></td><td>", (strstr($campos[1], ","))?"es":"");

  $autor = $campos[1];
  ParseNameMail ($campos[1]);

  echo "</td></tr>";

  /* A terceira linha tem o nome do(s) tradutor(es) e seu(s) 
     email(s)... */

  if ($campos[2])
    {
      printf ("<tr valign=top><td><b>Tradutor%s</b></td><td>", (strstr($campos[2], ","))?"es":"");

      $tradutor=$campos[2];
      ParseNameMail ($campos[2]);

      echo "</td></tr>";
    }

  /* A quarta linha tem o nome do revisor e seu email... */

  if ($campos[3])
    {
      echo "<tr><td><b>Revisor</b></td><td>";

      ParseNameMail ($campos[3]);

      echo "</td></tr>";
    }

  echo "</table><br /><hr />";


  /* os nomes dos arquivos... */

  $online = $campos[5];
	  
  /* a linha 7 guarda o nome do sgml base */

  $sgmlbase = $campos[6];
  $basename = explode (".sgml", $sgmlbase);
  $basename = $basename[0];

  $basename = "$basedir/$doc/$basename";

  $sgml = $basename . ".sgml.tar.gz";
  $html = $basename . ".html.tar.gz";
  $txt = $basename . ".txt";
  $pdf = $basename . ".pdf";
  $tgz = $basename . ".tar.gz";

  /* As proximas linhas sao uma pequena descricao sobre o documento */

  echo "</center><h3><b>Descri��o:</b></h3><p>";
	  
  reset ($campos);
  for ($i = 0 ; $i < 6 ; $i++)
    next ($campos);

  while ($line = next ($campos))
    {
      if (!strcmp (".", $line))
	{
	  echo "<p>";
	}
      else
	{
	  echo $line;
	}
    }

  if ($online)
    {
      echo "<p><a href=\"${basename}.html/$online\">Navegue Online</a>";
    }

  echo "<p><hr><h3><b>Download:</b></h3><center><p>";

  echo "<table>";
	  
  if (file_exists($html))
    {
      echo "<tr><td>.html</td><td><a href=\"$html\">$html</a></td></tr>";
    }
  if (file_exists ($txt))
    {
      echo "<tr><td>.txt</td><td><a href=\"$txt\">$txt</a></td></tr>";
    }
  if (file_exists($pdf))
    {
      echo "<tr><td>.pdf</td><td><a href=\"$pdf\">$pdf</a></td></tr>";
    }
  if (file_exists($sgml))
    {
      echo "<tr><td>.sgml</td><td><a href=\"$sgml\">$sgml</a></td></tr>";
    }
  if (file_exists($tgz))
    {
      echo "<tr><td>.tar.gz (todos)</td><td><a href=\"$tgz\">$tgz</a></td></tr>";
    }
  echo "</table></center>";

}

?>

<?
if ($comentario)
{
  if ($nome)
    {
      $mailfrom="$nome";
    }

  if ($email)
    {
      $mailfrom="($mailfrom) $email";
    }
  
  $assunto = "[doc-sys] Coment�rio sobre o $documento"; 
  $para = "(Debian-BR) debian-l10n-portuguese@lists.debian.org";
  /*  $para = "(Kov) kov@debian.org"; */
  $others = "From: $mailfrom\nCC: ";

  if ($tradutor)
    {
      $others = "$others $tradutor";
    }
  else
    {
      $others = "$others $autor";
    }

  mail ($para, $assunto, $comentario, $others);
  echo "<hr><font color=red>Coment�rio enviado, obrigado!</font><hr>";
}

?>

<hr>
Queremos saber o que voc� acha desse documento, se tem sugest�es ou qualquer
outro tipo de informa��o que possa ser �til para a comunidade sobre ele, digite
abaixo os seus dados e escreva um coment�rio pra gente.
<hr>
<?php
echo "<form action=\"{$_SERVER['PHPSELF']}\" method=\"post\">";
?>

<table>
<tr><td>
<b>Documento:</b>
</td><td>
<input name="documento" type="text" value="<? echo $doc ?>"> 
</td></tr>
<tr><td>
<b>Nome:</b> 
</td><td>
<input name="nome" type="text"> 
</td></tr>
<tr><td>
<b>Email:</b> 
</td><td>
<input name="email" type="text"> 
</td></tr>
<tr><td>
<b>URL:</b> 
</td><td>
<input name="url" type="text"> 
</td></tr>
</table>

<b>Coment�rio:</b>
<p>
<textarea name="comentario" cols="80" rows="10" wrap="hard"></textarea>
<hr>
<input type="submit" value="Enviar">
<hr>
</form>

Mantenedores: 
<br />
Guilherme de S. Pastore
&lt;<a href="mailto:gpastore@colband.com.br">gpastore@colband.com.br</a>&gt;
<br />
Gustavo Noronha 
&lt;<a href="mailto:kov@debian.org">kov@debian.org</a>&gt;
<br />
Gleydson Mazioli 
&lt;<a href="mailto:gleydson@debian.org">gleydson@debian.org</a>&gt;
<br />
Ricardo Sandrin
&lt;<a href="mailto:rsandrin@ccinet.com.br">rsandrin@ccinet.com.br</a>&gt;
<p>

</td>

<?
include ('direita.inc');
include ('fim.inc');
?>

</html>