<?php

#  The StaticPages plugin allows you to put some .html or .php files
#  into dedicated directories, which then will get available with their
#  basename as ewiki pages. The files can be in wiki format (.txt or no
#  extension), they can also be in .html format and they may even contain
#  php code (.php).
#
#  Of course it is not possible to provide anything other than viewing
#  those pages (editing is not possible), but it is of course up to you
#  to add php code to achieve some interactivity.
#  The idea for this plugin was 'borought' from http://geeklog.org/.
#
#  In your static page .php files you cannot do everything you could
#  normally do, there are some restrictions because of the way these
#  static pages are processed. You cannot for example return any
#  headers() and you have no access to the global variable scope (you
#  must explicitely use : or $GLOBALS or the global statement).


#-- specify which dirs to grasp for page files
ewiki_init_spages(array(
   "./spages",
  #"/usr/local/share/wikipages", "C:/Documents/StaticPages/"
));


#-- plugin glue
# will be added automatically by _init_spages()


#-- return page
function ewiki_spage($id, $data, $action) {

   global $ewiki_spages, $ewiki_plugins;

   $r = "";

   #-- filename from $id
   $fn = $ewiki_spages[strtolower($id)];

   #-- php file
   if (strpos($fn, ".php") || strpos($fn, ".htm")) {

      #-- start new ob level
      ob_start();
      ob_implicit_flush(0);

      #-- prepare environment
      global $ewiki_id, $ewiki_title, $ewiki_author, $ewiki_ring,
             $ewiki_t, $ewiki_script, $ewiki_script_binary, $_EWIKI;

      #-- execute script
      include($fn);

      #-- close ob
      $r = ob_get_contents();
      ob_end_clean();
   }

   #-- wiki file
   else {

      $f = fopen($fn, "rb");
      $r = fread($f, 256<<10);
      fclose($f);

      $r = $ewiki_plugins["render"][0]($r);
   }

   #-- strip <html> and <head> parts (if any)
   if (($l = strpos(strtolower($r), "<body")) &&
       ($w = strpos(strtolower($r), "</body"))  )
   {
      $l = strpos($r, ">", $l+1) + 1;
      $w = $w - $l;
      $r = substr($r, $l, $w);
   }

   #-- return body (means successful handled)
   return($r);

}



#-- return page
#<old># $ewiki_plugins["handler"][] = "ewiki_handler_spages";
function ewiki_handler_spages($id, $data, $action) {

   global $ewiki_spages;

   #-- compare requested page $id with spages` $id values
   $i0 = strtolower($id);
   foreach ($ewiki_spages as $i1 => $fn) {
      if (strtolower($i1)==$i0) {

         return(ewiki_spage($id));

      }
   }

}


#-- init
function ewiki_init_spages($dirs, $idprep="") {

   global $ewiki_spages, $ewiki_plugins;

   foreach ($dirs as $dir) {

      #-- read in one directory
      $dh = opendir($dir);
      while ($fn = readdir($dh)) {

         #-- skip over . and ..
         if ($fn[0] == ".") { continue; }

         #-- be recursive
         if ($fn && is_dir("$dir/$fn")) {
            if ($fn != trim($fn, ".")) {
               $fnadd = trim($fn, ".") . ".";
            }
            else {
               $fnadd = "$fn/";
            }

            ewiki_init_spages(array("$dir/$fn"), "$idprep$fnadd");

            continue;
         }

         #-- strip filename extensions
         $id = str_replace(
                  array(".html", ".htm", ".php", ".txt"),
                  "",
                  basename($fn)
         );

         #-- register spage file and as page plugin (one for every spage)
         $ewiki_spages[strtolower("$idprep$id")] = "$dir/$fn";
         $ewiki_plugins["page"]["$idprep$id"] = "ewiki_spage";

      }
      closedir($dh);
   }

}



?>