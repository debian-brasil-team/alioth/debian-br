<?php

/*
* This plugin extracts todo items(@@TODO, @@DONE, @@CANCELLED ) from the 
* page it is run on.  A change to ewiki_control_links submitted with 
* this plugin adds a link back to the 'normal' view.
* 
* This plugin is designed to work with the EWikiCSS plugins but they are
* not necessary.
* Load this plugin _after_ the core ewiki.php script.
* 
* See http://erfurtwiki.sourceforge.net/?id=TodoExtractorPlugin for more 
* details.
* 
* AndyFundinger(Andy@burgiss.com)
*/

 $ewiki_t["en"]["TODOSFROM"] = "Todos extracted from ";
 $ewiki_t["en"]["EXTTODO"] = "Extract todo List";
 $ewiki_t["en"]["VIEWCOMPL"] = "View complete page";
 $ewiki_t["de"]["TODOSFROM"] = "ToDo's aus ";
 $ewiki_t["de"]["EXTTODO"] = "Zeige ToDo-Liste";
 $ewiki_t["de"]["VIEWCOMPL"] = "Zeige komplette Seite";

 $ewiki_plugins["action"]["extodo"] = "ewiki_extract_todo";
 $ewiki_plugins["action_links"]["extodo"] = $ewiki_plugins["action_links"]["view"];
 $ewiki_plugins["action_links"]["extodo"]["view"] = ewiki_t("VIEWCOMPL");
 $ewiki_plugins["action_links"]["view"]["extodo"] = ewiki_t("EXTTODO");
#$ewiki_plugins["action_links"]["extodo"];


 function ewiki_extract_todo($id, $data, $action){
   global $ewiki_links, $ewiki_plugins, $ewiki_ring, $ewiki_title;

 
    $todotypes = array("TODO","DONE","CANCELLED","SUBJECT");
 
    (EWIKI_PRINT_TITLE) &&
    ($o = "<h2 id='title'>".ewiki_t("TODOSFROM")
        . '<a href="' . ewiki_sqcript("view", $id)
        . '">' . �ewiki_title . "</a></h2>\n");
	
				//ignore any number of list markup tags in front of an @@{todotype},
				//extract only the @@, the types, and their message
				
					//or
				
				//extract any header line			
				
					//1 2         3  4-Class                      5     6   
	preg_match_all("/^(([;:#\* ]*)(@@(".implode("|",$todotypes).")(.*))|(!+.*))$/im",$data["content"],$matches);
	for($index=0;$index<sizeof($matches[0]);$index++){
		//a line will be either header or todo, concatenate the two sub expressions 
		$extractedContent.=$matches[3][$index].$matches[6][$index]."\n\n";
	}
	
	//Render extracted lines as a wiki page, this code extracted from ewiki_page
		
      #-- render requested wiki page  <-- goal !!!
      $o .= "<div class='ewiki_page_todolist'>".$ewiki_plugins["render"][0] ( $extractedContent, 1,
            EWIKI_ALLOW_HTML || (@$data["flags"]&EWIKI_DB_F_HTML) )."</div>";

      #-- control line + other per-page info stuff
      if ($pf_a = $ewiki_plugins["view_append"]) {
         ksort($pf_a);
         foreach ($pf_a as $n => $pf) { 
			
		 	$o .= $pf($id, $data, $action); 
		}
      }
      if ($pf_a = $ewiki_plugins["view_final"]) {
         ksort($pf_a);
         foreach ($pf_a as $n => $pf) { 
		 	if(!preg_match('/_print_title/',$pf)){
			 	$pf($o, $id, $data, $action); 
			}
		}
      }

    return($o);
 }


?>