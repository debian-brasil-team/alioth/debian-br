<?php

#  makes a pages` calendar title more readable,
#  replaces the standard _print_title with a _calendar_title
#
#  Andy Fundinger
#


#-- glue
$ewiki_plugins["view_final"][] = "ewiki_print_calendar_title";


#-- title string replacing
function ewiki_print_calendar_title(&$html, $id, $data, $action, $split=EWIKI_SPLIT_TITLE) {
   global $ewiki_title;

   if (ewiki_isCalendarId($id)) {
     $html=str_replace(">$ewiki_title<", ">".ewiki_calendar_page_title($id,$split)."<", $html);
   }
}



function ewiki_isCalendarId($id){	
	return(preg_match('#(.*)_(\d){8}#',$id));
}


 
function ewiki_calendar_page_title ($id='', $split=EWIKI_SPLIT_TITLE) {
   strlen($id) or ($id = $GLOBALS["ewiki_page"]);

   if(preg_match('#(.*)_(\d{4})(\d{2})(\d{2})#',$id,$dateParts) ){
   		$id=ewiki_t("CALENDERFOR")." ".ewiki_page_title($dateParts[1],$split).", ".ewiki_t("MONTH".$dateParts[3])." ".$dateParts[4].", ".$dateParts[2];
   }else{
   		$id=ewiki_page_title($id,split);
   }
   return($id);
} 


?>