<?php

#
#  The otherwise invisible markup [notify:you@there.net] will trigger a
#  mail, whenever a page is changed. The TLD decides in which language
#  the message will be delivered. One can also append the lang code after
#  a comma or semicolon behind the mail address to set it explicitely:
#  [notify:me@here.org,de] or [notify:you@there.net;eo]
#
#  Nevertheless English will be used as the default automagically, if
#  nothing else was specified, no need to worry about this.
#
#  To include a diff, just set the following variable. Also use it to
#  define the minimum number of changed bytes that are necessary to
#  result in a notification mail. Only use it with Linux/UNIX.

define("EWIKI_NOTIFY_WITH_DIFF", 20);


#-- glue
$ewiki_plugins["edit_hook"][] = "ewiki_notify_edit_hook";
$ewiki_plugins["format_source"][] = "ewiki_format_remove_notify";


#-- email message text ---------------------------------------------------
$ewiki_t["en"]["NOTIFY_BODY"] = <<< _END_OF_STRING
Oi,

Este e-mail foi automaticamente gerado pelo Wiki em www.debianbrasil.org.
A pagina '\$id' foi modificada por \$author, e pode ser encontrada aqui:

\$link

--
ErfurtWiki
http://\$server/

Para nao receber mais esta notificacao, desinscreva-se da lista
debian-br-wiki@lists.alioth.debian.org. Consulte
http://lists.alioth.debian.org para maiores informa��es.
_END_OF_STRING
;
$ewiki_t["en"]["NOTIFY_SUBJECT"] = '[MOD] $id foi modificada';


#-- translation.de
$ewiki_t["de"]["NOTIFY_BODY"] = <<<_END_OF_STRING
Hi,

Eine WikiSeite hat sich ge�ndert, und du wolltest ja unbedingt wissen,
wenn das passiert. Die ge�nderte Seite war '\$id' und
ist leicht zu finden unter folgender URL:
\$link

Wenn du diese Benachrichtigungen nicht mehr bekommen willst, solltest du
deine [notify:...]-Adresse aus der entsprechenden Edit-Box herausl�schen:
\$edit_link

(\$wiki_title auf http://\$server/)
\$server_admin
_END_OF_STRING
;
$ewiki_t["de"]["NOTIFY_SUBJECT"] = '$id hat sich ge�ndert [notify:...]';


#----------------------------------------------------------------------------



#-- implementatition
function ewiki_notify_edit_hook($id, $data, &$hidden_postdata) {

   global $ewiki_t, $ewiki_plugins;

   if (!isset($_REQUEST["save"])) {
      return(false);
   }

   $mailto = ewiki_notify_links($data["content"], 0);

   if (!count($mailto)) {
      return(false); 
   }

   #-- generate diff
   $diff = "";
   if (EWIKI_NOTIFY_WITH_DIFF && (DIRECTORY_SEPARATOR=="/")) {

      #-- save page versions temporarily as files
      $fn1 = "/tmp/ewiki.tmp.notify.diff.".md5($data["content"]);
      $fn2 = "/tmp/ewiki.tmp.notify.diff.".md5($_REQUEST["content"]);
      $f = fopen($fn1, "w");
      fwrite($f, $data["content"]);
      fclose($f);
      $f = fopen($fn2, "w");
      fwrite($f, $_REQUEST["content"]);
      fclose($f);

      #-- get diff output, rm temp files
      $f = popen("diff -u  --ignore-case --ignore-space-change  $fn1 $fn2 ", "r");
      $diff .= fread($f, 128<<10);
      #fclose($f);
      unlink($fn1);
      unlink($fn2);

      #-- do not [notify:] if changes were minimal
      if (strlen($diff) < EWIKI_NOTIFY_WITH_DIFF) {
         return(false);
      }

      $diff = "\n\n-----------------------------------------------------------------------------\n\n"
            . $diff;
   }

   #-- separate addresses into (TLD) groups
   $mailto_lang = array(
   );
   foreach ($mailto as $m) {

      $lang = "";

      #-- remove lang selection trailer
      $m = strtok($m, ",");
      if ($uu = strtok(",")) {
         $lang = $uu;
      }
      $m = strtok($m, ";");
      if ($uu = strtok(";")) {
         $lang = $uu;
      }

      #-- else use TLD as language code
      if (empty($lang)) {
         $r = strrpos($m, ".");
         $lang = substr($m, $r+1);
      }

      $mailto_lang[trim($lang)][] = trim($m);

   }

   #-- go thru email address groups
   foreach ($mailto_lang as $lang=>$mailto) {

      $pref_langs = array_merge(array(
         "$lang", "en"
      ), $ewiki_t["languages"]);

      ($server = $_SERVER["HTTP_HOST"]) or
      ($server = $_SERVER["SERVER_NAME"]);
      $s_4 = "http://" . $server . $_SERVER["REQUEST_URI"];
      $link = str_replace("edit/$id", "$id", $s_4);

      $m_text = ewiki_t("NOTIFY_BODY", array(
         "id" => $id,
         "link" => $link,
         "edit_link" => $s_4,
         "server_admin" => $_SERVER["SERVER_ADMIN"],
         "server" => $server,
         "wiki_title" => EWIKI_PAGE_INDEX,
	 "author" => $_SERVER["REMOTE_ADDR"],
      ), $pref_langs);
      $m_text .= $diff;

      $m_from = "noreply@debianbrasil.org";
      $m_subject = ewiki_t("NOTIFY_SUBJECT", array(
         "id" => $id,
      ), $pref_langs);

      $m_to = implode(", ", $mailto);

      mail($m_to, $m_subject, $m_text, "From: \"$s_2\" <$m_from>\nX-Mailer: ErfurtWiki/".EWIKI_VERSION);

   }

}



function ewiki_notify_links(&$source, $strip=1) {
   $links = array();
   $l = 0;
  
   $links[] = "debian-br-wiki@lists.alioth.debian.org";

   if (strlen($source) > 10)
   while (($l = @strpos($source, "[notify:", $l)) !== false) {
      $r = strpos($source, "]", $l);
      $str = substr($source, $l, $r + 1 - $l);
      if (!strpos("\n", $str)) {
         $links[] = trim(substr($str, 8, -1));
         if ($strip) {
            $source = substr($source, 0, $l) . substr($source, $r + 1);
         }
      }
      $l++;
   }
   return($links);
}



function ewiki_format_remove_notify(&$source) {
   ewiki_notify_links($source, 1);
}



?>
